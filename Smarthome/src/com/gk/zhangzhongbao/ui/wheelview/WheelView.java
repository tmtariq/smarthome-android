package com.gk.zhangzhongbao.ui.wheelview;

import android.widget.*;
import android.content.*;
import android.os.*;
import android.util.*;
import android.text.*;
import android.graphics.*;
import android.graphics.drawable.*;
import java.util.*;
import android.view.*;
import android.view.animation.Interpolator;

public class WheelView extends View
{
    private static final int ADDITIONAL_ITEMS_SPACE = 10;
    private static final int ADDITIONAL_ITEM_HEIGHT = 20;
    private static final int DEF_VISIBLE_ITEMS = 7;
    private static final int ITEMS_TEXT_COLOR = -7303024;
    private static final int LABEL_OFFSET = 18;
    private static final int MIN_DELTA_FOR_SCROLLING = 1;
    private static final int PADDING = 30;
    private static final int SCROLLING_DURATION = 400;
    private static final int[] SHADOWS_COLORS;
    private static final int VALUE_TEXT_COLOR = -268435456;
    private final int ITEM_OFFSET;
    private final int MESSAGE_JUSTIFY;
    private final int MESSAGE_SCROLL;
    public int TEXT_SIZE;
    private WheelAdapter adapter;
    private Handler animationHandler;
    private GradientDrawable bottomShadow;
    private Drawable centerDrawable;
    private List<OnWheelChangedListener> changingListeners;
    private int currentItem;
    private GestureDetector gestureDetector;
    private GestureDetector.SimpleOnGestureListener gestureListener;
    boolean isCyclic;
    private boolean isScrollingPerformed;
    private int itemHeight;
    private StaticLayout itemsLayout;
    private TextPaint itemsPaint;
    private int itemsWidth;
    private String label;
    private StaticLayout labelLayout;
    private int labelWidth;
    private int lastScrollY;
    private Scroller scroller;
    private List<OnWheelScrollListener> scrollingListeners;
    private int scrollingOffset;
    private GradientDrawable topShadow;
    private StaticLayout valueLayout;
    private TextPaint valuePaint;
    private int visibleItems;
    
    static {
        SHADOWS_COLORS = new int[] { -5723992, 11184810, 11184810 };
    }
    
    public WheelView(final Context context) {
        super(context);
        this.ITEM_OFFSET = this.TEXT_SIZE / 10;
        this.adapter = null;
        this.currentItem = 0;
        this.itemsWidth = 0;
        this.labelWidth = 0;
        this.visibleItems = 7;
        this.itemHeight = 0;
        this.isCyclic = false;
        this.changingListeners = new LinkedList<OnWheelChangedListener>();
        this.scrollingListeners = new LinkedList<OnWheelScrollListener>();
        this.gestureListener = new GestureDetector.SimpleOnGestureListener() {
            public boolean onDown(final MotionEvent motionEvent) {
                if (WheelView.this.isScrollingPerformed) {
                    WheelView.this.scroller.forceFinished(true);
                    WheelView.this.clearMessages();
                    return true;
                }
                return false;
            }
            
            public boolean onFling(final MotionEvent motionEvent, final MotionEvent motionEvent2, final float n, final float n2) {
                WheelView.access$8(WheelView.this, WheelView.this.currentItem * WheelView.this.getItemHeight() + WheelView.this.scrollingOffset);
                int n3;
                if (WheelView.this.isCyclic) {
                    n3 = Integer.MAX_VALUE;
                }
                else {
                    n3 = WheelView.this.adapter.getItemsCount() * WheelView.this.getItemHeight();
                }
                int n4;
                if (WheelView.this.isCyclic) {
                    n4 = -n3;
                }
                else {
                    n4 = 0;
                }
                WheelView.this.scroller.fling(0, WheelView.this.lastScrollY, 0, (int)(-n2) / 2, 0, 0, n4, n3);
                WheelView.this.setNextMessage(0);
                return true;
            }
            
            public boolean onScroll(final MotionEvent motionEvent, final MotionEvent motionEvent2, final float n, final float n2) {
                WheelView.this.startScrolling();
                WheelView.this.doScroll((int)(-n2));
                return true;
            }
        };
        this.MESSAGE_SCROLL = 0;
        this.MESSAGE_JUSTIFY = 1;
        this.animationHandler = new Handler() {
            public void handleMessage(final Message message) {
                WheelView.this.scroller.computeScrollOffset();
                final int currY = WheelView.this.scroller.getCurrY();
                final int n = WheelView.this.lastScrollY - currY;
                WheelView.access$8(WheelView.this, currY);
                if (n != 0) {
                    WheelView.this.doScroll(n);
                }
                if (Math.abs(currY - WheelView.this.scroller.getFinalY()) < 1) {
                    WheelView.this.scroller.getFinalY();
                    WheelView.this.scroller.forceFinished(true);
                }
                if (!WheelView.this.scroller.isFinished()) {
                    WheelView.this.animationHandler.sendEmptyMessage(message.what);
                    return;
                }
                if (message.what == 0) {
                    WheelView.this.justify();
                    return;
                }
                WheelView.this.finishScrolling();
            }
        };
        this.initData(context);
    }
    
    public WheelView(final Context context, final AttributeSet set) {
        super(context, set);
        this.ITEM_OFFSET = this.TEXT_SIZE / 10;
        this.adapter = null;
        this.currentItem = 0;
        this.itemsWidth = 0;
        this.labelWidth = 0;
        this.visibleItems = 7;
        this.itemHeight = 0;
        this.isCyclic = false;
        this.changingListeners = new LinkedList<OnWheelChangedListener>();
        this.scrollingListeners = new LinkedList<OnWheelScrollListener>();
        this.gestureListener = new GestureDetector.SimpleOnGestureListener() {
            public boolean onDown(final MotionEvent motionEvent) {
                if (WheelView.this.isScrollingPerformed) {
                    WheelView.this.scroller.forceFinished(true);
                    WheelView.this.clearMessages();
                    return true;
                }
                return false;
            }
            
            public boolean onFling(final MotionEvent motionEvent, final MotionEvent motionEvent2, final float n, final float n2) {
                WheelView.access$8(WheelView.this, WheelView.this.currentItem * WheelView.this.getItemHeight() + WheelView.this.scrollingOffset);
                int n3;
                if (WheelView.this.isCyclic) {
                    n3 = Integer.MAX_VALUE;
                }
                else {
                    n3 = WheelView.this.adapter.getItemsCount() * WheelView.this.getItemHeight();
                }
                int n4;
                if (WheelView.this.isCyclic) {
                    n4 = -n3;
                }
                else {
                    n4 = 0;
                }
                WheelView.this.scroller.fling(0, WheelView.this.lastScrollY, 0, (int)(-n2) / 2, 0, 0, n4, n3);
                WheelView.this.setNextMessage(0);
                return true;
            }
            
            public boolean onScroll(final MotionEvent motionEvent, final MotionEvent motionEvent2, final float n, final float n2) {
                WheelView.this.startScrolling();
                WheelView.this.doScroll((int)(-n2));
                return true;
            }
        };
        this.MESSAGE_SCROLL = 0;
        this.MESSAGE_JUSTIFY = 1;
        this.animationHandler = new Handler() {
            public void handleMessage(final Message message) {
                WheelView.this.scroller.computeScrollOffset();
                final int currY = WheelView.this.scroller.getCurrY();
                final int n = WheelView.this.lastScrollY - currY;
                WheelView.access$8(WheelView.this, currY);
                if (n != 0) {
                    WheelView.this.doScroll(n);
                }
                if (Math.abs(currY - WheelView.this.scroller.getFinalY()) < 1) {
                    WheelView.this.scroller.getFinalY();
                    WheelView.this.scroller.forceFinished(true);
                }
                if (!WheelView.this.scroller.isFinished()) {
                    WheelView.this.animationHandler.sendEmptyMessage(message.what);
                    return;
                }
                if (message.what == 0) {
                    WheelView.this.justify();
                    return;
                }
                WheelView.this.finishScrolling();
            }
        };
        this.initData(context);
    }
    
    public WheelView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.ITEM_OFFSET = this.TEXT_SIZE / 10;
        this.adapter = null;
        this.currentItem = 0;
        this.itemsWidth = 0;
        this.labelWidth = 0;
        this.visibleItems = 7;
        this.itemHeight = 0;
        this.isCyclic = false;
        this.changingListeners = new LinkedList<OnWheelChangedListener>();
        this.scrollingListeners = new LinkedList<OnWheelScrollListener>();
        this.gestureListener = new GestureDetector.SimpleOnGestureListener() {
            public boolean onDown(final MotionEvent motionEvent) {
                if (WheelView.this.isScrollingPerformed) {
                    WheelView.this.scroller.forceFinished(true);
                    WheelView.this.clearMessages();
                    return true;
                }
                return false;
            }
            
            public boolean onFling(final MotionEvent motionEvent, final MotionEvent motionEvent2, final float n, final float n2) {
                WheelView.access$8(WheelView.this, WheelView.this.currentItem * WheelView.this.getItemHeight() + WheelView.this.scrollingOffset);
                int n3;
                if (WheelView.this.isCyclic) {
                    n3 = Integer.MAX_VALUE;
                }
                else {
                    n3 = WheelView.this.adapter.getItemsCount() * WheelView.this.getItemHeight();
                }
                int n4;
                if (WheelView.this.isCyclic) {
                    n4 = -n3;
                }
                else {
                    n4 = 0;
                }
                WheelView.this.scroller.fling(0, WheelView.this.lastScrollY, 0, (int)(-n2) / 2, 0, 0, n4, n3);
                WheelView.this.setNextMessage(0);
                return true;
            }
            
            public boolean onScroll(final MotionEvent motionEvent, final MotionEvent motionEvent2, final float n, final float n2) {
                WheelView.this.startScrolling();
                WheelView.this.doScroll((int)(-n2));
                return true;
            }
        };
        this.MESSAGE_SCROLL = 0;
        this.MESSAGE_JUSTIFY = 1;
        this.animationHandler = new Handler() {
            public void handleMessage(final Message message) {
                WheelView.this.scroller.computeScrollOffset();
                final int currY = WheelView.this.scroller.getCurrY();
                final int n = WheelView.this.lastScrollY - currY;
                WheelView.access$8(WheelView.this, currY);
                if (n != 0) {
                    WheelView.this.doScroll(n);
                }
                if (Math.abs(currY - WheelView.this.scroller.getFinalY()) < 1) {
                    WheelView.this.scroller.getFinalY();
                    WheelView.this.scroller.forceFinished(true);
                }
                if (!WheelView.this.scroller.isFinished()) {
                    WheelView.this.animationHandler.sendEmptyMessage(message.what);
                    return;
                }
                if (message.what == 0) {
                    WheelView.this.justify();
                    return;
                }
                WheelView.this.finishScrolling();
            }
        };
        this.initData(context);
    }
    
    static /* synthetic */ void access$8(final WheelView wheelView, final int lastScrollY) {
        wheelView.lastScrollY = lastScrollY;
    }
    
    private String buildText(final boolean b) {
        final StringBuilder sb = new StringBuilder();
        for (int n = this.visibleItems / 2 + 1, i = this.currentItem - n; i <= this.currentItem + n; ++i) {
            if (b || i != this.currentItem) {
                final String textItem = this.getTextItem(i);
                if (textItem != null) {
                    sb.append(textItem);
                }
            }
            if (i < this.currentItem + n) {
                sb.append("\n");
            }
        }
        return sb.toString();
    }
    
    private int calculateLayoutWidth(int n, final int n2) {
        this.initResourcesIfNecessary();
        final int maxTextLength = this.getMaxTextLength();
        if (maxTextLength > 0) {
            this.itemsWidth = (int)(maxTextLength * FloatMath.ceil(Layout.getDesiredWidth((CharSequence)"0", this.itemsPaint)));
        }
        else {
            this.itemsWidth = 0;
        }
        this.itemsWidth += 10;
        this.labelWidth = 0;
        if (this.label != null && this.label.length() > 0) {
            this.labelWidth = (int)FloatMath.ceil(Layout.getDesiredWidth((CharSequence)this.label, this.valuePaint));
        }
        final boolean b = false;
        int n3;
        int n4;
        if (n2 == 1073741824) {
            n3 = 1;
            n4 = n;
        }
        else {
            int n5 = this.itemsWidth + this.labelWidth + 60;
            if (this.labelWidth > 0) {
                n5 += 18;
            }
            final int max = Math.max(n5, this.getSuggestedMinimumWidth());
            n3 = (b ? 1 : 0);
            n4 = max;
            if (n2 == Integer.MIN_VALUE) {
                n3 = (b ? 1 : 0);
                if (n < (n4 = max)) {
                    n3 = 1;
                    n4 = n;
                }
            }
        }
        if (n3 != 0) {
            n = n4 - 18 - 60;
            if (n <= 0) {
                this.labelWidth = 0;
                this.itemsWidth = 0;
            }
            if (this.labelWidth > 0) {
                this.itemsWidth = this.itemsWidth * n / (this.itemsWidth + this.labelWidth);
                this.labelWidth = n - this.itemsWidth;
            }
            else {
                this.itemsWidth = n + 18;
            }
        }
        if (this.itemsWidth > 0) {
            this.createLayouts(this.itemsWidth, this.labelWidth);
        }
        return n4;
    }
    
    private void clearMessages() {
        this.animationHandler.removeMessages(0);
        this.animationHandler.removeMessages(1);
    }
    
    private void createLayouts(final int n, final int n2) {
        final String s = null;
        if (this.itemsLayout == null || this.itemsLayout.getWidth() > n) {
            final String buildText = this.buildText(this.isScrollingPerformed);
            final TextPaint itemsPaint = this.itemsPaint;
            Layout.Alignment layout$Alignment;
            if (n2 > 0) {
                layout$Alignment = Layout.Alignment.ALIGN_OPPOSITE;
            }
            else {
                layout$Alignment = Layout.Alignment.ALIGN_CENTER;
            }
            this.itemsLayout = new StaticLayout((CharSequence)buildText, itemsPaint, n, layout$Alignment, 1.0f, 20.0f, false);
        }
        else {
            this.itemsLayout.increaseWidthTo(n);
        }
        if (!this.isScrollingPerformed && (this.valueLayout == null || this.valueLayout.getWidth() > n)) {
            String item = s;
            if (this.getAdapter() != null) {
                item = this.getAdapter().getItem(this.currentItem);
            }
            if (item == null) {
                item = "";
            }
            final TextPaint valuePaint = this.valuePaint;
            Layout.Alignment layout$Alignment2;
            if (n2 > 0) {
                layout$Alignment2 = Layout.Alignment.ALIGN_OPPOSITE;
            }
            else {
                layout$Alignment2 = Layout.Alignment.ALIGN_CENTER;
            }
            this.valueLayout = new StaticLayout((CharSequence)item, valuePaint, n, layout$Alignment2, 1.0f, 20.0f, false);
        }
        else if (this.isScrollingPerformed) {
            this.valueLayout = null;
        }
        else {
            this.valueLayout.increaseWidthTo(n);
        }
        if (n2 > 0) {
            if (this.labelLayout != null && this.labelLayout.getWidth() <= n2) {
                this.labelLayout.increaseWidthTo(n2);
                return;
            }
            this.labelLayout = new StaticLayout((CharSequence)this.label, this.valuePaint, n2, Layout.Alignment.ALIGN_NORMAL, 1.0f, 20.0f, false);
        }
    }
    
    private void doScroll(int min) {
        this.scrollingOffset += min;
        int currentItem = this.scrollingOffset / this.getItemHeight();
        int i = this.currentItem - currentItem;
        if (this.isCyclic && this.adapter.getItemsCount() > 0) {
            while (i < 0) {
                i += this.adapter.getItemsCount();
            }
            min = i % this.adapter.getItemsCount();
        }
        else if (this.isScrollingPerformed) {
            if (i < 0) {
                currentItem = this.currentItem;
                min = 0;
            }
            else if ((min = i) >= this.adapter.getItemsCount()) {
                currentItem = this.currentItem - this.adapter.getItemsCount() + 1;
                min = this.adapter.getItemsCount() - 1;
            }
        }
        else {
            min = Math.min(Math.max(i, 0), this.adapter.getItemsCount() - 1);
        }
        final int scrollingOffset = this.scrollingOffset;
        if (min != this.currentItem) {
            this.setCurrentItem(min, false);
        }
        else {
            this.invalidate();
        }
        this.scrollingOffset = scrollingOffset - this.getItemHeight() * currentItem;
        if (this.scrollingOffset > this.getHeight()) {
            this.scrollingOffset = this.scrollingOffset % this.getHeight() + this.getHeight();
        }
    }
    
    private void drawCenterRect(final Canvas canvas) {
        final int n = this.getHeight() / 2;
        final int n2 = this.getItemHeight() / 2;
        this.centerDrawable.setBounds(0, n - n2, this.getWidth(), n + n2);
        this.centerDrawable.draw(canvas);
    }
    
    private void drawItems(final Canvas canvas) {
        canvas.save();
        canvas.translate(0.0f, (float)(-this.itemsLayout.getLineTop(1) + this.scrollingOffset));
        this.itemsPaint.setColor(-7303024);
        this.itemsPaint.drawableState = this.getDrawableState();
        this.itemsLayout.draw(canvas);
        canvas.restore();
    }
    
    private void drawShadows(final Canvas canvas) {
        this.topShadow.setBounds(0, 0, this.getWidth(), this.getHeight() / this.visibleItems);
        this.topShadow.draw(canvas);
        this.bottomShadow.setBounds(0, this.getHeight() - this.getHeight() / this.visibleItems, this.getWidth(), this.getHeight());
        this.bottomShadow.draw(canvas);
    }
    
    private void drawValue(final Canvas canvas) {
        this.valuePaint.setColor(-268435456);
        this.valuePaint.drawableState = this.getDrawableState();
        final Rect rect = new Rect();
        this.itemsLayout.getLineBounds(this.visibleItems / 2, rect);
        if (this.labelLayout != null) {
            canvas.save();
            canvas.translate((float)(this.itemsLayout.getWidth() + 18), (float)rect.top);
            this.labelLayout.draw(canvas);
            canvas.restore();
        }
        if (this.valueLayout != null) {
            canvas.save();
            canvas.translate(0.0f, (float)(rect.top + this.scrollingOffset));
            this.valueLayout.draw(canvas);
            canvas.restore();
        }
    }
    
    private int getDesiredHeight(final Layout layout) {
        if (layout == null) {
            return 0;
        }
        return Math.max(this.getItemHeight() * this.visibleItems - this.ITEM_OFFSET * 2 - 20, this.getSuggestedMinimumHeight());
    }
    
    private int getItemHeight() {
        if (this.itemHeight != 0) {
            return this.itemHeight;
        }
        if (this.itemsLayout != null && this.itemsLayout.getLineCount() > 2) {
            return this.itemHeight = this.itemsLayout.getLineTop(2) - this.itemsLayout.getLineTop(1);
        }
        return this.getHeight() / this.visibleItems;
    }
    
    private int getMaxTextLength() {
        final WheelAdapter adapter = this.getAdapter();
        if (adapter != null) {
            final int maximumLength = adapter.getMaximumLength();
            if (maximumLength > 0) {
                return maximumLength;
            }
            String s = null;
            String s2;
            for (int i = Math.max(this.currentItem - this.visibleItems / 2, 0); i < Math.min(this.currentItem + this.visibleItems, adapter.getItemsCount()); ++i, s = s2) {
                final String item = adapter.getItem(i);
                s2 = s;
                if (item != null) {
                    if (s != null) {
                        s2 = s;
                        if (s.length() >= item.length()) {
                            continue;
                        }
                    }
                    s2 = item;
                }
            }
            if (s != null) {
                return s.length();
            }
        }
        return 0;
    }
    
    private String getTextItem(final int n) {
        if (this.adapter != null && this.adapter.getItemsCount() != 0) {
            final int itemsCount = this.adapter.getItemsCount();
            int i;
            if (n < 0 || (i = n) >= itemsCount) {
                if (!this.isCyclic) {
                    return null;
                }
                i = n;
            }
            while (i < 0) {
                i += itemsCount;
            }
            return this.adapter.getItem(i % itemsCount);
        }
        return null;
    }
    
    private void initData(final Context context) {
        (this.gestureDetector = new GestureDetector(context, (GestureDetector.OnGestureListener)this.gestureListener)).setIsLongpressEnabled(false);
        this.scroller = new Scroller(context);
    }
    
    private void initResourcesIfNecessary() {
        if (this.itemsPaint == null) {
            (this.itemsPaint = new TextPaint(33)).setTextSize((float)this.TEXT_SIZE);
        }
        if (this.valuePaint == null) {
            (this.valuePaint = new TextPaint(37)).setTextSize((float)this.TEXT_SIZE);
            this.valuePaint.setShadowLayer(0.1f, 0.0f, 0.1f, -4144960);
        }
        if (this.centerDrawable == null) {
            this.centerDrawable = this.getContext().getResources().getDrawable(2130838534);
        }
        if (this.topShadow == null) {
            this.topShadow = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, WheelView.SHADOWS_COLORS);
        }
        if (this.bottomShadow == null) {
            this.bottomShadow = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, WheelView.SHADOWS_COLORS);
        }
        this.setBackgroundResource(2130838533);
    }
    
    private void invalidateLayouts() {
        this.itemsLayout = null;
        this.valueLayout = null;
        this.scrollingOffset = 0;
    }
    
    private void justify() {
        if (this.adapter == null) {
            return;
        }
        this.lastScrollY = 0;
        final int scrollingOffset = this.scrollingOffset;
        final int itemHeight = this.getItemHeight();
        int n;
        if (scrollingOffset > 0) {
            if (this.currentItem < this.adapter.getItemsCount()) {
                n = 1;
            }
            else {
                n = 0;
            }
        }
        else if (this.currentItem > 0) {
            n = 1;
        }
        else {
            n = 0;
        }
        int n2 = 0;
        Label_0086: {
            if (!this.isCyclic) {
                n2 = scrollingOffset;
                if (n == 0) {
                    break Label_0086;
                }
            }
            n2 = scrollingOffset;
            if (Math.abs((float)scrollingOffset) > itemHeight / 2.0f) {
                if (scrollingOffset < 0) {
                    n2 = scrollingOffset + (itemHeight + 1);
                }
                else {
                    n2 = scrollingOffset - (itemHeight + 1);
                }
            }
        }
        if (Math.abs(n2) > 1) {
            this.scroller.startScroll(0, 0, 0, n2, 400);
            this.setNextMessage(1);
            return;
        }
        this.finishScrolling();
    }
    
    private void setNextMessage(final int n) {
        this.clearMessages();
        this.animationHandler.sendEmptyMessage(n);
    }
    
    private void startScrolling() {
        if (!this.isScrollingPerformed) {
            this.isScrollingPerformed = true;
            this.notifyScrollingListenersAboutStart();
        }
    }
    
    public void addChangingListener(final OnWheelChangedListener onWheelChangedListener) {
        this.changingListeners.add(onWheelChangedListener);
    }
    
    public void addScrollingListener(final OnWheelScrollListener onWheelScrollListener) {
        this.scrollingListeners.add(onWheelScrollListener);
    }
    
    void finishScrolling() {
        if (this.isScrollingPerformed) {
            this.notifyScrollingListenersAboutEnd();
            this.isScrollingPerformed = false;
        }
        this.invalidateLayouts();
        this.invalidate();
    }
    
    public WheelAdapter getAdapter() {
        return this.adapter;
    }
    
    public int getCurrentItem() {
        return this.currentItem;
    }
    
    public String getLabel() {
        return this.label;
    }
    
    public int getVisibleItems() {
        return this.visibleItems;
    }
    
    public boolean isCyclic() {
        return this.isCyclic;
    }
    
    protected void notifyChangingListeners(final int n, final int n2) {
        final Iterator<OnWheelChangedListener> iterator = this.changingListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().onChanged(this, n, n2);
        }
    }
    
    protected void notifyScrollingListenersAboutEnd() {
        final Iterator<OnWheelScrollListener> iterator = this.scrollingListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().onScrollingFinished(this);
        }
    }
    
    protected void notifyScrollingListenersAboutStart() {
        final Iterator<OnWheelScrollListener> iterator = this.scrollingListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().onScrollingStarted(this);
        }
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        if (this.itemsLayout == null) {
            if (this.itemsWidth == 0) {
                this.calculateLayoutWidth(this.getWidth(), 1073741824);
            }
            else {
                this.createLayouts(this.itemsWidth, this.labelWidth);
            }
        }
        if (this.itemsWidth > 0) {
            canvas.save();
            canvas.translate(30.0f, (float)(-this.ITEM_OFFSET));
            this.drawItems(canvas);
            this.drawValue(canvas);
            canvas.restore();
        }
        this.drawCenterRect(canvas);
        this.drawShadows(canvas);
    }
    
    protected void onMeasure(int n, int size) {
        final int mode = View.MeasureSpec.getMode(n);
        final int mode2 = View.MeasureSpec.getMode(size);
        n = View.MeasureSpec.getSize(n);
        size = View.MeasureSpec.getSize(size);
        final int calculateLayoutWidth = this.calculateLayoutWidth(n, mode);
        if (mode2 == 1073741824) {
            n = size;
        }
        else {
            final int n2 = n = this.getDesiredHeight((Layout)this.itemsLayout);
            if (mode2 == Integer.MIN_VALUE) {
                n = Math.min(n2, size);
            }
        }
        this.setMeasuredDimension(calculateLayoutWidth, n);
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        if (this.getAdapter() != null && !this.gestureDetector.onTouchEvent(motionEvent) && motionEvent.getAction() == 1) {
            this.justify();
            return true;
        }
        return true;
    }
    
    public void removeChangingListener(final OnWheelChangedListener onWheelChangedListener) {
        this.changingListeners.remove(onWheelChangedListener);
    }
    
    public void removeScrollingListener(final OnWheelScrollListener onWheelScrollListener) {
        this.scrollingListeners.remove(onWheelScrollListener);
    }
    
    public void scroll(final int n, final int n2) {
        this.scroller.forceFinished(true);
        this.lastScrollY = this.scrollingOffset;
        this.scroller.startScroll(0, this.lastScrollY, 0, n * this.getItemHeight() - this.lastScrollY, n2);
        this.setNextMessage(0);
        this.startScrolling();
    }
    
    public void setAdapter(final WheelAdapter adapter) {
        this.adapter = adapter;
        this.invalidateLayouts();
        this.invalidate();
    }
    
    public void setCurrentItem(final int n) {
        this.setCurrentItem(n, false);
    }
    
    public void setCurrentItem(int i, final boolean b) {
        if (this.adapter != null && this.adapter.getItemsCount() != 0) {
            int currentItem;
            if (i < 0 || (currentItem = i) >= this.adapter.getItemsCount()) {
                if (!this.isCyclic) {
                    return;
                }
                while (i < 0) {
                    i += this.adapter.getItemsCount();
                }
                currentItem = i % this.adapter.getItemsCount();
            }
            if (currentItem != this.currentItem) {
                if (b) {
                    this.scroll(currentItem - this.currentItem, 400);
                    return;
                }
                this.invalidateLayouts();
                i = this.currentItem;
                this.notifyChangingListeners(i, this.currentItem = currentItem);
                this.invalidate();
            }
        }
    }
    
    public void setCyclic(final boolean isCyclic) {
        this.isCyclic = isCyclic;
        this.invalidate();
        this.invalidateLayouts();
    }
    
    public void setInterpolator(final Interpolator interpolator) {
        this.scroller.forceFinished(true);
        this.scroller = new Scroller(this.getContext(), interpolator);
    }
    
    public void setLabel(final String label) {
        if (this.label == null || !this.label.equals(label)) {
            this.label = label;
            this.labelLayout = null;
            this.invalidate();
        }
    }
    
    public void setVisibleItems(final int visibleItems) {
        this.visibleItems = visibleItems;
        this.invalidate();
    }
}
