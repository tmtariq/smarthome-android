package com.gk.zhangzhongbao.ui.wheelview;

public class NumericWheelAdapter implements WheelAdapter
{
    public static final int DEFAULT_MAX_VALUE = 9;
    private static final int DEFAULT_MIN_VALUE = 0;
    private String format;
    private int maxValue;
    private int minValue;
    
    public NumericWheelAdapter() {
        this(0, 9);
    }
    
    public NumericWheelAdapter(final int n, final int n2) {
        this(n, n2, null);
    }
    
    public NumericWheelAdapter(final int minValue, final int maxValue, final String format) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.format = format;
    }
    
    @Override
    public String getItem(int n) {
        if (n < 0 || n >= this.getItemsCount()) {
            return null;
        }
        n += this.minValue;
        if (this.format != null) {
            return String.format(this.format, n);
        }
        return Integer.toString(n);
    }
    
    @Override
    public int getItemsCount() {
        return this.maxValue - this.minValue + 1;
    }
    
    @Override
    public int getMaximumLength() {
        int length = Integer.toString(Math.max(Math.abs(this.maxValue), Math.abs(this.minValue))).length();
        if (this.minValue < 0) {
            ++length;
        }
        return length;
    }
}
