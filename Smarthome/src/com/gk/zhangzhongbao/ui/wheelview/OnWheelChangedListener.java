package com.gk.zhangzhongbao.ui.wheelview;

public interface OnWheelChangedListener
{
    void onChanged(WheelView p0, int p1, int p2);
}
