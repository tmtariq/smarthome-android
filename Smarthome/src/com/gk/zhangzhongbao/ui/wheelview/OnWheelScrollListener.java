package com.gk.zhangzhongbao.ui.wheelview;

public interface OnWheelScrollListener
{
    void onScrollingFinished(WheelView p0);
    
    void onScrollingStarted(WheelView p0);
}
