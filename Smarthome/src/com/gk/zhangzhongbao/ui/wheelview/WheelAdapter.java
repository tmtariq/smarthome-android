package com.gk.zhangzhongbao.ui.wheelview;

public interface WheelAdapter
{
    String getItem(int p0);
    
    int getItemsCount();
    
    int getMaximumLength();
}
