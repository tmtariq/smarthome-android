package com.gk.zhangzhongbao.ui.app;

import java.io.*;
import java.util.regex.*;
import java.text.*;
import java.util.*;

public class PubFunc
{
    public static boolean CreatePath(final String s) {
        String string = s;
        if (!s.endsWith("/")) {
            string = String.valueOf(s) + "/";
        }
        final File file = new File(string);
        return file.exists() || file.mkdirs();
    }
    
    public static boolean IsDate(final String s) {
        return Pattern.compile("^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s(((0?[0-9])|([1-2][0-3]))\\:([0-5]?[0-9])((\\s)|(\\:([0-5]?[0-9])))))?$").matcher(s).matches();
    }
    
    public static int StringToInteger(final String s) {
        return StringToInteger(s, 0);
    }
    
    public static int StringToInteger(final String s, final int n) {
        try {
            return Integer.parseInt(s);
        }
        catch (Exception ex) {
            return n;
        }
    }
    
    public static long StringToLong(final String s) {
        return StringToLong(s, 0L);
    }
    
    public static long StringToLong(final String s, final long n) {
        try {
            return Long.parseLong(s);
        }
        catch (Exception ex) {
            return n;
        }
    }
    
    public static boolean bNumberData(final String s) {
        try {
            Double.parseDouble(s);
            return true;
        }
        catch (Exception ex) {
            return false;
        }
    }
    
    public static String getFormatDate(String string) {
        if (string.length() < 8) {
            return "";
        }
        final int[] array2;
        final int[] array = array2 = new int[6];
        array2[0] = 4;
        array2[1] = 2;
        array2[3] = (array2[2] = 2);
        array2[5] = (array2[4] = 2);
        int n = 0;
        String string2 = "";
        final String[] array3 = { "", "", "", "00", "00", "00" };
        final String[][] array4 = { { "1900", "9999" }, { "01", "12" }, { "01", "31" }, { "00", "23" }, { "00", "59" }, { "00", "59" } };
        int n4;
        String s;
        for (int n2 = 0; n2 < string.length() && n < array.length; ++n2, n = n4, string2 = s) {
            final char char1 = string.charAt(n2);
            int n3;
            if (char1 >= '0' && char1 <= '9') {
                string2 = String.valueOf(string2) + char1;
                if (string2.length() == array[n]) {
                    n3 = 1;
                }
                else {
                    n3 = 0;
                }
            }
            else {
                n3 = 1;
            }
            n4 = n;
            s = string2;
            if (n3 != 0) {
                n4 = n;
                s = string2;
                if (string2.length() > 0) {
                    if (string2.length() == array[n]) {
                        array3[n] = string2;
                    }
                    else {
                        array3[n] = getStringFillZero(string2, array[n]);
                    }
                    s = "";
                    n4 = n + 1;
                }
            }
        }
        if (string2.length() > 0 && n < array.length) {
            if (string2.length() == array[n]) {
                array3[n] = string2;
            }
            else {
                array3[n] = getStringFillZero(string2, array[n]);
            }
        }
        string = "";
        for (int i = 0; i < array.length; ++i) {
            if (array3[i].compareTo(array4[i][0]) < 0 || array3[i].compareTo(array4[i][1]) > 0) {
                string = "";
                break;
            }
            string = String.valueOf(string) + array3[i] + (new String[] { "-", "-", " ", ":", ":", "" })[i];
        }
        if (string.length() > 0) {
            String formatDate;
            if (!(formatDate = getFormatDate(string, "yyyy-MM-dd HH:mm:ss")).equals(string)) {
                formatDate = "";
            }
            return formatDate;
        }
        return "";
    }
    
    public static String getFormatDate(String format, final String s) {
        if (format.equals("") || format == null) {
            return "";
        }
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(s);
        try {
            format = simpleDateFormat2.format(simpleDateFormat.parse(format));
            return format;
        }
        catch (Exception ex) {
            return "";
        }
    }
    
    public static String getStringFillZero(String string, final int n) {
        while (string.length() < n) {
            string = "0" + string;
        }
        return string;
    }
    
    public static String getSystemTime() {
        return getSystemTime("yyyy-MM-dd HH:mm:ss");
    }
    
    public static String getSystemTime(final String s) {
        final Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        return new SimpleDateFormat(s).format(instance.getTime());
    }
}
