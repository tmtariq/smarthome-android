package com.gk.zhangzhongbao.ui.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Paint;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Thumbnails;
import android.view.View;
import android.view.Window;

import java.io.File;

public class MethodsCompat
{
	@TargetApi(value=8)
    public static File getExternalCacheDir(Context context) {
        return context.getExternalCacheDir();
    }

    @TargetApi(value=7)
    public static Bitmap getThumbnail(ContentResolver contentResolver, long l, int n, BitmapFactory.Options options) {
        return MediaStore.Images.Thumbnails.getThumbnail((ContentResolver)contentResolver, (long)l, (int)n, (BitmapFactory.Options)options);
    }

    @TargetApi(value=5)
    public static void overridePendingTransition(Activity activity, int n, int n2) {
        activity.overridePendingTransition(n, n2);
    }

    @TargetApi(value=11)
    public static void recreate(Activity activity) {
        if (Build.VERSION.SDK_INT >= 11) {
            activity.recreate();
        }
    }

    @TargetApi(value=11)
    public static void setLayerType(View view, int n, Paint paint) {
        if (Build.VERSION.SDK_INT >= 11) {
            view.setLayerType(n, paint);
        }
    }

    @TargetApi(value=14)
    public static void setUiOptions(Window window, int n) {
        if (Build.VERSION.SDK_INT >= 14) {
            window.setUiOptions(n);
        }
    }
}


/* Location:              /Users/tariq/Downloads/output_jar.jar!/com/gk/zhangzhongbao/ui/app/MethodsCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */