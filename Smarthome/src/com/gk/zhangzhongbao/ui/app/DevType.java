package com.gk.zhangzhongbao.ui.app;

public class DevType
{
    public static final int AIR = 32;
    public static final int AIR_R = 6;
    public static final int AUX_F = 0;
    public static final int CG = 80;
    public static final int CG_L = 96;
    public static final int DVB = 1;
    public static final int DVB_F = 2;
    public static final int DVD_F = 5;
    public static final int HEATER = 48;
    public static final int LIGHT = 16;
    public static final int LOCK = 64;
    public static final int TV = 3;
    public static final int TV_F = 4;
}
