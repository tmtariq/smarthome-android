package com.gk.zhangzhongbao.ui.app;

import android.database.sqlite.*;
import android.util.*;
import java.util.*;
import com.gk.wifictr.net.data.*;
import android.database.*;
import com.gk.zhangzhongbao.ui.bean.*;
import com.gk.zhangzhongbao.ui.view.*;
import android.content.*;

public class DataBase
{
    public static String CGTYPE = "type";
    public static String CG_ID = "_id";
    public static String CG_NAME = "cg_name";
    public static String CG_NO = "cg_dev_no";
    public static String CG_SN = "cg_sn";
    public static String CG_TABLE = "CREATE TABLE IF NOT EXISTS cg_table (_id INTEGER PRIMARY KEY,host_name TEXT,cg_dev_no INTEGER,type INTEGER,cg_name TEXT,cg_sn TEXT,cg_type INTEGER)";
    public static String CG_TABLE_NAME = "cg_table";
    public static String CG_TYPE = "cg_type";
    public static String CHANNEL_ID = "_id";
    public static String CHANNEL_LOGO_PATH = "path";
    public static String CHANNEL_LOGO_RES = "res_name";
    public static String CHANNEL_NAME = "name";
    public static String CHANNEL_NUM = "num";
    private static String CHANNEL_TABLE = "CREATE TABLE IF NOT EXISTS channel_table (_id INTEGER PRIMARY KEY,host_name TEXT,num INTEGER,name TEXT,res_name TEXT,channel_id INTEGER,path TEXT)";
    public static String CHANNEL_TABLE_NAME = "channel_table";
    public static String CH_ID = "channel_id";
    private static String DATABASE_NAME = "com.gk.smarthome.db";
    public static String HOST_ADD_TIME = "host_add_time";
    public static String HOST_ID = "_id";
    public static String HOST_NAME = "hosts_name";
    public static String HOST_NUM = "hosts_num";
    public static String HOST_SN = "host_name";
    public static String HOST_TABLE = "CREATE TABLE IF NOT EXISTS hosts_table (_id INTEGER PRIMARY KEY,hosts_num INTEGER,host_add_time TEXT,hosts_name TEXT)";
    public static String HOST_TABLE_NAME = "hosts_table";
    public static String LIGHT1_STATUS = "light1_status";
    public static String LIGHT2_STATUS = "light2_status";
    public static String LIGHT3_STATUS = "light3_status";
    public static String LIGHT_COUNT = "count";
    public static String LIGHT_ID = "_id";
    public static String LIGHT_NAME = "name";
    public static String LIGHT_NO = "light_no";
    public static String LIGHT_SNCODE = "sncode";
    private static String LIGHT_TABLE = "CREATE TABLE IF NOT EXISTS light_table (_id INTEGER PRIMARY KEY,host_name TEXT,light_no INTEGER,type INTEGER,count INTEGER,name TEXT,sncode TEXT,one_light_status INTEGER,light1_status INTEGER,light2_status INTEGER,light3_status INTEGER)";
    public static String LIGHT_TABLE_NAME = "light_table";
    public static String LIGHT_TYPE = "type";
    public static String LOCK_ID = "_id";
    public static String LOCK_NAME = "lock_name";
    public static String LOCK_NO = "lock_dev_no";
    public static String LOCK_SN = "lock_sn";
    public static String LOCK_TABLE = "CREATE TABLE IF NOT EXISTS lock_table (_id INTEGER PRIMARY KEY,host_name TEXT,lock_dev_no INTEGER,type INTEGER,lock_name TEXT,lock_sn TEXT)";
    public static String LOCK_TABLE_NAME = "lock_table";
    public static String LOCK_TYPE = "type";
    public static String MB_ADD = "isadd";
    public static String MB_HOST_NUM = "host_num";
    public static String MB_ID = "_id";
    public static String MB_KEY = "key";
    public static String MB_TABLE = "CREATE TABLE IF NOT EXISTS main_buttons_table (_id INTEGER PRIMARY KEY,host_num INTEGER,host_name TEXT,key TEXT,isadd INTEGER)";
    public static String MB_TABLE_NAME = "main_buttons_table";
    public static String ONE_LIGHT_STATUS = "one_light_status";
    public static DataBase db;
    Context context;
    private SQLiteDatabase mSQLiteDatabase;
    
    public DataBase(Context context) {
        this.mSQLiteDatabase = null;
        this.context = context;
        this.mSQLiteDatabase = this.context.openOrCreateDatabase("com.gk.smarthome.db", 0, (SQLiteDatabase.CursorFactory)null);
        try {
            this.mSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS channel_table (_id INTEGER PRIMARY KEY,host_name TEXT,num INTEGER,name TEXT,res_name TEXT,channel_id INTEGER,path TEXT)");
            this.mSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS light_table (_id INTEGER PRIMARY KEY,host_name TEXT,light_no INTEGER,type INTEGER,count INTEGER,name TEXT,sncode TEXT,one_light_status INTEGER,light1_status INTEGER,light2_status INTEGER,light3_status INTEGER)");
            this.mSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS lock_table (_id INTEGER PRIMARY KEY,host_name TEXT,lock_dev_no INTEGER,type INTEGER,lock_name TEXT,lock_sn TEXT)");
            this.mSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS cg_table (_id INTEGER PRIMARY KEY,host_name TEXT,cg_dev_no INTEGER,type INTEGER,cg_name TEXT,cg_sn TEXT,cg_type INTEGER)");
            this.mSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS main_buttons_table (_id INTEGER PRIMARY KEY,host_num INTEGER,host_name TEXT,key TEXT,isadd INTEGER)");
            this.mSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS hosts_table (_id INTEGER PRIMARY KEY,hosts_num INTEGER,host_add_time TEXT,hosts_name TEXT)");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void DeleteDataBase(String s) {
        this.context.deleteDatabase(s);
    }
    
    public void DeleteTable(String s) {
        this.mSQLiteDatabase.execSQL("DROP TABLE " + s);
    }
    
    public boolean deleteData(String s, String s2, String s3) {
        try {
            this.mSQLiteDatabase.execSQL("DELETE FROM " + s + " WHERE " + s2 + "=?", (Object[])new String[] { s3 });
            return true;
        }
        catch (Exception ex) {
            Log.e("database", "\u5220\u9664\u6570\u636e\u51fa\u9519:" + ex.getMessage());
            return false;
        }
    }
    
    public boolean deleteData(String s, String s2, String s3, String s4) {
        try {
            this.mSQLiteDatabase.execSQL("DELETE FROM " + s + " WHERE " + s2 + "=?" + " And " + "host_name" + "=?", (Object[])new String[] { s3, s4 });
            return true;
        }
        catch (Exception ex) {
            Log.e("database", "\u5220\u9664\u6570\u636e\u51fa\u9519:" + ex.getMessage());
            return false;
        }
    }
    
    public boolean deleteHost(String s) {
        try {
            this.mSQLiteDatabase.execSQL("DELETE FROM hosts_table WHERE host_add_time=?", (Object[])new String[] { s });
            return true;
        }
        catch (Exception ex) {
            Log.e("database", "\u5220\u9664\u6570\u636e\u51fa\u9519:" + ex.getMessage());
            return false;
        }
    }
    
    public boolean deleteMBData(String s) {
        try {
            this.mSQLiteDatabase.execSQL("DELETE FROM main_buttons_table WHERE key=?", (Object[])new String[] { s });
            return true;
        }
        catch (Exception ex) {
            Log.e("database", "\u5220\u9664\u6570\u636e\u51fa\u9519:" + ex.getMessage());
            return false;
        }
    }
    
    public void deleteTable() {
        this.mSQLiteDatabase.execSQL("DROP TABLE IF EXISTS light_table");
        try {
            this.mSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS light_table (_id INTEGER PRIMARY KEY,host_name TEXT,light_no INTEGER,type INTEGER,count INTEGER,name TEXT,sncode TEXT,one_light_status INTEGER,light1_status INTEGER,light2_status INTEGER,light3_status INTEGER)");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public ArrayList<SubDevice> getCGList(String string) {
        ArrayList<SubDevice> list = new ArrayList<SubDevice>();
        string = "select * from cg_table where host_name='" + string + "'";
        Cursor rawQuery = this.mSQLiteDatabase.rawQuery(string, (String[])null);
        rawQuery.moveToFirst();
        for (int i = 0; i < rawQuery.getCount(); ++i) {
            SubDevice subDevice = new SubDevice(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("cg_dev_no")), rawQuery.getString(rawQuery.getColumnIndexOrThrow("cg_name")), rawQuery.getString(rawQuery.getColumnIndexOrThrow("cg_sn")), (byte)rawQuery.getInt(rawQuery.getColumnIndexOrThrow("type")));
            subDevice.cg_type = rawQuery.getInt(rawQuery.getColumnIndexOrThrow("cg_type"));
            rawQuery.moveToNext();
            list.add(subDevice);
        }
        rawQuery.close();
        return list;
    }
    
    public ArrayList<Channel> getChlList(String string) {
        ArrayList<Channel> list = new ArrayList<Channel>();
        string = "select * from channel_table where host_name='" + string + "'";
        Cursor rawQuery = this.mSQLiteDatabase.rawQuery(string, (String[])null);
        rawQuery.moveToFirst();
        for (int i = 0; i < rawQuery.getCount(); ++i) {
            Channel channel = new Channel();
            channel.setId(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("channel_id")));
            channel.setName(rawQuery.getString(rawQuery.getColumnIndexOrThrow("name")));
            channel.setChannelNum(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("num")));
            channel.setImgResName(rawQuery.getString(rawQuery.getColumnIndexOrThrow("res_name")));
            channel.setLogoPath(rawQuery.getString(rawQuery.getColumnIndexOrThrow("path")));
            rawQuery.moveToNext();
            list.add(channel);
        }
        rawQuery.close();
        return list;
    }
    
    public ArrayList<Host> getHostList(Context context) {
        ArrayList<Host> list = new ArrayList<Host>();
        Cursor rawQuery = this.mSQLiteDatabase.rawQuery("select * from hosts_table", null);
        rawQuery.moveToFirst();
        for (int i = 0; i < rawQuery.getCount(); ++i) {
            Host host = new Host();
            host.num = rawQuery.getInt(rawQuery.getColumnIndex("hosts_num"));
            host.addTime = rawQuery.getString(rawQuery.getColumnIndex("host_add_time"));
            host.name = rawQuery.getString(rawQuery.getColumnIndex("hosts_name"));
            rawQuery.moveToNext();
            list.add(host);
        }
        rawQuery.close();
        return list;
    }
    
    public ArrayList<LightItem> getLightList() {
        ArrayList<LightItem> list = new ArrayList<LightItem>();
        Cursor query = this.mSQLiteDatabase.query("light_table", new String[] { "_id", "light_no", "name", "sncode", "count", "one_light_status", "light1_status", "light2_status", "light3_status" }, (String)null, (String[])null, (String)null, (String)null, (String)null, (String)null);
        query.moveToFirst();
        for (int i = 0; i < query.getCount(); ++i) {
            LightItem lightItem = new LightItem();
            lightItem.setDevNo(query.getInt(query.getColumnIndexOrThrow("light_no")));
            lightItem.setDevType(query.getInt(query.getColumnIndexOrThrow("type")));
            lightItem.setName(query.getString(query.getColumnIndexOrThrow("name")));
            lightItem.setSnCode(query.getString(query.getColumnIndexOrThrow("sncode")));
            lightItem.setCount(query.getInt(query.getColumnIndexOrThrow("count")));
            lightItem.setLight1(query.getInt(query.getColumnIndexOrThrow("light1_status")));
            lightItem.setLight2(query.getInt(query.getColumnIndexOrThrow("light2_status")));
            lightItem.setLight3(query.getInt(query.getColumnIndexOrThrow("light3_status")));
            lightItem.setOneLight(query.getInt(query.getColumnIndexOrThrow("one_light_status")));
            query.moveToNext();
            list.add(lightItem);
        }
        query.close();
        return list;
    }
    
    public ArrayList<LightItem> getLightList(String string) {
        ArrayList<LightItem> list = new ArrayList<LightItem>();
        string = "select * from light_table where host_name='" + string + "'";
        Cursor rawQuery = this.mSQLiteDatabase.rawQuery(string, (String[])null);
        rawQuery.moveToFirst();
        for (int i = 0; i < rawQuery.getCount(); ++i) {
            LightItem lightItem = new LightItem();
            lightItem.setDevNo(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("light_no")));
            lightItem.setDevType(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("type")));
            lightItem.setName(rawQuery.getString(rawQuery.getColumnIndexOrThrow("name")));
            lightItem.setSnCode(rawQuery.getString(rawQuery.getColumnIndexOrThrow("sncode")));
            lightItem.setCount(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("count")));
            lightItem.setLight1(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("light1_status")));
            lightItem.setLight2(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("light2_status")));
            lightItem.setLight3(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("light3_status")));
            lightItem.setOneLight(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("one_light_status")));
            rawQuery.moveToNext();
            list.add(lightItem);
        }
        rawQuery.close();
        return list;
    }
    
    public ArrayList<SubDevice> getLockList(String string) {
        ArrayList<SubDevice> list = new ArrayList<SubDevice>();
        string = "select * from lock_table where host_name='" + string + "'";
        Cursor rawQuery = this.mSQLiteDatabase.rawQuery(string, (String[])null);
        rawQuery.moveToFirst();
        for (int i = 0; i < rawQuery.getCount(); ++i) {
            SubDevice subDevice = new SubDevice(rawQuery.getInt(rawQuery.getColumnIndexOrThrow("lock_dev_no")), rawQuery.getString(rawQuery.getColumnIndexOrThrow("lock_name")), rawQuery.getString(rawQuery.getColumnIndexOrThrow("lock_sn")), (byte)rawQuery.getInt(rawQuery.getColumnIndexOrThrow("type")));
            rawQuery.moveToNext();
            list.add(subDevice);
        }
        rawQuery.close();
        return list;
    }
    
    public ArrayList<MainButton> getMBList(Context context, int i) {
        ArrayList<MainButton> list = new ArrayList<MainButton>();
        Cursor rawQuery = this.mSQLiteDatabase.rawQuery("select * from main_buttons_table where host_num=" + i, (String[])null);
        rawQuery.moveToFirst();
        MainButton mainButton;
        for (i = 0; i < rawQuery.getCount(); ++i) {
            mainButton = new MainButton(context);
            mainButton.key = rawQuery.getString(rawQuery.getColumnIndex("key"));
            mainButton.isAdd = rawQuery.getInt(rawQuery.getColumnIndex("isadd"));
            rawQuery.moveToNext();
            list.add(mainButton);
        }
        rawQuery.close();
        return list;
    }
    
    public void insert(Channel channel, String s) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("host_name", s);
        contentValues.put("channel_id", channel.getId());
        contentValues.put("num", channel.getChannelNum());
        contentValues.put("name", channel.getName());
        contentValues.put("res_name", channel.getImgResName());
        contentValues.put("path", channel.getLogoPath());
        this.mSQLiteDatabase.insert("channel_table", (String)null, contentValues);
    }
    
    public void insertCG(SubDevice subDevice, int n, String s) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("host_name", s);
        contentValues.put("cg_dev_no", subDevice.devNo);
        contentValues.put("type", subDevice.devType);
        contentValues.put("cg_name", subDevice.devName);
        contentValues.put("cg_sn", subDevice.devSN);
        contentValues.put("cg_type", n);
        this.mSQLiteDatabase.insert("cg_table", (String)null, contentValues);
    }
    
    public void insertHost(Host host) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("hosts_num", host.num);
        contentValues.put("host_add_time", host.addTime);
        contentValues.put("hosts_name", host.name);
        this.mSQLiteDatabase.insert("hosts_table", (String)null, contentValues);
    }
    
    public void insertLight(final LightItem lightItem, final String s) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                ContentValues contentValues = new ContentValues();
                contentValues.put("host_name", s);
                contentValues.put("light_no", lightItem.getDevNo());
                contentValues.put("type", lightItem.getDevType());
                contentValues.put("name", lightItem.getName());
                contentValues.put("sncode", lightItem.getSnCode());
                contentValues.put("count", lightItem.getCount());
                contentValues.put("one_light_status", lightItem.getLightStatus());
                contentValues.put("light1_status", lightItem.get3LightStatus()[0]);
                contentValues.put("light2_status", lightItem.get3LightStatus()[1]);
                contentValues.put("light3_status", lightItem.get3LightStatus()[2]);
                mSQLiteDatabase.insert("light_table", (String)null, contentValues);
            }
        }).start();
    }
    
    public void insertLock(final SubDevice subDevice, final String s) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                ContentValues contentValues = new ContentValues();
                contentValues.put("host_name", s);
                contentValues.put("lock_dev_no", subDevice.devNo);
                contentValues.put("type", subDevice.devType);
                contentValues.put("lock_name", subDevice.devName);
                contentValues.put("lock_sn", subDevice.devSN);
                mSQLiteDatabase.insert("lock_table", (String)null, contentValues);
            }
        }).start();
    }
    
    public void insertMB(MainButton mainButton, int n) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("host_num", n);
        contentValues.put("key", mainButton.key);
        contentValues.put("isadd", mainButton.isAdd);
        this.mSQLiteDatabase.insert("main_buttons_table", (String)null, contentValues);
    }
    
    public void update(final String s, final String s2, final String s3, final String s4) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    updateSQL("Update " + s + " Set " + s2 + "=? Where " + s3 + "=?", new String[] { new StringBuilder(String.valueOf(s2)).toString(), s4 });
                }
                catch (Exception ex) {
                    Log.e("database", "\u66f4\u65b0\u6570\u636e\u51fa\u9519:" + ex.getMessage());
                }
            }
        }).start();
    }
    
    public void updateCG(final SubDevice subDevice, final String s) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    updateSQL("Update cg_table Set cg_dev_no=? Where cg_sn=? And host_name=? And type=?", new String[] { new StringBuilder(String.valueOf(subDevice.devNo)).toString(), subDevice.devSN, s, new StringBuilder(String.valueOf(subDevice.devType)).toString() });
                    updateSQL("Update cg_table Set cg_name=? Where cg_sn=? And host_name=? And type=?", new String[] { subDevice.devName, subDevice.devSN, s, new StringBuilder(String.valueOf(subDevice.devType)).toString() });
                }
                catch (Exception ex) {
                    Log.e("database", "\u66f4\u65b0\u6570\u636e\u51fa\u9519:" + ex.getMessage());
                }
            }
        }).start();
    }
    
    public boolean updateChannelNum(int n, String s, String s2) {
        try {
            this.updateSQL("Update channel_table Set num=? Where channel_id=? And host_name=?", new String[] { new StringBuilder(String.valueOf(n)).toString(), s, s2 });
            return true;
        }
        catch (Exception ex) {
            Log.e("database", "\u66f4\u65b0\u6570\u636e\u51fa\u9519:" + ex.getMessage());
            return false;
        }
    }
    
    public void updateHost(Host host) {
        try {
            this.updateSQL("Update hosts_table Set hosts_name=? Where host_add_time=?", new String[] { host.name, host.addTime });
        }
        catch (Exception ex) {
            Log.e("database", "\u66f4\u65b0\u6570\u636e\u51fa\u9519:" + ex.getMessage());
        }
    }
    
    public void updateLight(final LightItem lightItem, final String s) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    updateSQL("Update light_table Set name=? Where sncode=? And host_name=? And type=?", new String[] { lightItem.getName(), lightItem.getSnCode(), s, new StringBuilder(String.valueOf(lightItem.getDevType())).toString() });
                    updateSQL("Update light_table Set light_no=? Where sncode=? And host_name=? And type=?", new String[] { new StringBuilder(String.valueOf(lightItem.getDevNo())).toString(), lightItem.getSnCode(), s, new StringBuilder(String.valueOf(lightItem.getDevType())).toString() });
                    switch (lightItem.getCount()) {
                        case 1: {
                            updateSQL("Update light_table Set one_light_status=? Where sncode=? And host_name=? And type=?", new String[] { new StringBuilder(String.valueOf(lightItem.getLightStatus())).toString(), lightItem.getSnCode(), s, new StringBuilder(String.valueOf(lightItem.getDevType())).toString() });
                            return;
                        }
                        case 3: {
                            break;
                        }
                        default: {
                            return;
                        }
                    }
                }
                catch (Exception ex) {
                    Log.e("database", "\u66f4\u65b0\u6570\u636e\u51fa\u9519:" + ex.getMessage());
                    return;
                }
                updateSQL("Update light_table Set light1_status=? Where sncode=? And host_name=? And type=?", new String[] { new StringBuilder(String.valueOf(lightItem.get3LightStatus()[0])).toString(), lightItem.getSnCode(), s, new StringBuilder(String.valueOf(lightItem.getDevType())).toString() });
                updateSQL("Update light_table Set light2_status=? Where sncode=? And host_name=? And type=?", new String[] { new StringBuilder(String.valueOf(lightItem.get3LightStatus()[1])).toString(), lightItem.getSnCode(), s, new StringBuilder(String.valueOf(lightItem.getDevType())).toString() });
                updateSQL("Update light_table Set light3_status=? Where sncode=? And host_name=? And type=?", new String[] { new StringBuilder(String.valueOf(lightItem.get3LightStatus()[2])).toString(), lightItem.getSnCode(), s, new StringBuilder(String.valueOf(lightItem.getDevType())).toString() });
            }
        }).start();
    }
    
    public void updateLock(final SubDevice subDevice, final String s) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    updateSQL("Update lock_table Set lock_dev_no=? Where lock_sn=? And host_name=?", new String[] { new StringBuilder(String.valueOf(subDevice.devNo)).toString(), subDevice.devSN, s });
                    updateSQL("Update lock_table Set lock_name=? Where lock_sn=? And host_name=?", new String[] { subDevice.devName, subDevice.devSN, s });
                }
                catch (Exception ex) {
                    Log.e("database", "\u66f4\u65b0\u6570\u636e\u51fa\u9519:" + ex.getMessage());
                }
            }
        }).start();
    }
    
    public void updateMB(MainButton mainButton, String s) {
        try {
            this.updateSQL("Update main_buttons_table Set isadd=? Where host_name=?", new String[] { new StringBuilder(String.valueOf(mainButton.isAdd)).toString(), mainButton.key, s });
        }
        catch (Exception ex) {
            Log.e("database", "\u66f4\u65b0\u6570\u636e\u51fa\u9519:" + ex.getMessage());
        }
    }
    
    public void updateSQL(Channel channel) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("num", channel.getChannelNum());
        contentValues.put("name", channel.getName());
        contentValues.put("res_name", channel.getImgResName());
        contentValues.put("path", channel.getLogoPath());
        this.mSQLiteDatabase.update("channel_table", contentValues, "name=" + channel.getName(), (String[])null);
    }
    
    public boolean updateSQL(String s, String[] array) {
        try {
            this.mSQLiteDatabase.execSQL(s, (Object[])array);
            return true;
        }
        catch (Exception ex) {
            Log.d("MyTest", "\u66f4\u65b0SQL\u51fa\u9519:" + ex.getMessage());
            return false;
        }
    }
}
