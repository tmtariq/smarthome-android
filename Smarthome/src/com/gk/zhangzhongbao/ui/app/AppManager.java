package com.gk.zhangzhongbao.ui.app;

import java.util.*;
import android.content.*;
import android.app.*;

public class AppManager
{
    private static Stack<Activity> activityStack;
    private static AppManager appManager;
    
    public static AppManager getAppManager() {
        if (AppManager.appManager == null) {
            AppManager.appManager = new AppManager();
        }
        return AppManager.appManager;
    }
    
    public void AppExit(final Context context) {
        try {
            this.finishAllActivity();
            ((ActivityManager)context.getSystemService("activity")).restartPackage(context.getPackageName());
            System.exit(0);
        }
        catch (Exception ex) {}
    }
    
    public void addActivity(final Activity activity) {
        if (AppManager.activityStack == null) {
            AppManager.activityStack = new Stack<Activity>();
        }
        AppManager.activityStack.add(activity);
    }
    
    public Activity currentActivity() {
        return AppManager.activityStack.lastElement();
    }
    
    public void finishActivity() {
        this.finishActivity(AppManager.activityStack.lastElement());
    }
    
    public void finishActivity(final Activity activity) {
        if (activity != null) {
            AppManager.activityStack.remove(activity);
            activity.finish();
        }
    }
    
    public void finishAllActivity() {
        for (int i = 0; i < AppManager.activityStack.size(); ++i) {
            if (AppManager.activityStack.get(i) != null) {
                ((Activity)AppManager.activityStack.get(i)).finish();
            }
        }
        AppManager.activityStack.clear();
    }
}
