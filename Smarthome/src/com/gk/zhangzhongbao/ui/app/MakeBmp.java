package com.gk.zhangzhongbao.ui.app;

import android.content.*;
import android.graphics.*;
import android.media.*;
import java.io.*;

public class MakeBmp
{
    public static int POS_CENTER = 2;
    public static int POS_END = 3;
    public static int POS_START = 1;
    
    public static Bitmap CreateBitmap(Context context, int n, int n2) {
        return CreateBitmap(compressImage(context, n, n2), n2, n2, -1.0f, 0, Bitmap.Config.ARGB_8888);
    }
    
    public static Bitmap CreateBitmap(Context context, int n, int n2, int n3) {
        return CreateBitmap(compressImage(context, n, n2, n3), n2, n3, -1.0f, 0, Bitmap.Config.ARGB_8888);
    }
    
    public static Bitmap CreateBitmap(Context context, int n, int n2, Bitmap.Config bitmap$Config) {
        return CreateBitmap(compressImage(context, n, n2), n2, n2, -1.0f, 0, bitmap$Config);
    }
    
    public static Bitmap CreateBitmap(Bitmap bitmap, int n, int n2, float n3, int n4, Bitmap.Config bitmap$Config) {
        Bitmap bitmap2 = null;
        if (bitmap != null) {
            int width = bitmap.getWidth();
            int height;
            int n5 = height = bitmap.getHeight();
            int n6 = width;
            if (n4 % 180 != 0) {
                int n7 = width + n5;
                height = n7 - n5;
                n6 = n7 - height;
            }
            MyRect makeRect;
            if (n3 > 0.0f) {
                makeRect = MakeRect(n6, height, n3);
            }
            else {
                makeRect = new MyRect();
                makeRect.m_x = 0.0f;
                makeRect.m_y = 0.0f;
                makeRect.m_w = n6;
                makeRect.m_h = height;
            }
            float n8 = 1.0f;
            if (n > 0 && n2 > 0 && (n < makeRect.m_w || n2 < makeRect.m_h)) {
                n3 = n / makeRect.m_w;
                float n9 = n2 / makeRect.m_h;
                if (n3 > n9) {
                    n3 = n9;
                }
            }
            else if (n > 0) {
                n3 = n8;
                if (n < makeRect.m_w) {
                    n3 = n / makeRect.m_w;
                }
            }
            else {
                n3 = n8;
                if (n2 > 0) {
                    n3 = n8;
                    if (n2 < makeRect.m_h) {
                        n3 = n2 / makeRect.m_h;
                    }
                }
            }
            float n10 = makeRect.m_w * n3;
            float n11 = makeRect.m_h * n3;
            float n12 = n10 / 2.0f;
            float n13 = n11 / 2.0f;
            Matrix matrix = new Matrix();
            matrix.postTranslate((n10 - bitmap.getWidth()) / 2.0f, (n11 - bitmap.getHeight()) / 2.0f);
            matrix.postRotate((float)n4, n12, n13);
            matrix.postScale(n3, n3, n12, n13);
            bitmap2 = Bitmap.createBitmap((int)n10, (int)n11, bitmap$Config);
            Canvas canvas = new Canvas(bitmap2);
            canvas.setDrawFilter((DrawFilter)new PaintFlagsDrawFilter(0, 3));
            canvas.drawBitmap(bitmap, matrix, (Paint)null);
        }
        return bitmap2;
    }
    
    public static Bitmap CreateBitmap(String s, int n) {
        return CreateBitmap(compressImage(s, n), n, n, -1.0f, getBitmapRotate(s), Bitmap.Config.ARGB_8888);
    }
    
    public static Bitmap CreateFixBitmap(Bitmap bitmap, int n, int n2, int n3, int n4, Bitmap.Config bitmap$Config) {
        Bitmap bitmap2 = null;
        if (bitmap != null) {
            int width = bitmap.getWidth();
            int height;
            int n5 = height = bitmap.getHeight();
            int n6 = width;
            if (n4 % 180 != 0) {
                int n7 = width + n5;
                height = n7 - n5;
                n6 = n7 - height;
            }
            float n8;
            if (height * (n8 = n / n6) < n2) {
                n8 = n2 / height;
            }
            Matrix matrix = new Matrix();
            Label_0124: {
                switch (n3) {
                    case 1: {
                        switch (n4 % 360) {
                            default: {
                                break Label_0124;
                            }
                            case 0: {
                                matrix.postScale(n8, n8, 0.0f, 0.0f);
                                break Label_0124;
                            }
                            case 90: {
                                matrix.postTranslate((float)n6, 0.0f);
                                matrix.postRotate((float)n4, (float)n6, 0.0f);
                                matrix.postScale(n8, n8, 0.0f, 0.0f);
                                break Label_0124;
                            }
                            case 180: {
                                matrix.postTranslate((float)n6, (float)height);
                                matrix.postRotate((float)n4, (float)n6, (float)height);
                                matrix.postScale(n8, n8, 0.0f, 0.0f);
                                break Label_0124;
                            }
                            case 270: {
                                matrix.postTranslate(0.0f, (float)height);
                                matrix.postRotate((float)n4, 0.0f, (float)height);
                                matrix.postScale(n8, n8, 0.0f, 0.0f);
                                break Label_0124;
                            }
                        }
                        //break;
                    }
                    case 2: {
                        matrix.postTranslate((n - bitmap.getWidth()) / 2.0f, (n2 - bitmap.getHeight()) / 2.0f);
                        matrix.postRotate((float)n4, n / 2.0f, n2 / 2.0f);
                        matrix.postScale(n8, n8, n / 2.0f, n2 / 2.0f);
                        break;
                    }
                    case 3: {
                        switch (n4 % 360) {
                            default: {
                                break Label_0124;
                            }
                            case 0: {
                                matrix.postTranslate((float)(n - n6), (float)(n2 - height));
                                matrix.postScale(n8, n8, (float)n, (float)n2);
                                break Label_0124;
                            }
                            case 90: {
                                matrix.postRotate((float)n4, 0.0f, 0.0f);
                                matrix.postTranslate((float)n, (float)(n2 - height));
                                matrix.postScale(n8, n8, (float)n, (float)n2);
                                break Label_0124;
                            }
                            case 180: {
                                matrix.postRotate((float)n4, 0.0f, 0.0f);
                                matrix.postTranslate((float)n, (float)n2);
                                matrix.postScale(n8, n8, (float)n, (float)n2);
                                break Label_0124;
                            }
                            case 270: {
                                matrix.postRotate((float)n4, 0.0f, 0.0f);
                                matrix.postTranslate((float)(n - n6), (float)n2);
                                matrix.postScale(n8, n8, (float)n, (float)n2);
                                break Label_0124;
                            }
                        }
                        //break;
                    }
                }
            }
            bitmap2 = Bitmap.createBitmap(n, n2, bitmap$Config);
            Canvas canvas = new Canvas(bitmap2);
            canvas.setDrawFilter((DrawFilter)new PaintFlagsDrawFilter(0, 3));
            canvas.drawBitmap(bitmap, matrix, (Paint)null);
        }
        return bitmap2;
    }
    
    public static Bitmap CreateTensileBitmap(Bitmap bitmap, int n, int n2, int n3, Bitmap.Config bitmap$Config) {
        Bitmap bitmap2 = null;
        if (bitmap != null) {
            int width = bitmap.getWidth();
            int height;
            int n4 = height = bitmap.getHeight();
            int n5 = width;
            if (n3 % 180 != 0) {
                int n6 = width + n4;
                height = n6 - n4;
                n5 = n6 - height;
            }
            Matrix matrix = new Matrix();
            matrix.postTranslate((n - bitmap.getWidth()) / 2.0f, (n2 - bitmap.getHeight()) / 2.0f);
            matrix.postRotate((float)n3, n / 2.0f, n2 / 2.0f);
            matrix.postScale(n / n5, n2 / height, n / 2.0f, n2 / 2.0f);
            bitmap2 = Bitmap.createBitmap(n, n2, bitmap$Config);
            Canvas canvas = new Canvas(bitmap2);
            canvas.setDrawFilter((DrawFilter)new PaintFlagsDrawFilter(0, 3));
            canvas.drawBitmap(bitmap, matrix, (Paint)null);
        }
        return bitmap2;
    }
    
    public static MyRect MakeRect(float w, float h, float n) {
        MyRect myRect = new MyRect();
        myRect.m_w = w;
        myRect.m_h = w / n;
        if (myRect.m_h > h) {
            myRect.m_h = h;
            myRect.m_w = h * n;
        }
        myRect.m_x = (w - myRect.m_w) / 2.0f;
        myRect.m_y = (h - myRect.m_h) / 2.0f;
        return myRect;
    }
    
    public static Bitmap compressImage(Context context, int n, int inSampleSize) {
        BitmapFactory.Options bitmapFactory$Options = new BitmapFactory.Options();
        bitmapFactory$Options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), n, bitmapFactory$Options);
        if ((inSampleSize = bitmapFactory$Options.outWidth / inSampleSize) < 1) {
            inSampleSize = 1;
        }
        bitmapFactory$Options.inSampleSize = inSampleSize;
        bitmapFactory$Options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(context.getResources(), n, bitmapFactory$Options);
    }
    
    public static Bitmap compressImage(Context context, int n, int inSampleSize, int n2) {
        BitmapFactory.Options bitmapFactory$Options = new BitmapFactory.Options();
        bitmapFactory$Options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), n, bitmapFactory$Options);
        int n3 = bitmapFactory$Options.outWidth / inSampleSize;
        int n4 = bitmapFactory$Options.outHeight / n2;
        inSampleSize = n3;
        if (n3 < 1) {
            inSampleSize = 1;
        }
        if ((n2 = n4) < 1) {
            n2 = 1;
        }
        if (inSampleSize <= n2) {
            inSampleSize = n2;
        }
        bitmapFactory$Options.inSampleSize = inSampleSize;
        bitmapFactory$Options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(context.getResources(), n, bitmapFactory$Options);
    }
    
    public static Bitmap compressImage(String s, int inSampleSize) {
        BitmapFactory.Options bitmapFactory$Options = new BitmapFactory.Options();
        bitmapFactory$Options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(s, bitmapFactory$Options);
        if ((inSampleSize = bitmapFactory$Options.outWidth / inSampleSize) < 1) {
            inSampleSize = 1;
        }
        bitmapFactory$Options.inSampleSize = inSampleSize;
        bitmapFactory$Options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(s, bitmapFactory$Options);
    }
    
    public static Bitmap compressImage(String s, int inSampleSize, int n) {
        BitmapFactory.Options bitmapFactory$Options = new BitmapFactory.Options();
        bitmapFactory$Options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(s, bitmapFactory$Options);
        int n2 = bitmapFactory$Options.outWidth / inSampleSize;
        int n3 = bitmapFactory$Options.outHeight / n;
        inSampleSize = n2;
        if (n2 < 1) {
            inSampleSize = 1;
        }
        if ((n = n3) < 1) {
            n = 1;
        }
        if (inSampleSize <= n) {
            inSampleSize = n;
        }
        bitmapFactory$Options.inSampleSize = inSampleSize;
        bitmapFactory$Options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(s, bitmapFactory$Options);
    }
    
    private static int computeInitialSampleSize(BitmapFactory.Options bitmapFactory$Options, int n, int n2) {
        double n3 = bitmapFactory$Options.outWidth;
        double n4 = bitmapFactory$Options.outHeight;
        int n5;
        if (n2 == -1) {
            n5 = 1;
        }
        else {
            n5 = (int)Math.ceil(Math.sqrt(n3 * n4 / n2));
        }
        int n6;
        if (n == -1) {
            n6 = 128;
        }
        else {
            n6 = (int)Math.min(Math.floor(n3 / n), Math.floor(n4 / n));
        }
        if (n6 >= n5) {
            if (n2 == -1 && n == -1) {
                return 1;
            }
            if (n != -1) {
                return n6;
            }
        }
        return n5;
    }
    
    public static int computeSampleSize(BitmapFactory.Options bitmapFactory$Options, int i, int computeInitialSampleSize) {
        computeInitialSampleSize = computeInitialSampleSize(bitmapFactory$Options, i, computeInitialSampleSize);
        if (computeInitialSampleSize <= 8) {
            for (i = 1; i < computeInitialSampleSize; i <<= 1) {}
            return i;
        }
        return (computeInitialSampleSize + 7) / 8 * 8;
    }
    
    public static Bitmap getBitmapFromRes(Context context, int n, int n2, int n3) {
        BitmapFactory.Options bitmapFactory$Options2 = null;
        //BitmapFactory.Options bitmapFactory$Options = bitmapFactory$Options2 = null;
        if (n2 > 0) {
            //bitmapFactory$Options2 = bitmapFactory$Options;
            if (n3 > 0) {
                bitmapFactory$Options2 = new BitmapFactory.Options();
                bitmapFactory$Options2.inJustDecodeBounds = true;
                BitmapFactory.decodeResource(context.getResources(), n, bitmapFactory$Options2);
                bitmapFactory$Options2.inSampleSize = computeSampleSize(bitmapFactory$Options2, Math.min(n2, n3), n2 * n3);
                bitmapFactory$Options2.inJustDecodeBounds = false;
                bitmapFactory$Options2.inInputShareable = true;
                bitmapFactory$Options2.inPurgeable = true;
            }
        }
        try {
            return BitmapFactory.decodeResource(context.getResources(), n, bitmapFactory$Options2);
        }
        catch (OutOfMemoryError outOfMemoryError) {
            outOfMemoryError.printStackTrace();
            return null;
        }
    }
    
    public static int getBitmapRotate(String s) {
        int n = 0;
        try {
            int attributeInt = new ExifInterface(s).getAttributeInt("Orientation", -1);
            if (attributeInt == 6) {
                n = 90;
            }
            else {
                if (attributeInt == 3) {
                    return 180;
                }
                if (attributeInt == 8) {
                    return 270;
                }
            }
            return n;
        }
        catch (IOException ex) {
            ex.printStackTrace();
            return 0;
        }
    }
    
    public static class MyRect
    {
        public float m_h;
        public float m_w;
        public float m_x;
        public float m_y;
    }
}
