package com.gk.zhangzhongbao.ui.app;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.preference.ListPreference;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;



public class Util
{
    public static String FROM = "from_which_acticity";
    public static int FROM_AddCurtainActivity = 3;
    public static int FROM_AddLightActivity = 1;
    public static int FROM_SetHostInfoActivity = 2;
    public static String HOST_NAME = "host_name";
    public static String HOST_PASS = "host_pass";
    public static String HOST_SN = "host_sn";
    public static int MSG_SNCODE = 10;
    public static String SP_NAME;
    static MediaPlayer clickMp3;
    public static SharedPreferences.Editor editor;
    public static boolean isBeepSound;
    public static boolean isRightAppPassword;
    public static boolean isSound;
    public static boolean isStbSound;
    public static boolean isVibrator;
    public static String lan;
    static MediaPlayer lightMp3;
    public static ListPreference listSp;
    static Dialog loadingDialog;
    static Toast mToast;
    public static String packageName;
    static ProgressDialog progressDlg;
    public static int sh;
    public static SharedPreferences sp;
    public static int sw;
    public static Util util;
    static Vibrator vib;
    
    static {
        Util.SP_NAME = "";
        Util.isRightAppPassword = true;
        Util.lightMp3 = null;
        Util.clickMp3 = null;
        Util.vib = null;
        Util.packageName = "";
    }
    
    public Util(Context context) {
        Util.packageName = context.getPackageName();
        Util.SP_NAME = Util.packageName;
        Util.sp = context.getSharedPreferences(Util.SP_NAME, 0);
        Util.editor = Util.sp.edit();
        Util.isSound = getBooleanValue("click_sound");
        Util.isStbSound = getBooleanValue("click_stb_sound");
        Util.isVibrator = getBooleanValue("click_vibrator");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager)context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        Util.sw = displayMetrics.widthPixels;
        Util.sh = displayMetrics.heightPixels;
        Util.lan = Locale.getDefault().getLanguage();
    }
    
    public static void alertDlg(Context context, String title, String message, View view, final Runnable runnable) {
        AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder(context);
        alertDialog$Builder.setTitle(title);
        alertDialog$Builder.setMessage(message);
        alertDialog$Builder.setView(view);
        alertDialog$Builder.setPositiveButton(context.getString(2131296285), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int n) {
                runnable.run();
            }
        });
        alertDialog$Builder.setNegativeButton(context.getString(2131296286), null);
        alertDialog$Builder.show();
    }
    
    public static void alertDlg(Context context, String title, String message, final Runnable runnable) {
        AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder(context);
        alertDialog$Builder.setTitle(title);
        alertDialog$Builder.setMessage(message);
        alertDialog$Builder.setPositiveButton(context.getString(2131296285), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int n) {
                runnable.run();
            }
        });
        alertDialog$Builder.setNegativeButton(context.getString(2131296286), null);
        alertDialog$Builder.show();
    }
    
    public static int boolean2Int(boolean b) {
        if (b) {
            return 1;
        }
        return 0;
    }
    
    public static void clearData(String s) {
        SharedPreferences.Editor edit = Util.sp.edit();
        int int1 = Util.sp.getInt(String.valueOf(s) + "_size", 0);
        edit.putInt(String.valueOf(s) + "_size", 0);
        for (int i = 0; i < int1; ++i) {
            edit.remove(String.valueOf(s) + "Status_" + i);
        }
        edit.commit();
    }
    
    public static void clickBeepSound(Context context) {
        if (Util.isStbSound) {
            if (Util.clickMp3 == null) {
                Util.clickMp3 = MediaPlayer.create(context, 2131034115);
            }
            Util.clickMp3.start();
        }
    }
    
    public static void clickSound(Context context) {
        if (Util.isSound) {
            if (Util.lightMp3 == null) {
                Util.lightMp3 = MediaPlayer.create(context, 2131034116);
            }
            Util.lightMp3.start();
        }
    }
    
    public static String codeToSn(int i) {
        String s = Integer.toHexString(i);
        Log.e("code", new StringBuilder(String.valueOf(i)).toString());
        Log.e("code", s);
        int length;
        for (length = s.length(), i = 0; i < 4 - length; ++i) {
            s = "0" + s;
        }
        String string = String.valueOf(s.substring(2, s.length())) + s.substring(0, 2) + "00";
        Log.e("code", string);
        return string;
    }
    
    public static void createLoadingDialog(Context context, String text, TextView textView, final Runnable runnable) {
        View inflate = LayoutInflater.from(context).inflate(2130903078, (ViewGroup)null);
        LinearLayout linearLayout = (LinearLayout)inflate.findViewById(2131231014);
        ImageView imageView = (ImageView)inflate.findViewById(2131231015);
        TextView textView2 = (TextView)inflate.findViewById(2131231016);
        //while (true) {
            if (textView != null) {
                imageView.startAnimation(AnimationUtils.loadAnimation(context, 2130968576));
                textView2.setText(text);
                (Util.loadingDialog = new Dialog(context, 2131361810)).setContentView((View)linearLayout, new LinearLayout.LayoutParams(dip(context, 300.0f), -1));
                Util.loadingDialog.setCanceledOnTouchOutside(false);
                Util.loadingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialogInterface) {
                        if (runnable != null) {
                            runnable.run();
                        }
                    }
                });
                Util.loadingDialog.show();
                return;
            }
            //continue;
        //}
    }
    
    public static int dip(Context context, float n) {
        return (int)(n * context.getResources().getDisplayMetrics().density / 1.5);
    }
    
    public static boolean getBooleanValue(String s) {
        return Util.sp.getBoolean(s, true);
    }
    
    public static boolean getBooleanValue(String s, boolean b) {
        return Util.sp.getBoolean(s, b);
    }
    
    public static int getIntValue(String s) {
        return Util.sp.getInt(s, 0);
    }
    
    public static int getIntValue(String s, int n) {
        return Util.sp.getInt(s, n);
    }
    
    public static String getResString(Context context, int n) {
        return context.getResources().getString(n);
    }
    
    public static int getResourceId(Context context, String s) {
        return getResourceId(context, s, "drawable", context.getPackageName());
    }
    
    public static int getResourceId(Context context, String s, String s2, String s3) {
        PackageManager packageManager = context.getPackageManager();
        try {
            return packageManager.getResourcesForApplication(s3).getIdentifier(s, s2, s3);
        }
        catch (PackageManager.NameNotFoundException ex) {
            ex.printStackTrace();
            return 0;
        }
    }
    
    public static int getScreenHeight(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager)context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }
    
    public static int getScreenWidth(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager)context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }
    
    public static int getStatusBarHeight(Context context) {
        Rect rect = new Rect();
        ((Activity)context).getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        return rect.top;
    }
    
    public static String getStringValue(String s) {
        return Util.sp.getString(s, "000000");
    }
    
    public static String getStringValue(String s, String s2) {
        return Util.sp.getString(s, s2);
    }
    
    public static boolean isAppOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager)context.getApplicationContext().getSystemService("activity");
        String packageName = context.getPackageName();
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (ActivityManager.RunningAppProcessInfo activityManager$RunningAppProcessInfo : runningAppProcesses) {
                if (activityManager$RunningAppProcessInfo.processName.equals(packageName) && activityManager$RunningAppProcessInfo.importance == 100) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public static List<String> loadArray(SharedPreferences sharedPreferences, String s) {
        ArrayList<String> list = new ArrayList<String>();
        list.clear();
        for (int int1 = sharedPreferences.getInt(String.valueOf(s) + "_size", 0), i = 0; i < int1; ++i) {
            list.add(sharedPreferences.getString(String.valueOf(s) + "Status_" + i, (String)null));
        }
        return list;
    }
    
    public static List<String> loadArray(String s) {
        ArrayList<String> list = new ArrayList<String>();
        list.clear();
        for (int int1 = Util.sp.getInt(String.valueOf(s) + "_size", 0), i = 0; i < int1; ++i) {
            list.add(Util.sp.getString(String.valueOf(s) + "Status_" + i, (String)null));
        }
        return list;
    }
    
    public static List<Boolean> loadBooleanArray(String s) {
        ArrayList<Boolean> list = new ArrayList<Boolean>();
        list.clear();
        for (int int1 = Util.sp.getInt(String.valueOf(s) + "_size", 0), i = 0; i < int1; ++i) {
            list.add(Util.sp.getBoolean(String.valueOf(s) + "Status_" + i, false));
        }
        return list;
    }
    
    public static void loadingDlgDismiss() {
        if (Util.loadingDialog != null) {
            Util.loadingDialog.dismiss();
            Util.loadingDialog = null;
        }
    }
    
    public static int nextInt(int n, int n2) {
        return Math.abs(new Random().nextInt()) % (n2 - n + 1) + n;
    }
    
    public static void noticeSound(Context context) {
        if (!Util.isSound) {
            return;
        }
        Uri defaultUri = RingtoneManager.getDefaultUri(2);
        while (true) {
            try {
                if (Util.lightMp3 == null) {
                    Util.lightMp3 = new MediaPlayer();
                }
                Util.lightMp3.setDataSource(context, defaultUri);
                if (((AudioManager)context.getSystemService("audio")).getStreamVolume(4) != 0) {
                    Util.lightMp3.setAudioStreamType(4);
                    Util.lightMp3.setLooping(false);
                    Util.lightMp3.prepare();
                }
                if (Util.lightMp3 != null) {
                    Util.lightMp3.start();
                }
            }
            catch (IOException ex) {
                ex.printStackTrace();
                continue;
            }
            break;
        }
    }
    
    public static void noticeSoundStop() {
        if (Util.lightMp3 != null) {
            Util.lightMp3.stop();
            Util.lightMp3 = null;
        }
    }
    
    public static void progressDlg(Context context) {
        (Util.progressDlg = new ProgressDialog(context)).setTitle(context.getString(2131296288));
        Util.progressDlg.show();
    }
    
    public static void progressDlg(Context context, View view) {
        (Util.progressDlg = new ProgressDialog(context)).setView(view);
        Util.progressDlg.show();
    }
    
    public static void progressDlg(Context context, String title) {
        (Util.progressDlg = new ProgressDialog(context)).setTitle(title);
        Util.progressDlg.show();
    }
    
    public static void progressDlgCancle() {
        if (Util.progressDlg != null) {
            Util.progressDlg.dismiss();
            Util.progressDlg = null;
        }
    }
    
    public static Bitmap readBitMap(Context context, int n) {
        BitmapFactory.Options bitmapFactory$Options = new BitmapFactory.Options();
        bitmapFactory$Options.inPreferredConfig = Bitmap.Config.RGB_565;
        bitmapFactory$Options.inPurgeable = true;
        bitmapFactory$Options.inInputShareable = true;
        return BitmapFactory.decodeStream(context.getResources().openRawResource(n), (Rect)null, bitmapFactory$Options);
    }
    
    public static BitmapDrawable readDrawable(Context context, int n) {
        BitmapFactory.Options bitmapFactory$Options = new BitmapFactory.Options();
        bitmapFactory$Options.inPreferredConfig = Bitmap.Config.RGB_565;
        bitmapFactory$Options.inPurgeable = true;
        bitmapFactory$Options.inInputShareable = true;
        return new BitmapDrawable(context.getResources(), BitmapFactory.decodeStream(context.getResources().openRawResource(n), (Rect)null, bitmapFactory$Options));
    }
    
    public static boolean saveArray(SharedPreferences sharedPreferences, List<String> list, String s) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt(String.valueOf(s) + "_size", list.size());
        for (int i = 0; i < list.size(); ++i) {
            edit.remove(String.valueOf(s) + "Status_" + i);
            edit.putString(String.valueOf(s) + "Status_" + i, (String)list.get(i));
        }
        return edit.commit();
    }
    
    public static boolean saveArray(List<String> list, String s) {
        SharedPreferences.Editor edit = Util.sp.edit();
        edit.putInt(String.valueOf(s) + "_size", list.size());
        for (int i = 0; i < list.size(); ++i) {
            edit.remove(String.valueOf(s) + "Status_" + i);
            edit.putString(String.valueOf(s) + "Status_" + i, (String)list.get(i));
        }
        return edit.commit();
    }
    
    public static boolean saveBooleanArray(List<Boolean> list, String s) {
        SharedPreferences.Editor edit = Util.sp.edit();
        edit.putInt(String.valueOf(s) + "_size", list.size());
        for (int i = 0; i < list.size(); ++i) {
            edit.remove(String.valueOf(s) + "Status_" + i);
            edit.putBoolean(String.valueOf(s) + "Status_" + i, (boolean)list.get(i));
        }
        return edit.commit();
    }
    
    public static void saveValue(String s, int n) {
        Util.editor.putInt(s, n);
        Util.editor.commit();
    }
    
    public static void saveValue(String s, Boolean b) {
        Util.editor.putBoolean(s, (boolean)b);
        Util.editor.commit();
    }
    
    public static void saveValue(String s, String s2) {
        Util.editor.putString(s, s2);
        Util.editor.commit();
    }
    
    public static Button setButton(Activity activity, int n, View.OnClickListener onClickListener) {
        Button button = (Button)activity.findViewById(n);
        button.setOnClickListener(onClickListener);
        return button;
    }
    
    public static Button setButton(View view, int n, View.OnClickListener onClickListener) {
        Button button = (Button)view.findViewById(n);
        button.setOnClickListener(onClickListener);
        return button;
    }
    
    public static void setButton(Activity activity, Button button, int n, View.OnClickListener onClickListener) {
        ((Button)activity.findViewById(n)).setOnClickListener(onClickListener);
    }
    
    public static TextView setTextView(Activity activity, int n, View.OnClickListener onClickListener) {
        Button button = (Button)activity.findViewById(n);
        ((TextView)button).setOnClickListener(onClickListener);
        return (TextView)button;
    }
    
    public static TextView setTextView(View view, int n, View.OnClickListener onClickListener) {
        Button button = (Button)view.findViewById(n);
        ((TextView)button).setOnClickListener(onClickListener);
        return (TextView)button;
    }
    
    public static void setTextView(Activity activity, TextView textView, int n, View.OnClickListener onClickListener) {
        ((TextView)activity.findViewById(n)).setOnClickListener(onClickListener);
    }
    
    public static View setView(Activity activity, int n, View.OnClickListener onClickListener) {
        View viewById = activity.findViewById(n);
        if (onClickListener != null) {
            viewById.setOnClickListener(onClickListener);
        }
        return viewById;
    }
    
    public static View setView(View viewById, int n, View.OnClickListener onClickListener) {
        viewById = viewById.findViewById(n);
        if (onClickListener != null) {
            viewById.setOnClickListener(onClickListener);
        }
        return viewById;
    }
    
    public static void setView(Activity activity, View view, int n, View.OnClickListener onClickListener) {
        View viewById = activity.findViewById(n);
        if (onClickListener != null) {
            viewById.setOnClickListener(onClickListener);
        }
    }
    
    public static int snToCode(String s) {
        int int1;
        int n = int1 = 0;
        if (s != null) {
            int1 = n;
            if (!s.equals("")) {
                int1 = Integer.parseInt(String.valueOf(s.substring(2, 4)) + s.substring(0, 2), 16);
            }
        }
        return int1;
    }
    
    public static String time(String s) {
        return new SimpleDateFormat(s).format(new Date(System.currentTimeMillis()));
    }
    
    public static void toast(Context context, String s) {
        if (Util.mToast != null) {
            Util.mToast.cancel();
        }
        (Util.mToast = Toast.makeText(context, s, 0)).show();
    }
    
    public static void vibrator(Context context) {
        if (Util.isVibrator) {
            if (Util.vib == null) {
                Util.vib = (Vibrator)context.getSystemService("vibrator");
            }
            Util.vib.vibrate(50L);
        }
    }
    
    public static void vibrator(Context context, long n) {
        if (Util.isVibrator) {
            if (Util.vib == null) {
                Util.vib = (Vibrator)context.getSystemService("vibrator");
            }
            Util.vib.vibrate(n);
        }
    }
    
    public static void vibratorCancle() {
        if (Util.vib != null) {
            Util.vib.cancel();
            Util.vib = null;
        }
    }
}
