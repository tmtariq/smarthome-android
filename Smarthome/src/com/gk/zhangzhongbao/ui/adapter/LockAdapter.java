package com.gk.zhangzhongbao.ui.adapter;

import android.content.*;
import java.util.*;
import com.gk.wifictr.net.data.*;
import com.gk.zhangzhongbao.ui.view.*;
import android.view.*;
import android.widget.*;
import com.gk.wifictr.net.*;
import com.gk.zhangzhongbao.ui.app.*;
import com.gk.wifictr.net.command.station.*;

public class LockAdapter extends BaseAdapter
{
    Context context;
    List<SubDevice> list;
    
    public LockAdapter(final Context context, final List<SubDevice> list) {
        this.context = context;
        this.list = list;
    }
    
    public int getCount() {
        return this.list.size();
    }
    
    public Object getItem(final int n) {
        return this.list.get(n);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int n, View inflate, final ViewGroup viewGroup) {
        inflate = View.inflate(this.context, 2130903079, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131231017);
        final SwitchView switchView = (SwitchView)inflate.findViewById(2131231018);
        final SubDevice subDevice = this.list.get(n);
        switchView.setFocusable(true);
        textView.setText((CharSequence)subDevice.devName);
        textView.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(final View view) {
                final EditText editText = new EditText(LockAdapter.this.context);
                Util.alertDlg(LockAdapter.this.context, "\u4fee\u6539\u9501\u540d", null, (View)editText, new Runnable() {
                    @Override
                    public void run() {
                        final String string = editText.getText().toString();
                        if (string != null && !string.equals("")) {
                            textView.setText((CharSequence)string);
                            subDevice.devName = string;
                            new BroadcastHelper().writeData(new Out_25H().make(subDevice));
                        }
                    }
                });
                return true;
            }
        });
        switchView.setOnSwitchListener((SwitchView.OnSwitchListener)new SwitchView.OnSwitchListener() {
            @Override
            public void onCheck(final SwitchView switchView, final boolean b, final boolean b2) {
                Util.vibrator(LockAdapter.this.context);
                Util.clickSound(LockAdapter.this.context);
                new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(subDevice.devNo, subDevice.devName, subDevice.devSN, (byte)64, new boolean[] { true }), new boolean[] { true }));
            }
        });
        return inflate;
    }
}
