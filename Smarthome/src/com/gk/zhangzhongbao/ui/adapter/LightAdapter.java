package com.gk.zhangzhongbao.ui.adapter;

import java.util.*;
import com.gk.zhangzhongbao.ui.bean.*;
import android.app.*;
import android.content.*;
import com.gk.wifictr.net.*;
import android.os.*;
import com.gk.zhangzhongbao.ui.activity.*;
import android.view.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.widget.*;
import android.util.*;
import com.gk.wifictr.net.command.station.*;
import com.gk.wifictr.net.data.*;

public class LightAdapter extends BaseAdapter
{
    boolean clicked;
    int[] imgsOff;
    int[] imgsOn;
    int[] indexs;
    Context mContext;
    List<LightItem> mList;
    Callback mListener;
    int sh;
    int sw;
    
    public LightAdapter(final Context mContext, final List<LightItem> mList) {
        this.indexs = new int[] { 0, 1, 2 };
        this.imgsOn = new int[] { 2130838147, 2130838149, 2130838151 };
        this.imgsOff = new int[] { 2130838146, 2130838148, 2130838150 };
        this.mList = mList;
        this.mContext = mContext;
    }
    
    public int getCount() {
        return this.mList.size();
    }
    
    public Object getItem(final int n) {
        return this.mList.get(n);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(int i, final View view, final ViewGroup viewGroup) {
        final LightItem lightItem = this.mList.get(i);
        if (lightItem.getDevType() == 98 || lightItem.getDevType() == 99) {
            final View inflate = View.inflate(this.mContext, 2130903074, (ViewGroup)null);
            final TextView textView = (TextView)inflate.findViewById(2131231000);
            final Button button = (Button)inflate.findViewById(2131230999);
            textView.setText((CharSequence)lightItem.getName());
            textView.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(final View view) {
                    final AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder(LightAdapter.this.mContext);
                    alertDialog$Builder.setTitle((CharSequence)LightAdapter.this.mContext.getString(2131296291));
                    final EditText view2 = new EditText(LightAdapter.this.mContext);
                    view2.setSingleLine();
                    alertDialog$Builder.setView((View)view2);
                    alertDialog$Builder.setPositiveButton((CharSequence)LightAdapter.this.mContext.getString(2131296306), new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialogInterface, final int n) {
                            final String string = view2.getText().toString();
                            if (string == null || string.equals("")) {
                                return;
                            }
                            new BroadcastHelper().writeData(new Out_25H().make(lightItem.getDevNo(), string, lightItem.getSnCode(), (byte)lightItem.getDevType(), lightItem.getCount()));
                        }
                    });
                    alertDialog$Builder.setNegativeButton((CharSequence)LightAdapter.this.mContext.getString(2131296286), null);
                    alertDialog$Builder.show();
                    return true;
                }
            });
            button.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(final View view) {
                    lightItem.actionCode = 1;
                    final Message message = new Message();
                    message.what = 5;
                    message.obj = lightItem;
                    LightActivity.handler.sendMessage(message);
                    return true;
                }
            });
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(final View view) {
                    Util.vibrator(LightAdapter.this.mContext, 50L);
                    Util.clickSound(LightAdapter.this.mContext);
                    new BroadcastHelper().writeData(new Out_27H().make((byte)1, lightItem.getSnCode(), lightItem.getDevType()));
                }
            });
            return inflate;
        }
        final View inflate2 = View.inflate(this.mContext, 2130903076, (ViewGroup)null);
        final ImageView lightSwitch = (ImageView)inflate2.findViewById(2131231003);
        final TextView textView2 = (TextView)inflate2.findViewById(2131231005);
        final LinearLayout linearLayout = (LinearLayout)inflate2.findViewById(2131231006);
        final ImageView imageView = (ImageView)inflate2.findViewById(2131231007);
        final LinearLayout linearLayout2 = (LinearLayout)inflate2.findViewById(2131231008);
        final ImageView imageView2 = (ImageView)inflate2.findViewById(2131231009);
        final ImageView imageView3 = (ImageView)inflate2.findViewById(2131231010);
        final ImageView imageView4 = (ImageView)inflate2.findViewById(2131231011);
        textView2.setText((CharSequence)lightItem.getName());
        lightItem.lights[0] = imageView2;
        lightItem.lights[1] = imageView3;
        lightItem.lights[2] = imageView4;
        lightItem.lightSwitch = lightSwitch;
        if (lightItem.longClickable) {
            textView2.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(final View view) {
                    final AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder(LightAdapter.this.mContext);
                    alertDialog$Builder.setTitle((CharSequence)LightAdapter.this.mContext.getString(2131296287));
                    final EditText view2 = new EditText(LightAdapter.this.mContext);
                    view2.setSingleLine();
                    alertDialog$Builder.setView((View)view2);
                    alertDialog$Builder.setPositiveButton((CharSequence)LightAdapter.this.mContext.getString(2131296285), new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialogInterface, final int n) {
                            final String string = view2.getText().toString();
                            if (string == null || string.equals("")) {
                                return;
                            }
                            new BroadcastHelper().writeData(new Out_25H().make(lightItem.getDevNo(), string, lightItem.getSnCode(), (byte)16, lightItem.getCount()));
                        }
                    });
                    alertDialog$Builder.setNegativeButton((CharSequence)LightAdapter.this.mContext.getString(2131296286), null);
                    alertDialog$Builder.show();
                    return true;
                }
            });
            lightSwitch.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(final View view) {
                    final AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder(LightAdapter.this.mContext);
                    alertDialog$Builder.setTitle((CharSequence)LightAdapter.this.mContext.getString(2131296290));
                    alertDialog$Builder.setPositiveButton((CharSequence)LightAdapter.this.mContext.getString(2131296285), new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialogInterface, final int n) {
                            Log.e("\u8bbe\u5907\u53f7", new StringBuilder(String.valueOf(lightItem.getDevNo())).toString());
                            Log.e("\u8bbe\u5907sn", new StringBuilder(String.valueOf(lightItem.getSnCode())).toString());
                            new BroadcastHelper().writeData(new Out_24H().make(lightItem.getDevNo(), lightItem.getSnCode(), (byte)16));
                        }
                    });
                    alertDialog$Builder.setNegativeButton((CharSequence)LightAdapter.this.mContext.getString(2131296286), null);
                    alertDialog$Builder.show();
                    return true;
                }
            });
        }
        if (lightItem.getCount() == 1) {
            linearLayout.setVisibility(0);
            lightItem.setLightOn(lightItem.state_one[0]);
            if (lightItem.isLightOn()) {
                lightSwitch.setImageResource(2130838158);
                imageView.setImageResource(2130838018);
            }
            else {
                lightSwitch.setImageResource(2130838157);
                imageView.setImageResource(2130838017);
            }
            lightSwitch.setOnClickListener(new View.OnClickListener() {
                public void onClick(final View view) {
                    Util.vibrator(LightAdapter.this.mContext, 50L);
                    Util.clickSound(LightAdapter.this.mContext);
                    if (lightItem.isLightOn()) {
                        lightSwitch.setImageResource(2130838157);
                        lightItem.setLightOn(false);
                        imageView.setImageResource(2130838017);
                        lightItem.state_one[0] = false;
                    }
                    else {
                        lightSwitch.setImageResource(2130838158);
                        lightItem.setLightOn(true);
                        imageView.setImageResource(2130838018);
                        lightItem.state_one[0] = true;
                    }
                    new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(lightItem.getDevNo(), lightItem.getName(), lightItem.getSnCode(), (byte)16, lightItem.state_one), lightItem.state_one));
                    LightActivity.handler.sendEmptyMessage(3);
                }
            });
        }
        else if (lightItem.getCount() == 3) {
            textView2.setBackgroundResource(2130838152);
            linearLayout2.setVisibility(0);
            if (lightItem.allLightOff()) {
                lightItem.setLightOn(false);
            }
            else {
                lightItem.setLightOn(true);
            }
            if (lightItem.isLightOn()) {
                lightSwitch.setImageResource(2130838158);
            }
            else {
                lightSwitch.setImageResource(2130838157);
            }
            lightSwitch.setOnClickListener(new View.OnClickListener() {
                public void onClick(final View view) {
                    Util.vibrator(LightAdapter.this.mContext, 50L);
                    Util.clickSound(LightAdapter.this.mContext);
                    if (lightItem.isLightOn()) {
                        lightItem.setAllLightOff();
                    }
                    else {
                        lightItem.setAllLightOn();
                    }
                    LightActivity.handler.sendEmptyMessage(3);
                }
            });
            final int[] indexs = this.indexs;
            int length;
            for (length = indexs.length, i = 0; i < length; ++i) {
            	final  int n = indexs[i];
                if (lightItem.status_three[n]) {
                    lightItem.lights[n].setImageResource(this.imgsOn[n]);
                }
                else {
                    lightItem.lights[n].setImageResource(this.imgsOff[n]);
                }
                lightItem.lights[n].setOnClickListener(new View.OnClickListener() {
                    public void onClick(final View view) {
                        Util.vibrator(LightAdapter.this.mContext);
                        Util.clickSound(LightAdapter.this.mContext);
                        if (lightItem.status_three[n]) {
                            lightItem.lights[n].setImageResource(LightAdapter.this.imgsOff[n]);
                            lightItem.status_three[n] = false;
                            if (lightItem.allLightOff()) {
                                lightItem.setLightOn(false);
                                lightSwitch.setImageResource(2130838157);
                            }
                        }
                        else {
                            lightItem.lights[n].setImageResource(LightAdapter.this.imgsOn[n]);
                            lightItem.status_three[n] = true;
                            if (!lightItem.isLightOn()) {
                                lightItem.setLightOn(true);
                                lightSwitch.setImageResource(2130838158);
                            }
                        }
                        new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(lightItem.getDevNo(), lightItem.getName(), lightItem.getSnCode(), (byte)16, lightItem.status_three), lightItem.status_three));
                        LightActivity.handler.sendEmptyMessage(3);
                    }
                });
            }
        }
        return inflate2;
    }
    
    public void setCallback(final Callback mListener) {
        this.mListener = mListener;
    }
    
    public interface Callback
    {
        void getItem(LightItem p0);
    }
}
