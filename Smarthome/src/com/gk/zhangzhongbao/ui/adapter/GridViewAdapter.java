package com.gk.zhangzhongbao.ui.adapter;

import android.content.*;
import com.gk.zhangzhongbao.ui.bean.*;
import java.util.*;
import android.view.*;
import android.widget.*;
import com.gk.zhangzhongbao.ui.app.*;

public class GridViewAdapter extends BaseAdapter
{
    Context mContext;
    List<Channel> mList;
    
    public GridViewAdapter(Context mContext) {
        this.mList = new ArrayList<Channel>();
        this.mContext = mContext;
    }
    
    public GridViewAdapter(Context mContext, List<Channel> mList) {
        this.mList = mList == null ? new ArrayList<Channel>() : mList;
        //this.mList = mList;
        this.mContext = mContext;
    }
    
    public int getCount() {
        return this.mList.size();
    }
    
    public Channel getItem(int n) {
        return this.mList.get(n);
    }
    
    public long getItemId(int n) {
        return n;
    }
    
    public View getView(int n, View inflate, ViewGroup viewGroup) {
        Channel channel = this.mList.get(n);
        ViewHolder tag;
        if (inflate == null) {
            tag = new ViewHolder();
            inflate = View.inflate(this.mContext, 2130903066, (ViewGroup)null);
            tag.imgv = (ImageView)inflate.findViewById(2131230888);
            tag.txtv = (TextView)inflate.findViewById(2131230889);
            inflate.setTag((Object)tag);
        }
        else {
            tag = (ViewHolder)inflate.getTag();
        }
        int resourceId = Util.getResourceId(this.mContext, channel.getImgResName());
        if (resourceId > 0) {
            tag.imgv.setImageResource(resourceId);
        }
        tag.txtv.setText((CharSequence)this.mList.get(n).getName());
        return inflate;
    }
    
    public void setList(List<Channel> mList) {
        this.mList = mList;
    }
    
    class ViewHolder
    {
        ImageView imgv;
        TextView txtv;
    }
}
