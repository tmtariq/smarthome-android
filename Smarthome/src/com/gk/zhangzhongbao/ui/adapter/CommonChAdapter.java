package com.gk.zhangzhongbao.ui.adapter;

import android.content.*;
import java.util.*;
import android.view.*;
import android.widget.*;

public class CommonChAdapter extends BaseAdapter
{
    Context mContext;
    List<String> mList;
    
    public CommonChAdapter(final Context mContext, final List<String> mList) {
        this.mList = mList;
        this.mContext = mContext;
    }
    
    public int getCount() {
        return this.mList.size();
    }
    
    public Object getItem(final int n) {
        return this.mList.get(n);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int n, View inflate, final ViewGroup viewGroup) {
        inflate = View.inflate(this.mContext, 2130903054, (ViewGroup)null);
        if (n % 2 != 0) {
            inflate.setBackgroundResource(2130838164);
        }
        ((TextView)inflate.findViewById(2131230850)).setText((CharSequence)this.mList.get(n));
        return inflate;
    }
}
