package com.gk.zhangzhongbao.ui.adapter;

import java.util.*;
import android.support.v4.app.*;

public class MyFragmentPagerAdapter extends FragmentPagerAdapter
{
    private ArrayList<Fragment> fragmentsList;
    
    public MyFragmentPagerAdapter(final FragmentManager fragmentManager) {
        super(fragmentManager);
    }
    
    public MyFragmentPagerAdapter(final FragmentManager fragmentManager, final ArrayList<Fragment> fragmentsList) {
        super(fragmentManager);
        this.fragmentsList = fragmentsList;
    }
    
    @Override
    public int getCount() {
        return this.fragmentsList.size();
    }
    
    @Override
    public Fragment getItem(final int n) {
        return this.fragmentsList.get(n);
    }
    
    @Override
    public int getItemPosition(final Object o) {
        return super.getItemPosition(o);
    }
}
