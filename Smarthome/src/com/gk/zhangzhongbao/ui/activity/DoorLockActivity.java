package com.gk.zhangzhongbao.ui.activity;

import com.gk.zhangzhongbao.ui.view.*;
import android.widget.*;
import com.gk.zhangzhongbao.ui.app.*;
import com.gk.wifictr.net.data.*;
import android.view.*;
import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.command.station.*;
import java.util.*;
import android.content.*;
import com.gk.wifictr.net.*;
import android.os.*;

public class DoorLockActivity extends BaseViewActivity implements View.OnClickListener
{
    public static final int MSG_DELETE = 2;
    public static Handler handler;
    private String SN;
    private Context context;
    private List<DoorLock> list;
    private LinearLayout llLayout;
    
    static {
        DoorLockActivity.handler = null;
    }
    
    public DoorLockActivity() {
        this.list = new ArrayList<DoorLock>();
        this.context = (Context)this;
    }
    
    private void getDevices() {
        final Iterator<SubDevice> iterator = DataBase.db.getLockList(this.SN).iterator();
        while (iterator.hasNext()) {
            this.list.add(new DoorLock(this.context, iterator.next()));
        }
        final Iterator<DoorLock> iterator2 = this.list.iterator();
        while (iterator2.hasNext()) {
            this.llLayout.addView((View)iterator2.next());
        }
    }
    
    protected void decodeData(final ReadData readData) {
        super.decodeData(readData);
        if (readData.type == 19) {
            final ArrayList<SubDevice> decode = new In_13H().decode(readData);
            if (decode.size() > 0) {
                final ArrayList<DoorLock> list = new ArrayList<DoorLock>();
                for (final DoorLock doorLock : this.list) {
                    int n = 0;
                    for (final SubDevice subDevice : decode) {
                        if (subDevice.devType == 64 && subDevice.devSN.equals(doorLock.subDevice.devSN) && subDevice.devType == doorLock.subDevice.devType) {
                            n = 1;
                            break;
                        }
                    }
                    if (n == 0) {
                        list.add(doorLock);
                        this.llLayout.removeView((View)doorLock);
                    }
                }
                final Iterator<DoorLock> iterator3 = list.iterator();
                while (iterator3.hasNext()) {
                    DataBase.db.deleteData("lock_table", "lock_sn", iterator3.next().subDevice.devSN, this.SN);
                }
                this.list.removeAll(list);
            }
            for (final SubDevice device : decode) {
                int n2 = 0;
                if (device.devType == 64) {
                    for (final DoorLock doorLock2 : this.list) {
                        if (device.devSN.equals(doorLock2.subDevice.devSN) && device.devType == doorLock2.subDevice.devType) {
                            n2 = 1;
                            if (doorLock2.subDevice.devNo != device.devNo || !doorLock2.subDevice.devName.equals(device.devName)) {
                                DataBase.db.updateLock(device, this.SN);
                            }
                            doorLock2.setDevice(device);
                        }
                    }
                    if (n2 != 0) {
                        continue;
                    }
                    final DoorLock doorLock3 = new DoorLock(this.context, device);
                    this.llLayout.addView((View)doorLock3);
                    this.list.add(doorLock3);
                    DataBase.db.insertLock(device, this.SN);
                }
            }
        }
    }
    
    public void onClick(final View view) {
        if (view == this.btnRight) {
            final Intent intent = new Intent(this.context, (Class)AddLockActivity.class);
            intent.putExtra("which_activity", 1);
            this.startActivity(intent);
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        DoorLockActivity.handler = new LockHandler(Looper.myLooper());
        this.title.setText((CharSequence)this.getString(2131296312));
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(2130837506);
        this.btnRight.setOnClickListener((View.OnClickListener)this);
        this.setCenterView(2130903059);
        this.list = new ArrayList<DoorLock>();
        this.llLayout = (LinearLayout)this.findViewById(2131230861);
        this.SN = ConnectService.defWifi.SN;
        this.getDevices();
        this.linkState();
        this.reflash();
    }
    
    @Override
    protected void stateChange() {
        super.stateChange();
        this.linkState();
    }
    
    protected class LockHandler extends Handler
    {
        Looper looper;
        
        public LockHandler(final Looper looper) {
            this.looper = looper;
        }
        
        public void handleMessage(final Message message) {
            switch (message.what) {
                default: {}
            }
        }
    }
}
