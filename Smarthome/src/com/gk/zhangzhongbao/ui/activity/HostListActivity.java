package com.gk.zhangzhongbao.ui.activity;

import com.gk.zhangzhongbao.ui.bean.*;
import java.util.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.os.*;
import com.gk.zhangzhongbao.ui.view.*;
import com.gk.wifictr.net.*;
import android.content.*;
import android.widget.*;
import android.view.*;
import android.annotation.*;

public class HostListActivity extends BaseActivity
{
    HostAdapter adapter;
    GridView gridView;
    RelativeLayout hostLayout;
    boolean isDelGone;
    RelativeLayout layout;
    List<Host> mList;
    
    public HostListActivity() {
        mList = new ArrayList<Host>();
        isDelGone = true;
    }
    
    private void addHost(int num, String name) {
        Host host = new Host();
        host.num = num;
        host.name = name;
        host.addTime = new StringBuilder(String.valueOf(System.currentTimeMillis())).toString();
        mList.add(host);
        DataBase.db.insertHost(host);
    }
    
    private void showdialog() {
        final EditText editText = new EditText(this);
        editText.setHint((CharSequence)getString(2131296299));
        Util.alertDlg(this, getString(2131296300), null, (View)editText, new Runnable() {
            @Override
            public void run() {
                if (editText.getText().toString().equals("")) {
                    showToast(getString(2131296299));
                    return;
                }
                if (!isDelGone) {
                    isDelGone = true;
                    Iterator<Host> iterator = mList.iterator();
                    while (iterator.hasNext()) {
                        iterator.next().delGone = true;
                    }
                    adapter.notifyDataSetChanged();
                }
                int num = mList.get(mList.size() - 1).num;
                if (mList.size() < 6) {
                    addHost(num + 1, editText.getText().toString());
                    adapter.notifyDataSetChanged();
                    gridView.setSelection(mList.size() - 1);
                    return;
                }
                showToast(getString(2131296301));
            }
        });
    }
    
    public void onClick(View view) {
        switch (view.getId()) {
            case 2131230937: {
                showdialog();
                break;
            }
            case 2131230938: {
                isDelGone = !isDelGone;
                if (isDelGone) {
                    Iterator<Host> iterator = mList.iterator();
                    while (iterator.hasNext()) {
                        iterator.next().delGone = true;
                    }
                }
                else {
                    Iterator<Host> iterator2 = mList.iterator();
                    while (iterator2.hasNext()) {
                        iterator2.next().delGone = false;
                    }
                }
                adapter.notifyDataSetChanged();
                break;
            }
        }
    }
    
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2130903071);
        mList = DataBase.db.getHostList(this);
        if (mList.size() <= 0) {
            addHost(1, getString(2131296277));
            addHost(2, getString(2131296278));
            addHost(3, getString(2131296279));
            addHost(4, getString(2131296280));
        }
        gridView = (GridView)findViewById(2131230935);
        adapter = new HostAdapter(this, mList);
        gridView.setAdapter((ListAdapter)adapter);
        layout = (RelativeLayout)findViewById(2131230862);
        SharedPreferences sharedPreferences = getSharedPreferences(Util.SP_NAME, 0);
        if (sharedPreferences.getBoolean("app_first_start", true)) {
            final GuideViewPager guideViewPager = new GuideViewPager(this);
            layout.addView((View)guideViewPager, new ViewGroup.LayoutParams(-1, -1));
            guideViewPager.setPageTouchLisenter((GuideViewPager.OnPageTouchLisenter)new GuideViewPager.OnPageTouchLisenter() {
                @Override
                public void onLastPage() {
                    guideViewPager.setVisibility(8);
                    layout.removeView((View)guideViewPager);
                    guideViewPager.recycle();
                }
            });
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean("app_first_start", false);
        edit.commit();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, ConnectService.class));
        System.exit(0);
    }
    
    public class HostAdapter extends BaseAdapter
    {
        Context mContext;
        List<Host> mList;
        
        public HostAdapter(Context mContext, List<Host> mList) {
            this.mList = mList;
            this.mContext = mContext;
        }
        
        public int getCount() {
            return mList.size();
        }
        
        public Object getItem(int n) {
            return mList.get(n);
        }
        
        public long getItemId(int n) {
            return n;
        }
        
        @SuppressLint({ "ViewHolder" })
        public View getView(final int n, View inflate, ViewGroup viewGroup) {
            inflate = View.inflate(mContext, 2130903070, null);
            Button button = (Button)inflate.findViewById(2131230933);
            Button button2 = (Button)inflate.findViewById(2131230934);
            final Host host = mList.get(n);
            if (host.delGone) {
                button2.setVisibility(8);
            }
            else {
                button2.setVisibility(0);
            }
            button.setText((CharSequence)host.name);
            button.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent(HostListActivity.this, MainActivity.class);
                    intent.putExtra("defwifinum", host.num);
                    startActivity(intent);
                }
            });
            button.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(View view) {
                    final EditText editText = new EditText(HostListActivity.this);
                    editText.setText((CharSequence)host.name);
                    Util.alertDlg(HostListActivity.this, getResString(2131296283), null, (View)editText, new Runnable() {
                        @Override
                        public void run() {
                            mList.get(n).name = editText.getText().toString();
                            adapter.notifyDataSetChanged();
                            DataBase.db.updateHost(host);
                        }
                    });
                    return true;
                }
            });
            button2.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(View view) {
                    Util.alertDlg(HostListActivity.this, getString(2131296284), String.valueOf(getString(2131296302)) + " " + host.name + "?", new Runnable() {
                        @Override
                        public void run() {
                            mList.remove(n);
                            adapter.notifyDataSetChanged();
                            DataBase.db.deleteHost(host.addTime);
                        }
                    });
                }
            });
            return inflate;
        }
    }
}
