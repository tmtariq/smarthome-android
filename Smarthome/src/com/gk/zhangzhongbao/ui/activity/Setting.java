package com.gk.zhangzhongbao.ui.activity;

import android.view.*;
import android.content.*;
import android.os.*;

public class Setting extends BaseViewActivity
{
    public void onClick(final View view) {
        switch (view.getId()) {
            case 2131231062: {
                this.startActivity(new Intent(this, ShowModeActivity.class));
                break;
            }
            case 2131231070: {
                this.startActivity(new Intent(this, SubDevActivity.class));
                break;
            }
            case 2131231064: {
                this.startActivity(new Intent(this, SetHostInfoActivity.class));
                break;
            }
            case 2131231066: {
                this.startActivity(new Intent(this, SetHostNetActivity.class));
                break;
            }
            case 2131231068: {
                this.startActivity(new Intent(this, SetHostPasswordActivity.class));
                break;
            }
            case 2131231072: {
                this.startActivity(new Intent(this, GaojiActivity.class));
                break;
            }
            case 2131231074: {
                this.startActivity(new Intent(this, GuideActivity.class));
                break;
            }
            case 2131231076: {
                this.startActivity(new Intent(this, AboutActivity.class));
                break;
            }
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText(this.getResString(2131296412));
        this.setCenterView(2130903093);
    }
}
