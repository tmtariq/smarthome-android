package com.gk.zhangzhongbao.ui.activity;

import com.gk.wifictr.net.data.*;
import com.gk.zhangzhongbao.ui.view.*;
import android.view.*;
import android.widget.*;
import com.gk.zhangzhongbao.ui.app.*;
import java.util.*;
import android.util.*;
import com.gk.wifictr.net.command.station.*;
import com.gk.wifictr.net.command.*;
import android.os.*;
import com.gk.wifictr.net.*;
import android.content.*;

public class SubDevActivity extends BaseViewActivity implements View.OnClickListener
{
    private String SN;
    private List<Map<String, Object>> ablist;
    private byte[] accode;
    private RelativeLayout air;
    private SubDevice airDevice;
    private int airPosition;
    private TextView airTextView;
    String brandString;
    int code;
    int[] codes;
    BrandDialog dialog;
    int end;
    int index;
    private List<Map<String, Object>> list;
    private LinearLayout ll_light;
    private Context mContext;
    PopupWindow popupWindow;
    String sn;
    int start;
    private RelativeLayout stb;
    private SubDevice stbDevice;
    private TextView stbTextView;
    private RelativeLayout tv;
    private SubDevice tvDevice;
    private int tvPosition;
    private TextView tvTextView;
    private BrandDialog tvdialog;
    
    public SubDevActivity() {
        this.mContext = (Context)this;
        this.list = new ArrayList<Map<String, Object>>();
        this.brandString = "";
        this.accode = new byte[14];
    }
    
    static /* synthetic */ void access$4(final SubDevActivity subDevActivity, final int tvPosition) {
        subDevActivity.tvPosition = tvPosition;
    }
    
    static /* synthetic */ void access$9(final SubDevActivity subDevActivity, final int airPosition) {
        subDevActivity.airPosition = airPosition;
    }
    
    private void powerOn() {
        this.sn = Util.codeToSn(this.code);
        this.accode[10] |= 0x1;
        this.accode[9] = 0;
        new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(this.airDevice.devNo, this.airDevice.devName, this.airDevice.devSN, (byte)6), (byte)0, this.accode));
    }
    
    private void send(final int n) {
        new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(this.tvDevice.devNo, this.tvDevice.devName, this.tvDevice.devSN, this.tvDevice.devType), (byte)n));
    }
    
    private void setDeviceView(final int imageResource, final SubDevice subDevice, final String s) {
        final View inflate = View.inflate(this.mContext, 2130903092, (ViewGroup)null);
        final RelativeLayout relativeLayout = (RelativeLayout)inflate.findViewById(2131231058);
        final ImageView imageView = (ImageView)inflate.findViewById(2131231059);
        final ImageView imageView2 = (ImageView)inflate.findViewById(2131231061);
        final TextView textView = (TextView)inflate.findViewById(2131231060);
        if (imageResource > 0) {
            imageView.setImageResource(imageResource);
        }
        imageView2.setImageResource(2130838254);
        textView.setText((CharSequence)subDevice.devName);
        this.ll_light.addView(inflate, new ViewGroup.LayoutParams(-1, -2));
        relativeLayout.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                final EditText editText = new EditText(SubDevActivity.this.mContext);
                Util.alertDlg(SubDevActivity.this.mContext, s, null, (View)editText, new Runnable() {
                    @Override
                    public void run() {
                        final String string = editText.getText().toString();
                        if (string == null || string.equals("")) {
                            return;
                        }
                        if (subDevice.devType == 80) {
                            new BroadcastHelper().writeData(new Out_25H().make(subDevice.devNo, string, subDevice.devSN, subDevice.devType, subDevice.devCode));
                            return;
                        }
                        new BroadcastHelper().writeData(new Out_25H().make(subDevice.devNo, string, subDevice.devSN, subDevice.devType, subDevice.lightCount));
                    }
                });
            }
        });
        imageView2.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                Util.alertDlg(SubDevActivity.this.mContext, SubDevActivity.this.getResString(2131296284), SubDevActivity.this.getResString(2131296290), new Runnable() {
                    @Override
                    public void run() {
                        new BroadcastHelper().writeData(new Out_24H().make(subDevice.devNo, subDevice.devSN, subDevice.devType));
                        if (subDevice.devType == 16) {
                            DataBase.db.deleteData("light_table", "sncode", subDevice.devSN, SubDevActivity.this.SN);
                        }
                        else {
                            if (subDevice.devType == 64) {
                                DataBase.db.deleteData("lock_table", "lock_sn", subDevice.devSN, SubDevActivity.this.SN);
                                return;
                            }
                            if (subDevice.devType == 80 || subDevice.devType == 96) {
                                DataBase.db.deleteData("cg_table", "cg_sn", subDevice.devSN, SubDevActivity.this.SN);
                            }
                        }
                    }
                });
            }
        });
    }
    
    private void showDlg() {
        if (this.dialog == null) {
            this.ablist = new ArrayList<Map<String, Object>>();
            for (int i = 0; i < Brands.brandsString.length; ++i) {
                final HashMap<String, Object> hashMap = new HashMap<String, Object>();
                hashMap.put("brand", Brands.brandsString[i]);
                hashMap.put("start", Brands.brandCodes[i][0]);
                hashMap.put("end", Brands.brandCodes[i][1]);
                hashMap.put("code", Brands.brandCodes[i][0]);
                this.ablist.add((Map<String, Object>)hashMap);
            }
            (this.dialog = new BrandDialog(this.mContext, this.ablist, 2131361809)).setListener((BrandDialog.ClickListener)new BrandDialog.ClickListener() {
                @Override
                public void itemClick(final int n) {
                    final Map<String, Object> map = SubDevActivity.this.ablist.get(n);
                    SubDevActivity.this.brandString = (String)map.get("brand");
                    SubDevActivity.this.start = Integer.valueOf((String)map.get("start"));
                    SubDevActivity.this.end = Integer.valueOf((String)map.get("end"));
                    SubDevActivity.this.code = Integer.valueOf((String)map.get("code"));
                    Log.e("code", new StringBuilder(String.valueOf(SubDevActivity.this.code)).toString());
                    SubDevActivity.this.sn = Util.codeToSn(SubDevActivity.this.code);
                    SubDevActivity.this.dialog.brandName.setText((CharSequence)SubDevActivity.this.brandString);
                    SubDevActivity.this.dialog.brandNum.setText((CharSequence)new StringBuilder(String.valueOf(SubDevActivity.this.code - SubDevActivity.this.start + 1)).toString());
                    SubDevActivity.access$9(SubDevActivity.this, n);
                    SubDevActivity.this.powerOn();
                }
                
                @Override
                public void leftClick() {
                    final SubDevActivity this$0 = SubDevActivity.this;
                    --this$0.code;
                    if (SubDevActivity.this.code < SubDevActivity.this.start) {
                        SubDevActivity.this.code = SubDevActivity.this.end;
                    }
                    SubDevActivity.this.dialog.brandNum.setText((CharSequence)new StringBuilder(String.valueOf(SubDevActivity.this.code - SubDevActivity.this.start + 1)).toString());
                    SubDevActivity.this.powerOn();
                }
                
                @Override
                public void okClick() {
                    SubDevActivity.this.airDevice.devSN = Util.codeToSn(SubDevActivity.this.code);
                    SubDevActivity.this.airDevice.devName = SubDevActivity.this.brandString;
                    SubDevActivity.this.dialog.dismiss();
                    new BroadcastHelper().writeData(new Out_25H().make(SubDevActivity.this.airDevice));
                }
                
                @Override
                public void rightClick() {
                    final SubDevActivity this$0 = SubDevActivity.this;
                    ++this$0.code;
                    if (SubDevActivity.this.code > SubDevActivity.this.end) {
                        SubDevActivity.this.code = SubDevActivity.this.start;
                    }
                    SubDevActivity.this.dialog.brandNum.setText((CharSequence)new StringBuilder(String.valueOf(SubDevActivity.this.code - SubDevActivity.this.start + 1)).toString());
                    Log.e("hex", Util.codeToSn(SubDevActivity.this.code));
                    SubDevActivity.this.powerOn();
                }
            });
        }
        this.dialog.show();
        final Map<String, Object> map = this.dialog.ablist.get(this.airPosition);
        this.brandString = (String)map.get("brand");
        this.start = Integer.valueOf((String)map.get("start"));
        this.end = Integer.valueOf((String)map.get("end"));
        this.code = Integer.valueOf((String)map.get("code"));
        this.dialog.brandName.setText((CharSequence)this.brandString);
    }
    
    protected void decodeData(final ReadData readData) {
        super.decodeData(readData);
        if (readData.type == 19) {
            this.ll_light.removeAllViews();
            final ArrayList<SubDevice> decode = new In_13H().decode(readData);
            Log.e("list", "size:" + decode.size());
            for (int i = 0; i < decode.size(); ++i) {
                final SubDevice airDevice = decode.get(i);
                if (airDevice.devType == 16 || airDevice.devType == 98 || airDevice.devType == 99) {
                    this.setDeviceView(2130838255, airDevice, this.getResString(2131296291));
                }
                else if (airDevice.devType == 64) {
                    this.setDeviceView(2130838259, airDevice, this.getResString(2131296292));
                }
                else if (airDevice.devType == 80 || airDevice.devType == 96 || airDevice.devType == 97) {
                    switch (ByteUtil.byte2hex(new byte[] { airDevice.devCode }, true).charAt(0)) {
                        case 'a': {
                            this.setDeviceView(2130838250, airDevice, this.getResString(2131296294));
                            break;
                        }
                        case '9': {
                            this.setDeviceView(2130838250, airDevice, this.getResString(2131296294));
                            break;
                        }
                        case '1': {
                            this.setDeviceView(2130838250, airDevice, this.getResString(2131296293));
                            break;
                        }
                    }
                }
                else if (airDevice.devType == 1) {
                    this.stbDevice = airDevice;
                    this.stbTextView.setText((CharSequence)this.stbDevice.devName);
                }
                else if (airDevice.devType == 3) {
                    this.tvDevice = airDevice;
                    this.tvTextView.setText((CharSequence)this.tvDevice.devName);
                    Util.saveValue("tv_sn", this.tvDevice.devSN);
                    Util.saveValue("tv_brand_name", this.tvDevice.devName);
                    Util.saveValue("tv_devno", this.tvDevice.devNo);
                    Log.e("mSubDevice.devName length", new StringBuilder(String.valueOf(this.tvDevice.devName.length())).toString());
                    for (int j = 0; j < Brands.tvBrandStrings.length; ++j) {
                        if (Brands.tvBrandStrings[j].equals(this.tvDevice.devName)) {
                            Util.saveValue("tv_brand_index", this.tvPosition = j);
                        }
                    }
                }
                else if (airDevice.devType == 6) {
                    this.airDevice = airDevice;
                    this.airTextView.setText((CharSequence)this.airDevice.devName);
                    Util.saveValue("air_sn", this.airDevice.devSN);
                    Util.saveValue("air_brand_name", this.airDevice.devName);
                    Util.saveValue("air_devno", this.airDevice.devNo);
                    for (int k = 0; k < Brands.brandsString.length; ++k) {
                        if (Brands.brandsString[k].equals(this.airDevice.devName)) {
                            Util.saveValue("air_brand_index", this.airPosition = k);
                        }
                    }
                }
            }
        }
    }
    
    public void onClick(final View view) {
        if (view == this.tv) {
            this.showTVBrandList();
        }
        else {
            if (view == this.air) {
                this.showDlg();
                return;
            }
            if (view == this.stb) {
                final EditText editText = new EditText(this.mContext);
                Util.alertDlg(this.mContext, this.getResString(2131296295), null, (View)editText, new Runnable() {
                    @Override
                    public void run() {
                        final String string = editText.getText().toString();
                        if (string == null || string.equals("")) {
                            return;
                        }
                        new BroadcastHelper().writeData(new Out_25H().make(SubDevActivity.this.stbDevice.devNo, string, SubDevActivity.this.stbDevice.devSN, SubDevActivity.this.stbDevice.devType, SubDevActivity.this.stbDevice.lightCount));
                    }
                });
            }
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.SN = ConnectService.defWifi.SN;
        this.title.setText((CharSequence)this.getResString(2131296417));
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(2130838048);
        this.btnRight.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                SubDevActivity.this.startActivity(new Intent(SubDevActivity.this.mContext, (Class)AddDevActvitity.class));
            }
        });
        this.setCenterView(2130903098);
        this.stb = (RelativeLayout)this.findViewById(2131231094);
        this.tv = (RelativeLayout)this.findViewById(2131231096);
        this.air = (RelativeLayout)this.findViewById(2131231099);
        this.stbTextView = (TextView)this.findViewById(2131231095);
        this.tvTextView = (TextView)this.findViewById(2131231098);
        this.airTextView = (TextView)this.findViewById(2131231101);
        this.ll_light = (LinearLayout)this.findViewById(2131231102);
        this.stb.setOnClickListener((View.OnClickListener)this);
        this.tv.setOnClickListener((View.OnClickListener)this);
        this.air.setOnClickListener((View.OnClickListener)this);
        this.reflash();
        this.linkState();
        this.stbDevice = new SubDevice(0, Util.getStringValue("stb_brand_name", this.getResString(2131296308)), Util.getStringValue("stb_sn", "830000"), (byte)1);
        this.airDevice = new SubDevice(1, Util.getStringValue("air_brand_name", this.getResString(2131296310)), Util.getStringValue("air_sn", "010000"), (byte)6);
        this.tvDevice = new SubDevice(2, Util.getStringValue("tv_brand_name", this.getResString(2131296309)), Util.getStringValue("tv_sn", "660300"), (byte)3);
        this.stbTextView.setText((CharSequence)this.stbDevice.devName);
        this.airTextView.setText((CharSequence)this.airDevice.devName);
        this.tvTextView.setText((CharSequence)this.tvDevice.devName);
    }
    
    protected void onDestroy() {
        super.onDestroy();
    }
    
    public void showTVBrandList() {
        if (this.tvdialog == null) {
            for (int i = 0; i < Brands.tvBrandStrings.length; ++i) {
                final HashMap<String, Object> hashMap = new HashMap<String, Object>();
                hashMap.put("brand", Brands.tvBrandStrings[i]);
                hashMap.put("codes", (Integer)(Object)Brands.tvBrandCodes[i]);
                hashMap.put("index", 1);
                this.list.add((Map<String, Object>)hashMap);
            }
            (this.tvdialog = new BrandDialog(this.mContext, this.list, 2131361809)).setListener((BrandDialog.ClickListener)new BrandDialog.ClickListener() {
                @Override
                public void itemClick(final int n) {
                    Util.vibrator(SubDevActivity.this.mContext);
                    final Map<String, Object> map = SubDevActivity.this.list.get(n);
                    SubDevActivity.access$4(SubDevActivity.this, n);
                    SubDevActivity.this.brandString = (String)map.get("brand");
                    SubDevActivity.this.codes = (int[])(Object)map.get("codes");
                    SubDevActivity.this.index = Integer.valueOf((String)map.get("index"));
                    SubDevActivity.this.tvdialog.brandName.setText((CharSequence)SubDevActivity.this.brandString);
                    SubDevActivity.this.tvdialog.brandNum.setText((CharSequence)new StringBuilder(String.valueOf(SubDevActivity.this.index)).toString());
                    SubDevActivity.this.tvDevice.devSN = Util.codeToSn(SubDevActivity.this.codes[SubDevActivity.this.index - 1]);
                    SubDevActivity.this.send(15);
                }
                
                @Override
                public void leftClick() {
                    final SubDevActivity this$0 = SubDevActivity.this;
                    --this$0.index;
                    if (SubDevActivity.this.index <= 0) {
                        SubDevActivity.this.index = SubDevActivity.this.codes.length;
                    }
                    SubDevActivity.this.tvdialog.brandNum.setText((CharSequence)new StringBuilder(String.valueOf(SubDevActivity.this.index)).toString());
                    SubDevActivity.this.tvDevice.devSN = Util.codeToSn(SubDevActivity.this.codes[SubDevActivity.this.index - 1]);
                    SubDevActivity.this.send(15);
                }
                
                @Override
                public void okClick() {
                    SubDevActivity.this.tvDevice.devSN = Util.codeToSn(SubDevActivity.this.codes[SubDevActivity.this.index - 1]);
                    SubDevActivity.this.tvDevice.devName = SubDevActivity.this.brandString;
                    SubDevActivity.this.send(15);
                    SubDevActivity.this.tvdialog.dismiss();
                    new BroadcastHelper().writeData(new Out_25H().make(SubDevActivity.this.tvDevice));
                }
                
                @Override
                public void rightClick() {
                    final SubDevActivity this$0 = SubDevActivity.this;
                    ++this$0.index;
                    if (SubDevActivity.this.index > SubDevActivity.this.codes.length) {
                        SubDevActivity.this.index = 1;
                    }
                    SubDevActivity.this.tvdialog.brandNum.setText((CharSequence)new StringBuilder(String.valueOf(SubDevActivity.this.index)).toString());
                    SubDevActivity.this.tvDevice.devSN = Util.codeToSn(SubDevActivity.this.codes[SubDevActivity.this.index - 1]);
                    SubDevActivity.this.send(15);
                }
            });
        }
        this.tvdialog.show();
        final Map<String, Object> map = this.list.get(this.tvPosition);
        this.brandString = (String)map.get("brand");
        this.codes = (int[])map.get("codes");
        final int snToCode = Util.snToCode(this.tvDevice.devSN);
        for (int j = 0; j < this.codes.length; ++j) {
            if (snToCode == this.codes[j]) {
                this.index = j + 1;
            }
        }
        this.tvdialog.brandNum.setText((CharSequence)new StringBuilder(String.valueOf(this.index)).toString());
        this.tvdialog.brandName.setText((CharSequence)this.brandString);
    }
    
    @Override
    protected void stateChange() {
        super.stateChange();
        this.linkState();
    }
}
