package com.gk.zhangzhongbao.ui.activity;

import android.app.*;
import android.graphics.*;
import com.gk.zhangzhongbao.ui.view.*;
import android.widget.*;
import com.gk.wifictr.net.*;
import android.content.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.os.*;
import android.util.*;

public class StartActivity extends Activity
{
    protected Bitmap background;
    private LockPatternUtils lockPatternUtils;
    Bitmap logo;
    ImageView startImg;
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.requestWindowFeature(1);
        this.setContentView(2130903096);
        Util.util = new Util(this);
        DataBase.db = new DataBase(this);
        this.startService(new Intent(this, ConnectService.class));
        ConnectService.APP_TYPE = 2;
        this.startImg = (ImageView)this.findViewById(2131231084);
        this.lockPatternUtils = new LockPatternUtils(this);
        int n = 2130838284;
        if (Util.lan.equals("zh")) {
            n = 2130838286;
        }
        else if (Util.lan.equals("en")) {
            n = 2130838286;
        }
        this.background = MakeBmp.CreateBitmap(this, n, Util.sw, Util.sh);
        this.startImg.setImageBitmap(this.background);
        new Handler().postDelayed((Runnable)new Runnable() {
            @Override
            public void run() {
                Util.isRightAppPassword = false;
                if (lockPatternUtils.getLockPaternString().equals("")) {
                    startActivity(new Intent(StartActivity.this, HostListActivity.class));
                }
                else {
                    startActivity(new Intent(StartActivity.this, LockScreenActivity.class));
                }
                finish();
            }
        }, 2000L);
    }
    
    protected void onDestroy() {
        super.onDestroy();
        if (this.background != null && !this.background.isRecycled()) {
            Log.e("", "recycle");
            this.background.recycle();
            this.background = null;
        }
    }
}
