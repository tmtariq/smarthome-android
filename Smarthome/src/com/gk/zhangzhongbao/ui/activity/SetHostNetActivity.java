package com.gk.zhangzhongbao.ui.activity;

import android.widget.*;
import java.util.*;
import android.util.*;
import com.gk.wifictr.net.command.*;
import com.gk.zhangzhongbao.ui.app.*;
import com.gk.wifictr.net.*;
import android.view.*;
import android.os.*;
import android.app.*;
import android.support.v4.content.*;
import android.content.*;

public class SetHostNetActivity extends BaseViewActivity implements View.OnClickListener
{
    private Context context;
    private EditText edtPassward;
    private EditText edtRouterId;
    Handler handler;
    private boolean link;
    private BroadcastReceiver receiver;
    private int second;
    private Button sure;
    private Timer timer;
    private TimerTask timerTask;
    
    public SetHostNetActivity() {
        this.context = (Context)this;
        this.link = false;
        this.receiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final int int1 = intent.getExtras().getInt("cmd");
                Log.e("set net receive", "cmd:" + int1);
                switch (int1) {
                    case 7: {
                        Log.e("log", intent.getStringExtra("log"));
                        break;
                    }
                    case 8: {
                        SetHostNetActivity.this.timerCancle();
                        new BroadcastHelper().stopEasyLink();
                        Log.e("status", intent.getStringExtra("status"));
                        if (SetHostNetActivity.this.link) {
                            SetHostNetActivity.this.showTipsDlg(SetHostNetActivity.this.getString(2131296437));
                            SetHostNetActivity.access$2(SetHostNetActivity.this, false);
                            final String string = SetHostNetActivity.this.edtRouterId.getText().toString();
                            final String string2 = SetHostNetActivity.this.edtPassward.getText().toString();
                            ConnectService.defWifi.routerSSID = string;
                            ConnectService.defWifi.routerPassword = string2;
                            ConnectService.defWifi.save(ConnectService.defWifinum);
                            return;
                        }
                        break;
                    }
                    case 4: {
                        final ReadData readData = (ReadData)intent.getParcelableExtra("data");
                        if (readData != null) {
                            Log.e("data body", ByteUtil.byte2hex(readData.body));
                            Log.e("type", ByteUtil.byte2hex(new byte[] { readData.type }));
                            return;
                        }
                        break;
                    }
                }
            }
        };
        this.second = 60;
        this.handler = new Handler() {
            public void handleMessage(final Message message) {
                switch (message.what) {
                    case 1: {
                        SetHostNetActivity.this.loadingTv.setText((CharSequence)(String.valueOf(SetHostNetActivity.this.getString(2131296440)) + "\n00:" + message.obj));
                        break;
                    }
                }
            }
        };
    }
    
    static /* synthetic */ void access$2(final SetHostNetActivity setHostNetActivity, final boolean link) {
        setHostNetActivity.link = link;
    }
    
    static /* synthetic */ void access$7(final SetHostNetActivity setHostNetActivity, final int second) {
        setHostNetActivity.second = second;
    }
    
    private void stopEasylink() {
        this.link = false;
        new BroadcastHelper().stopEasyLink();
    }
    
    private void submint() {
        final String string = this.edtRouterId.getText().toString();
        final String string2 = this.edtPassward.getText().toString();
        if (string.equals("")) {
            Util.toast((Context)this, this.getString(2131296438));
            return;
        }
        if (!new NetHelper((Context)this).isWifiConnected()) {
            this.showToast(this.getString(2131296439));
            return;
        }
        this.createLoadingDialog(this.context, this.getString(2131296440), new Runnable() {
            @Override
            public void run() {
                SetHostNetActivity.this.stopEasylink();
                SetHostNetActivity.this.timerCancle();
            }
        });
        new BroadcastHelper().startEasyLink(string, string2);
        this.timeStart();
    }
    
    private void timeStart() {
        if (this.timer == null && this.timerTask == null) {
            this.timer = new Timer();
            this.timerTask = new TimerTask() {
                @Override
                public void run() {
                    final SetHostNetActivity this$0 = SetHostNetActivity.this;
                    SetHostNetActivity.access$7(this$0, this$0.second - 1);
                    final Message message = new Message();
                    message.what = 1;
                    message.obj = SetHostNetActivity.this.second;
                    SetHostNetActivity.this.handler.sendMessage(message);
                    if (SetHostNetActivity.this.second == 10) {
                        SetHostNetActivity.this.stopEasylink();
                    }
                    else if (SetHostNetActivity.this.second <= 0) {
                        SetHostNetActivity.this.handler.post((Runnable)new Runnable() {
                            @Override
                            public void run() {
                                SetHostNetActivity.this.loadingDialog.dismiss();
                                SetHostNetActivity.this.loadingDialog = null;
                                Util.toast(SetHostNetActivity.this.context, SetHostNetActivity.this.getString(2131296441));
                                SetHostNetActivity.this.timerCancle();
                            }
                        });
                    }
                }
            };
            this.timer.schedule(this.timerTask, 0L, 1000L);
        }
    }
    
    private void timerCancle() {
        this.second = 60;
        if (this.timer != null) {
            this.timer.cancel();
            this.timer = null;
        }
        if (this.timerTask != null) {
            this.timerTask = null;
        }
    }
    
    public void onClick(final View view) {
        switch (view.getId()) {
            case 2131231057: {
                this.link = true;
                if (this.link) {
                    this.submint();
                    return;
                }
                break;
            }
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText((CharSequence)this.getString(2131296415));
        this.title.setTextSize(2, 18.0f);
        this.btnLeft.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                SetHostNetActivity.this.finish();
            }
        });
        this.setCenterView(2130903091);
        this.edtRouterId = (EditText)Util.setView(this, 2131231055, null);
        this.edtPassward = (EditText)Util.setView(this, 2131231056, null);
        (this.sure = (Button)this.findViewById(2131231057)).setOnClickListener((View.OnClickListener)this);
        if (new NetHelper((Context)this).isWifiConnected()) {
            final String wifiSSID = new NetHelper((Context)this).getWifiSSID();
            if (ConnectService.defWifi.routerSSID.equals(wifiSSID)) {
                this.edtRouterId.setText((CharSequence)ConnectService.defWifi.routerSSID);
                this.edtPassward.setText((CharSequence)ConnectService.defWifi.routerPassword);
            }
            else {
                this.edtRouterId.setText((CharSequence)wifiSSID);
                this.edtPassward.setText((CharSequence)"");
            }
        }
        this.lbcManager = LocalBroadcastManager.getInstance((Context)this);
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.gk.wifictr.net");
        this.lbcManager.registerReceiver(this.receiver, intentFilter);
    }
    
    protected void onDestroy() {
        super.onDestroy();
        this.lbcManager.unregisterReceiver(this.receiver);
        this.stopEasylink();
        this.timerCancle();
    }
}
