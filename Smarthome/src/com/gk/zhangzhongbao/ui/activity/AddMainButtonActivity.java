package com.gk.zhangzhongbao.ui.activity;

import com.gk.zhangzhongbao.ui.view.*;
import android.widget.*;
import android.view.*;
import android.os.*;
import java.util.*;

public class AddMainButtonActivity extends BaseViewActivity implements View.OnClickListener
{
    public String INTENT_AIR;
    public String INTENT_CURTAIN;
    public String INTENT_DOOR;
    public String INTENT_LEARN;
    public String INTENT_LIGHT;
    public String INTENT_STB;
    public String INTENT_TV;
    public String INTENT_VEDIO;
    private MainButton air;
    private ImageView cbAir;
    private ImageView cbCurtain;
    private ImageView cbLearn;
    private ImageView cbLight;
    private ImageView cbLock;
    private ImageView cbStb;
    private ImageView cbTv;
    private ImageView cbVedio;
    private MainButton curtain;
    private MainButton doorLock;
    private MainButton learn;
    private MainButton light;
    private MainButton stb;
    private MainButton tv;
    private MainButton vedio;
    
    public AddMainButtonActivity() {
        this.INTENT_STB = "intent_STBActivity";
        this.INTENT_TV = "intent_TvActivity";
        this.INTENT_LIGHT = "intent_LightActivity";
        this.INTENT_AIR = "intent_AirActivity";
        this.INTENT_DOOR = "intent_DoorLockActivity";
        this.INTENT_CURTAIN = "intent_CurtainActivity";
        this.INTENT_LEARN = "intent_Learn";
        this.INTENT_VEDIO = "intent_Vedio";
    }
    
    private void sendMsg(final int what, final String obj) {
        final Message message = new Message();
        message.what = what;
        message.obj = obj;
        MainActivity.handler.sendMessage(message);
    }
    
    private void setChecked(final MainButton mainButton, final ImageView imageView, final String s) {
        if (mainButton.isAdd == 1) {
            mainButton.isAdd = 0;
            this.setImageView(false, imageView);
            this.sendMsg(2, s);
            return;
        }
        mainButton.isAdd = 1;
        this.setImageView(true, imageView);
        this.sendMsg(1, s);
    }
    
    private void setImageView(final boolean b, final ImageView imageView) {
        if (b) {
            imageView.setImageResource(2130837997);
            return;
        }
        imageView.setImageResource(2130837996);
    }
    
    public void onClick(final View view) {
        if (view == this.stb) {
            this.setChecked(this.stb, this.cbStb, this.INTENT_STB);
        }
        else {
            if (view == this.tv) {
                this.setChecked(this.tv, this.cbTv, this.INTENT_TV);
                return;
            }
            if (view == this.light) {
                this.setChecked(this.light, this.cbLight, this.INTENT_LIGHT);
                return;
            }
            if (view == this.air) {
                this.setChecked(this.air, this.cbAir, this.INTENT_AIR);
                return;
            }
            if (view == this.doorLock) {
                this.setChecked(this.doorLock, this.cbLock, this.INTENT_DOOR);
                return;
            }
            if (view == this.curtain) {
                this.setChecked(this.curtain, this.cbCurtain, this.INTENT_CURTAIN);
                return;
            }
            if (view == this.learn) {
                this.setChecked(this.learn, this.cbLearn, this.INTENT_LEARN);
                return;
            }
            if (view == this.vedio) {
                this.setChecked(this.vedio, this.cbVedio, this.INTENT_VEDIO);
                return;
            }
            if (view == this.cbStb) {
                this.setChecked(this.stb, this.cbStb, this.INTENT_STB);
                return;
            }
            if (view == this.cbTv) {
                this.setChecked(this.tv, this.cbTv, this.INTENT_TV);
                return;
            }
            if (view == this.cbLight) {
                this.setChecked(this.light, this.cbLight, this.INTENT_LIGHT);
                return;
            }
            if (view == this.cbAir) {
                this.setChecked(this.air, this.cbAir, this.INTENT_AIR);
                return;
            }
            if (view == this.cbLock) {
                this.setChecked(this.doorLock, this.cbLock, this.INTENT_DOOR);
                return;
            }
            if (view == this.cbCurtain) {
                this.setChecked(this.curtain, this.cbCurtain, this.INTENT_CURTAIN);
                return;
            }
            if (view == this.cbLearn) {
                this.setChecked(this.learn, this.cbLearn, this.INTENT_LEARN);
                return;
            }
            if (view == this.cbVedio) {
                this.setChecked(this.vedio, this.cbVedio, this.INTENT_VEDIO);
            }
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText((CharSequence)this.getString(2131296316));
        this.setCenterView(2130903047);
        this.stb = (MainButton)this.findViewById(2131230772);
        this.tv = (MainButton)this.findViewById(2131230774);
        this.light = (MainButton)this.findViewById(2131230778);
        this.air = (MainButton)this.findViewById(2131230776);
        this.doorLock = (MainButton)this.findViewById(2131230780);
        this.curtain = (MainButton)this.findViewById(2131230782);
        this.learn = (MainButton)this.findViewById(2131230784);
        this.vedio = (MainButton)this.findViewById(2131230786);
        this.stb.setText(this.getString(2131296308));
        this.tv.setText(this.getString(2131296309));
        this.light.setText(this.getString(2131296311));
        this.air.setText(this.getString(2131296310));
        this.doorLock.setText(this.getString(2131296312));
        this.curtain.setText(this.getString(2131296313));
        this.learn.setText(this.getString(2131296314));
        this.vedio.setText(this.getString(2131296315));
        this.stb.setOnClickListener(this);
        this.tv.setOnClickListener(this);
        this.light.setOnClickListener(this);
        this.air.setOnClickListener(this);
        this.doorLock.setOnClickListener(this);
        this.curtain.setOnClickListener(this);
        this.learn.setOnClickListener(this);
        this.vedio.setOnClickListener(this);
        this.cbStb = (ImageView)this.findViewById(2131230773);
        this.cbTv = (ImageView)this.findViewById(2131230775);
        this.cbLight = (ImageView)this.findViewById(2131230779);
        this.cbAir = (ImageView)this.findViewById(2131230777);
        this.cbLock = (ImageView)this.findViewById(2131230781);
        this.cbCurtain = (ImageView)this.findViewById(2131230783);
        this.cbLearn = (ImageView)this.findViewById(2131230785);
        this.cbVedio = (ImageView)this.findViewById(2131230787);
        this.cbStb.setOnClickListener(this);
        this.cbTv.setOnClickListener(this);
        this.cbLight.setOnClickListener(this);
        this.cbAir.setOnClickListener(this);
        this.cbLock.setOnClickListener(this);
        this.cbCurtain.setOnClickListener(this);
        this.cbLearn.setOnClickListener(this);
        this.cbVedio.setOnClickListener(this);
        this.INTENT_STB = String.valueOf(this.INTENT_STB) + MainActivity.hostNum;
        this.INTENT_TV = String.valueOf(this.INTENT_TV) + MainActivity.hostNum;
        this.INTENT_LIGHT = String.valueOf(this.INTENT_LIGHT) + MainActivity.hostNum;
        this.INTENT_AIR = String.valueOf(this.INTENT_AIR) + MainActivity.hostNum;
        this.INTENT_DOOR = String.valueOf(this.INTENT_DOOR) + MainActivity.hostNum;
        this.INTENT_CURTAIN = String.valueOf(this.INTENT_CURTAIN) + MainActivity.hostNum;
        this.INTENT_LEARN = String.valueOf(this.INTENT_LEARN) + MainActivity.hostNum;
        this.INTENT_VEDIO = String.valueOf(this.INTENT_VEDIO) + MainActivity.hostNum;
        this.stb.key = this.INTENT_STB;
        this.tv.key = this.INTENT_TV;
        this.light.key = this.INTENT_LIGHT;
        this.air.key = this.INTENT_AIR;
        this.doorLock.key = this.INTENT_DOOR;
        this.curtain.key = this.INTENT_CURTAIN;
        this.learn.key = this.INTENT_LEARN;
        this.vedio.key = this.INTENT_VEDIO;
        for (final MainButton mainButton : MainActivity.buttons) {
            if (mainButton.key.equals(this.stb.key)) {
                this.stb.isAdd = 1;
                this.setImageView(true, this.cbStb);
            }
            else if (mainButton.key.equals(this.tv.key)) {
                this.tv.isAdd = 1;
                this.setImageView(true, this.cbTv);
            }
            else if (mainButton.key.equals(this.light.key)) {
                this.light.isAdd = 1;
                this.setImageView(true, this.cbLight);
            }
            else if (mainButton.key.equals(this.air.key)) {
                this.air.isAdd = 1;
                this.setImageView(true, this.cbAir);
            }
            else if (mainButton.key.equals(this.doorLock.key)) {
                this.doorLock.isAdd = 1;
                this.setImageView(true, this.cbLock);
            }
            else if (mainButton.key.equals(this.curtain.key)) {
                this.curtain.isAdd = 1;
                this.setImageView(true, this.cbCurtain);
            }
            else if (mainButton.key.equals(this.learn.key)) {
                this.learn.isAdd = 1;
                this.setImageView(true, this.cbLearn);
            }
            else {
                if (!mainButton.key.equals(this.vedio.key)) {
                    continue;
                }
                this.vedio.isAdd = 1;
                this.setImageView(true, this.cbVedio);
            }
        }
    }
}
