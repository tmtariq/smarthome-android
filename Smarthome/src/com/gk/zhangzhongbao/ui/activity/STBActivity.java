package com.gk.zhangzhongbao.ui.activity;

import java.util.*;

import android.support.v4.app.*;
import android.support.v4.app.Fragment;
import android.support.v4.content.*;

import com.gk.zhangzhongbao.ui.fragment.*;
import com.gk.zhangzhongbao.ui.view.*;

import android.util.*;

import com.gk.wifictr.net.command.*;
import com.gk.zhangzhongbao.ui.app.*;
import com.gk.wifictr.net.*;
import com.gk.wifictr.net.data.*;

import android.os.*;

import com.gk.zhangzhongbao.ui.adapter.*;

import android.support.v4.view.*;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.widget.*;

import com.gk.wifictr.net.command.station.*;

import android.app.*;
import android.content.*;
import android.view.*;

public class STBActivity extends FragmentActivity implements View.OnClickListener, OnPageChangeListener
{
    private final int CODE_NO;
    private final String INDEX;
    private final int TAB_C;
    private final int TAB_F;
    private final int TAB_R;
    private Button btnBack;
    private Button btnPowerSwitch;
    private CommonChFragment commonch;
    private Context context;
    private int currIndex;
    private FavoriteChFragment favorite;
    private ArrayList<Fragment> fragmentsList;
    LocalBroadcastManager lbcManager;
    private TextView learnCodeTextView;
    private ProgressBar progressBar;
    BroadcastReceiver receiver;
    private STBFragment remoteControl;
    private ImageView signal;
    private ImageView tabCommon;
    private ImageView tabFavorite;
    private ImageView tabRemote;
    private TextView tvTitle;
    private MyViewPager viewPager;
    
    public STBActivity() {
        this.fragmentsList = new ArrayList<Fragment>();
        this.TAB_R = 0;
        this.TAB_F = 1;
        this.TAB_C = 2;
        this.INDEX = "index";
        this.context = (Context)this;
        this.CODE_NO = 16;
        this.receiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final int int1 = intent.getExtras().getInt("cmd");
                Log.e("stb activity receive", "cmd:" + int1);
                switch (int1) {
                    case 7: {
                        Log.e("send log", intent.getStringExtra("log"));
                    }
                    case 4: {
                        final ReadData readData = (ReadData)intent.getParcelableExtra("data");
                        if (readData == null) {
                            break;
                        }
                        Log.e("data body", ByteUtil.byte2hex(readData.body));
                        Log.e("type", ByteUtil.byte2hex(new byte[] { readData.type }));
                        if (readData.type != 22) {
                            break;
                        }
                        final In_16H in_16H = new In_16H(readData.body);
                        if (in_16H.get_code_num() != 16) {
                            break;
                        }
                        Util.saveValue("stb_tv_ir_code", ByteUtil.byte2hex(in_16H.get_IR_code()));
                        if (STBActivity.this.learnCodeTextView != null && STBActivity.this.progressBar != null) {
                            STBActivity.this.progressBar.setVisibility(8);
                            STBActivity.this.learnCodeTextView.setVisibility(0);
                            STBActivity.this.learnCodeTextView.setText((CharSequence)STBActivity.this.getString(2131296305));
                            return;
                        }
                        break;
                    }
                    case 0: {
                        STBActivity.this.linkState();
                    }
                }
            }
        };
    }
    
    static /* synthetic */ void access$3(final STBActivity stbActivity, final TextView learnCodeTextView) {
        stbActivity.learnCodeTextView = learnCodeTextView;
    }
    
    static /* synthetic */ void access$4(final STBActivity stbActivity, final ProgressBar progressBar) {
        stbActivity.progressBar = progressBar;
    }
    
    private void pagerChange(final int n) {
        switch (n) {
            default: {}
            case 0: {
                this.tvTitle.setText((CharSequence)this.getString(2131296308));
                this.signal.setVisibility(0);
                this.viewPager.setCurrentItem(0);
                Util.saveValue("index", this.currIndex = 0);
                this.setBackground(this.tabRemote, 2130838328);
                this.setBackground(this.tabFavorite, 2130838331);
                this.setBackground(this.tabCommon, 2130838329);
            }
            case 1: {
                this.tvTitle.setText((CharSequence)this.getString(2131296317));
                this.signal.setVisibility(8);
                this.viewPager.setCurrentItem(1);
                Util.saveValue("index", this.currIndex = 1);
                this.setBackground(this.tabRemote, 2130838327);
                this.setBackground(this.tabFavorite, 2130838332);
                this.setBackground(this.tabCommon, 2130838329);
            }
            case 2: {
                this.tvTitle.setText((CharSequence)this.getString(2131296318));
                this.signal.setVisibility(8);
                this.viewPager.setCurrentItem(2);
                this.currIndex = 2;
                Util.saveValue("index", 0);
                this.setBackground(this.tabRemote, 2130838327);
                this.setBackground(this.tabFavorite, 2130838331);
                this.setBackground(this.tabCommon, 2130838330);
            }
        }
    }
    
    protected void linkState() {
        if (ConnectService.mConnectService.isLink()) {
            this.setBackground(this.signal, 2130838283);
            return;
        }
        this.setBackground(this.signal, 2130838282);
    }
    
    public void onClick(final View view) {
        if (view == this.btnBack) {
            if (CommonChFragment.gvLayoutState != 0 || this.currIndex != 2) {
                this.finish();
                return;
            }
            this.commonch.setGridViewGone();
        }
        else if (view == this.btnPowerSwitch) {
            Util.vibrator(this.context);
            Util.clickBeepSound(this.context);
            final String stringValue = Util.getStringValue("stb_tv_ir_code", "");
            Log.e("tv power code", stringValue);
            if (!stringValue.equals("")) {
                new BroadcastHelper().writeData(new Out_2FH().make(STBFragment.tvDevice.devType, 16, ByteUtil.hex2byte(stringValue)));
                return;
            }
            new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(STBFragment.tvDevice.devNo, STBFragment.tvDevice.devName, STBFragment.tvDevice.devSN, STBFragment.tvDevice.devType), (byte)2));
        }
        else {
            if (view == this.tabRemote) {
                this.pagerChange(0);
                return;
            }
            if (view == this.tabFavorite) {
                this.pagerChange(1);
                return;
            }
            if (view == this.tabCommon) {
                this.pagerChange(2);
            }
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.requestWindowFeature(1);
        this.setContentView(2130903097);
        this.setVolumeControlStream(3);
        (this.btnBack = (Button)this.findViewById(2131231086)).setOnClickListener(this);
        (this.btnPowerSwitch = (Button)this.findViewById(2131231088)).setOnClickListener(this);
        this.signal = (ImageView)this.findViewById(2131231089);
        this.tvTitle = (TextView)this.findViewById(2131231087);
        this.viewPager = (MyViewPager)this.findViewById(2131231090);
        this.tabRemote = (ImageView)this.findViewById(2131231091);
        this.tabFavorite = (ImageView)this.findViewById(2131231092);
        this.tabCommon = (ImageView)this.findViewById(2131231093);
        this.tabRemote.setOnClickListener(this);
        this.tabFavorite.setOnClickListener(this);
        this.tabCommon.setOnClickListener(this);
        this.remoteControl = new STBFragment();
        this.fragmentsList.add(this.remoteControl);
        this.favorite = new FavoriteChFragment();
        this.fragmentsList.add(this.favorite);
        this.commonch = new CommonChFragment();
        this.fragmentsList.add(this.commonch);
        this.viewPager.setAdapter(new MyFragmentPagerAdapter(this.getSupportFragmentManager(), this.fragmentsList));
        this.viewPager.setScrollEnabled(true);
        this.viewPager.setOnPageChangeListener((ViewPager.OnPageChangeListener)this);
        this.pagerChange(Util.getIntValue("index"));
        this.btnPowerSwitch.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View inflate) {
                inflate = View.inflate(STBActivity.this.context, 2130903101, (ViewGroup)null);
                final EditText editText = (EditText)inflate.findViewById(2131231138);
                editText.setVisibility(8);
                final Button button = (Button)inflate.findViewById(2131231139);
                STBActivity.access$3(STBActivity.this, (TextView)inflate.findViewById(2131231142));
                STBActivity.access$4(STBActivity.this, (ProgressBar)inflate.findViewById(2131231141));
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(final View view) {
                        new BroadcastHelper().writeData(new Out_2EH().make(STBFragment.tvDevice.devType, (byte)16));
                        STBActivity.this.progressBar.setVisibility(0);
                        STBActivity.this.learnCodeTextView.setVisibility(0);
                        STBActivity.this.learnCodeTextView.setText((CharSequence)STBActivity.this.getString(2131296306));
                    }
                });
                final AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder(STBActivity.this.context);
                alertDialog$Builder.setTitle((CharSequence)STBActivity.this.getString(2131296309));
                alertDialog$Builder.setView(inflate);
                alertDialog$Builder.setPositiveButton((CharSequence)STBActivity.this.getString(2131296285), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        if (editText.getText().toString().equals("")) {
                            return;
                        }
                    }
                });
                alertDialog$Builder.show();
                return true;
            }
        });
        this.lbcManager = LocalBroadcastManager.getInstance((Context)this);
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.gk.wifictr.net");
        this.lbcManager.registerReceiver(this.receiver, intentFilter);
        this.linkState();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.lbcManager.unregisterReceiver(this.receiver);
        CommonChFragment.gvLayoutState = 8;
    }
    
    @Override
    public boolean onKeyDown(final int n, final KeyEvent keyEvent) {
        if (n != 4) {
            return super.onKeyDown(n, keyEvent);
        }
        if (CommonChFragment.gvLayoutState == 0 && this.currIndex == 2) {
            this.commonch.setGridViewGone();
            return true;
        }
        return super.onKeyDown(n, keyEvent);
    }
    
    public void onPageScrollStateChanged(final int n) {
    }
    
    public void onPageScrolled(final int n, final float n2, final int n3) {
    }
    
    public void onPageSelected(final int n) {
        this.pagerChange(n);
    }
    
    protected void setBackground(final ImageView imageView, final int backgroundResource) {
        imageView.setBackgroundResource(backgroundResource);
    }
}
