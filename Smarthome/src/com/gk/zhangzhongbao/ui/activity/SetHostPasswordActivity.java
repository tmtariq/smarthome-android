package com.gk.zhangzhongbao.ui.activity;

import android.widget.*;
import com.gk.wifictr.net.command.*;
import android.os.*;
import android.view.*;
import com.gk.wifictr.net.*;
import com.gk.wifictr.net.command.station.*;

public class SetHostPasswordActivity extends BaseViewActivity
{
    private boolean isSet;
    private EditText neweEditText;
    private Button okButton;
    private EditText oldEditText;
    
    public SetHostPasswordActivity() {
        this.isSet = false;
    }
    
    static /* synthetic */ void access$2(final SetHostPasswordActivity setHostPasswordActivity, final boolean isSet) {
        setHostPasswordActivity.isSet = isSet;
    }
    
    @Override
    protected void decodeData(final ReadData readData) {
        super.decodeData(readData);
        if (readData.type == 18 && this.isSet && readData.goodData == 0) {
            this.showToast(this.getString(2131296447));
            ConnectService.defWifi.password = this.neweEditText.getText().toString();
            ConnectService.defWifi.save(ConnectService.defWifinum);
            this.finish();
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText((CharSequence)this.getString(2131296416));
        this.setCenterView(2130903089);
        this.oldEditText = (EditText)this.findViewById(2131231046);
        this.neweEditText = (EditText)this.findViewById(2131231047);
        this.okButton = (Button)this.findViewById(2131231048);
        this.oldEditText.setText((CharSequence)ConnectService.defWifi.password);
        this.okButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                final String string = SetHostPasswordActivity.this.oldEditText.getText().toString();
                final String string2 = SetHostPasswordActivity.this.neweEditText.getText().toString();
                if (!ConnectService.defWifi.password.equals("")) {
                    if (string.equals("")) {
                        SetHostPasswordActivity.this.showToast(SetHostPasswordActivity.this.getString(2131296444));
                    }
                    else {
                        if (!string.equals(ConnectService.defWifi.password)) {
                            SetHostPasswordActivity.this.showToast(SetHostPasswordActivity.this.getString(2131296445));
                            return;
                        }
                        if (string2.equals("")) {
                            SetHostPasswordActivity.this.showToast(SetHostPasswordActivity.this.getString(2131296446));
                            return;
                        }
                        new BroadcastHelper().writeData(new Out_20H().make(string2));
                        SetHostPasswordActivity.access$2(SetHostPasswordActivity.this, true);
                    }
                }
            }
        });
    }
}
