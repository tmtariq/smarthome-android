package com.gk.zhangzhongbao.ui.activity;

import com.gk.wifictr.net.command.*;
import com.gk.zhangzhongbao.ui.app.*;

import android.view.*;

import com.zijunlin.Zxing.Demo.CaptureActivity;

import android.content.*;

import com.gk.wifictr.net.*;
import com.gk.wifictr.net.command.station.*;

import android.app.*;
import android.widget.*;
import android.os.*;

public class AddCurtainGarageActivity extends BaseViewActivity implements View.OnClickListener
{
    public static Handler handler;
    private int CURTAIN_H;
    private int CURTAIN_V;
    private int GARAGE_H;
    private int GARAGE_V;
    private Button cancle;
    private Context context;
    private ImageView curtain;
    private byte devStatus;
    private int deviceType;
    private EditText edtName;
    private EditText edtSnCode;
    private ImageView garage_h;
    private ImageView garage_v;
    private Button imgCamera;
    private int learnCodeDev;
    private Button searchButton;
    private String snCode;
    private Button sure;
    private boolean sureAdd;
    
    static {
        AddCurtainGarageActivity.handler = null;
    }
    
    public AddCurtainGarageActivity() {
        this.context = (Context)this;
        this.deviceType = -1;
        this.snCode = "";
        this.sureAdd = false;
        this.CURTAIN_V = 0;
        this.CURTAIN_H = 1;
        this.GARAGE_V = 2;
        this.GARAGE_H = 3;
        this.learnCodeDev = 0;
    }
    
    static /* synthetic */ void access$1(AddCurtainGarageActivity addCurtainGarageActivity, String snCode) {
        addCurtainGarageActivity.snCode = snCode;
    }
    
    private void exitDlg() {
        String string = this.edtName.getText().toString();
        String string2 = this.edtSnCode.getText().toString();
        if (string.equals("") && string2.equals("") && this.deviceType < 0) {
            super.onBackPressed();
            return;
        }
        AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder((Context)this);
        alertDialog$Builder.setTitle((CharSequence)this.getString(2131296284));
        alertDialog$Builder.setMessage((CharSequence)this.getString(2131296380));
        alertDialog$Builder.setPositiveButton((CharSequence)this.getString(2131296285), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int n) {
                AddCurtainGarageActivity.this.finish();
            }
        });
        alertDialog$Builder.setNegativeButton((CharSequence)this.getString(2131296286), null);
        alertDialog$Builder.show();
    }
    
    protected void decodeData(ReadData readData) {
        super.decodeData(readData);
        if (readData.type == 19 && this.sureAdd) {
            if (!readData.ERR_OPERATION_FAILURE) {
                Util.toast(this.context, this.getString(2131296376));
                this.finish();
                return;
            }
            this.showToast(this.getString(2131296404));
        }
        else if (readData.type == 23) {
            In_17H in_17H = new In_17H(readData.body);
            if (in_17H.isGoodSubSn) {
                this.edtSnCode.setText((CharSequence)in_17H.subsn);
                this.showToast(this.getString(2131296377));
            }
            else {
                this.showToast(this.getString(2131296378));
            }
            Util.loadingDlgDismiss();
        }
    }
    
    public void onBackPressed() {
        this.exitDlg();
    }
    
    public void onClick(View view) {
        switch (view.getId()) {
            case 2131230751: {
                this.type(1);
                break;
            }
            case 2131230752: {
                this.type(2);
                break;
            }
            case 2131230753: {
                this.type(3);
                break;
            }
            case 2131230757: {
                Intent intent = new Intent((Context)this, CaptureActivity.class);
                intent.putExtra("from_which_acticity", 3);
                this.startActivity(intent);
                break;
            }
            case 2131230759: {
                String string = this.edtName.getText().toString();
                if (string.equals("")) {
                    this.showToast(this.getString(2131296398));
                    return;
                }
                if (this.deviceType < 0) {
                    this.showToast(this.getString(2131296399));
                    return;
                }
                if (this.learnCodeDev == 0) {
                    this.snCode = this.edtSnCode.getText().toString();
                    if (this.snCode.equals("")) {
                        this.showToast(this.getString(2131296400));
                        return;
                    }
                    if (this.snCode.length() != 6) {
                        this.showToast(this.getString(2131296401));
                        return;
                    }
                }
                if (this.learnCodeDev > 0) {
                    if (this.learnCodeDev == 1) {
                        new BroadcastHelper().writeData(new Out_23H().make(string, "000000", (byte)96, this.devStatus));
                    }
                    else if (this.learnCodeDev == 2) {
                        new BroadcastHelper().writeData(new Out_23H().make(string, "000000", (byte)97, this.devStatus));
                    }
                }
                else {
                    new BroadcastHelper().writeData(new Out_23H().make(string, this.snCode, (byte)80, this.devStatus));
                }
                this.sureAdd = true;
                break;
            }
            case 2131230760: {
                this.exitDlg();
                break;
            }
            case 2131230758: {
                new BroadcastHelper().writeData(new Out_56H().make((byte)80));
                Util.createLoadingDialog(this.context, this.getString(2131296379), null, null);
                break;
            }
        }
    }
    
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.outer = (LinearLayout)this.setCenterView(2130903043);
        this.title.setText((CharSequence)this.getString(2131296393));
        this.btnLeft.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(View view) {
                AddCurtainGarageActivity.this.exitDlg();
            }
        });
        AddCurtainGarageActivity.handler = new AddCurtainHandler(Looper.myLooper());
        Util.setButton(this, this.sure, 2131230759, (View.OnClickListener)this);
        Util.setButton(this, this.cancle, 2131230760, (View.OnClickListener)this);
        this.searchButton = Util.setButton(this, 2131230758, (View.OnClickListener)this);
        this.edtName = (EditText)this.findViewById(2131230748);
        this.curtain = (ImageView)this.findViewById(2131230751);
        this.garage_v = (ImageView)this.findViewById(2131230752);
        this.garage_h = (ImageView)this.findViewById(2131230753);
        this.edtSnCode = (EditText)this.findViewById(2131230755);
        this.imgCamera = (Button)this.findViewById(2131230757);
        this.curtain.setOnClickListener((View.OnClickListener)this);
        this.garage_v.setOnClickListener((View.OnClickListener)this);
        this.garage_h.setOnClickListener((View.OnClickListener)this);
        this.imgCamera.setOnClickListener((View.OnClickListener)this);
        this.learnCodeDev = this.getIntent().getIntExtra("learn_code_dev", 0);
        if (this.learnCodeDev > 0) {
            ((RelativeLayout)this.findViewById(2131230754)).setVisibility(8);
            ((RelativeLayout)this.findViewById(2131230756)).setVisibility(8);
            ((Button)this.findViewById(2131230758)).setVisibility(8);
        }
    }
    
    public void type(int n) {
        switch (n) {
            case 1: {
                this.curtain.setImageResource(2130837995);
                this.garage_v.setImageResource(2130837994);
                this.garage_h.setImageResource(2130837994);
                this.deviceType = 1;
                this.devStatus = 17;
                break;
            }
            case 2: {
                this.garage_v.setImageResource(2130837995);
                this.garage_h.setImageResource(2130837994);
                this.curtain.setImageResource(2130837994);
                this.deviceType = 2;
                this.devStatus = -95;
                break;
            }
            case 3: {
                this.garage_h.setImageResource(2130837995);
                this.garage_v.setImageResource(2130837994);
                this.curtain.setImageResource(2130837994);
                this.deviceType = 3;
                this.devStatus = -111;
                break;
            }
        }
    }
    
    protected class AddCurtainHandler extends Handler
    {
        Looper looper;
        
        public AddCurtainHandler(Looper looper) {
            this.looper = looper;
        }
        
        public void handleMessage(Message message) {
            switch (message.what) {
                case 10: {
                    if (message.obj == null) {
                        break;
                    }
                    String s = (String)message.obj;
                    if (!s.substring(0, 2).equals("XY")) {
                        Util.toast(AddCurtainGarageActivity.this.context, AddCurtainGarageActivity.this.getString(2131296402));
                        return;
                    }
                    s.charAt(2);
                    s.charAt(3);
                    AddCurtainGarageActivity.access$1(AddCurtainGarageActivity.this, s.substring(4, s.length()));
                    AddCurtainGarageActivity.this.edtSnCode.setText((CharSequence)AddCurtainGarageActivity.this.snCode);
                    AddCurtainGarageActivity.this.type(AddCurtainGarageActivity.this.deviceType);
                    break;
                }
            }
        }
    }
}
