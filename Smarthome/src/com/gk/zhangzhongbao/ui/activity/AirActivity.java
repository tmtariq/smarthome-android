package com.gk.zhangzhongbao.ui.activity;

import android.R.integer;
import android.content.*;

import com.gk.zhangzhongbao.ui.view.*;
import com.gk.wifictr.net.data.*;

import android.widget.*;
import android.view.*;
import android.app.*;

import com.gk.wifictr.net.*;

import android.util.*;

import com.gk.zhangzhongbao.ui.app.*;
import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.command.station.*;

import java.util.*;

import android.os.*;

public class AirActivity extends BaseViewActivity
{
    public static String AIR_BRAND_NAME = "air_brand_name";
    public static String AIR_BRAND_POSITION = "air_brand_index";
    public static String AIR_DEVNO = "air_devno";
    public static String AIR_POWER_ON = "air_power_on";
    public static String AIR_SN = "air_sn";
    public static String MODE = "air_mode";
    public static String TEMPRERATURE = "temprerature";
    public static String VOLUME = "air_volume";
    public static String WIND_AUTO = "air_wind_auto";
    public static String WIND_DIRECTION_AUTO = "air_wind_direction_auto";
    public static String WIND_HAND = "air_wind_hand";
    private List<Map<String, Object>> ablist;
    private byte[] accode;
    private SelectorImageView airDown;
    private TextView airMode;
    private boolean airPowerOn;
    private SelectorImageView airUp;
    private Button brand;
    String brandString;
    int code;
    String codeHex;
    private Context context;
    private boolean dfrOn;
    BrandDialog dialog;
    private ImageView displayBg;
    int end;
    private boolean lightOn;
    private View.OnClickListener mClickListener;
    private SubDevice mSubDevice;
    PopupWindow popupWindow;
    private int position;
    private ImageView powerOffImg;
    private boolean sleepOn;
    int start;
    private boolean strongOn;
    private int[] tempeImgs;
    private ImageView temprerature;
    private boolean thriftyOn;
    private ImageView time;
    private int[] timeImgs;
    private ImageView volum;
    private TextView volumAuto;
    private ImageView windDir;
    
    public AirActivity() {
        context = this;
        sleepOn = false;
        dfrOn = false;
        strongOn = false;
        lightOn = false;
        thriftyOn = false;
        accode = new byte[14];
        tempeImgs = new int[] { 2130837647, 2130837648, 2130837649, 2130837650, 2130837651, 2130837652, 2130837653, 2130837654, 2130837655, 2130837656, 2130837657, 2130837658, 2130837659, 2130837660, 2130837661, 2130837662, 2130837663 };
        timeImgs = new int[] { 2130837543, 2130837544, 2130837545, 2130837546, 2130837547, 2130837548, 2130837549, 2130837550, 2130837551, 2130837552 };
        mClickListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(View view) {
                Util.vibrator(context);
                Util.clickBeepSound(context);
                switch (view.getId()) {
                    case 2131230804: {
                        ++accode[1];
                        if (accode[1] >= tempeImgs.length) {
                            accode[1] = 16;
                        }
                        setTemperature(accode[1]);
                        Util.saveValue("temprerature", accode[1]);
                        send(9, 1);
                        break;
                    }
                    case 2131230805: {
                        --accode[1];
                        if (accode[1] < 0) {
                            accode[1] = 0;
                        }
                        setTemperature(accode[1]);
                        Util.saveValue("temprerature", accode[1]);
                        send(9, 2);
                        break;
                    }
                }
            }
        };
        codeHex = "";
        brandString = "";
    }
    
    static /* synthetic */ void access$6(AirActivity airActivity, int position) {
        airActivity.position = position;
    }
    
    private void initMode() {
        switch (accode[0]) {
            case 0: {
                airMode.setText(getString(2131296353));
                break;
            }
            case 1: {
                airMode.setText(getString(2131296354));
                break;
            }
            case 2: {
                airMode.setText(getString(2131296355));
                break;
            }
            case 3: {
                airMode.setText(getString(2131296356));
                break;
            }
            case 4: {
                airMode.setText(getString(2131296357));
                break;
            }
        }
    }
    
    private void initView() {
        powerOffImg = (ImageView)findViewById(2131230801);
        if (airPowerOn) {
            powerOffImg.setVisibility(8);
        }
        else {
            powerOffImg.setVisibility(0);
        }
        (displayBg = (ImageView)findViewById(2131230788)).setImageResource(2130837520);
        temprerature = (ImageView)findViewById(2131230795);
        setTemperature(accode[1]);
        airMode = (TextView)findViewById(2131230799);
        time = (ImageView)findViewById(2131230800);
        volum = (ImageView)findViewById(2131230791);
        volumAuto = (TextView)findViewById(2131230792);
        windDir = (ImageView)findViewById(2131230790);
        if (Util.getBooleanValue("air_wind_direction_auto")) {
            windDir.setImageResource(2130838538);
        }
        else {
            windDir.setImageResource(2130838539);
        }
        setSelectorImgView(airUp, this, 2131230804, 2130837560, 2130837561, mClickListener);
        setSelectorImgView(airDown, this, 2131230805, 2130837521, 2130837522, mClickListener);
        brand = (Button)findViewById(2131230819);
    }
    
    private void initVolume() {
        switch (accode[2]) {
            case 0: {
                volumAuto.setVisibility(0);
                volum.setVisibility(4);
                break;
            }
            case 1: {
                volumAuto.setVisibility(4);
                volum.setVisibility(0);
                volum.setImageResource(2130838535);
                break;
            }
            case 2: {
                volumAuto.setVisibility(4);
                volum.setVisibility(0);
                volum.setImageResource(2130838536);
                break;
            }
            case 3: {
                volumAuto.setVisibility(4);
                volum.setVisibility(0);
                volum.setImageResource(2130838537);
                break;
            }
        }
    }
    
    private void powerOn(String s) {
        mSubDevice.devSN = Util.codeToSn(code);
        powerOffImg.setVisibility(8);
        Util.saveValue("air_power_on", Boolean.valueOf(true));
        accode[10] |= 0x1;
        send(s, 9, 0);
    }
    
    private void send(int n, int n2) {
        accode[n] = (byte)n2;
        new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(mSubDevice.devNo, mSubDevice.devName, mSubDevice.devSN, mSubDevice.devType), (byte)1, accode));
    }
    
    private void send(String s, int n, int n2) {
        accode[n] = (byte)n2;
        new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(mSubDevice.devNo, mSubDevice.devName, s, mSubDevice.devType), (byte)1, accode));
    }
    
    private void setTemperature(int n) {
        Log.e("index", new StringBuilder(String.valueOf(n)).toString());
        if (n >= 0 && n < tempeImgs.length) {
            temprerature.setImageResource(tempeImgs[n]);
        }
    }
    
    private void showDlg() {
        if (dialog == null) {
            ablist = new ArrayList<Map<String, Object>>();
            for (int i = 0; i < Brands.brandsString.length; ++i) {
                HashMap<String, Object> hashMap = new HashMap<String, Object>();
                hashMap.put("brand", Brands.brandsString[i]);
                hashMap.put("start", Brands.brandCodes[i][0]);
                hashMap.put("end", Brands.brandCodes[i][1]);
                hashMap.put("code", Brands.brandCodes[i][0]);
                ablist.add((Map<String, Object>)hashMap);
            }
            (dialog = new BrandDialog(context, ablist, 2131361809)).setListener((BrandDialog.ClickListener)new BrandDialog.ClickListener() {
                @Override
                public void itemClick(int n) {
                    Map<String, Object> map = ablist.get(n);
                    brandString = (String)map.get("brand");
                    start = Integer.parseInt((String)map.get("start"));
                    end = Integer.parseInt((String)map.get("end"));
                    code = Integer.parseInt((String)map.get("code"));
                    Log.e("code", new StringBuilder(String.valueOf(code)).toString());
                    dialog.brandName.setText(brandString);
                    dialog.brandNum.setText(new StringBuilder(String.valueOf(code - start + 1)).toString());
                    //AirActivity.access$6(AirActivity.this, n);
                    position = n;
                    powerOn(Util.codeToSn(code));
                }
                
                @Override
                public void leftClick() {
                    //AirActivity this$0 = AirActivity.this;
                    --code;
                    if (code < start) {
                        code = end;
                    }
                    dialog.brandNum.setText(new StringBuilder(String.valueOf(code - start + 1)).toString());
                    powerOn(Util.codeToSn(code));
                }
                
                @Override
                public void okClick() {
                    powerOn(Util.codeToSn(code));
                    mSubDevice.devSN = Util.codeToSn(code);
                    mSubDevice.devName = brandString;
                    dialog.dismiss();
                    new BroadcastHelper().writeData(new Out_25H().make(mSubDevice));
                }
                
                @Override
                public void rightClick() {
                    //AirActivity this$0 = AirActivity.this;
                    ++code;
                    if (code > end) {
                        code = start;
                    }
                    dialog.brandNum.setText(new StringBuilder(String.valueOf(code - start + 1)).toString());
                    Log.e("hex", Util.codeToSn(code));
                    powerOn(Util.codeToSn(code));
                }
            });
        }
        dialog.show();
        Map<String, Object> map = dialog.ablist.get(position);
        brandString = (String)map.get("brand");
        start = Integer.parseInt((String)map.get("start"));
        end = Integer.parseInt((String)map.get("end"));
        code = Integer.parseInt((String)map.get("code"));
        dialog.brandNum.setText(new StringBuilder(String.valueOf(code - start + 1)).toString());
        dialog.brandName.setText(brandString);
    }
    
    @Override
    protected void decodeData(ReadData readData) {
        super.decodeData(readData);
        if (readData.type != 22 && readData.type == 19) {
            for (SubDevice mSubDevice : new In_13H().decode(readData)) {
                if (mSubDevice.devType == 6) {
                    this.mSubDevice = mSubDevice;
                    brand.setText(mSubDevice.devName);
                    Util.saveValue("air_sn", mSubDevice.devSN);
                    Util.saveValue("air_brand_name", mSubDevice.devName);
                    Util.saveValue("air_devno", mSubDevice.devNo);
                    for (int i = 0; i < Brands.brandsString.length; ++i) {
                        if (Brands.brandsString[i].equals(mSubDevice.devName)) {
                            Util.saveValue("air_brand_index", position = i);
                        }
                    }
                }
            }
        }
    }
    
    public void onClick(View view) {
        boolean b = false;
        boolean b2 = false;
        boolean b3 = false;
        boolean b4 = false;
        boolean sleepOn = false;
        Util.vibrator(context);
        Util.clickBeepSound(context);
        switch (view.getId()) {
            case 2131230802: {
                powerOffImg.setVisibility(0);
                airPowerOn = false;
                Util.saveValue("air_power_on", Boolean.valueOf(airPowerOn));
                accode[10] &= (byte)254;
                send(9, 0);
                break;
            }
            case 2131230803: {
                powerOffImg.setVisibility(8);
                airPowerOn = true;
                Util.saveValue("air_power_on", Boolean.valueOf(airPowerOn));
                accode[10] |= 0x1;
                send(9, 0);
                break;
            }
            case 2131230806: {
                ++accode[0];
                if (accode[0] > 4) {
                    accode[0] = 0;
                }
                initMode();
                Util.saveValue("air_mode", accode[0]);
                send(9, 3);
                break;
            }
            case 2131230807: {
                ++accode[2];
                if (accode[2] > 3) {
                    accode[2] = 0;
                }
                Util.saveValue("air_volume", accode[2]);
                initVolume();
                send(9, 4);
                break;
            }
            case 2131230808: {
                windDir.setImageResource(2130838538);
                ++accode[4];
                if (accode[4] > 8) {
                    accode[4] = 0;
                }
                Util.saveValue("air_wind_direction_auto", Boolean.valueOf(true));
                Util.saveValue("air_wind_auto", accode[4]);
                send(9, 5);
                break;
            }
            case 2131230809: {
                windDir.setImageResource(2130838539);
                ++accode[5];
                if (accode[5] > 8) {
                    accode[5] = 0;
                }
                Util.saveValue("air_wind_direction_auto", Boolean.valueOf(false));
                Util.saveValue("air_wind_auto", accode[5]);
                send(9, 6);
                break;
            }
            case 2131230813: {
                accode[11] = 1;
                accode[8] = 0;
                ++accode[7];
                if (accode[7] > 9) {
                    accode[7] = 0;
                }
                time.setImageResource(timeImgs[accode[7]]);
                send(9, 7);
                break;
            }
            case 2131230814: {
                accode[7] = 0;
                ++accode[8];
                if (accode[8] > 9) {
                    accode[8] = 0;
                }
                time.setImageResource(timeImgs[accode[8]]);
                send(9, 8);
                break;
            }
            case 2131230812: {
                if (!sleepOn) {
                    sleepOn = true;
                }
                this.sleepOn = sleepOn;
                if (sleepOn) {
                    accode[10] &= (byte)253;
                }
                else {
                    accode[10] |= 0x2;
                }
                send(9, 10);
                break;
            }
            case 2131230817: {
                dfrOn = (!dfrOn || b);
                if (dfrOn) {
                    accode[10] &= (byte)251;
                }
                else {
                    accode[10] |= 0x4;
                }
                send(9, 11);
                break;
            }
            case 2131230810: {
                strongOn = (!strongOn || b2);
                if (strongOn) {
                    accode[10] &= (byte)239;
                }
                else {
                    accode[10] |= 0x10;
                }
                send(9, 12);
                break;
            }
            case 2131230816: {
                lightOn = (!lightOn || b3);
                if (lightOn) {
                    accode[10] &= (byte)223;
                }
                else {
                    accode[10] |= 0x20;
                }
                send(9, 13);
                break;
            }
            case 2131230811: {
                thriftyOn = (!thriftyOn || b4);
                if (thriftyOn) {
                    accode[10] &= 0x7F;
                }
                else {
                    accode[10] |= (byte)128;
                }
                send(9, 15);
                break;
            }
            case 2131230819: {
                showDlg();
                break;
            }
        }
    }
    
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        title.setText(getString(2131296310));
        setCenterView(2130903048);
        for (int i = 0; i < accode.length; ++i) {
            accode[i] = 0;
        }
        accode[0] = (byte)Util.getIntValue("air_mode", 0);
        accode[1] = (byte)Util.getIntValue("temprerature", 9);
        accode[2] = (byte)Util.getIntValue("air_volume", 0);
        accode[4] = (byte)Util.getIntValue("air_wind_auto", 0);
        accode[5] = (byte)Util.getIntValue("air_wind_hand", 0);
        airPowerOn = Util.getBooleanValue("air_power_on");
        if (airPowerOn) {
            accode[10] |= 0x1;
        }
        else {
            accode[10] &= (byte)254;
        }
        initView();
        initMode();
        initVolume();
        linkState();
        reflash();
        mSubDevice = new SubDevice(Util.getIntValue("air_devno"), Util.getStringValue("air_brand_name", getString(2131296373)), Util.getStringValue("air_sn", "010000"), (byte)6);
        brandString = mSubDevice.devName;
        brand.setText(mSubDevice.devName);
        position = Util.getIntValue("air_brand_index");
    }
    
    @Override
    protected void stateChange() {
        super.stateChange();
    }
}
