package com.gk.zhangzhongbao.ui.activity;

import com.gk.zhangzhongbao.ui.view.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.view.*;
import android.widget.*;
import android.content.*;
import android.text.*;
import com.gk.wifictr.net.*;
import com.gk.wifictr.net.command.station.*;
import android.os.*;
import android.app.*;

public class GaojiActivity extends BaseViewActivity implements View.OnClickListener
{
    private ImageView clickApppw;
    private ImageView clickSound;
    private ImageView clickStbSound;
    private ImageView clickVibrator;
    private boolean isApMode;
    private LockPatternUtils lockPatternUtils;
    private RelativeLayout reset;
    private RelativeLayout setAppPass;
    
    private void setLocalMode(final boolean b) {
        if (b) {
            ConnectService.useAP = true;
            return;
        }
        ConnectService.useAP = false;
    }
    
    public void onClick(final View view) {
        final boolean b = false;
        final boolean b2 = false;
        boolean isSound = false;
        switch (view.getId()) {
            default: {}
            case 2131230874: {
                if (!Util.isSound) {
                    isSound = true;
                }
                Util.isSound = isSound;
                if (Util.isSound) {
                    this.clickSound.setImageResource(2130838258);
                    Util.clickSound((Context)this);
                }
                else {
                    this.clickSound.setImageResource(2130838257);
                }
                Util.saveValue("click_sound", Boolean.valueOf(Util.isSound));
            }
            case 2131230877: {
                Util.isStbSound = (!Util.isStbSound || b);
                if (Util.isStbSound) {
                    this.clickStbSound.setImageResource(2130838258);
                    Util.clickBeepSound((Context)this);
                }
                else {
                    this.clickStbSound.setImageResource(2130838257);
                }
                Util.saveValue("click_stb_sound", Boolean.valueOf(Util.isStbSound));
            }
            case 2131230880: {
                Util.isVibrator = (!Util.isVibrator || b2);
                if (Util.isVibrator) {
                    this.clickVibrator.setImageResource(2130838258);
                    Util.vibrator((Context)this);
                }
                else {
                    this.clickVibrator.setImageResource(2130838257);
                }
                Util.saveValue("click_vibrator", Boolean.valueOf(Util.isVibrator));
            }
            case 2131230883: {
                if (this.lockPatternUtils.getLockPaternString().equals("")) {
                    this.clickApppw.setImageResource(2130838258);
                    this.startActivity(new Intent((Context)this, (Class)SetAppPassActivity.class));
                    this.setAppPass.setOnClickListener((View.OnClickListener)this);
                    return;
                }
                this.clickApppw.setImageResource(2130838257);
                this.lockPatternUtils.clearLock();
                this.setAppPass.setClickable(false);
            }
            case 2131230884: {
                final LinearLayout view2 = new LinearLayout((Context)this);
                view2.setPadding(Util.dip((Context)this, 6.0f), 0, Util.dip((Context)this, 6.0f), 0);
                final TextView textView = new TextView((Context)this);
                textView.setTextColor(-1);
                textView.setText((CharSequence)new StringBuilder(String.valueOf(Util.nextInt(1000, 9999))).toString());
                view2.addView((View)textView, (ViewGroup.LayoutParams)new LinearLayout.LayoutParams(-2, -2));
                final EditText editText = new EditText((Context)this);
                editText.setHint((CharSequence)this.getString(2131296448));
                editText.setInputType(2);
                view2.addView((View)editText, (ViewGroup.LayoutParams)new LinearLayout.LayoutParams(-1, -2));
                final AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder((Context)this);
                alertDialog$Builder.setTitle((CharSequence)this.getString(2131296449));
                alertDialog$Builder.setView((View)view2);
                alertDialog$Builder.setPositiveButton((CharSequence)this.getString(2131296285), (DialogInterface.OnClickListener)new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialogInterface, final int n) {
                        if (TextUtils.equals(textView.getText(), (CharSequence)editText.getText().toString())) {
                            new BroadcastHelper().writeData(new Out_2DH().make());
                            return;
                        }
                        GaojiActivity.this.showToast(GaojiActivity.this.getString(2131296450));
                    }
                });
                alertDialog$Builder.setNegativeButton((CharSequence)this.getString(2131296286), (DialogInterface.OnClickListener)null);
                alertDialog$Builder.show();
            }
            case 2131230881: {
                this.startActivity(new Intent((Context)this, (Class)SetAppPassActivity.class));
            }
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText((CharSequence)this.getString(2131296418));
        this.setCenterView(2130903064);
        Util.setView(this, (View)this.reset, 2131230884, this);
        (this.setAppPass = (RelativeLayout)this.findViewById(2131230881)).setOnClickListener(this);
        (this.clickSound = (ImageView)this.findViewById(2131230874)).setOnClickListener(this);
        (this.clickStbSound = (ImageView)this.findViewById(2131230877)).setOnClickListener(this);
        (this.clickVibrator = (ImageView)this.findViewById(2131230880)).setOnClickListener(this);
        (this.clickApppw = (ImageView)this.findViewById(2131230883)).setOnClickListener(this);
        this.setLocalMode(this.isApMode = ConnectService.useAP);
        if (Util.isSound) {
            this.clickSound.setImageResource(2130838258);
        }
        else {
            this.clickSound.setImageResource(2130838257);
        }
        if (Util.isStbSound) {
            this.clickStbSound.setImageResource(2130838258);
        }
        else {
            this.clickStbSound.setImageResource(2130838257);
        }
        if (Util.isVibrator) {
            this.clickVibrator.setImageResource(2130838258);
        }
        else {
            this.clickVibrator.setImageResource(2130838257);
        }
        this.lockPatternUtils = new LockPatternUtils((Context)this);
    }
    
    protected void onResume() {
        super.onResume();
    }
}
