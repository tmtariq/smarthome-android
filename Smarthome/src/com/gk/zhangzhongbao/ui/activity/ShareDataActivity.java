package com.gk.zhangzhongbao.ui.activity;

import android.view.*;
import android.content.*;
import android.os.*;

public class ShareDataActivity extends BaseViewActivity
{
    public void onClick(final View view) {
        final Intent intent = new Intent((Context)this, (Class)RecvSendDataActivity.class);
        switch (view.getId()) {
            case 2131231078: {
                intent.putExtra("recv_send", 1);
                break;
            }
            case 2131231079: {
                intent.putExtra("recv_send", 2);
                break;
            }
        }
        this.startActivity(intent);
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText((CharSequence)this.getString(2131296405));
        this.setCenterView(2130903094);
    }
}
