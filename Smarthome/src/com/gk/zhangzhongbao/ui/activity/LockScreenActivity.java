package com.gk.zhangzhongbao.ui.activity;

import com.gk.zhangzhongbao.ui.view.*;
import com.gk.zhangzhongbao.ui.view.LockPatternView.Cell;
import com.gk.zhangzhongbao.ui.view.LockPatternView.DisplayMode;

import android.widget.*;

import com.gk.wifictr.net.*;

import android.content.*;
import android.os.*;

import java.util.*;

public class LockScreenActivity extends BaseActivity
{
    private Context context;
    private LockPatternUtils lockPatternUtils;
    private LockPatternView lockPatternView;
    private boolean opFLag;
    private TextView textView;
    
    public LockScreenActivity() {
        this.context = (Context)this;
    }
    
    public void onBackPressed() {
        this.stopService(new Intent((Context)this, (Class)ConnectService.class));
        System.exit(0);
        super.onBackPressed();
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2130903080);
        this.textView = (TextView)this.findViewById(2131231019);
        this.lockPatternView = (LockPatternView)this.findViewById(2131231020);
        this.lockPatternUtils = new LockPatternUtils((Context)this);
        this.lockPatternView.setOnPatternListener(new LockPatternView.OnPatternListener() {
            @Override
            public void onPatternCellAdded(final List<Cell> list) {
            }
            
            @Override
            public void onPatternCleared() {
            }
            
            @Override
            public void onPatternDetected(final List<Cell> list) {
                final int checkPattern = LockScreenActivity.this.lockPatternUtils.checkPattern(list);
                if (checkPattern == 1) {
                    LockScreenActivity.this.startActivity(new Intent((Context)LockScreenActivity.this, (Class)HostListActivity.class));
                    LockScreenActivity.this.finish();
                    return;
                }
                if (checkPattern == 0) {
                    LockScreenActivity.this.lockPatternView.setDisplayMode(DisplayMode.Wrong);
                    return;
                }
                LockScreenActivity.this.lockPatternView.clearPattern();
                LockScreenActivity.this.showToast("\u8bf7\u8bbe\u7f6e\u5bc6\u7801");
            }
            
            @Override
            public void onPatternStart() {
                if (!LockScreenActivity.this.opFLag) {
                    LockScreenActivity.this.textView.setText((CharSequence)"\u5b8c\u6210\u65f6\u677e\u5f00\u624b\u6307");
                }
            }
        });
    }
}
