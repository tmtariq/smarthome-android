package com.gk.zhangzhongbao.ui.activity;

import com.gk.zhangzhongbao.ui.view.*;
import com.gk.zhangzhongbao.ui.view.LockPatternView.Cell;
import com.gk.zhangzhongbao.ui.view.LockPatternView.DisplayMode;

import java.util.*;

import android.widget.*;
import android.view.*;

import com.gk.zhangzhongbao.ui.app.*;

import android.content.*;
import android.os.*;

public class SetAppPassActivity extends BaseViewActivity implements View.OnClickListener
{
    private Button cancle;
    private Button clearPw;
    private LockPatternUtils lockPatternUtils;
    private LockPatternView lockPatternView;
    private List<LockPatternView.Cell> mPattern;
    private boolean opFLag;
    private Button setAppPassword;
    private TextView textView;
    
    public SetAppPassActivity() {
        this.opFLag = true;
    }
    
    static /* synthetic */ void access$4(final SetAppPassActivity setAppPassActivity, final boolean opFLag) {
        setAppPassActivity.opFLag = opFLag;
    }
    
    static /* synthetic */ void access$5(final SetAppPassActivity setAppPassActivity, final List mPattern) {
        setAppPassActivity.mPattern = (List<LockPatternView.Cell>)mPattern;
    }
    
    public void onClick(final View view) {
        if (view == this.setAppPassword) {
            if (this.mPattern != null) {
                this.lockPatternUtils.saveLockPattern(this.mPattern);
                this.showToast(this.getString(2131296463));
                this.finish();
            }
        }
        else {
            if (view == this.cancle) {
                this.finish();
                return;
            }
            if (view == this.clearPw) {
                Util.alertDlg((Context)this, this.getString(2131296284), this.getString(2131296464), new Runnable() {
                    @Override
                    public void run() {
                        SetAppPassActivity.access$4(SetAppPassActivity.this, false);
                        SetAppPassActivity.this.textView.setText((CharSequence)SetAppPassActivity.this.getString(2131296457));
                        SetAppPassActivity.this.lockPatternUtils.clearLock();
                        SetAppPassActivity.this.lockPatternView.clearPattern();
                        SetAppPassActivity.this.showToast(SetAppPassActivity.this.getString(2131296465));
                    }
                });
            }
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText((CharSequence)this.getString(2131296454));
        this.setCenterView(2130903088);
        this.setAppPassword = (Button)this.findViewById(2131231043);
        this.cancle = (Button)this.findViewById(2131231044);
        this.clearPw = (Button)this.findViewById(2131231045);
        this.setAppPassword.setOnClickListener((View.OnClickListener)this);
        this.cancle.setOnClickListener(this);
        this.clearPw.setOnClickListener(this);
        this.textView = (TextView)this.findViewById(2131231041);
        this.lockPatternView = (LockPatternView)this.findViewById(2131231042);
        this.lockPatternUtils = new LockPatternUtils((Context)this);
        if (this.lockPatternUtils.getLockPaternString().equals("")) {
            this.opFLag = false;
            this.textView.setText((CharSequence)this.getString(2131296457));
        }
        else {
            this.opFLag = true;
            this.textView.setText((CharSequence)this.getString(2131296458));
        }
        this.lockPatternView.setOnPatternListener(new LockPatternView.OnPatternListener() {
            @Override
            public void onPatternCellAdded(List<Cell> list) {
            }
            
            @Override
            public void onPatternCleared() {
            }
            
            @Override
            public void onPatternDetected(List<Cell> list) {
                if (SetAppPassActivity.this.opFLag) {
                    final int checkPattern = SetAppPassActivity.this.lockPatternUtils.checkPattern(list);
                    if (checkPattern == 1) {
                        SetAppPassActivity.access$4(SetAppPassActivity.this, false);
                        SetAppPassActivity.this.textView.setText((CharSequence)SetAppPassActivity.this.getString(2131296457));
                        SetAppPassActivity.this.lockPatternView.clearPattern();
                        return;
                    }
                    if (checkPattern == 0) {
                        SetAppPassActivity.this.lockPatternView.setDisplayMode(DisplayMode.Wrong);
                        return;
                    }
                    SetAppPassActivity.this.lockPatternView.clearPattern();
                    SetAppPassActivity.this.showToast(SetAppPassActivity.this.getString(2131296460));
                }
                else {
                    if (list.size() > 3) {
                        SetAppPassActivity.access$5(SetAppPassActivity.this, list);
                        SetAppPassActivity.this.textView.setText((CharSequence)SetAppPassActivity.this.getString(2131296461));
                        return;
                    }
                    SetAppPassActivity.this.lockPatternView.setDisplayMode(DisplayMode.Wrong);
                    SetAppPassActivity.this.textView.setText((CharSequence)SetAppPassActivity.this.getString(2131296462));
                }
            }
            
            @Override
            public void onPatternStart() {
                if (!SetAppPassActivity.this.opFLag) {
                    SetAppPassActivity.this.textView.setText((CharSequence)SetAppPassActivity.this.getString(2131296459));
                }
            }
        });
    }
}
