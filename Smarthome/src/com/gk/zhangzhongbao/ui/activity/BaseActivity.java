package com.gk.zhangzhongbao.ui.activity;

import java.util.ArrayList;

import com.gk.wifictr.net.BroadcastHelper;
import com.gk.wifictr.net.ConnectService;
import com.gk.wifictr.net.command.ByteUtil;
import com.gk.wifictr.net.command.ReadData;
import com.gk.wifictr.net.command.station.In_42H;
import com.gk.wifictr.net.command.station.Out_26H;
import com.gk.zhangzhongbao.ui.app.AppManager;
import com.gk.zhangzhongbao.ui.app.MyApplication;
import com.gk.zhangzhongbao.ui.app.Util;
import com.gk.zhangzhongbao.ui.view.MyAlertDialog;
import com.gk.zhangzhongbao.ui.view.SelectorImageView;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;



public class BaseActivity extends Activity
{
    protected MyApplication app;
    EditText appDlgEdt;
    protected Bitmap background;
    protected ArrayList<Bitmap> bmpList;
    MyAlertDialog check_appDialog;
    protected MyAlertDialog dialog;
    protected boolean isToast;
    protected LocalBroadcastManager lbcManager;
    Bitmap mBitmap;
    protected Toast mToast;
    protected BroadcastReceiver receiver;
    
    public BaseActivity() {
        isToast = true;
        receiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                int int1 = intent.getExtras().getInt("cmd");
                Log.e("base activity receive", "cmd:" + int1);
                switch (int1) {
                    case 7: {
                        Log.e("log", intent.getStringExtra("log"));
                        break;
                    }
                    case 4: {
                        ReadData readData = (ReadData)intent.getParcelableExtra("data");
                        if (readData != null) {
                            Log.e("data body", ByteUtil.byte2hex(readData.body));
                            Log.e("type", ByteUtil.byte2hex(new byte[] { readData.type }));
                            if (readData.type == 66) {
                                if (new In_42H(readData).onlineType == 0) {
                                    showToast(getString(2131296271));
                                }
                            }
                            else if (readData.type == 16 || readData.type == 18 || readData.type == 19) {
                                notice(readData);
                            }
                            decodeData(readData);
                            return;
                        }
                        break;
                    }
                    case 0: {
                        stateChange();
                        break;
                    }
                    case 9: {
                        getSn(intent);
                        break;
                    }
                }
            }
        };
        mBitmap = null;
    }
    
    public void check_passwd() {
        final String stringValue = Util.getStringValue("app_password", "");
        if (!stringValue.equals("")) {
            if (check_appDialog == null) {
                appDlgEdt = (EditText)View.inflate(this, 2130903061, null);
                check_appDialog = new MyAlertDialog(this, 2131361809, new Runnable() {
                    @Override
                    public void run() {
                        if (appDlgEdt.getText().toString().equals(stringValue)) {
                            check_appDialog.dismiss();
                            return;
                        }
                        showToast("\u5bc6\u7801\u4e0d\u6b63\u786e");
                    }
                });
                appDlgEdt.setLayoutParams((ViewGroup.LayoutParams)new RelativeLayout.LayoutParams(-1, -2));
            }
            check_appDialog.show();
            check_appDialog.setTitle("\u8f93\u5165APP\u5bc6\u7801");
            check_appDialog.removeAllView();
            appDlgEdt.setText("");
            check_appDialog.addView((View)appDlgEdt);
        }
    }
    
    protected void decodeData(ReadData readData) {
    }
    
    protected int getResDimens(int n) {
        return (int)getResources().getDimension(n);
    }
    
    protected String getResString(int n) {
        return getResources().getString(n);
    }
    
    protected void getSn(Intent intent) {
    }
    
    protected boolean isLink() {
        return ConnectService.mConnectService.isLink();
    }
    
    protected void notice(ReadData readData) {
        if (!readData.ERR_CRC_WRONG) {
            if (readData.ERR_PASSWD_WRONG) {
                showToast(getString(2131296272));
            }
            else {
                if (readData.ERR_SN_WRONG) {
                    showToast(getString(2131296273));
                    return;
                }
                if (readData.ERR_VERSION_NO_WRONG) {
                    showToast(getString(2131296274));
                    return;
                }
                if (readData.ERR_OUTOF_MAXTCPLINK) {
                    showToast(getString(2131296275));
                    return;
                }
                if (readData.ERR_OPERATION_FAILURE) {
                    showToast(getString(2131296276));
                }
            }
        }
    }
    
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        app = (MyApplication)getApplication();
        bmpList = new ArrayList<Bitmap>();
        AppManager.getAppManager().addActivity(this);
        lbcManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.gk.wifictr.net");
        lbcManager.registerReceiver(receiver, intentFilter);
        setVolumeControlStream(3);
    }
    
    protected void onDestroy() {
        super.onDestroy();
        AppManager.getAppManager().finishActivity(this);
        recycle();
        System.gc();
        lbcManager.unregisterReceiver(receiver);
    }
    
    protected void onResume() {
        super.onResume();
    }
    
    protected void onStop() {
        super.onStop();
    }
    
    protected void recycle() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.e("", "recycle");
                    ArrayList<Object> list = new ArrayList<Object>();
                    for (Bitmap bitmap : bmpList) {
                        if (bitmap != null && !bitmap.isRecycled()) {
                            Log.e("", "recycle");
                            bitmap.recycle();
                            list.add(bitmap);
                        }
                    }
                    bmpList.removeAll(list);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }
    
    public void reflash() {
        new BroadcastHelper().writeData(new Out_26H().make());
    }
    
    protected void resycle(View view) {
        Log.e("", "resycle");
        BitmapDrawable bitmapDrawable = (BitmapDrawable)view.getBackground();
        view.setBackgroundResource(0);
        bitmapDrawable.setCallback((Drawable.Callback)null);
        bitmapDrawable.getBitmap().recycle();
    }
    
    protected void setImageView(ImageView imageView, Activity activity, int n, int n2) {
        if (imageView == null) {
            imageView = (ImageView)Util.setView(activity, n, null);
        }
        if (n2 > 0) {
            imageView.setImageBitmap(Util.readBitMap(activity, n2));
        }
    }
    
    protected void setSelectorImgView(SelectorImageView selectorImageView, Activity activity,  int n,  int n2,  int n3,  View.OnClickListener view$OnClickListener) {
        //SelectorImageView selectorImageView2 = selectorImageView;
        if (selectorImageView == null) {
            selectorImageView = (SelectorImageView)Util.setView(activity, n, view$OnClickListener);
        }
        selectorImageView.setRes(n2, n3);
        if (selectorImageView.bitmap_n != null) {
            bmpList.add(selectorImageView.bitmap_n);
        }
        if (selectorImageView.bitmap_p != null) {
            bmpList.add(selectorImageView.bitmap_p);
        }
    }
    
    protected void showToast(String s) {
        if (mToast != null) {
            mToast.cancel();
        }
        (mToast = Toast.makeText(this, s, 0)).show();
    }
    
    protected void stateChange() {
    }
}
