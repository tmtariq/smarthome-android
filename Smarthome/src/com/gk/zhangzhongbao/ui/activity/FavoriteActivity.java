package com.gk.zhangzhongbao.ui.activity;

import com.gk.zhangzhongbao.ui.bean.*;
import android.content.*;
import android.widget.*;
import com.gk.zhangzhongbao.ui.wheelview.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.view.*;
import com.gk.zhangzhongbao.ui.fragment.*;
import com.gk.wifictr.net.*;
import com.gk.wifictr.net.command.station.*;
import android.os.*;

public class FavoriteActivity extends BaseViewActivity implements View.OnClickListener
{
    private String adTime;
    private MyApplication app;
    private Button btnCancle;
    private Button btnDelete;
    private Button btnOk;
    private Channel channel;
    private Context context;
    private Handler handler;
    private ImageView imgChannel;
    private boolean isDelete;
    private TextView txtAdTime;
    
    public FavoriteActivity() {
        this.adTime = "";
        this.isDelete = false;
        this.handler = new Handler();
        this.context = (Context)this;
    }
    
    private void setAdTime(final int n, final int n2) {
        final LinearLayout linearLayout = new LinearLayout(this.context);
        linearLayout.setGravity(17);
        linearLayout.setOrientation(0);
        final WheelView wheelView = new WheelView(this.context);
        wheelView.setAdapter(new NumericWheelAdapter(0, 59));
        wheelView.setCyclic(true);
        wheelView.setLabel(this.getString(2131296265));
        wheelView.setCurrentItem(this.channel.min);
        wheelView.TEXT_SIZE = Util.sh / 100 * 4;
        linearLayout.addView((View)wheelView);
        final WheelView wheelView2 = new WheelView(this.context);
        wheelView2.setAdapter(new NumericWheelAdapter(0, 59));
        wheelView2.setCyclic(true);
        wheelView2.setLabel(this.getString(2131296266));
        wheelView2.setCurrentItem(this.channel.second);
        wheelView2.TEXT_SIZE = Util.sh / 100 * 4;
        linearLayout.addView((View)wheelView2);
        Util.alertDlg(this.context, this.getString(2131296296), null, (View)linearLayout, new Runnable() {
            @Override
            public void run() {
                FavoriteActivity.this.channel.hour = 0;
                FavoriteActivity.this.channel.min = wheelView.getCurrentItem();
                FavoriteActivity.this.channel.second = wheelView2.getCurrentItem();
                FavoriteActivity.this.setTime(FavoriteActivity.this.channel.hour, FavoriteActivity.this.channel.min, FavoriteActivity.this.channel.second);
            }
        });
    }
    
    private void setTime(final int n, final int n2, final int n3) {
        if (n < 10) {
            new StringBuilder("0").append(n).toString();
        }
        else {
            new StringBuilder().append(n).toString();
        }
        String s;
        if (n2 < 10) {
            s = "0" + n2;
        }
        else {
            s = new StringBuilder().append(n2).toString();
        }
        String s2;
        if (n3 < 10) {
            s2 = "0" + n3;
        }
        else {
            s2 = new StringBuilder().append(n3).toString();
        }
        this.txtAdTime.setText((CharSequence)(String.valueOf(s) + ":" + s2));
    }
    
    public void onClick(final View view) {
        switch (view.getId()) {
            case 2131230868: {
                if (this.channel != null) {
                    this.setAdTime(this.channel.hour, this.channel.min);
                    return;
                }
                break;
            }
            case 2131230870: {
                Util.alertDlg(this.context, this.getString(2131296284), this.getString(2131296289), new Runnable() {
                    @Override
                    public void run() {
                        FavoriteActivity.this.channel.setFavorite(false);
                        final Message message = new Message();
                        message.what = 2;
                        message.obj = FavoriteActivity.this.channel;
                        FavoriteChFragment.handler.sendMessage(message);
                        FavoriteActivity.this.finish();
                    }
                });
            }
            case 2131230848: {
                if (this.app.fChannel != null) {
                    this.app.fChannel.timerCancle();
                    this.app.fChannel = null;
                }
                if (this.channel != null) {
                    if (this.channel.min > 0 || this.channel.second > 0) {
                        this.channel.setAdTime(this.context, this.channel.hour, this.channel.min, this.channel.second, this.txtAdTime, this.handler);
                    }
                    final int[] chCodes = this.channel.getChCodes();
                    new BroadcastHelper().writeData(new Out_53H().make(this.app.stbDevice.devSN, (byte)1, (byte)this.channel.getBtnCode(chCodes[0]), (byte)this.channel.getBtnCode(chCodes[1]), (byte)this.channel.getBtnCode(chCodes[2]), this.channel.min, this.channel.second));
                    this.app.fChannel = this.channel;
                }
                this.finish();
            }
            case 2131230849: {
                this.finish();
            }
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText((CharSequence)this.getString(2131296317));
        this.setCenterView(2130903062);
        this.app = (MyApplication)this.getApplication();
        this.imgChannel = (ImageView)this.findViewById(2131230869);
        this.txtAdTime = (TextView)this.findViewById(2131230868);
        this.btnDelete = (Button)this.findViewById(2131230870);
        this.btnOk = (Button)this.findViewById(2131230848);
        this.btnCancle = (Button)this.findViewById(2131230849);
        this.txtAdTime.setOnClickListener((View.OnClickListener)this);
        this.btnDelete.setOnClickListener((View.OnClickListener)this);
        this.btnOk.setOnClickListener((View.OnClickListener)this);
        this.btnCancle.setOnClickListener((View.OnClickListener)this);
        final int intExtra = this.getIntent().getIntExtra("gv_index", -1);
        if (intExtra >= 0) {
            this.channel = FavoriteChFragment.list.get(intExtra);
            this.imgChannel.setImageResource(Util.getResourceId(this.context, this.channel.getImgResName()));
            this.channel.setTime(this.txtAdTime);
        }
    }
}
