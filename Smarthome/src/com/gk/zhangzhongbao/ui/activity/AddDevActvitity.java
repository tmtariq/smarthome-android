package com.gk.zhangzhongbao.ui.activity;

import android.view.*;
import android.content.*;
import android.os.*;

public class AddDevActvitity extends BaseViewActivity
{
    public void onClick(final View view) {
        switch (view.getId()) {
            case 2131230761: {
                this.startActivity(new Intent(this, AddLightActivity.class));
                break;
            }
            case 2131230763: {
                this.startActivity(new Intent(this, AddLockActivity.class));
                break;
            }
            case 2131230765: {
                this.startActivity(new Intent(this, AddCurtainGarageActivity.class));
                break;
            }
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText(this.getString(2131296316));
        this.setCenterView(2130903044);
    }
}
