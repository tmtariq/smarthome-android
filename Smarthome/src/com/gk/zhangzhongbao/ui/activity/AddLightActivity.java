package com.gk.zhangzhongbao.ui.activity;

import com.gk.wifictr.net.command.*;
import com.gk.zhangzhongbao.ui.app.*;

import android.view.*;

import com.zijunlin.Zxing.Demo.CaptureActivity;

import android.content.*;

import com.gk.wifictr.net.*;
import com.gk.wifictr.net.command.station.*;

import android.app.*;
import android.widget.*;
import android.os.*;

public class AddLightActivity extends BaseViewActivity implements View.OnClickListener
{
    public static Handler handler;
    private Button cancle;
    private Context context;
    private EditText edtName;
    private EditText edtSnCode;
    private ImageView img3gear;
    private Button imgCamera;
    private ImageView imgCommon;
    private int learnCodeDev;
    private int lightCount;
    private Button searchButton;
    private String snCode;
    private Button sure;
    private boolean sureAdd;
    
    static {
        AddLightActivity.handler = null;
    }
    
    public AddLightActivity() {
        this.context = this;
        this.lightCount = -1;
        this.snCode = "";
        this.sureAdd = false;
        this.learnCodeDev = 0;
    }
    
    static /* synthetic */ void access$1(final AddLightActivity addLightActivity, final int lightCount) {
        addLightActivity.lightCount = lightCount;
    }
    
    static /* synthetic */ void access$2(final AddLightActivity addLightActivity, final String snCode) {
        addLightActivity.snCode = snCode;
    }
    
    private void exitDlg() {
        final String string = this.edtName.getText().toString();
        final String string2 = this.edtSnCode.getText().toString();
        if (string.equals("") && string2.equals("") && this.lightCount < 0) {
            super.onBackPressed();
            return;
        }
        final AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder(this);
        alertDialog$Builder.setTitle(this.getString(2131296284));
        alertDialog$Builder.setMessage(this.getString(2131296380));
        alertDialog$Builder.setPositiveButton(this.getString(2131296285), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                AddLightActivity.this.finish();
            }
        });
        alertDialog$Builder.setNegativeButton(this.getString(2131296286), null);
        alertDialog$Builder.show();
    }
    
    protected void decodeData(final ReadData readData) {
        super.decodeData(readData);
        if (readData.type == 19 && this.sureAdd) {
            Util.toast(this.context, this.getString(2131296376));
            this.finish();
        }
        else if (readData.type == 23) {
            final In_17H in_17H = new In_17H(readData.body);
            if (in_17H.isGoodSubSn) {
                this.edtSnCode.setText(in_17H.subsn);
                this.showToast(this.getString(2131296377));
            }
            else {
                this.showToast(this.getString(2131296378));
            }
            Util.loadingDlgDismiss();
        }
    }
    
    public void onBackPressed() {
        this.exitDlg();
    }
    
    public void onClick(final View view) {
        switch (view.getId()) {
            default: {}
            case 2131230751: {
                this.type(1);
            }
            case 2131230752: {
                this.type(3);
            }
            case 2131230757: {
                final Intent intent = new Intent(this, CaptureActivity.class);
                intent.putExtra("from_which_acticity", 1);
                this.startActivity(intent);
            }
            case 2131230759: {
                final String string = this.edtName.getText().toString();
                if (string.equals("")) {
                    Toast.makeText(this.getApplicationContext(), this.getString(2131296398), 0).show();
                    return;
                }
                if (this.lightCount < 0) {
                    Toast.makeText(this.getApplicationContext(), this.getString(2131296399), 0).show();
                    return;
                }
                if (this.learnCodeDev == 0) {
                    this.snCode = this.edtSnCode.getText().toString();
                    if (this.snCode.equals("")) {
                        Toast.makeText(this.getApplicationContext(), this.getString(2131296400), 0).show();
                        return;
                    }
                    if (this.snCode.length() != 6) {
                        Toast.makeText(this.getApplicationContext(), this.getString(2131296401), 0).show();
                        return;
                    }
                }
                if (this.learnCodeDev > 0) {
                    if (this.learnCodeDev == 1) {
                        new BroadcastHelper().writeData(new Out_23H().make(string, "000000", (byte)98, 1));
                    }
                    else if (this.learnCodeDev == 2) {
                        new BroadcastHelper().writeData(new Out_23H().make(string, "000000", (byte)99, 1));
                    }
                }
                else {
                    new BroadcastHelper().writeData(new Out_23H().make(string, this.snCode, (byte)16, this.lightCount));
                }
                this.sureAdd = true;
            }
            case 2131230760: {
                this.exitDlg();
            }
            case 2131230758: {
                new BroadcastHelper().writeData(new Out_56H().make((byte)16));
                Util.createLoadingDialog(this.context, this.getString(2131296379), null, null);
            }
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.outer = (LinearLayout)this.setCenterView(2130903045);
        this.title.setText(this.getString(2131296374));
        this.btnLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                AddLightActivity.this.exitDlg();
            }
        });
        AddLightActivity.handler = new AddLightHandler(Looper.myLooper());
        Util.setButton(this, this.sure, 2131230759, this);
        Util.setButton(this, this.cancle, 2131230760, this);
        this.searchButton = Util.setButton(this, 2131230758, this);
        this.edtName = (EditText)this.findViewById(2131230748);
        this.imgCommon = (ImageView)this.findViewById(2131230751);
        this.img3gear = (ImageView)this.findViewById(2131230752);
        this.edtSnCode = (EditText)this.findViewById(2131230755);
        this.imgCamera = (Button)this.findViewById(2131230757);
        this.imgCommon.setOnClickListener(this);
        this.img3gear.setOnClickListener(this);
        this.imgCamera.setOnClickListener(this);
        this.learnCodeDev = this.getIntent().getIntExtra("learn_code_dev", 0);
        if (this.learnCodeDev > 0) {
            this.lightCount = 1;
            ((RelativeLayout)this.findViewById(2131230749)).setVisibility(8);
            ((RelativeLayout)this.findViewById(2131230754)).setVisibility(8);
            ((RelativeLayout)this.findViewById(2131230756)).setVisibility(8);
            ((Button)this.findViewById(2131230758)).setVisibility(8);
        }
    }
    
    public void type(final int n) {
        switch (n) {
            default: {}
            case 1: {
                this.imgCommon.setImageResource(2130837995);
                this.img3gear.setImageResource(2130837994);
                this.lightCount = 1;
            }
            case 3: {
                this.img3gear.setImageResource(2130837995);
                this.imgCommon.setImageResource(2130837994);
                this.lightCount = 3;
            }
        }
    }
    
    protected class AddLightHandler extends Handler
    {
        Looper looper;
        
        public AddLightHandler(final Looper looper) {
            this.looper = looper;
        }
        
        public void handleMessage(final Message message) {
            switch (message.what) {
                case 10: {
                    if (message.obj == null) {
                        break;
                    }
                    final String s = (String)message.obj;
                    if (!s.substring(0, 2).equals("XY")) {
                        Util.toast(AddLightActivity.this.context, AddLightActivity.this.getString(2131296402));
                        return;
                    }
                    if (!String.valueOf(s.charAt(2)).equals("A")) {
                        Util.toast(AddLightActivity.this.context, AddLightActivity.this.getString(2131296403));
                        return;
                    }
                    final String value = String.valueOf(s.charAt(3));
                    if (value.equals("1")) {
                        AddLightActivity.access$1(AddLightActivity.this, 1);
                    }
                    else if (value.equals("3")) {
                        AddLightActivity.access$1(AddLightActivity.this, 3);
                    }
                    AddLightActivity.access$2(AddLightActivity.this, s.substring(4, s.length()));
                    AddLightActivity.this.edtSnCode.setText(AddLightActivity.this.snCode);
                    AddLightActivity.this.type(AddLightActivity.this.lightCount);
                }
            }
        }
    }
}
