package com.gk.zhangzhongbao.ui.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.gk.wifictr.net.ConnectService;
import com.gk.zhangzhongbao.ui.app.DataBase;
import com.gk.zhangzhongbao.ui.app.Util;
import com.gk.zhangzhongbao.ui.view.MainButton;



public class MainActivity extends BaseActivity
{
    public static int MSG_ADD_BUTTON = 1;
    public static int MSG_REMOVE_BUTTON = 2;
    public static List<MainButton> buttons;
    public static Handler handler;
    public static int hostNum;
    public String INTENT_AIR;
    public String INTENT_CURTAIN;
    public String INTENT_DOOR;
    public String INTENT_LEARN;
    public String INTENT_LIGHT;
    public String INTENT_STB;
    public String INTENT_TV;
    public String INTENT_VEDIO;
    private String SN;
    private Context context;
    private LinearLayout ll;
    private LinearLayout llButtons;
    private View.OnClickListener mClickListener;
    private View.OnLongClickListener mLongClickListener;
    private RelativeLayout mainLayout;
    
    static {
        buttons = new ArrayList<MainButton>();
        handler = null;
    }
    
    public MainActivity() {
        context = this;
        INTENT_STB = "intent_STBActivity";
        INTENT_TV = "intent_TvActivity";
        INTENT_LIGHT = "intent_LightActivity";
        INTENT_AIR = "intent_AirActivity";
        INTENT_DOOR = "intent_DoorLockActivity";
        INTENT_CURTAIN = "intent_CurtainActivity";
        INTENT_LEARN = "intent_Learn";
        INTENT_VEDIO = "intent_Vedio";
        mClickListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(View view) {
                MainButton mainButton = (MainButton)view;
                if (mainButton.key.equals(INTENT_STB)) {
                    startActivity(new Intent(context, STBActivity.class));
                }
                if (mainButton.key.equals(INTENT_TV)) {
                    startActivity(new Intent(context, TvActivity.class));
                }
                if (mainButton.key.equals(INTENT_LIGHT)) {
                    startActivity(new Intent(context, LightActivity.class));
                }
                if (mainButton.key.equals(INTENT_AIR)) {
                    startActivity(new Intent(context, AirActivity.class));
                }
                if (mainButton.key.equals(INTENT_DOOR)) {
                    startActivity(new Intent(context, DoorLockActivity.class));
                }
                if (mainButton.key.equals(INTENT_CURTAIN)) {
                    startActivity(new Intent(context, CurtainActivity.class));
                }
                if (mainButton.key.equals(INTENT_LEARN)) {
                    startActivity(new Intent(context, LearnCodeActivity.class));
                }
                if (mainButton.key.equals(INTENT_VEDIO)) {
                    showToast("\u6b64\u529f\u80fd\u5efa\u8bbe\u4e2d");
                }
            }
        };
        mLongClickListener = (View.OnLongClickListener)new View.OnLongClickListener() {
            public boolean onLongClick(final View view) {
                Util.alertDlg(context, "\u63d0\u793a", "\u786e\u5b9a\u8981\u5220\u9664\u8bbe\u5907?", new Runnable() {
                    //private /* synthetic */ MainButton val$button = (MainButton)view;
                    
                    @Override
                    public void run() {
                        removeButton(((MainButton)view).key);
                    }
                });
                return true;
            }
        };
    }
    
    private void addButton(String key) {
        MainButton mainButton = new MainButton(context);
        mainButton.key = key;
        mainButton.setBackground(getResidByKey(mainButton.key));
        mainButton.isAdd = 1;
        addMainButton(mainButton, 6);
        buttons.add(mainButton);
        DataBase.db.insertMB(mainButton, hostNum);
    }
    
    private void addMainButton() {
        buttons = DataBase.db.getMBList(context, hostNum);
        for (MainButton mainButton : buttons) {
            mainButton.setBackground(getResidByKey(mainButton.key));
            addMainButton(mainButton, 6);
        }
    }
    
    private void addMainButton(MainButton mainButton, int n) {
        mainButton.setText(getNameByKey(mainButton.key));
        mainButton.setGravity(21);
        LinearLayout.LayoutParams linearLayout$LayoutParams = new LinearLayout.LayoutParams(Util.dip(context, 384.0f), Util.dip(context, 87.0f));
        if (n > 0) {
            linearLayout$LayoutParams.topMargin = Util.dip(context, n);
            linearLayout$LayoutParams.bottomMargin = Util.dip(context, n);
        }
        llButtons.addView((View)mainButton, (ViewGroup.LayoutParams)linearLayout$LayoutParams);
        mainButton.setOnClickListener(mClickListener);
        mainButton.setOnLongClickListener(mLongClickListener);
    }
    
    private int getDimensByKey(String s) {
        if (s.equals(INTENT_STB)) {
            return getResDimens(2131165184);
        }
        if (s.equals(INTENT_TV)) {
            return getResDimens(2131165185);
        }
        if (s.equals(INTENT_LIGHT)) {
            return getResDimens(2131165186);
        }
        if (s.equals(INTENT_AIR)) {
            return getResDimens(2131165187);
        }
        if (s.equals(INTENT_DOOR)) {
            return getResDimens(2131165188);
        }
        if (s.equals(INTENT_CURTAIN)) {
            return getResDimens(2131165189);
        }
        if (s.equals(INTENT_LEARN)) {
            return getResDimens(2131165190);
        }
        if (s.equals(INTENT_VEDIO)) {
            return getResDimens(2131165191);
        }
        return 0;
    }
    
    private String getNameByKey(String s) {
        if (s.equals(INTENT_STB)) {
            return getString(2131296308);
        }
        if (s.equals(INTENT_TV)) {
            return getString(2131296309);
        }
        if (s.equals(INTENT_LIGHT)) {
            return getString(2131296311);
        }
        if (s.equals(INTENT_AIR)) {
            return getString(2131296310);
        }
        if (s.equals(INTENT_DOOR)) {
            return getString(2131296312);
        }
        if (s.equals(INTENT_CURTAIN)) {
            return getString(2131296313);
        }
        if (s.equals(INTENT_LEARN)) {
            return getString(2131296314);
        }
        if (s.equals(INTENT_VEDIO)) {
            return getString(2131296315);
        }
        return "";
    }
    
    private int getResidByKey(String s) {
        if (s.equals(INTENT_STB)) {
            return 2130838183;
        }
        if (s.equals(INTENT_TV)) {
            return 2130838218;
        }
        if (s.equals(INTENT_LIGHT)) {
            return 2130838196;
        }
        if (s.equals(INTENT_AIR)) {
            return 2130838181;
        }
        if (s.equals(INTENT_DOOR)) {
            return 2130838202;
        }
        if (s.equals(INTENT_CURTAIN)) {
            return 2130838186;
        }
        if (s.equals(INTENT_LEARN)) {
            return 2130838193;
        }
        if (s.equals(INTENT_VEDIO)) {
            return 2130838221;
        }
        return -1;
    }
    
    private void removeButton(String s) {
        for (MainButton mainButton : buttons) {
            if (mainButton.key.equals(s)) {
                llButtons.removeView((View)mainButton);
                buttons.remove(mainButton);
                DataBase.db.deleteMBData(mainButton.key);
            }
        }
    }
    
    public void onClick(View view) {
        switch (view.getId()) {
            case 2131231030: {
                startActivity(new Intent(context, SetHostNetActivity.class));
                break;
            }
            case 2131231031: {
                ll.setVisibility(8);
                break;
            }
            case 2131231028: {
                startActivity(new Intent(this, Setting.class));
                break;
            }
            case 2131231027: {
                startActivity(new Intent(this, AddMainButtonActivity.class));
                break;
            }
        }
    }
    
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2130903082);
        handler = new MainHandler(Looper.myLooper());
        SN = ConnectService.defWifi.SN;
        hostNum = getIntent().getIntExtra("defwifinum", 1);
        ConnectService.mConnectService.changeDefWifiNum(hostNum);
        INTENT_STB = String.valueOf(INTENT_STB) + hostNum;
        INTENT_TV = String.valueOf(INTENT_TV) + hostNum;
        INTENT_LIGHT = String.valueOf(INTENT_LIGHT) + hostNum;
        INTENT_AIR = String.valueOf(INTENT_AIR) + hostNum;
        INTENT_DOOR = String.valueOf(INTENT_DOOR) + hostNum;
        INTENT_CURTAIN = String.valueOf(INTENT_CURTAIN) + hostNum;
        INTENT_LEARN = String.valueOf(INTENT_LEARN) + hostNum;
        INTENT_VEDIO = String.valueOf(INTENT_VEDIO) + hostNum;
        llButtons = (LinearLayout)findViewById(2131231026);
        ((Button)findViewById(2131231027)).setVisibility(0);
        addMainButton();
        if (buttons.size() <= 0) {
            addButton(INTENT_STB);
            addButton(INTENT_TV);
            addButton(INTENT_AIR);
        }
        mainLayout = (RelativeLayout)findViewById(2131231024);
        (ll = (LinearLayout)findViewById(2131231029)).setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(View view) {
            }
        });
        SharedPreferences sharedPreferences = getSharedPreferences(Util.SP_NAME, 0);
        if (sharedPreferences.getBoolean("app_first_start", true)) {
            ll.setVisibility(0);
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean("app_first_start", false);
        edit.commit();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    
    protected class MainHandler extends Handler
    {
        Looper looper;
        
        public MainHandler(Looper looper) {
            this.looper = looper;
        }
        
        public void handleMessage(Message message) {
            String key = (String)message.obj;
            switch (message.what) {
                case 1: {
                    MainButton mainButton = new MainButton(context);
                    mainButton.key = key;
                    mainButton.setBackground(getResidByKey(key));
                    mainButton.isAdd = 1;
                    addMainButton(mainButton, 6);
                    buttons.add(mainButton);
                    DataBase.db.insertMB(mainButton, hostNum);
                    break;
                }
                case 2: {
                    removeButton(key);
                    break;
                }
            }
        }
    }
}
