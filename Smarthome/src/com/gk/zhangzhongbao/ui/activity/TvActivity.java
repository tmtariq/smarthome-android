package com.gk.zhangzhongbao.ui.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gk.wifictr.net.BroadcastHelper;
import com.gk.wifictr.net.command.ByteUtil;
import com.gk.wifictr.net.command.ReadData;
import com.gk.wifictr.net.command.station.In_13H;
import com.gk.wifictr.net.command.station.In_16H;
import com.gk.wifictr.net.command.station.Out_25H;
import com.gk.wifictr.net.command.station.Out_27H;
import com.gk.wifictr.net.command.station.Out_2EH;
import com.gk.wifictr.net.command.station.Out_2FH;
import com.gk.wifictr.net.data.SubDevice;
import com.gk.zhangzhongbao.ui.app.Brands;
import com.gk.zhangzhongbao.ui.app.Util;
import com.gk.zhangzhongbao.ui.view.BrandDialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;



public class TvActivity extends BaseViewActivity
{
    public static String TV_BRAND_NAME = "tv_brand_name";
    public static String TV_BRAND_POSITION = "tv_brand_index";
    public static String TV_DEVNO = "tv_devno";
    public static String TV_SN = "tv_sn";
    private int CODE_NO1;
    private int CODE_NO2;
    private int CODE_NO3;
    String brandString;
    private View.OnClickListener clickListener;
    int[] codes;
    private Context context;
    int end;
    int index;
    private TextView learnCodeTextView;
    private List<Map<String, Object>> list;
    private SubDevice mSubDevice;
    private Button optional;
    PopupWindow popupWindow;
    int position;
    private ProgressBar progressBar;
    int start;
    private Button tempButton;
    private ImageView tvbottom1;
    private ImageView tvbottom2;
    private BrandDialog tvdialog;
    private ImageView tvtop1;
    private ImageView tvtop2;
    private Button userDefined1;
    private Button userDefined2;
    private Button userDefined3;
    
    public TvActivity() {
        context = this;
        CODE_NO1 = 1;
        CODE_NO2 = 2;
        CODE_NO3 = 3;
        list = new ArrayList<Map<String, Object>>();
        index = 1;
        clickListener = new View.OnClickListener() {
            public void onClick(View view) {
                switch (view.getId()) {
                    case 2131231134: {
                        String stringValue = Util.getStringValue("ir_code1");
                        Log.e("defined1 code", stringValue);
                        if (!stringValue.equals("")) {
                            new BroadcastHelper().writeData(new Out_2FH().make((byte)3, 1, ByteUtil.hex2byte(stringValue)));
                            return;
                        }
                        break;
                    }
                    case 2131231135: {
                        String stringValue2 = Util.getStringValue("ir_code2");
                        if (!stringValue2.equals("")) {
                            new BroadcastHelper().writeData(new Out_2FH().make((byte)3, 2, ByteUtil.hex2byte(stringValue2)));
                            return;
                        }
                        break;
                    }
                    case 2131231136: {
                        String stringValue3 = Util.getStringValue("ir_code3");
                        if (!stringValue3.equals("")) {
                            new BroadcastHelper().writeData(new Out_2FH().make((byte)3, 3, ByteUtil.hex2byte(stringValue3)));
                            return;
                        }
                        break;
                    }
                }
            }
        };
    }
    
    private void initView() {
        setImageView(tvtop1, this, 2131231109, 2130838321);
        setImageView(tvtop2, this, 2131231111, 2130838322);
        setImageView(tvbottom1, this, 2131231115, 2130838291);
        setImageView(tvbottom2, this, 2131231117, 2130838292);
        optional = (Button)findViewById(2131231137);
        userDefined1 = (Button)findViewById(2131231134);
        userDefined2 = (Button)findViewById(2131231135);
        userDefined3 = (Button)findViewById(2131231136);
        userDefined1.setOnClickListener(clickListener);
        userDefined2.setOnClickListener(clickListener);
        userDefined3.setOnClickListener(clickListener);
        userDefined1.setText(Util.getStringValue("userDefined1", getString(2131296345)));
        userDefined2.setText(Util.getStringValue("userDefined2", getString(2131296346)));
        userDefined3.setText(Util.getStringValue("userDefined3", getString(2131296347)));
        tempButton = userDefined1;
        userDefined1.setTag(1);
        userDefined2.setTag(2);
        userDefined3.setTag(3);
        userDefined1.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View view) {
                showDialog(userDefined1, "userDefined1", 1);
                return true;
            }
        });
        userDefined2.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View view) {
                showDialog(userDefined2, "userDefined2", 2);
                return true;
            }
        });
        userDefined3.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View view) {
                showDialog(userDefined3, "userDefined3", 3);
                return true;
            }
        });
    }
    
    private void send(int n) {
        new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(mSubDevice.devNo, mSubDevice.devName, mSubDevice.devSN, mSubDevice.devType), (byte)n));
    }
    
    private void send(String s, int n) {
        new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(mSubDevice.devNo, mSubDevice.devName, s, mSubDevice.devType), (byte)n));
    }
    
    private void showDialog(final Button tempButton, final String s, final int n) {
    	this.tempButton = tempButton;
        View inflate = View.inflate(getApplicationContext(), 2130903101, null);
        final EditText editText = (EditText)inflate.findViewById(2131231138);
        editText.setText(tempButton.getText());
        Button button = (Button)inflate.findViewById(2131231139);
        learnCodeTextView = (TextView)inflate.findViewById(2131231142);
        progressBar = (ProgressBar)inflate.findViewById(2131231141);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                new BroadcastHelper().writeData(new Out_2EH().make((byte)3, (byte)n));
                progressBar.setVisibility(0);
                learnCodeTextView.setVisibility(0);
                learnCodeTextView.setText(getString(2131296306));
            }
        });
        AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder(this);
        alertDialog$Builder.setTitle(getString(2131296344));
        alertDialog$Builder.setView(inflate);
        alertDialog$Builder.setPositiveButton(getString(2131296285), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int n) {
                String string = editText.getText().toString();
                if (string.equals("")) {
                    return;
                }
                tempButton.setText(string);
                Util.saveValue(s, string);
            }
        });
        alertDialog$Builder.show();
    }
    
    @Override
    protected void decodeData(ReadData readData) {
        super.decodeData(readData);
        if (readData.type == 22) {
            In_16H in_16H = new In_16H(readData.body);
            Log.e("code num", String.valueOf(in_16H.get_code_num()));
            int intValue = Integer.valueOf((String)tempButton.getTag());
            if (in_16H.get_code_num() == intValue) {
                Util.saveValue("ir_code" + intValue, ByteUtil.byte2hex(in_16H.get_IR_code()));
                if (learnCodeTextView != null && progressBar != null) {
                    progressBar.setVisibility(8);
                    learnCodeTextView.setVisibility(0);
                    learnCodeTextView.setText(getString(2131296305));
                }
            }
        }
        else if (readData.type == 19) {
            for (SubDevice mSubDevice : new In_13H().decode(readData)) {
                if (mSubDevice.devType == 3) {
                	this.mSubDevice = mSubDevice;
                    optional.setText(mSubDevice.devName);
                    Util.saveValue("tv_sn", mSubDevice.devSN);
                    Util.saveValue("tv_brand_name", mSubDevice.devName);
                    Util.saveValue("tv_devno", mSubDevice.devNo);
                    Log.e("mSubDevice.devName length", String.valueOf(mSubDevice.devName.length()));
                    for (int i = 0; i < Brands.tvBrandStrings.length; ++i) {
                        if (Brands.tvBrandStrings[i].equals(mSubDevice.devName)) {
                            Util.saveValue("tv_brand_index", position = i);
                        }
                    }
                }
            }
        }
    }
    
    public void onClick(View view) {
        Util.vibrator(context);
        Util.clickBeepSound(context);
        switch (view.getId()) {
            case 2131231106: {
                send(1);
                break;
            }
            case 2131231107: {
                send(2);
                break;
            }
            case 2131231108: {
                send(3);
                break;
            }
            case 2131231110: {
                send(4);
                break;
            }
            case 2131231112: {
                send(6);
                break;
            }
            case 2131231114: {
                send(8);
                break;
            }
            case 2131231116: {
                send(10);
                break;
            }
            case 2131231113: {
                send(7);
                break;
            }
            case 2131231120: {
                send(15);
                break;
            }
            case 2131231121: {
                send(12);
                break;
            }
            case 2131231118: {
                send(17);
                break;
            }
            case 2131231119: {
                send(14);
                break;
            }
            case 2131231122: {
                send(22);
                break;
            }
            case 2131231123: {
                send(23);
                break;
            }
            case 2131231124: {
                send(24);
                break;
            }
            case 2131231125: {
                send(25);
                break;
            }
            case 2131231126: {
                send(26);
                break;
            }
            case 2131231127: {
                send(27);
                break;
            }
            case 2131231128: {
                send(28);
                break;
            }
            case 2131231129: {
                send(29);
                break;
            }
            case 2131231130: {
                send(30);
                break;
            }
            case 2131231132: {
                send(32);
                break;
            }
            case 2131231131: {
                send(5);
                break;
            }
            case 2131231133: {
                send(11);
                break;
            }
            case 2131231137: {
                showTVBrandList();
                break;
            }
        }
    }
    
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        title.setText(getString(2131296309));
        setCenterView(2130903100);
        btnRight.setBackgroundResource(2130838048);
        btnRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(context, LearnCodeActivity.class));
            }
        });
        initView();
        reflash();
        linkState();
        mSubDevice = new SubDevice(Util.getIntValue("tv_devno"), Util.getStringValue("tv_brand_name", getString(2131296348)), Util.getStringValue("tv_sn", "660100"), (byte)3);
        optional.setText(mSubDevice.devName);
        position = Util.getIntValue("tv_brand_index");
    }
    
    public void showTVBrandList() {
        if (tvdialog == null) {
            for (int i = 0; i < Brands.tvBrandStrings.length; ++i) {
                HashMap<String, Object> hashMap = new HashMap<String, Object>();
                hashMap.put("brand", Brands.tvBrandStrings[i]);
                hashMap.put("codes", Brands.tvBrandCodes[i]);
                hashMap.put("index", 1);
                list.add((Map<String, Object>)hashMap);
            }
            (tvdialog = new BrandDialog(context, list, 2131361809)).setListener((BrandDialog.ClickListener)new BrandDialog.ClickListener() {
                @Override
                public void itemClick(int position) {
                    Util.vibrator(context);
                    Map<String, Object> map = list.get(position);
                    TvActivity.this.position = position;
                    brandString = (String)map.get("brand");
                    codes = (int[])map.get("codes");
                    index = Integer.valueOf((String)map.get("index"));
                    tvdialog.brandName.setText(brandString);
                    tvdialog.brandNum.setText(String.valueOf(index));
                    send(Util.codeToSn(codes[index - 1]), 15);
                }
                
                @Override
                public void leftClick() {
                    --index;
                    if (index <= 0) {
                        index = codes.length;
                    }
                    tvdialog.brandNum.setText(String.valueOf(index));
                    send(Util.codeToSn(codes[index - 1]), 15);
                }
                
                @Override
                public void okClick() {
                    mSubDevice.devSN = Util.codeToSn(codes[index - 1]);
                    mSubDevice.devName = brandString;
                    send(15);
                    tvdialog.dismiss();
                    new BroadcastHelper().writeData(new Out_25H().make(mSubDevice));
                }
                
                @Override
                public void rightClick() {
                    ++index;
                    if (index > codes.length) {
                        index = 1;
                    }
                    tvdialog.brandNum.setText(String.valueOf(index));
                    send(Util.codeToSn(codes[index - 1]), 15);
                }
            });
        }
        tvdialog.show();
        Map<String, Object> map = list.get(position);
        brandString = (String)map.get("brand");
        codes = (int[])map.get("codes");
        int snToCode = Util.snToCode(mSubDevice.devSN);
        for (int j = 0; j < codes.length; ++j) {
            if (snToCode == codes[j]) {
                index = j + 1;
            }
        }
        tvdialog.brandNum.setText(String.valueOf(index));
        tvdialog.brandName.setText(brandString);
    }
}
