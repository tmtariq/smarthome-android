package com.gk.zhangzhongbao.ui.activity;

import android.widget.*;
import android.os.*;
import android.text.*;
import android.text.method.*;
import android.graphics.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.view.*;
import android.net.*;
import android.content.*;

public class AboutActivity extends BaseViewActivity
{
    private ImageView imgView;
    private TextView linkTextView;
    private TextView tel;
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText((CharSequence)this.getResString(2131296420));
        this.setCenterView(2130903040);
        this.imgView = (ImageView)this.findViewById(2131230732);
        this.linkTextView = (TextView)this.findViewById(2131230735);
        this.tel = (TextView)this.findViewById(2131230736);
        this.linkTextView.setText((CharSequence)Html.fromHtml("<a href=\"http://www.zsgoldenocean.cn\"><u>www.zsgoldenocean.cn</u></a>"));
        this.linkTextView.setMovementMethod(LinkMovementMethod.getInstance());
        int n = 2130837504;
        if (Util.lan.equals("zh")) {
            n = 2130837504;
        }
        else if (Util.lan.equals("en")) {
            n = 2130837505;
        }
        this.background = MakeBmp.CreateBitmap((Context)this, n, Util.sw, Bitmap.Config.ARGB_8888);
        this.imgView.setImageBitmap(this.background);
        this.bmpList.add(this.background);
        this.tel.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                AboutActivity.this.startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:0760-87337088")));
            }
        });
    }
}
