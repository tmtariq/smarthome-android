package com.gk.zhangzhongbao.ui.activity;

import com.gk.zhangzhongbao.ui.bean.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.app.*;
import android.widget.*;
import android.view.*;
import android.content.*;
import com.gk.zhangzhongbao.ui.fragment.*;
import android.os.*;

public class ChannelActivity extends BaseViewActivity
{
    private Channel channel;
    private int channelNum;
    private ImageView imgChannel;
    private ImageView imgFavor;
    private boolean isFavorite;
    private TextView txtChannelNum;
    
    public ChannelActivity() {
        this.isFavorite = false;
    }
    
    static /* synthetic */ void access$1(final ChannelActivity channelActivity, final int channelNum) {
        channelActivity.channelNum = channelNum;
    }
    
    private void favorChannel(final boolean b) {
        if (b) {
            this.imgFavor.setImageBitmap(Util.readBitMap((Context)this, 2130837995));
            return;
        }
        this.imgFavor.setImageBitmap(Util.readBitMap((Context)this, 2130837994));
    }
    
    private void setChannelNum() {
        final AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder((Context)this);
        alertDialog$Builder.setTitle((CharSequence)this.getString(2131296338));
        final EditText view = new EditText((Context)this);
        view.setInputType(2);
        view.setSingleLine();
        view.setGravity(17);
        view.setText(this.txtChannelNum.getText());
        view.setSelection(this.txtChannelNum.getText().length());
        alertDialog$Builder.setView((View)view);
        alertDialog$Builder.setPositiveButton((CharSequence)this.getString(2131296285), (DialogInterface.OnClickListener)new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, int int1) {
                final String string = view.getText().toString();
                if (!string.equals("")) {
                    int1 = Integer.parseInt(string);
                    String text;
                    if (int1 < 100) {
                        text = "0" + int1;
                    }
                    else if (int1 < 10) {
                        text = "00" + int1;
                    }
                    else {
                        text = new StringBuilder().append(int1).toString();
                    }
                    ChannelActivity.this.txtChannelNum.setText((CharSequence)text);
                    ChannelActivity.access$1(ChannelActivity.this, int1);
                }
            }
        });
        alertDialog$Builder.setNegativeButton((CharSequence)this.getString(2131296286), (DialogInterface.OnClickListener)null);
        alertDialog$Builder.show();
    }
    
    public void onClick(final View view) {
        boolean isFavorite = true;
        switch (view.getId()) {
            default: {}
            case 2131230847: {
                this.setChannelNum();
            }
            case 2131230843: {
                if (this.isFavorite) {
                    isFavorite = false;
                }
                this.favorChannel(this.isFavorite = isFavorite);
            }
            case 2131230848: {
                if (this.channel != null) {
                    this.channel.saveFavor(this.isFavorite);
                    this.channel.setChannelNum(this.channelNum);
                    final Message message = new Message();
                    message.obj = this.channel;
                    if (this.channel != null && this.isFavorite) {
                        message.what = 1;
                    }
                    else if (this.channel != null && !this.isFavorite) {
                        message.what = 2;
                    }
                    FavoriteChFragment.handler.sendMessage(message);
                    CommonChFragment.handler.sendEmptyMessage(10);
                }
                this.finish();
            }
            case 2131230849: {
                this.finish();
            }
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setCenterView(2130903053);
        this.title.setText((CharSequence)this.getString(2131296318));
        this.imgChannel = (ImageView)this.findViewById(2131230846);
        this.txtChannelNum = (TextView)this.findViewById(2131230847);
        this.imgFavor = (ImageView)this.findViewById(2131230843);
        final int intExtra = this.getIntent().getIntExtra("gv_index", -1);
        if (intExtra >= 0) {
            this.channel = CommonChFragment.gvList.get(intExtra);
            this.channelNum = this.channel.getChannelNum();
            this.imgChannel.setImageResource(Util.getResourceId((Context)this, this.channel.getImgResName()));
            final int[] chCodes = this.channel.getChCodes();
            this.txtChannelNum.setText((CharSequence)(String.valueOf(chCodes[0]) + chCodes[1] + chCodes[2]));
            this.favorChannel(this.isFavorite = this.channel.getFavorValue());
        }
    }
}
