package com.gk.zhangzhongbao.ui.activity;

import java.util.*;
import com.gk.zhangzhongbao.ui.bean.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.util.*;
import com.gk.wifictr.net.*;
import android.text.*;
import java.io.*;
import android.view.*;
import android.widget.*;
import android.app.*;
import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.command.station.*;
import android.os.*;
import android.content.*;

public class LearnCodeActivity extends BaseViewActivity implements View.OnClickListener, View.OnLongClickListener
{
    public static LCButton[] buttons;
    public static Handler handler;
    public static ArrayList<BtnData> list;
    int[] btnIds;
    int[] codeNums;
    Context context;
    int currCodeNum;
    LCButton currLearnButton;
    TextView learnCodeTextView;
    ProgressBar progressBar;
    
    static {
        LearnCodeActivity.buttons = new LCButton[59];
        LearnCodeActivity.handler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 1: {
                        for (int i = 0; i < LearnCodeActivity.list.size(); ++i) {
                            BtnData btnData = LearnCodeActivity.list.get(i);
                            LCButton lcButton = LearnCodeActivity.buttons[i];
                            lcButton.btnData = btnData;
                            lcButton.button.setText(btnData.name);
                            Util.saveValue("learn" + lcButton.btnData.No, btnData.name);
                            if (btnData.data != null) {
                                Util.saveValue("learn_ircode" + lcButton.btnData.No, ByteUtil.byte2hex(btnData.data));
                            }
                        }
                        break;
                    }
                }
            }
        };
    }
    
    public LearnCodeActivity() {
        this.context = (Context)this;
        this.btnIds = new int[] { 2131230738, 2131230939, 2131230940, 2131230941, 2131230942, 2131230943, 2131230944, 2131230945, 2131230946, 2131230947, 2131230948, 2131230949, 2131230950, 2131230951, 2131230952, 2131230953, 2131230954, 2131230955, 2131230956, 2131230957, 2131230958, 2131230959, 2131230960, 2131230961, 2131230962, 2131230963, 2131230964, 2131230965, 2131230966, 2131230967, 2131230968, 2131230969, 2131230970, 2131230971, 2131230972, 2131230973, 2131230974, 2131230975, 2131230976, 2131230977, 2131230978, 2131230979, 2131230980, 2131230981, 2131230982, 2131230983, 2131230984, 2131230985, 2131230986, 2131230987, 2131230988, 2131230989, 2131230990, 2131230991, 2131230992, 2131230993, 2131230994, 2131230995, 2131230996 };
        this.codeNums = new int[] { 17, 16, 19, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74 };
    }
    
    private void send(BtnData btnData) {
        if (btnData != null && btnData.data != null) {
            Log.e("button ir code", ByteUtil.byte2hex(btnData.data));
            new BroadcastHelper().writeData(new Out_2FH().make((byte)3, btnData.No, btnData.data));
        }
    }
    
    private LCButton setButton(int n, int n2, int n3, View.OnClickListener view$OnClickListener, View.OnLongClickListener view$OnLongClickListener) {
        LCButton lcButton = new LCButton();
        lcButton.button = (Button)this.findViewById(n2);
        String stringValue = Util.getStringValue("learn" + n3, "");
        lcButton.button.setText(stringValue);
        lcButton.button.setOnClickListener((View.OnClickListener)this);
        lcButton.button.setOnLongClickListener((View.OnLongClickListener)this);
        lcButton.btnData.No = (byte)n3;
        lcButton.btnData.name = stringValue;
        byte[] btr1 = new byte[16];
        while (true) {
            try {
            	btr1 = stringValue.getBytes("UTF-8");
                lcButton.btnData.name_byte = ByteUtil.getByteWithLength(btr1, 16);
                //view$OnClickListener = Util.getStringValue("learn_ircode" + n3, "");
                if (TextUtils.isEmpty(Util.getStringValue("learn_ircode" + n3, ""))) {
                    lcButton.btnData.data = null;
                    return lcButton;
                }
                lcButton.btnData.data = ByteUtil.hex2byte((String)Util.getStringValue("learn_ircode" + n3, ""));
                return lcButton;
            }
            catch (UnsupportedEncodingException ex) {
                continue;
            }
            //break;
        }
    }
    
    private void showDialog(final Button button, final int n, final boolean b) {
        View inflate = View.inflate(this.getApplicationContext(), 2130903101, (ViewGroup)null);
        final EditText editText = (EditText)inflate.findViewById(2131231138);
        editText.setText(button.getText());
        if (!b) {
            editText.setVisibility(8);
        }
        Button button2 = (Button)inflate.findViewById(2131231139);
        this.learnCodeTextView = (TextView)inflate.findViewById(2131231142);
        this.progressBar = (ProgressBar)inflate.findViewById(2131231141);
        button2.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(View view) {
                LearnCodeActivity.this.currCodeNum = n;
                new BroadcastHelper().writeData(new Out_2EH().make((byte)3, (byte)n));
                LearnCodeActivity.this.progressBar.setVisibility(0);
                LearnCodeActivity.this.learnCodeTextView.setVisibility(0);
                LearnCodeActivity.this.learnCodeTextView.setText(LearnCodeActivity.this.getString(2131296306));
            }
        });
        AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder(this.context);
        alertDialog$Builder.setTitle(this.getString(2131296307));
        alertDialog$Builder.setView(inflate);
        alertDialog$Builder.setPositiveButton(this.getString(2131296285), (DialogInterface.OnClickListener)new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int n) {
                String string = editText.getText().toString();
                Util.saveValue("learn" + n, string);
                button.setText(string);
            }
        });
        alertDialog$Builder.show();
    }
    
    protected void decodeData(ReadData readData) {
        super.decodeData(readData);
        if (readData.type == 22) {
            In_16H in_16H = new In_16H(readData.body);
            Log.e("code num", new StringBuilder(String.valueOf(in_16H.get_code_num())).toString());
            if (in_16H.get_code_num() == this.currCodeNum) {
                this.currLearnButton.btnData.data = in_16H.get_IR_code();
                Util.saveValue("learn_ircode" + this.currCodeNum, ByteUtil.byte2hex(this.currLearnButton.btnData.data));
                if (this.learnCodeTextView != null && this.progressBar != null) {
                    this.progressBar.setVisibility(8);
                    this.learnCodeTextView.setVisibility(0);
                    this.learnCodeTextView.setText(this.getString(2131296305));
                }
                this.currCodeNum = 0;
            }
        }
    }
    
    public void onClick(View view) {
        Util.vibrator((Context)this);
        Util.clickBeepSound((Context)this);
        for (int i = 0; i < this.btnIds.length; ++i) {
            if (view.getId() == this.btnIds[i]) {
                this.send(LearnCodeActivity.buttons[i].btnData);
            }
        }
    }
    
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText(this.getString(2131296314));
        this.setCenterView(2130903072);
        this.btnRight.setBackgroundResource(2130838339);
        this.btnRight.setText(this.getString(2131296405));
        this.btnRight.setTextSize(2, 15.0f);
        this.btnRight.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(View view) {
                LearnCodeActivity.this.startActivity(new Intent(LearnCodeActivity.this.context, (Class)ShareDataActivity.class));
            }
        });
        LearnCodeActivity.list = new ArrayList<BtnData>();
        for (int i = 0; i < LearnCodeActivity.buttons.length; ++i) {
            LearnCodeActivity.buttons[i] = this.setButton(i, this.btnIds[i], this.codeNums[i], (View.OnClickListener)this, (View.OnLongClickListener)this);
        }
    }
    
    public boolean onLongClick(View view) {
        for (int i = 0; i < this.btnIds.length; ++i) {
            boolean b = true;
            if (view.getId() == this.btnIds[i]) {
                if (view.getId() == 2131230939 || view.getId() == 2131230942 || view.getId() == 2131230943 || view.getId() == 2131230944 || view.getId() == 2131230945 || view.getId() == 2131230947 || view.getId() == 2131230948 || view.getId() == 2131230949 || view.getId() == 2131230950 || view.getId() == 2131230959 || view.getId() == 2131230960 || view.getId() == 2131230961 || view.getId() == 2131230962) {
                    b = false;
                }
                this.currLearnButton = LearnCodeActivity.buttons[i];
                this.showDialog(LearnCodeActivity.buttons[i].button, this.codeNums[i], b);
            }
        }
        return true;
    }
}
