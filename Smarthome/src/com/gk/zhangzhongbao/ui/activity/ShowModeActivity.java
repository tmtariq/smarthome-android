package com.gk.zhangzhongbao.ui.activity;

import android.content.*;
import com.gk.zhangzhongbao.ui.bean.*;
import com.gk.zhangzhongbao.ui.app.*;
import com.gk.zhangzhongbao.ui.view.*;
import android.widget.*;
import android.view.*;
import android.os.*;

public class ShowModeActivity extends BaseViewActivity
{
    private SwitchView bigDoorOff;
    private Context context;
    private LinearLayout lightLayout;
    private LinearLayout outerLayout;
    
    public ShowModeActivity() {
        this.context = (Context)this;
    }
    
    private void init() {
        this.outerLayout = (LinearLayout)this.findViewById(2131231080);
        this.lightLayout = (LinearLayout)this.findViewById(2131231081);
        final LightItem setSnCode = new LightItem().setName(this.getResString(2131296421)).setCount(1).setSnCode("333333");
        setSnCode.longClickable = false;
        this.lightLayout.addView((View)new LightView(this.context, setSnCode, false));
        final LightItem setSnCode2 = new LightItem().setName(this.getResString(2131296422)).setCount(3).setSnCode("444444");
        setSnCode2.longClickable = false;
        this.lightLayout.addView((View)new LightView(this.context, setSnCode2, false));
        (this.bigDoorOff = (SwitchView)this.findViewById(2131231083)).setOnSwitchListener((SwitchView.OnSwitchListener)new SwitchView.OnSwitchListener() {
            @Override
            public void onCheck(final SwitchView switchView, final boolean b, final boolean b2) {
                if (b2) {
                    Util.vibrator(ShowModeActivity.this.context);
                    Util.clickSound(ShowModeActivity.this.context);
                }
            }
        });
        final CurtainGarageView curtainGarageView = new CurtainGarageView(this.context, 1, this.getResString(2131296423), false, false);
        final LinearLayout.LayoutParams linearLayout$LayoutParams = new LinearLayout.LayoutParams(-2, -2);
        linearLayout$LayoutParams.gravity = 1;
        this.outerLayout.addView((View)curtainGarageView, linearLayout$LayoutParams);
        if (curtainGarageView.mBitmap != null) {
            this.bmpList.add(curtainGarageView.mBitmap);
        }
        final CurtainGarageView curtainGarageView2 = new CurtainGarageView(this.context, 2, this.getResString(2131296424), false, false);
        final LinearLayout.LayoutParams linearLayout$LayoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        linearLayout$LayoutParams2.gravity = 1;
        this.outerLayout.addView((View)curtainGarageView2, linearLayout$LayoutParams2);
        if (curtainGarageView2.mBitmap != null) {
            this.bmpList.add(curtainGarageView2.mBitmap);
        }
        final CurtainGarageView curtainGarageView3 = new CurtainGarageView(this.context, 3, this.getResString(2131296425), false, false);
        final LinearLayout.LayoutParams linearLayout$LayoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        linearLayout$LayoutParams3.gravity = 1;
        this.outerLayout.addView((View)curtainGarageView3, linearLayout$LayoutParams3);
        if (curtainGarageView3.mBitmap != null) {
            this.bmpList.add(curtainGarageView3.mBitmap);
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText((CharSequence)this.getResString(2131296413));
        this.setCenterView(2130903095);
        this.init();
    }
}
