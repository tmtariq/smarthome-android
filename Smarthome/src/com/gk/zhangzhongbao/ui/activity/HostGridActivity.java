package com.gk.zhangzhongbao.ui.activity;

import com.gk.wifictr.net.ConnectService;
import com.gk.zhangzhongbao.ui.app.Util;
import com.gk.zhangzhongbao.ui.view.GuideViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;



public class HostGridActivity extends BaseActivity implements View.OnLongClickListener
{
    Button host1;
    Button host2;
    Button host3;
    Button host4;
    RelativeLayout layout;
    
    private void showdialog(final Button button, final String s) {
        final EditText editText = new EditText(this);
        editText.setText(button.getText());
        Util.alertDlg(this, getResString(2131296283), null, (View)editText, new Runnable() {
            @Override
            public void run() {
                String string = editText.getText().toString();
                if (string != null && !string.equals("")) {
                    button.setText((CharSequence)string);
                    Util.saveValue(s, string);
                }
            }
        });
    }
    
    public void onClick(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        switch (view.getId()) {
            case 2131230864: {
                intent.putExtra("defwifinum", 1);
                host2.setClickable(false);
                host3.setClickable(false);
                host4.setClickable(false);
                break;
            }
            case 2131230865: {
                intent.putExtra("defwifinum", 2);
                host1.setClickable(false);
                host3.setClickable(false);
                host4.setClickable(false);
                break;
            }
            case 2131230866: {
                intent.putExtra("defwifinum", 3);
                host2.setClickable(false);
                host1.setClickable(false);
                host4.setClickable(false);
                break;
            }
            case 2131230867: {
                intent.putExtra("defwifinum", 4);
                host2.setClickable(false);
                host3.setClickable(false);
                host1.setClickable(false);
                break;
            }
        }
        startActivity(intent);
    }
    
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2130903060);
        host1 = (Button)findViewById(2131230864);
        host2 = (Button)findViewById(2131230865);
        host3 = (Button)findViewById(2131230866);
        host4 = (Button)findViewById(2131230867);
        host1.setOnLongClickListener(this);
        host2.setOnLongClickListener(this);
        host3.setOnLongClickListener(this);
        host4.setOnLongClickListener(this);
        host1.setText((CharSequence)Util.getStringValue("duo_host1", getResString(2131296277)));
        host2.setText((CharSequence)Util.getStringValue("duo_host2", getResString(2131296278)));
        host3.setText((CharSequence)Util.getStringValue("duo_host3", getResString(2131296279)));
        host4.setText((CharSequence)Util.getStringValue("duo_host4", getResString(2131296280)));
        layout = (RelativeLayout)findViewById(2131230862);
        SharedPreferences sharedPreferences = getSharedPreferences(Util.SP_NAME, 0);
        if (sharedPreferences.getBoolean("app_first_start", true)) {
            final GuideViewPager guideViewPager = new GuideViewPager(this);
            layout.addView((View)guideViewPager, new ViewGroup.LayoutParams(-1, -1));
            guideViewPager.setPageTouchLisenter((GuideViewPager.OnPageTouchLisenter)new GuideViewPager.OnPageTouchLisenter() {
                @Override
                public void onLastPage() {
                    guideViewPager.setVisibility(8);
                    layout.removeView((View)guideViewPager);
                    guideViewPager.recycle();
                }
            });
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean("app_first_start", false);
        edit.commit();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, ConnectService.class));
        System.exit(0);
    }
    
    public boolean onLongClick(View view) {
        switch (view.getId()) {
            case 2131230864: {
                showdialog(host1, "duo_host1");
                break;
            }
            case 2131230865: {
                showdialog(host2, "duo_host2");
                break;
            }
            case 2131230866: {
                showdialog(host3, "duo_host3");
                break;
            }
            case 2131230867: {
                showdialog(host4, "duo_host4");
                break;
            }
        }
        return true;
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        host1.setClickable(true);
        host2.setClickable(true);
        host3.setClickable(true);
        host4.setClickable(true);
    }
}
