package com.gk.zhangzhongbao.ui.activity;

import com.gk.zhangzhongbao.ui.view.*;
import android.os.*;

public class GuideActivity extends BaseViewActivity
{
    GuideViewPager viewPager;
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText((CharSequence)this.getString(2131296419));
        this.setCenterView(2130903067);
        this.viewPager = (GuideViewPager)this.findViewById(2131230890);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.viewPager.recycle();
    }
}
