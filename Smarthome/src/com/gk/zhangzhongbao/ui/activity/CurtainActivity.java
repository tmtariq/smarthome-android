package com.gk.zhangzhongbao.ui.activity;

import com.gk.zhangzhongbao.ui.view.*;
import com.gk.wifictr.net.data.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.view.*;
import android.widget.*;
import android.app.*;
import java.util.*;
import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.command.station.*;
import android.util.*;
import android.content.*;
import com.gk.wifictr.net.*;
import android.os.*;

public class CurtainActivity extends BaseViewActivity implements View.OnClickListener
{
    public static final int LEARN_CODE = 10;
    public static Handler handler;
    private String SN;
    private Context context;
    private List<CurtainGarageView> cvList;
    private TextView learnCodeTextView;
    private LinearLayout outerLayout;
    private ProgressBar progressBar;
    
    static {
        CurtainActivity.handler = null;
    }
    
    public CurtainActivity() {
        this.cvList = new ArrayList<CurtainGarageView>();
        this.context = (Context)this;
    }
    
    private void getDevices() {
        for (final SubDevice device : DataBase.db.getCGList(this.SN)) {
            final CurtainGarageView curtainGarageView = new CurtainGarageView(this.context, device.cg_type, device.devName, true, true);
            curtainGarageView.setDevice(device);
            this.cvList.add(curtainGarageView);
        }
        for (final CurtainGarageView curtainGarageView2 : this.cvList) {
            final LinearLayout.LayoutParams linearLayout$LayoutParams = new LinearLayout.LayoutParams(-2, -2);
            linearLayout$LayoutParams.gravity = 1;
            this.outerLayout.addView((View)curtainGarageView2, (ViewGroup.LayoutParams)linearLayout$LayoutParams);
        }
    }
    
    private void showDlg(final SubDevice subDevice) {
        final View inflate = View.inflate(this.context, 2130903101, (ViewGroup)null);
        ((EditText)inflate.findViewById(2131231138)).setVisibility(8);
        final Button button = (Button)inflate.findViewById(2131231139);
        (this.learnCodeTextView = (TextView)inflate.findViewById(2131231142)).setText((CharSequence)this.getString(2131296306));
        this.progressBar = (ProgressBar)inflate.findViewById(2131231141);
        button.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                new BroadcastHelper().writeData(new Out_57H().make(subDevice.devSN, subDevice.devType, subDevice.actionCode));
                CurtainActivity.this.progressBar.setVisibility(0);
                CurtainActivity.this.learnCodeTextView.setVisibility(0);
                CurtainActivity.this.learnCodeTextView.setText((CharSequence)CurtainActivity.this.getString(2131296306));
            }
        });
        final AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder(this.context);
        alertDialog$Builder.setTitle((CharSequence)this.getString(2131296303));
        alertDialog$Builder.setView(inflate);
        alertDialog$Builder.setPositiveButton((CharSequence)this.getString(2131296285), (DialogInterface.OnClickListener)new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (CurtainActivity.this.learnCodeTextView != null && CurtainActivity.this.progressBar != null) {
                    CurtainActivity.this.progressBar.setVisibility(8);
                    CurtainActivity.this.learnCodeTextView.setVisibility(8);
                    CurtainActivity.this.learnCodeTextView.setText((CharSequence)CurtainActivity.this.getString(2131296306));
                }
            }
        });
        alertDialog$Builder.show();
    }
    
    protected void decodeData(final ReadData readData) {
        super.decodeData(readData);
        if (readData.type == 19) {
            final ArrayList<SubDevice> decode = new In_13H().decode(readData);
            if (decode.size() > 0) {
                final ArrayList<CurtainGarageView> list = new ArrayList<CurtainGarageView>();
                for (final CurtainGarageView curtainGarageView : this.cvList) {
                    int n = 0;
                    for (final SubDevice subDevice : decode) {
                        if ((subDevice.devType == 80 || subDevice.devType == 96 || subDevice.devType == 97) && curtainGarageView.subDevice.devSN.equals(subDevice.devSN) && curtainGarageView.subDevice.devType == subDevice.devType) {
                            n = 1;
                            break;
                        }
                    }
                    if (n == 0) {
                        list.add(curtainGarageView);
                        this.outerLayout.removeView((View)curtainGarageView);
                    }
                }
                final Iterator<CurtainGarageView> iterator3 = list.iterator();
                while (iterator3.hasNext()) {
                    DataBase.db.deleteData("cg_table", "cg_sn", iterator3.next().subDevice.devSN, this.SN);
                }
                this.cvList.removeAll(list);
            }
            for (final SubDevice subDevice2 : decode) {
                int n2 = 0;
                if (subDevice2.devType == 80 || subDevice2.devType == 96 || subDevice2.devType == 97) {
                    for (final CurtainGarageView curtainGarageView2 : this.cvList) {
                        if (curtainGarageView2.subDevice.devSN.equals(subDevice2.devSN) && curtainGarageView2.subDevice.devType == subDevice2.devType) {
                            if (curtainGarageView2.subDevice.devNo != subDevice2.devNo || !curtainGarageView2.subDevice.devName.equals(subDevice2.devName)) {
                                DataBase.db.updateCG(subDevice2, this.SN);
                            }
                            curtainGarageView2.setDevice(subDevice2);
                            n2 = 1;
                        }
                    }
                    if (n2 != 0) {
                        continue;
                    }
                    final char char1 = ByteUtil.byte2hex(new byte[] { subDevice2.devCode }, true).charAt(0);
                    new LinearLayout.LayoutParams(-2, -2);
                    Object o = null;
                    switch (char1) {
                        case 49: {
                            o = new CurtainGarageView(this.context, 3, subDevice2.devName, true, true);
                            break;
                        }
                        case 50: {
                            o = new CurtainGarageView(this.context, 4, subDevice2.devName, true, true);
                            break;
                        }
                        case 57: {
                            o = new CurtainGarageView(this.context, 1, subDevice2.devName, true, true);
                            break;
                        }
                        case 97: {
                            o = new CurtainGarageView(this.context, 2, subDevice2.devName, true, true);
                            break;
                        }
                    }
                    if (o == null) {
                        continue;
                    }
                    ((CurtainGarageView)o).setDevice(subDevice2);
                    final LinearLayout.LayoutParams linearLayout$LayoutParams = new LinearLayout.LayoutParams(-2, -2);
                    linearLayout$LayoutParams.gravity = 1;
                    this.outerLayout.addView((View)o, (ViewGroup.LayoutParams)linearLayout$LayoutParams);
                    this.cvList.add((CurtainGarageView)o);
                    DataBase.db.insertCG(subDevice2, ((CurtainGarageView)o).type, this.SN);
                }
            }
        }
        else if (readData.type == 22) {
            final In_16H in_16H = new In_16H(readData.body);
            Log.e("code type", String.valueOf(ByteUtil.byte2hex(new byte[] { in_16H.code_type }, true)) + "  " + ByteUtil.byte2hex(readData.body[15]).length());
            Log.e("code num", new StringBuilder(String.valueOf(in_16H.get_code_num())).toString());
            if (ByteUtil.byte2hex(new byte[] { in_16H.code_type }, true).equals("ff")) {
                this.showToast(this.getString(2131296304));
                return;
            }
            if (this.learnCodeTextView != null && this.progressBar != null) {
                this.progressBar.setVisibility(8);
                this.learnCodeTextView.setVisibility(0);
                this.learnCodeTextView.setText((CharSequence)this.getString(2131296305));
            }
        }
    }
    
    public void onClick(final View view) {
        if (view == this.btnRight) {
            final Intent intent = new Intent((Context)this, (Class)TempActivity.class);
            intent.putExtra("intent_to_temp", 2);
            this.startActivity(intent);
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText((CharSequence)this.getString(2131296313));
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(2130837506);
        this.btnRight.setOnClickListener((View.OnClickListener)this);
        this.setCenterView(2130903056);
        CurtainActivity.handler = new LearnCodeHandler(Looper.myLooper());
        this.outerLayout = (LinearLayout)this.findViewById(2131230854);
        this.SN = ConnectService.defWifi.SN;
        this.getDevices();
        this.linkState();
        this.reflash();
    }
    
    protected void onDestroy() {
        for (final CurtainGarageView curtainGarageView : this.cvList) {
            if (curtainGarageView.mBitmap != null) {
                this.bmpList.add(curtainGarageView.mBitmap);
            }
        }
        super.onDestroy();
    }
    
    @Override
    protected void stateChange() {
        super.stateChange();
        this.linkState();
    }
    
    protected class LearnCodeHandler extends Handler
    {
        Looper looper;
        
        public LearnCodeHandler(final Looper looper) {
            this.looper = looper;
        }
        
        public void handleMessage(final Message message) {
            switch (message.what) {
                case 10: {
                    if (message.obj != null) {
                        CurtainActivity.this.showDlg((SubDevice)message.obj);
                        return;
                    }
                    break;
                }
            }
        }
    }
}
