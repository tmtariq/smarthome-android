package com.gk.zhangzhongbao.ui.activity;

import android.app.*;
import android.view.animation.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.widget.*;
import android.content.*;
import com.gk.wifictr.net.*;
import android.os.*;
import android.view.*;

public class BaseViewActivity extends BaseActivity
{
    protected Button btnLeft;
    protected Button btnRight;
    protected RelativeLayout center;
    protected Dialog loadingDialog;
    protected TextView loadingTv;
    protected ImageView netState;
    protected LinearLayout outer;
    protected TextView title;
    protected RelativeLayout topBar;
    
    protected void createLoadingDialog(Context context, String text, final Runnable runnable) {
        View inflate = LayoutInflater.from(context).inflate(2130903078, (ViewGroup)null);
        LinearLayout linearLayout = (LinearLayout)inflate.findViewById(2131231014);
        ImageView imageView = (ImageView)inflate.findViewById(2131231015);
        TextView textView = (TextView)inflate.findViewById(2131231016);
        this.loadingTv = textView;
        imageView.startAnimation(AnimationUtils.loadAnimation(context, 2130968576));
        textView.setText(text);
        this.loadingTv = textView;
        (this.loadingDialog = new Dialog(context, 2131361810)).setCanceledOnTouchOutside(false);
        this.loadingDialog.setContentView((View)linearLayout, new LinearLayout.LayoutParams(Util.dip(context, 300.0f), -1));
        this.loadingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                if (runnable != null) {
                    runnable.run();
                }
            }
        });
        this.loadingDialog.show();
    }
    
    protected void linkState() {
        this.netState.setVisibility(0);
        if (ConnectService.mConnectService.isLink()) {
            this.netState.setImageResource(2130838283);
            return;
        }
        this.netState.setImageResource(2130838282);
    }
    
    protected void loadingDlgDismiss() {
        if (this.loadingDialog != null) {
            this.loadingDialog.dismiss();
            this.loadingDialog = null;
        }
    }
    
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2130903050);
        this.title = (TextView)this.findViewById(2131230827);
        this.topBar = (RelativeLayout)this.findViewById(2131230825);
        this.center = (RelativeLayout)this.findViewById(2131230830);
        this.netState = (ImageView)this.findViewById(2131230829);
        (this.btnLeft = (Button)this.findViewById(2131230826)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                BaseViewActivity.this.finish();
            }
        });
        this.btnRight = (Button)this.findViewById(2131230828);
    }
    
    protected View setCenterView(int n) {
        View inflate = View.inflate((Context)this, n, (ViewGroup)null);
        inflate.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.center.addView(inflate);
        return inflate;
    }
    
    protected void showTipsDlg(String text) {
        View inflate = View.inflate((Context)this, 2130903058, (ViewGroup)null);
        TextView textView = (TextView)inflate.findViewById(2131230859);
        Button button = (Button)inflate.findViewById(2131230860);
        textView.setText(text);
        final Dialog dialog = new Dialog((Context)this, 2131361809);
        dialog.setContentView(inflate, new ViewGroup.LayoutParams(Util.dip((Context)this, 400.0f), Util.dip((Context)this, 400.0f)));
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
                BaseViewActivity.this.finish();
            }
        });
        dialog.show();
    }
    
    @Override
    protected void stateChange() {
        super.stateChange();
    }
}
