package com.gk.zhangzhongbao.ui.activity;

import android.widget.*;
import com.gk.wifictr.net.command.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.content.*;
import android.view.*;
import com.gk.wifictr.net.*;
import com.gk.wifictr.net.command.station.*;
import android.os.*;

public class AddLockActivity extends BaseViewActivity
{
    private EditText nameEditText;
    private boolean sureAdd;
    
    @Override
    protected void decodeData(final ReadData readData) {
        super.decodeData(readData);
        if (readData.type == 19 && this.sureAdd) {
            Util.toast((Context)this, this.getString(2131296376));
            this.finish();
        }
    }
    
    public void onClick(final View view) {
        switch (view.getId()) {
            default: {}
            case 2131230770: {
                final String string = this.nameEditText.getText().toString();
                if (string != null && !string.equals("")) {
                    new BroadcastHelper().writeData(new Out_23H().make(string, "000000", (byte)64, 1));
                    this.sureAdd = true;
                    return;
                }
                this.showToast(this.getString(2131296398));
            }
            case 2131230771: {
                this.finish();
            }
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.title.setText((CharSequence)this.getString(2131296392));
        this.setCenterView(2130903046);
        this.nameEditText = (EditText)this.findViewById(2131230769);
    }
}
