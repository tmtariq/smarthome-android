package com.gk.zhangzhongbao.ui.activity;

import com.gk.zhangzhongbao.ui.adapter.*;
import android.graphics.*;
import com.gk.zhangzhongbao.ui.bean.*;
import android.view.*;
import android.app.*;
import com.gk.wifictr.net.data.*;
import java.util.*;
import com.gk.wifictr.net.command.*;
import android.util.*;
import com.gk.zhangzhongbao.ui.app.*;
import com.gk.wifictr.net.command.station.*;
import android.content.*;
import com.gk.wifictr.net.*;
import android.widget.*;
import android.os.*;

public class LightActivity extends BaseViewActivity implements View.OnClickListener
{
    public static final int MSG_ADD_LIGHT = 1;
    public static final int MSG_DEL_LIGHT = 2;
    public static final int MSG_DEL_LIGHT_DATA = 4;
    public static final int MSG_LEARN = 5;
    public static final int MSG_MAINSWITCH = 3;
    public static Handler handler;
    private String SN;
    private LightAdapter adapter;
    private Context context;
    private View footer;
    private ImageView imgMain;
    private Bitmap imgON;
    private Bitmap imgOff;
    private TextView learnCodeTextView;
    private ListView listView;
    private List<LightItem> mList;
    private boolean mainOn;
    private ImageView mainSwitch;
    private Bitmap msON;
    private Bitmap msOff;
    private ProgressBar progressBar;
    
    static {
        LightActivity.handler = null;
    }
    
    public LightActivity() {
        this.context = (Context)this;
        this.mList = new ArrayList<LightItem>();
        this.mainOn = true;
    }
    
    private void mainSwitchStatus() {
        final Iterator<LightItem> iterator = this.mList.iterator();
        while (iterator.hasNext()) {
            if (!iterator.next().allLightOff()) {
                this.mainOn = true;
                this.mainSwitch.setImageBitmap(this.msON);
                this.imgMain.setImageBitmap(this.imgON);
                return;
            }
        }
        this.mainOn = false;
        this.mainSwitch.setImageBitmap(this.msOff);
        this.imgMain.setImageBitmap(this.imgOff);
    }
    
    private void showDlg(final LightItem lightItem) {
        final View inflate = View.inflate(this.context, 2130903101, (ViewGroup)null);
        ((EditText)inflate.findViewById(2131231138)).setVisibility(8);
        final Button button = (Button)inflate.findViewById(2131231139);
        (this.learnCodeTextView = (TextView)inflate.findViewById(2131231142)).setText((CharSequence)this.getString(2131296306));
        this.progressBar = (ProgressBar)inflate.findViewById(2131231141);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                new BroadcastHelper().writeData(new Out_57H().make(lightItem.getSnCode(), (byte)lightItem.getDevType(), lightItem.actionCode));
                LightActivity.this.progressBar.setVisibility(0);
                LightActivity.this.learnCodeTextView.setVisibility(0);
                LightActivity.this.learnCodeTextView.setText((CharSequence)LightActivity.this.getString(2131296306));
            }
        });
        final AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder(this.context);
        alertDialog$Builder.setTitle((CharSequence)this.getString(2131296303));
        alertDialog$Builder.setView(inflate);
        alertDialog$Builder.setPositiveButton((CharSequence)this.getString(2131296285), new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (LightActivity.this.learnCodeTextView != null && LightActivity.this.progressBar != null) {
                    LightActivity.this.progressBar.setVisibility(8);
                    LightActivity.this.learnCodeTextView.setVisibility(8);
                    LightActivity.this.learnCodeTextView.setText((CharSequence)LightActivity.this.getString(2131296306));
                }
            }
        });
        alertDialog$Builder.show();
    }
    
    protected void decodeData(final ReadData readData) {
        super.decodeData(readData);
        if (readData.type == 19) {
            final ArrayList<SubDevice> decode = new In_13H().decode(readData);
            if (decode.size() > 0) {
                final ArrayList<LightItem> list = new ArrayList<LightItem>();
                for (final LightItem lightItem : this.mList) {
                    int n = 0;
                    for (final SubDevice subDevice : decode) {
                        if ((subDevice.devType == 16 || subDevice.devType == 98 || subDevice.devType == 99) && lightItem.getSnCode().equals(subDevice.devSN) && lightItem.getDevType() == subDevice.devType) {
                            n = 1;
                            break;
                        }
                    }
                    if (n == 0) {
                        list.add(lightItem);
                    }
                }
                this.mList.removeAll(list);
                this.adapter.notifyDataSetChanged();
                final Iterator<LightItem> iterator3 = list.iterator();
                while (iterator3.hasNext()) {
                    DataBase.db.deleteData("light_table", "sncode", iterator3.next().getSnCode(), this.SN);
                }
            }
            for (final SubDevice subDevice2 : decode) {
                int n2 = 0;
                if (subDevice2.devType == 16 || subDevice2.devType == 98 || subDevice2.devType == 99) {
                    for (final LightItem lightItem2 : this.mList) {
                        if (lightItem2.getSnCode().equals(subDevice2.devSN) && lightItem2.getDevType() == subDevice2.devType) {
                            lightItem2.setName(subDevice2.devName);
                            lightItem2.setDevNo(subDevice2.devNo);
                            switch (lightItem2.getCount()) {
                                case 1: {
                                    lightItem2.state_one = subDevice2.lightStatus;
                                    break;
                                }
                                case 3: {
                                    lightItem2.status_three = subDevice2.lightStatus;
                                    break;
                                }
                            }
                            n2 = 1;
                            this.adapter.notifyDataSetChanged();
                            DataBase.db.updateLight(lightItem2, this.SN);
                        }
                    }
                    if (n2 != 0) {
                        continue;
                    }
                    final LightItem setDevType = new LightItem().setCount(subDevice2.lightCount).setName(subDevice2.devName).setSnCode(subDevice2.devSN).setDevNo(subDevice2.devNo).setDevType(subDevice2.devType);
                    switch (subDevice2.lightCount) {
                        case 1: {
                            setDevType.state_one = subDevice2.lightStatus;
                            break;
                        }
                        case 3: {
                            setDevType.status_three = subDevice2.lightStatus;
                            break;
                        }
                    }
                    this.mList.add(setDevType);
                    DataBase.db.insertLight(setDevType, this.SN);
                }
            }
            LightActivity.handler.sendEmptyMessage(3);
            if (this.mList.size() <= 1) {
                this.footer.setVisibility(8);
            }
            else {
                this.footer.setVisibility(0);
            }
        }
        else if (readData.type == 22) {
            final In_16H in_16H = new In_16H(readData.body);
            Log.e("code type", String.valueOf(ByteUtil.byte2hex(new byte[] { in_16H.code_type }, true)) + "  " + ByteUtil.byte2hex(readData.body[15]).length());
            Log.e("code num", new StringBuilder(String.valueOf(in_16H.get_code_num())).toString());
            if (ByteUtil.byte2hex(new byte[] { in_16H.code_type }, true).equals("ff")) {
                this.showToast(this.getString(2131296304));
            }
            else if (this.learnCodeTextView != null && this.progressBar != null) {
                this.progressBar.setVisibility(8);
                this.learnCodeTextView.setVisibility(0);
                this.learnCodeTextView.setText((CharSequence)this.getString(2131296305));
            }
        }
        this.adapter.notifyDataSetChanged();
    }
    
    public void onClick(final View view) {
        if (view == this.mainSwitch) {
            Util.vibrator(this.context);
            Util.clickSound(this.context);
            this.mainOn = !this.mainOn;
            if (this.mainOn) {
                this.mainSwitch.setImageBitmap(this.msON);
                this.imgMain.setImageBitmap(this.imgON);
                new BroadcastHelper().writeData(new Out_55H().make(true));
            }
            else {
                this.mainSwitch.setImageBitmap(this.msOff);
                this.imgMain.setImageBitmap(this.imgOff);
                new BroadcastHelper().writeData(new Out_55H().make(false));
            }
            this.adapter.notifyDataSetChanged();
        }
        else if (view == this.btnRight) {
            final Intent intent = new Intent((Context)this, (Class)TempActivity.class);
            intent.putExtra("intent_to_temp", 1);
            this.startActivity(intent);
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setVolumeControlStream(3);
        this.setCenterView(2130903073);
        this.title.setText((CharSequence)this.getString(2131296311));
        this.btnRight.setVisibility(0);
        this.btnRight.setBackgroundResource(2130837506);
        this.btnRight.setOnClickListener((View.OnClickListener)this);
        LightActivity.handler = new LightHandler(Looper.myLooper());
        this.listView = (ListView)this.findViewById(2131230998);
        this.SN = ConnectService.defWifi.SN;
        this.mList = DataBase.db.getLightList(this.SN);
        this.adapter = new LightAdapter((Context)this, this.mList);
        this.footer = View.inflate(this.context, 2130903075, (ViewGroup)null);
        this.mainSwitch = (ImageView)this.footer.findViewById(2131231001);
        this.imgMain = (ImageView)this.footer.findViewById(2131231002);
        this.msON = Util.readBitMap(this.context, 2130838212);
        this.msOff = Util.readBitMap(this.context, 2130838211);
        this.imgON = Util.readBitMap(this.context, 2130838204);
        this.imgOff = Util.readBitMap(this.context, 2130838203);
        this.bmpList.add(this.msON);
        this.bmpList.add(this.msOff);
        this.bmpList.add(this.imgON);
        this.bmpList.add(this.imgOff);
        this.mainSwitch.setImageBitmap(this.msON);
        this.imgMain.setImageBitmap(this.imgON);
        this.mainSwitch.setFocusable(true);
        this.mainSwitch.setOnClickListener((View.OnClickListener)this);
        this.listView.addFooterView(this.footer);
        this.listView.setAdapter((ListAdapter)this.adapter);
        if (this.mList.size() <= 1) {
            this.footer.setVisibility(8);
        }
        this.reflash();
        this.linkState();
    }
    
    protected void onDestroy() {
        super.onDestroy();
        this.lbcManager.unregisterReceiver(this.receiver);
    }
    
    @Override
    protected void stateChange() {
        super.stateChange();
        this.linkState();
    }
    
    protected class LightHandler extends Handler
    {
        Looper looper;
        
        public LightHandler(final Looper looper) {
            this.looper = looper;
        }
        
        public void handleMessage(final Message message) {
            switch (message.what) {
                case 1: {
                    if (message.obj != null) {
                        Log.e("name", ((LightItem)message.obj).getName());
                        return;
                    }
                    break;
                }
                case 4: {
                    DataBase.db.deleteData("light_table", "sncode", (String)message.obj, LightActivity.this.SN);
                }
                case 2: {
                    if (message.obj != null) {
                        final String s = (String)message.obj;
                        for (int i = 0; i < LightActivity.this.mList.size(); ++i) {
                            if (s.equals(((LightItem)LightActivity.this.mList.get(i)).getSnCode())) {
                                LightActivity.this.mList.remove(i);
                                LightActivity.this.adapter.notifyDataSetChanged();
                                break;
                            }
                        }
                    }
                    if (LightActivity.this.mList.size() > 1) {
                        LightActivity.this.footer.setVisibility(0);
                        return;
                    }
                    LightActivity.this.footer.setVisibility(8);
                }
                case 3: {
                    LightActivity.this.mainSwitchStatus();
                }
                case 5: {
                    if (message.obj != null) {
                        LightActivity.this.showDlg((LightItem)message.obj);
                        return;
                    }
                    break;
                }
            }
        }
    }
}
