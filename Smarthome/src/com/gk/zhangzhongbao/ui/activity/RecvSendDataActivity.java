package com.gk.zhangzhongbao.ui.activity;

import android.content.*;
import android.widget.*;
import android.view.*;
import com.gk.wifictr.net.command.station.*;
import android.os.*;
import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.*;
import com.gk.zhangzhongbao.ui.bean.*;
import android.util.*;
import java.net.*;
import java.io.*;

public class RecvSendDataActivity extends BaseViewActivity
{
    Button btnRecv;
    Button btnSend;
    Context context;
    int flag;
    Handler handler;
    byte[] read;
    String recvIp;
    byte[] sendDatas;
    ServerThread serverThread;
    int tcpPort;
    SendThread tcpThread;
    TextView textData;
    TextView tips;
    UdpThread udpThread;
    
    public RecvSendDataActivity() {
        this.context = (Context)this;
        this.tcpPort = 44322;
        this.sendDatas = new byte[0];
        this.handler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 1: {
                        RecvSendDataActivity.this.tips.setText((CharSequence)RecvSendDataActivity.this.getString(2131296408));
                    }
                    case 2: {
                        RecvSendDataActivity.this.showToast((String)message.obj);
                    }
                    case 4: {
                        String s = (String)message.obj;
                        RecvSendDataActivity.this.tips.setText((CharSequence)RecvSendDataActivity.this.getString(2131296409));
                        if (RecvSendDataActivity.this.tcpThread == null) {
                            (RecvSendDataActivity.this.tcpThread = new SendThread(s, RecvSendDataActivity.this.tcpPort)).start();
                            return;
                        }
                        break;
                    }
                    case 5: {
                        RecvSendDataActivity.this.textData.setText((CharSequence)new StringBuilder(String.valueOf(message.obj)).toString());
                    }
                    case 6: {
                        RecvSendDataActivity.this.tips.setText((CharSequence)RecvSendDataActivity.this.getString(2131296410));
                    }
                    case 7: {
                        RecvSendDataActivity.this.tips.setText((CharSequence)RecvSendDataActivity.this.getString(2131296411));
                    }
                }
            }
        };
        this.read = new byte[0];
    }
    
    public void onClick(View view) {
        switch (view.getId()) {
            case 2131231040: {
                if (this.tcpThread != null) {
                    this.tcpThread.write(new O89H().makeOut(this.sendDatas));
                    return;
                }
                break;
            }
            case 2131231037: {
                int i = 0;
                while (i < 20) {
                    while (true) {
                        try {
                            Thread.sleep(100L);
                            this.udpThread.send(new O88H().makeOut(this.recvIp));
                            ++i;
                        }
                        catch (InterruptedException ex) {
                            continue;
                        }
                        break;
                    }
                }
                break;
            }
        }
    }
    
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        LCButton[] buttons = LearnCodeActivity.buttons;
        for (int length = buttons.length, i = 0; i < length; ++i) {
            this.sendDatas = ByteUtil.arraycat(this.sendDatas, buttons[i].btnData.make());
        }
        switch (this.flag = this.getIntent().getIntExtra("recv_send", 1)) {
            case 1: {
                this.title.setText((CharSequence)this.getString(2131296407));
                this.setCenterView(2130903086);
                this.tips = (TextView)this.findViewById(2131231036);
                this.textData = (TextView)this.findViewById(2131231038);
                this.recvIp = new NetHelper(this.context).getIP();
                if (this.serverThread == null) {
                    (this.serverThread = new ServerThread(this.tcpPort)).start();
                    break;
                }
                break;
            }
            case 2: {
                this.title.setText((CharSequence)this.getString(2131296406));
                this.setCenterView(2130903087);
                this.tips = (TextView)this.findViewById(2131231039);
                break;
            }
        }
        (this.udpThread = new UdpThread()).start();
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (this.udpThread != null) {
            this.udpThread.close();
            this.udpThread.interrupt();
        }
        if (this.tcpThread != null) {
            this.tcpThread.disconnect();
            this.tcpThread.interrupt();
        }
        if (this.serverThread != null) {
            this.serverThread.interrupt();
            this.serverThread.disconnect();
            this.serverThread.interrupt();
        }
    }
    
    private class SendThread extends Thread
    {
        private DataInputStream reader;
        private Socket socketClient;
        public String targetAddress;
        public int targetPort;
        private DataOutputStream writer;
        
        public SendThread(String targetAddress, int targetPort) {
            this.targetAddress = targetAddress;
            this.targetPort = targetPort;
        }
        
        public void connect() {
            try {
                (this.socketClient = new Socket()).connect(new InetSocketAddress(this.targetAddress, this.targetPort));
                this.socketClient.setSoTimeout(30000);
                this.reader = new DataInputStream(this.socketClient.getInputStream());
                this.writer = new DataOutputStream(this.socketClient.getOutputStream());
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        
        public void disconnect() {
        	try
            {
              if (socketClient != null)
              {
                if (!socketClient.isInputShutdown()) {
                  socketClient.shutdownInput();
                }
                if (!socketClient.isOutputShutdown()) {
                  socketClient.shutdownOutput();
                }
              }
              if ((socketClient != null) && (!socketClient.isClosed())) {
                socketClient.close();
              }
              if (writer != null) {
                writer.close();
              }
              if (reader != null) {
                reader.close();
              }
              return;
            }
            catch (Exception localException)
            {
              //for (;;)
              //{
                localException = localException;
                localException.printStackTrace();
                writer = null;
                reader = null;
                socketClient = null;
              //}
            }
            finally
            {
              writer = null;
              reader = null;
              socketClient = null;
            }
        }
        
        @Override
        public void run() {
            this.connect();
        }
        
        public boolean write(byte[] array) {
            try {
                this.writer.write(array);
                this.writer.flush();
                return true;
            }
            catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        }
    }
    
    private class ServerThread extends Thread
    {
        private DataInputStream reader;
        private ServerSocket socketClient;
        public int targetPort;
        private DataOutputStream writer;
        
        public ServerThread(int targetPort) {
            this.targetPort = targetPort;
        }
        
        public void connect() {
            try {
                (this.socketClient = new ServerSocket(this.targetPort)).setSoTimeout(30000);
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        
        public void disconnect() {
        	try
            {
              if ((socketClient != null) && (!socketClient.isClosed())) {
                socketClient.close();
              }
              if (writer != null) {
                writer.close();
              }
              if (reader != null) {
                reader.close();
              }
              return;
            }
            catch (Exception localException)
            {
              //for (;;)
              //{
                //localException = localException;
                localException.printStackTrace();
                writer = null;
                reader = null;
                socketClient = null;
              //}
            }
            finally
            {
              writer = null;
              reader = null;
              socketClient = null;
            }
        }
        
        @Override
        public void run() {
            this.connect();
            do {
                Socket socket;
                if (this.isInterrupted()) {
                    return;
                }
                try {
                    Thread.sleep(300);
                }
                catch (InterruptedException var2_4) {
                    var2_4.printStackTrace();
                }
                try {
                    socket = this.socketClient.accept();
                    this.reader = new DataInputStream(socket.getInputStream());
                    this.writer = new DataOutputStream(socket.getOutputStream());
                }
                catch (IOException var2_6) {}
                byte[] btr1 = new byte[1024];
                try {
                    int n = this.reader.read(btr1);
                    RecvSendDataActivity.this.read = ByteUtil.arraycat(RecvSendDataActivity.this.read, ByteUtil.copyOfRange(btr1, 0, n));
                    Log.e((String)"read", (String)ByteUtil.byte2hex(RecvSendDataActivity.this.read));
                    LearnCodeActivity.list = new O89H().decodeIn(RecvSendDataActivity.this.read);
                    if (LearnCodeActivity.list == null) {
                        RecvSendDataActivity.this.handler.sendEmptyMessage(6);
                    }
                    RecvSendDataActivity.this.handler.sendEmptyMessage(7);
                    Message message = new Message();
                    message.what = 1;
                    message.obj = RecvSendDataActivity.this.read;
                    LearnCodeActivity.handler.sendMessage((Message)message);
                    continue;
                }
                catch (SocketException var2_3) {
                    var2_3.printStackTrace();
                }
                catch (Exception var2_5) {
                    var2_5.printStackTrace();
                    continue;
                }
                break;
            } while (true);
        }
    }
    
    private class UdpThread extends Thread
    {
        public Boolean IsUdpThreadDisable;
        DatagramSocket datagramSocket;
        String mPort;
        
        private UdpThread() {
            this.IsUdpThreadDisable = false;
            this.mPort = "12344";
        }
        
        public void close() {
            this.stopUdpListen();
            if (this.datagramSocket != null) {
                this.datagramSocket.close();
                this.datagramSocket = null;
            }
        }
        
        public void newSocket() {
            int int1 = Integer.parseInt(this.mPort);
            if (this.datagramSocket != null) {
                return;
            }
            try {
                (this.datagramSocket = new DatagramSocket((SocketAddress)null)).setReuseAddress(true);
                this.datagramSocket.bind(new InetSocketAddress(int1));
                this.datagramSocket.setSoTimeout(20000);
                this.datagramSocket.setBroadcast(true);
                RecvSendDataActivity.this.handler.sendEmptyMessage(1);
            }
            catch (SocketException ex) {}
        }
        
        @Override
        public void run() {
            this.startListen();
        }
        
        public void send(byte[] array) {
            int int1 = Integer.parseInt(this.mPort);
            Log.d("UDP Demo", "UDP\u53d1\u9001\u6570\u636e:" + array);
            if (this.datagramSocket == null) {
                this.newSocket();
            }
            InetAddress byName = null;
            while (true) {
                try {
                    byName = InetAddress.getByName("255.255.255.255");
                    //array = (byte[])(Object)new DatagramPacket(array, array.length, byName, int1);
                    //UdpThread udpThread = this;
                    //DatagramSocket datagramSocket = udpThread.datagramSocket;
                    //byte[] array2 = array;
                    datagramSocket.send(new DatagramPacket(array, array.length, byName, int1));
                    //return;
                }
                catch (UnknownHostException ex) {
                    ex.printStackTrace();
                    continue;
                }
                catch (IOException ex2) {
                    ex2.printStackTrace();
                    RecvSendDataActivity.this.handler.sendEmptyMessage(3);
                }
                break;
            }
            try {
                //UdpThread udpThread = this;
                //DatagramSocket datagramSocket = udpThread.datagramSocket;
                //byte[] array2 = array;
                datagramSocket.send(new DatagramPacket(array, array.length, byName, int1));
            }
            catch (IOException ex2) {
                ex2.printStackTrace();
                RecvSendDataActivity.this.handler.sendEmptyMessage(3);
            }
        }
        
        public void startListen() {
            if (this.datagramSocket == null) {
                this.newSocket();
            }
            while (!this.IsUdpThreadDisable) {
                try {
                    DatagramPacket datagramPacket = new DatagramPacket(new byte[255], 255);
                    this.datagramSocket.receive(datagramPacket);
                    byte[] copyOfRange = ByteUtil.copyOfRange(datagramPacket.getData(), 0, datagramPacket.getLength());
                    Log.e("data recv", new StringBuilder(String.valueOf(copyOfRange.length)).toString());
                    if (RecvSendDataActivity.this.flag != 2) {
                        continue;
                    }
                    String decodeIn = new O88H().decodeIn(copyOfRange);
                    Message message = new Message();
                    message.what = 4;
                    message.obj = decodeIn;
                    RecvSendDataActivity.this.handler.sendMessage(message);
                }
                catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        
        public void stopUdpListen() {
            this.IsUdpThreadDisable = true;
        }
    }
}
