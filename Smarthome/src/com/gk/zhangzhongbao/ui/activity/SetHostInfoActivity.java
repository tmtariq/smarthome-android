package com.gk.zhangzhongbao.ui.activity;

import android.widget.*;

import java.util.*;

import com.gk.zhangzhongbao.ui.app.*;

import android.content.*;

import com.gk.wifictr.net.*;
import com.zijunlin.Zxing.Demo.CaptureActivity;

import android.view.*;
import android.os.*;
import android.app.*;

public class SetHostInfoActivity extends BaseViewActivity
{
    public static Handler handler;
    private Context context;
    private EditText edtPassward;
    private EditText edtSnCode;
    private int second;
    Handler timeHandler;
    private Timer timer;
    private TimerTask timerTask;
    
    static {
        SetHostInfoActivity.handler = null;
    }
    
    public SetHostInfoActivity() {
        this.context = (Context)this;
        this.second = 60;
        this.timeHandler = new Handler() {
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 1: {
                        SetHostInfoActivity.this.loadingTv.setText((String.valueOf(SetHostInfoActivity.this.getString(2131296427)) + "\n00:" + message.obj));
                        break;
                    }
                }
            }
        };
    }
    
    static /* synthetic */ void access$5(SetHostInfoActivity setHostInfoActivity, int second) {
        setHostInfoActivity.second = second;
    }
    
    private void exitDlg() {
        String string = this.edtSnCode.getText().toString();
        String string2 = this.edtPassward.getText().toString();
        if (string.equals("") && string2.equals("")) {
            super.onBackPressed();
            return;
        }
        AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder((Context)this);
        alertDialog$Builder.setTitle(this.getResString(2131296284));
        alertDialog$Builder.setMessage(this.getString(2131296380));
        alertDialog$Builder.setPositiveButton(this.getResString(2131296285), (DialogInterface.OnClickListener)new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int n) {
                SetHostInfoActivity.this.finish();
            }
        });
        alertDialog$Builder.setNegativeButton(this.getResString(2131296286), (DialogInterface.OnClickListener)null);
        alertDialog$Builder.show();
    }
    
    private void reset() {
        String string = this.edtSnCode.getText().toString();
        String string2 = this.edtPassward.getText().toString();
        if (string.equals("") && string2.equals("")) {
            return;
        }
        AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder((Context)this);
        alertDialog$Builder.setTitle(this.getResString(2131296284));
        alertDialog$Builder.setMessage(this.getResString(2131296470));
        alertDialog$Builder.setPositiveButton(this.getResString(2131296285), (DialogInterface.OnClickListener)new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int n) {
                SetHostInfoActivity.this.edtSnCode.setText("");
                SetHostInfoActivity.this.edtPassward.setText("");
                Util.saveValue("host_name", "");
                Util.saveValue("host_sn", "");
                Util.saveValue("host_pass", "");
            }
        });
        alertDialog$Builder.setNegativeButton(this.getResString(2131296286), (DialogInterface.OnClickListener)null);
        alertDialog$Builder.show();
    }
    
    private void submint() {
        String string = this.edtSnCode.getText().toString();
        String string2 = this.edtPassward.getText().toString();
        if (string.length() != 12) {
            this.showToast(this.getString(2131296428));
            return;
        }
        if (string2.equals("")) {
            Util.toast((Context)this, this.getString(2131296429));
            return;
        }
        ConnectService.defWifi.SN = string;
        ConnectService.defWifi.password = string2;
        ConnectService.defWifi.save(ConnectService.defWifinum);
        this.finish();
    }
    
    private void timeStart() {
        if (this.timer == null && this.timerTask == null) {
            this.timer = new Timer();
            this.timerTask = new TimerTask() {
                @Override
                public void run() {
                    SetHostInfoActivity this$0 = SetHostInfoActivity.this;
                    SetHostInfoActivity.access$5(this$0, this$0.second - 1);
                    Message message = new Message();
                    message.what = 1;
                    message.obj = SetHostInfoActivity.this.second;
                    SetHostInfoActivity.this.timeHandler.sendMessage(message);
                    if (SetHostInfoActivity.this.second <= 0) {
                        SetHostInfoActivity.this.timeHandler.post((Runnable)new Runnable() {
                            @Override
                            public void run() {
                                SetHostInfoActivity.this.loadingDialog.dismiss();
                                SetHostInfoActivity.this.timerCancle();
                            }
                        });
                    }
                }
            };
            this.timer.schedule(this.timerTask, 0L, 1000L);
        }
    }
    
    private void timerCancle() {
        ConnectService.getSnSuccess = false;
        this.second = 60;
        if (this.timer != null) {
            this.timer.cancel();
            this.timer = null;
        }
        if (this.timerTask != null) {
            this.timerTask = null;
        }
    }
    
    @Override
    protected void getSn(Intent intent) {
        if (ConnectService.getSnSuccess) {
            String stringExtra = intent.getStringExtra("SN");
            String stringExtra2 = intent.getStringExtra("PASSWD");
            if (stringExtra != null && stringExtra2 != null) {
                this.edtSnCode.setText(stringExtra);
                this.edtPassward.setText(stringExtra2);
                ConnectService.defWifi.SN = stringExtra;
                ConnectService.defWifi.password = stringExtra2;
                ConnectService.defWifi.save(ConnectService.defWifinum);
                this.loadingDlgDismiss();
                ConnectService.getSnSuccess = false;
                this.showTipsDlg(this.getResString(2131296469));
            }
        }
    }
    
    public void onBackPressed() {
        super.onBackPressed();
    }
    
    public void onClick(View view) {
        switch (view.getId()) {
            case 2131231049: {
                Intent intent = new Intent((Context)this, (Class)CaptureActivity.class);
                intent.putExtra("from_which_acticity", 2);
                this.startActivity(intent);
                break;
            }
            case 2131231052: {
                this.reset();
                break;
            }
            case 2131231053: {
                this.submint();
                break;
            }
            case 2131231054: {
                ConnectService.getSnSuccess = true;
                if (ConnectService.getSnSuccess) {
                    new BroadcastHelper().startUDPSearch(61);
                    this.createLoadingDialog(this.context, this.getString(2131296427), new Runnable() {
                        @Override
                        public void run() {
                            SetHostInfoActivity.this.timerCancle();
                        }
                    });
                    this.timeStart();
                    return;
                }
                break;
            }
        }
    }
    
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.title.setVisibility(0);
        this.title.setText(this.getResString(2131296414));
        this.setCenterView(2130903090);
        this.btnLeft.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(View view) {
                SetHostInfoActivity.this.finish();
            }
        });
        SetHostInfoActivity.handler = new SetHandler(Looper.myLooper());
        this.edtSnCode = (EditText)Util.setView(this, 2131231050, null);
        this.edtPassward = (EditText)Util.setView(this, 2131231051, null);
        this.edtSnCode.setText(ConnectService.defWifi.SN);
        this.edtPassward.setText(ConnectService.defWifi.password);
        this.edtSnCode.setSelection(this.edtSnCode.getText().toString().length());
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.timerCancle();
    }
    
    protected class SetHandler extends Handler
    {
        Looper looper;
        
        public SetHandler(Looper looper) {
            this.looper = looper;
        }
        
        public void handleMessage(Message message) {
            switch (message.what) {
                case 10: {
                    if (message.obj == null) {
                        break;
                    }
                    String s = (String)message.obj;
                    if (!s.substring(0, 2).equals("XY")) {
                        Util.toast(SetHostInfoActivity.this.context, SetHostInfoActivity.this.getString(2131296402));
                        return;
                    }
                    if (!String.valueOf(s.charAt(2)).equals("B")) {
                        Util.toast(SetHostInfoActivity.this.context, SetHostInfoActivity.this.getString(2131296430));
                        return;
                    }
                    SetHostInfoActivity.this.edtSnCode.setText(s.substring(4, s.length()));
                }
            }
        }
    }
}
