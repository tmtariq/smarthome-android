package com.gk.zhangzhongbao.ui.activity;

import android.widget.*;
import android.view.*;
import android.content.*;
import android.os.*;

public class TempActivity extends BaseViewActivity
{
    Button btnLearn315;
    Button btnLearn433;
    Button btnNor;
    private View.OnClickListener mCurtainClickListener;
    private View.OnClickListener mLightClickListener;
    
    public TempActivity() {
        this.mLightClickListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                switch (view.getId()) {
                    case 2131231103: {
                        final Intent intent = new Intent((Context)TempActivity.this, (Class)AddLightActivity.class);
                        intent.putExtra("learn_code_dev", 0);
                        TempActivity.this.startActivity(intent);
                        break;
                    }
                    case 2131231104: {
                        final Intent intent2 = new Intent((Context)TempActivity.this, (Class)AddLightActivity.class);
                        intent2.putExtra("learn_code_dev", 1);
                        TempActivity.this.startActivity(intent2);
                        break;
                    }
                    case 2131231105: {
                        final Intent intent3 = new Intent((Context)TempActivity.this, (Class)AddLightActivity.class);
                        intent3.putExtra("learn_code_dev", 2);
                        TempActivity.this.startActivity(intent3);
                        break;
                    }
                    case 2131230828: {
                        TempActivity.this.startActivity(new Intent((Context)TempActivity.this, (Class)ShareDataActivity.class));
                        break;
                    }
                }
                TempActivity.this.finish();
            }
        };
        this.mCurtainClickListener = (View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                switch (view.getId()) {
                    case 2131231103: {
                        final Intent intent = new Intent((Context)TempActivity.this, (Class)AddCurtainGarageActivity.class);
                        intent.putExtra("learn_code_dev", 0);
                        TempActivity.this.startActivity(intent);
                        break;
                    }
                    case 2131231104: {
                        final Intent intent2 = new Intent((Context)TempActivity.this, (Class)AddCurtainGarageActivity.class);
                        intent2.putExtra("learn_code_dev", 1);
                        TempActivity.this.startActivity(intent2);
                        break;
                    }
                    case 2131231105: {
                        final Intent intent3 = new Intent((Context)TempActivity.this, (Class)AddCurtainGarageActivity.class);
                        intent3.putExtra("learn_code_dev", 2);
                        TempActivity.this.startActivity(intent3);
                        break;
                    }
                }
                TempActivity.this.finish();
            }
        };
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setCenterView(2130903099);
        this.btnRight.setBackgroundResource(2130837506);
        this.btnNor = (Button)this.findViewById(2131231103);
        this.btnLearn433 = (Button)this.findViewById(2131231104);
        this.btnLearn315 = (Button)this.findViewById(2131231105);
        switch (this.getIntent().getIntExtra("intent_to_temp", 1)) {
            default: {}
            case 1: {
                this.title.setText((CharSequence)this.getString(2131296374));
                this.btnNor.setOnClickListener(this.mLightClickListener);
                this.btnLearn433.setOnClickListener(this.mLightClickListener);
                this.btnLearn315.setOnClickListener(this.mLightClickListener);
                this.btnRight.setOnClickListener(this.mLightClickListener);
            }
            case 2: {
                this.title.setText((CharSequence)this.getString(2131296375));
                this.btnNor.setOnClickListener(this.mCurtainClickListener);
                this.btnLearn433.setOnClickListener(this.mCurtainClickListener);
                this.btnLearn315.setOnClickListener(this.mCurtainClickListener);
            }
        }
    }
}
