package com.gk.zhangzhongbao.ui.fragment;

import android.support.v4.app.*;
import com.gk.wifictr.net.data.*;
import android.graphics.*;
import com.gk.zhangzhongbao.ui.view.*;
import android.support.v4.content.*;
import android.util.*;
import com.gk.wifictr.net.command.*;
import com.gk.zhangzhongbao.ui.app.*;
import com.gk.wifictr.net.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.content.*;
import java.util.*;
import com.gk.wifictr.net.command.station.*;

public class STBFragment extends Fragment implements View.OnClickListener
{
    private static final String SN_DIANXIN = "5b0100";
    public static final String STB_BRAND_NAME = "stb_brand_name";
    public static final String STB_BRAND_POSITION = "stb_brand_position";
    public static final String STB_DEVNO = "stb_devno";
    public static final String STB_SN = "stb_sn";
    public static SubDevice tvDevice;
    private byte accode;
    private MyApplication app;
    private Button area;
    private SelectorImageView blue;
    private ArrayList<Bitmap> bmpList;
    private String brandString;
    private Button btnBTSPower;
    private SelectorImageView btnBottom;
    private SelectorImageView btnDefirm;
    private Button btnFirstPage;
    private Button btnLastPage;
    private SelectorImageView btnLeft;
    private Button btnNextPage;
    private Button btnNoSound;
    private Button btnReturn;
    private SelectorImageView btnRight;
    private Button btnTV;
    private SelectorImageView btnTop;
    String code;
    int[] codes;
    private Context context;
    private BrandDialog dialog;
    private SelectorImageView green;
    private int index;
    LocalBroadcastManager lbcManager;
    private List<Map<String, Object>> list;
    private int position;
    BroadcastReceiver receiver;
    private SelectorImageView red;
    private String snCode;
    private SelectorImageView stb_bottom1;
    private SelectorImageView stb_bottom2;
    private SelectorImageView stb_top1;
    private SelectorImageView stb_top2;
    private SelectorImageView yellow;
    
    public STBFragment() {
        this.snCode = "";
        this.receiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final int int1 = intent.getExtras().getInt("cmd");
                Log.e("channel receive", "cmd:" + int1);
                switch (int1) {
                    case 4: {
                        final ReadData readData = (ReadData)intent.getParcelableExtra("data");
                        if (readData == null) {
                            break;
                        }
                        Log.e("data body", ByteUtil.byte2hex(readData.body));
                        Log.e("type", ByteUtil.byte2hex(new byte[] { readData.type }));
                        if (readData.type == 19) {
                            for (final SubDevice subDevice : new In_13H().decode(readData)) {
                                if (subDevice.devType == 1) {
                                    STBFragment.this.app.stbDevice = subDevice;
                                    STBFragment.this.area.setText((CharSequence)STBFragment.this.app.stbDevice.devName);
                                    Util.saveValue("stb_sn", STBFragment.this.app.stbDevice.devSN);
                                    Util.saveValue("stb_brand_name", STBFragment.this.app.stbDevice.devName);
                                    Util.saveValue("stb_devno", STBFragment.this.app.stbDevice.devNo);
                                    for (int i = 0; i < Brands.stbBrandsNames.length; ++i) {
                                        if (Brands.stbBrandsNames[i].equals(STBFragment.this.app.stbDevice.devName)) {
                                            STBFragment.access$2(STBFragment.this, i);
                                            Util.saveValue("stb_brand_position", STBFragment.this.position);
                                        }
                                    }
                                }
                                else {
                                    if (subDevice.devType != 3) {
                                        continue;
                                    }
                                    STBFragment.tvDevice = subDevice;
                                }
                            }
                            break;
                        }
                        break;
                    }
                }
            }
        };
        this.list = new ArrayList<Map<String, Object>>();
        this.index = 1;
    }
    
    static /* synthetic */ void access$2(final STBFragment stbFragment, final int position) {
        stbFragment.position = position;
    }
    
    static /* synthetic */ void access$6(final STBFragment stbFragment, final String brandString) {
        stbFragment.brandString = brandString;
    }
    
    static /* synthetic */ void access$7(final STBFragment stbFragment, final int index) {
        stbFragment.index = index;
    }
    
    private void send(final int n) {
        Log.e("stb sn", this.app.stbDevice.devSN);
        new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(this.app.stbDevice.devNo, this.app.stbDevice.devName, this.app.stbDevice.devSN, (byte)1), (byte)n));
    }
    
    private void send(final String s, final int n) {
        Log.e("stb sn", this.app.stbDevice.devSN);
        new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(this.app.stbDevice.devNo, this.app.stbDevice.devName, s, (byte)1), (byte)n));
    }
    
    private void setSelectorImgView(SelectorImageView selectorImageView, final View view, final int n, final int n2, final int n3) {
        selectorImageView = (SelectorImageView)Util.setView(view, n, this);
        selectorImageView.setRes(n2, n3);
        if (selectorImageView.bitmap_n != null) {
            this.bmpList.add(selectorImageView.bitmap_n);
        }
        if (selectorImageView.bitmap_p != null) {
            this.bmpList.add(selectorImageView.bitmap_p);
        }
    }
    
    public void onClick(final View view) {
        Util.vibrator(this.context);
        Util.clickBeepSound(this.context);
        switch (view.getId()) {
            case 2131230931: {
                this.send(2);
            }
            case 2131230891: {
                this.send(9);
            }
            case 2131230892: {
                this.send(16);
            }
            case 2131230893: {
                this.send(1);
            }
            case 2131230894: {
                this.send(5);
            }
            case 2131230896: {
                this.send(4);
            }
            case 2131230898: {
                this.send(6);
            }
            case 2131230900: {
                this.send(8);
            }
            case 2131230902: {
                this.send(10);
            }
            case 2131230899: {
                this.send(7);
            }
            case 2131230905: {
                this.send(14);
            }
            case 2131230906: {
                this.send(17);
            }
            case 2131230909: {
                this.send(12);
            }
            case 2131230910: {
                this.send(15);
            }
            case 2131230907: {
                this.send(18);
            }
            case 2131230908: {
                this.send(19);
            }
            case 2131230915: {
                this.send(20);
            }
            case 2131230916: {
                this.send(21);
            }
            case 2131230917: {
                this.send(22);
            }
            case 2131230918: {
                this.send(23);
            }
            case 2131230919: {
                this.send(24);
            }
            case 2131230920: {
                this.send(25);
            }
            case 2131230921: {
                this.send(26);
            }
            case 2131230922: {
                this.send(27);
            }
            case 2131230923: {
                this.send(28);
            }
            case 2131230924: {
                this.send(29);
            }
            case 2131230925: {
                this.send(30);
            }
            case 2131230926: {
                this.send(31);
            }
            case 2131230927: {
                this.send(32);
            }
            case 2131230929: {
                this.send(34);
            }
            case 2131230928: {
                this.send(13);
            }
            case 2131230930: {
                this.send(35);
            }
            case 2131230932: {
                this.showDlg();
            }
            case 2131230911: {
                if (this.app.stbDevice.devSN.equals("5b0100")) {
                    new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(this.app.stbDevice.devNo, this.app.stbDevice.devName, "5b0100", (byte)5), (byte)20));
                    return;
                }
                break;
            }
            case 2131230912: {
                if (this.app.stbDevice.devSN.equals("5b0100")) {
                    new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(this.app.stbDevice.devNo, this.app.stbDevice.devName, "5b0100", (byte)5), (byte)23));
                    return;
                }
                break;
            }
            case 2131230913: {
                if (this.app.stbDevice.devSN.equals("5b0100")) {
                    new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(this.app.stbDevice.devNo, this.app.stbDevice.devName, "5b0100", (byte)5), (byte)21));
                    return;
                }
                break;
            }
            case 2131230914: {
                if (this.app.stbDevice.devSN.equals("5b0100")) {
                    new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(this.app.stbDevice.devNo, this.app.stbDevice.devName, "5b0100", (byte)5), (byte)22));
                    return;
                }
                break;
            }
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.context = (Context)this.getActivity();
        this.app = (MyApplication)this.getActivity().getApplication();
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        this.bmpList = new ArrayList<Bitmap>();
        final View inflate = layoutInflater.inflate(2130903068, viewGroup, false);
        this.setImageView(this.stb_top1, inflate, 2131230895, 2130838321);
        this.setImageView(this.stb_top2, inflate, 2131230897, 2130838322);
        this.setImageView(this.stb_bottom1, inflate, 2131230901, 2130838291);
        this.setImageView(this.stb_bottom2, inflate, 2131230903, 2130838292);
        this.setSelectorImgView(this.btnTop, inflate, 2131230896, 2130838320, 2130838324);
        this.setSelectorImgView(this.btnLeft, inflate, 2131230898, 2130838298, 2130838300);
        this.setSelectorImgView(this.btnRight, inflate, 2131230900, 2130838314, 2130838316);
        this.setSelectorImgView(this.btnBottom, inflate, 2131230902, 2130838290, 2130838294);
        this.setSelectorImgView(this.btnDefirm, inflate, 2131230899, 2130838301, 2130838303);
        this.btnBTSPower = Util.setButton(inflate, 2131230931, this);
        this.btnTV = Util.setButton(inflate, 2131230891, this);
        this.btnFirstPage = Util.setButton(inflate, 2131230892, this);
        this.btnNoSound = Util.setButton(inflate, 2131230893, this);
        this.btnReturn = Util.setButton(inflate, 2131230894, this);
        Util.setButton(inflate, 2131230905, this);
        Util.setButton(inflate, 2131230906, this);
        Util.setButton(inflate, 2131230909, this);
        Util.setButton(inflate, 2131230910, this);
        this.btnLastPage = Util.setButton(inflate, 2131230907, this);
        this.btnNextPage = Util.setButton(inflate, 2131230908, this);
        this.setSelectorImgView(this.red, inflate, 2131230915, 2130838231, 2130838233);
        this.setSelectorImgView(this.green, inflate, 2131230916, 2130838076, 2130838078);
        this.setSelectorImgView(this.yellow, inflate, 2131230917, 2130838555, 2130838557);
        this.setSelectorImgView(this.blue, inflate, 2131230918, 2130837572, 2130837574);
        Util.setButton(inflate, 2131230919, this);
        Util.setButton(inflate, 2131230920, this);
        Util.setButton(inflate, 2131230921, this);
        Util.setButton(inflate, 2131230922, this);
        Util.setButton(inflate, 2131230923, this);
        Util.setButton(inflate, 2131230924, this);
        Util.setButton(inflate, 2131230925, this);
        Util.setButton(inflate, 2131230926, this);
        Util.setButton(inflate, 2131230927, this);
        Util.setButton(inflate, 2131230929, this);
        Util.setButton(inflate, 2131230928, this);
        Util.setButton(inflate, 2131230930, this);
        this.area = Util.setButton(inflate, 2131230932, this);
        Util.setButton(inflate, 2131230911, this);
        Util.setButton(inflate, 2131230912, this);
        Util.setButton(inflate, 2131230913, this);
        Util.setButton(inflate, 2131230914, this);
        this.lbcManager = LocalBroadcastManager.getInstance((Context)this.getActivity());
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.gk.wifictr.net");
        this.lbcManager.registerReceiver(this.receiver, intentFilter);
        this.app.stbDevice = new SubDevice(Util.getIntValue("stb_devno"), Util.getStringValue("stb_brand_name", "\u9009\u62e9\u673a\u578b"), Util.getStringValue("stb_sn", "830000"), (byte)1);
        this.area.setText((CharSequence)this.app.stbDevice.devName);
        this.position = Util.getIntValue("stb_brand_position");
        STBFragment.tvDevice = new SubDevice(0, "ChangHong", "660100", (byte)3);
        new BroadcastHelper().writeData(new Out_26H().make());
        return inflate;
    }
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.recycle();
        this.lbcManager.unregisterReceiver(this.receiver);
    }
    
    public void recycle() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.e("bmpList size destory", new StringBuilder(String.valueOf(STBFragment.this.bmpList.size())).toString());
                    final ArrayList<Bitmap> list = new ArrayList<Bitmap>();
                    for (final Bitmap bitmap : STBFragment.this.bmpList) {
                        if (bitmap != null && !bitmap.isRecycled()) {
                            Log.e("", "recycle");
                            bitmap.recycle();
                            System.gc();
                            list.add(bitmap);
                        }
                    }
                    STBFragment.this.bmpList.removeAll(list);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }
    
    protected void setImageView(final ImageView imageView, final View view, final int n, final int n2) {
        final Bitmap bitmap = null;
        ImageView imageView2 = imageView;
        if (imageView == null) {
            imageView2 = (ImageView)Util.setView(view, n, null);
        }
        Bitmap bitMap = bitmap;
        if (n2 > 0) {
            bitMap = Util.readBitMap((Context)this.getActivity(), n2);
        }
        imageView2.setImageBitmap(bitMap);
    }
    
    public void showDlg() {
        if (this.dialog == null) {
            for (int i = 0; i < Brands.stbBrandsNames.length; ++i) {
                final HashMap<String, Object> hashMap = new HashMap<String, Object>();
                hashMap.put("brand", Brands.stbBrandsNames[i]);
                hashMap.put("codes", Brands.stbBrands[i]);
                hashMap.put("index", 1);
                this.list.add((Map<String, Object>)hashMap);
            }
            (this.dialog = new BrandDialog(this.context, this.list, 2131361809)).setListener((BrandDialog.ClickListener)new BrandDialog.ClickListener() {
                @Override
                public void itemClick(final int n) {
                    Util.vibrator(STBFragment.this.context);
                    final Map<String, Object> map = STBFragment.this.list.get(n);
                    //STBFragment.access$2(STBFragment.this, n);
                    position = n;
                    //STBFragment.access$6(STBFragment.this, map.get("brand"));
                    brandString = (String)map.get("brand");
                    STBFragment.this.codes = (int[])(Object)map.get("codes");
                    //STBFragment.access$7(STBFragment.this, (int)map.get("index"));
                    index = Integer.valueOf(((String)map.get("index")));
                    STBFragment.this.dialog.brandName.setText((CharSequence)STBFragment.this.brandString);
                    STBFragment.this.dialog.brandNum.setText((CharSequence)new StringBuilder(String.valueOf(STBFragment.this.index)).toString());
                    STBFragment.this.send(Util.codeToSn(STBFragment.this.codes[STBFragment.this.index - 1]), 12);
                }
                
                @Override
                public void leftClick() {
                    final STBFragment this$0 = STBFragment.this;
                    STBFragment.access$7(this$0, this$0.index - 1);
                    if (STBFragment.this.index <= 0) {
                        STBFragment.access$7(STBFragment.this, STBFragment.this.codes.length);
                    }
                    STBFragment.this.dialog.brandNum.setText((CharSequence)new StringBuilder(String.valueOf(STBFragment.this.index)).toString());
                    STBFragment.this.send(Util.codeToSn(STBFragment.this.codes[STBFragment.this.index - 1]), 12);
                }
                
                @Override
                public void okClick() {
                    STBFragment.this.app.stbDevice.devSN = Util.codeToSn(STBFragment.this.codes[STBFragment.this.index - 1]);
                    STBFragment.this.app.stbDevice.devName = STBFragment.this.brandString;
                    STBFragment.this.send(12);
                    STBFragment.this.area.setText((CharSequence)STBFragment.this.brandString);
                    STBFragment.this.dialog.dismiss();
                    new BroadcastHelper().writeData(new Out_25H().make(STBFragment.this.app.stbDevice));
                }
                
                @Override
                public void rightClick() {
                    final STBFragment this$0 = STBFragment.this;
                    STBFragment.access$7(this$0, this$0.index + 1);
                    if (STBFragment.this.index > STBFragment.this.codes.length) {
                        STBFragment.access$7(STBFragment.this, 1);
                    }
                    STBFragment.this.dialog.brandNum.setText((CharSequence)new StringBuilder(String.valueOf(STBFragment.this.index)).toString());
                    STBFragment.this.send(Util.codeToSn(STBFragment.this.codes[STBFragment.this.index - 1]), 12);
                }
            });
        }
        this.dialog.show();
        this.dialog.rlBrandName.setText((CharSequence)this.getString(2131296269));
        this.dialog.rlBrandNum.setText((CharSequence)this.getString(2131296270));
        final Map<String, Object> map = this.list.get(this.position);
        this.brandString = (String)map.get("brand");
        this.codes = (int[])map.get("codes");
        final int snToCode = Util.snToCode(this.app.stbDevice.devSN);
        for (int j = 0; j < this.codes.length; ++j) {
            if (snToCode == this.codes[j]) {
                this.index = j + 1;
                break;
            }
        }
        this.dialog.brandNum.setText((CharSequence)new StringBuilder(String.valueOf(this.index)).toString());
        this.dialog.brandName.setText((CharSequence)this.brandString);
    }
}
