package com.gk.zhangzhongbao.ui.fragment;

import java.util.*;
import com.gk.zhangzhongbao.ui.bean.*;
import com.gk.zhangzhongbao.ui.app.*;

public class TVStation
{
    public static ArrayList<Channel> Chinese(final int n) {
        final ArrayList<Channel> list = new ArrayList<Channel>();
        switch (n) {
            default: {
                return list;
            }
            case 0: {
                list.add(new Channel().setImgResName("ywds").setName("\u6570\u5b57\u7535\u89c6\u4e1a\u52a1\u5bfc\u89c6").setChannelNum(13).setId(229));
                list.add(new Channel().setImgResName("local_zs").setName("\u4e2d\u5c71\u7efc\u5408").setChannelNum(4).setId(10));
                list.add(new Channel().setImgResName("local_zs").setName("\u4e2d\u5c71\u516c\u5171").setChannelNum(5).setId(11));
                list.add(new Channel().setImgResName("local_hkfc").setName("\u7fe1\u7fe0\u53f0").setChannelNum(6).setId(13));
                list.add(new Channel().setImgResName("local_ys").setName("\u672c\u6e2f\u53f0").setChannelNum(9).setId(14));
                list.add(new Channel().setImgResName("local_zs").setName("\u4e2d\u5c71\u6559\u80b2").setChannelNum(3).setId(12));
                list.add(new Channel().setImgResName("local_zs").setName("\u4e2d\u5c71\u516c\u5171\u4fe1\u606f").setChannelNum(2).setId(17));
                list.add(new Channel().setImgResName("dst_xiaxi").setName("\u56fd\u9645\u53f0(\u6e05\u6d41)").setChannelNum(150).setId(16));
                list.add(new Channel().setImgResName("mingzhu").setName("\u660e\u73e0\u53f0").setChannelNum(10).setId(15));
                list.add(new Channel().setImgResName("dst_xiaxi").setName("\u56fd\u9645\u53f0").setChannelNum(11).setId(16));
                return list;
            }
            case 1: {
                list.add(new Channel().setImgResName("cctv1").setName("CCTV-1").setChannelNum(Brands.chNums[0]).setId(100));
                list.add(new Channel().setImgResName("cctv2").setName("CCTV-2").setChannelNum(Brands.chNums[12]).setId(101));
                list.add(new Channel().setImgResName("cctv3").setName("CCTV-3").setChannelNum(Brands.chNums[13]).setId(102));
                list.add(new Channel().setImgResName("cctv4").setName("CCTV-4").setChannelNum(Brands.chNums[14]).setId(103));
                list.add(new Channel().setImgResName("cctv5").setName("CCTV-5").setChannelNum(Brands.chNums[15]).setId(104));
                list.add(new Channel().setImgResName("cctv6").setName("CCTV-6").setChannelNum(Brands.chNums[16]).setId(105));
                list.add(new Channel().setImgResName("cctv7").setName("CCTV-7").setChannelNum(Brands.chNums[17]).setId(106));
                list.add(new Channel().setImgResName("cctv8").setName("CCTV-8").setChannelNum(Brands.chNums[18]).setId(107));
                list.add(new Channel().setImgResName("cctv9").setName("CCTV-9").setChannelNum(Brands.chNums[19]).setId(108));
                list.add(new Channel().setImgResName("cctv10").setName("CCTV-10").setChannelNum(Brands.chNums[20]).setId(110));
                list.add(new Channel().setImgResName("cctv11").setName("CCTV-11").setChannelNum(Brands.chNums[21]).setId(111));
                list.add(new Channel().setImgResName("cctv12").setName("CCTV-12").setChannelNum(Brands.chNums[22]).setId(112));
                list.add(new Channel().setImgResName("cctv13").setName("CCTV-13").setChannelNum(Brands.chNums[23]).setId(113));
                list.add(new Channel().setImgResName("cctv14").setName("CCTV-\u5c11\u513f").setChannelNum(Brands.chNums[25]).setId(114));
                list.add(new Channel().setImgResName("cctv_news_149").setName("CCTV-news").setChannelNum(149).setId(149));
                list.add(new Channel().setImgResName("cetv_1").setName("CETV-1").setChannelNum(143).setId(220));
                return list;
            }
            case 2: {
                list.add(new Channel().setImgResName("guangdong").setName("\u5e7f\u4e1c\u73e0\u6c5f").setChannelNum(131).setId(92));
                list.add(new Channel().setImgResName("guangdong").setName("\u73b0\u4ee3\u6559\u80b2").setChannelNum(51).setId(93));
                list.add(new Channel().setImgResName("weishi_guangdong").setName("\u5e7f\u4e1c\u536b\u89c6").setChannelNum(50).setId(91));
                list.add(new Channel().setImgResName("guangdong").setName("\u5e7f\u4e1c\u65b0\u95fb").setChannelNum(53).setId(23));
                list.add(new Channel().setImgResName("guangdong").setName("\u5e7f\u4e1c\u516c\u5171").setChannelNum(54).setId(28));
                list.add(new Channel().setImgResName("guangdong").setName("\u5e7f\u4e1c\u4f53\u80b2").setChannelNum(55).setId(30));
                list.add(new Channel().setImgResName("tvs").setName("TVS-1").setChannelNum(56).setId(20));
                list.add(new Channel().setImgResName("tvs").setName("TVS-2").setChannelNum(57).setId(21));
                list.add(new Channel().setImgResName("tvs").setName("TVS-3").setChannelNum(132).setId(90));
                list.add(new Channel().setImgResName("tvs").setName("TVS-4").setChannelNum(58).setId(29));
                list.add(new Channel().setImgResName("tvs").setName("TVS-5").setChannelNum(133).setId(22));
                list.add(new Channel().setImgResName("jiajiakatong").setName("\u5609\u4f73\u5361\u901a").setChannelNum(145).setId(24));
                list.add(new Channel().setImgResName("bs_dgzh").setName("\u4e1c\u839e\u7efc\u5408").setChannelNum(135).setId(25));
                list.add(new Channel().setImgResName("tv_hzzh").setName("\u60e0\u5dde\u7efc\u5408").setChannelNum(136).setId(26));
                list.add(new Channel().setImgResName("tv_jmzh").setName("\u6c5f\u95e8\u516c\u5171").setChannelNum(141).setId(161));
                list.add(new Channel().setImgResName("tv_fsgg").setName("\u4f5b\u5c71\u516c\u5171").setChannelNum(134).setId(31));
                list.add(new Channel().setImgResName("tv_zhyt").setName("\u73e0\u6d771\u5957").setChannelNum(139).setId(115));
                list.add(new Channel().setImgResName("bs_qyzh").setName("\u6e05\u8fdc\u7efc\u5408").setChannelNum(137).setId(150));
                list.add(new Channel().setImgResName("bs_czzh").setName("\u6f6e\u5dde\u7efc\u5408").setChannelNum(138).setId(151));
                list.add(new Channel().setImgResName("bs_sqzh").setName("\u8087\u5e86\u7efc\u5408").setChannelNum(140).setId(50));
                return list;
            }
            case 3: {
                list.add(new Channel().setImgResName("dst_anhui").setName("\u5b89\u5fbd\u536b\u89c6").setChannelNum(29).setId(158));
                list.add(new Channel().setImgResName("jiangsu").setName("\u6c5f\u82cf\u536b\u89c6").setChannelNum(30).setId(58));
                list.add(new Channel().setImgResName("tianjing").setName("\u5929\u6d25\u536b\u89c6").setChannelNum(31).setId(55));
                list.add(new Channel().setImgResName("shandong").setName("\u5c71\u4e1c\u536b\u89c6").setChannelNum(34).setId(57));
                list.add(new Channel().setImgResName("ws_dongfang").setName("\u4e1c\u65b9\u536b\u89c6").setChannelNum(38).setId(163));
                list.add(new Channel().setImgResName("ws_hubei").setName("\u6e56\u5317\u536b\u89c6").setChannelNum(37).setId(165));
                list.add(new Channel().setImgResName("liaoling").setName("\u8fbd\u5b81\u536b\u89c6").setChannelNum(36).setId(63));
                list.add(new Channel().setImgResName("ws_lvyou").setName("\u65c5\u6e38\u536b\u89c6").setChannelNum(39).setId(169));
                list.add(new Channel().setImgResName("sw_bingtuan").setName("\u65b0\u7586\u5175\u56e2\u536b\u89c6").setChannelNum(142).setId(140));
                list.add(new Channel().setImgResName("ws_zhejiang").setName("\u6d59\u6c5f\u536b\u89c6").setChannelNum(40).setId(171));
                list.add(new Channel().setImgResName("dst_jinyingkatong").setName("\u91d1\u9e70\u5361\u901a").setChannelNum(49).setId(145));
                list.add(new Channel().setImgResName("ch_youmankatong").setName("\u4f18\u6f2b\u5361\u901a").setChannelNum(59).setId(146));
                list.add(new Channel().setImgResName("ws_jiangxi").setName("\u6c5f\u897f\u536b\u89c6").setChannelNum(35).setId(164));
                list.add(new Channel().setImgResName("sichun").setName("\u56db\u5ddd\u536b\u89c6").setChannelNum(41).setId(53));
                list.add(new Channel().setImgResName("dst_ningxia").setName("\u5b81\u590f\u536b\u89c6").setChannelNum(42).setId(130));
                list.add(new Channel().setImgResName("henan").setName("\u6cb3\u5357\u536b\u89c6").setChannelNum(44).setId(67));
                list.add(new Channel().setImgResName("ws_chongqing").setName("\u91cd\u5e86\u536b\u89c6").setChannelNum(46).setId(172));
                list.add(new Channel().setImgResName("ws_xizang").setName("\u897f\u85cf\u536b\u89c6").setChannelNum(48).setId(170));
                list.add(new Channel().setImgResName("jiangxi").setName("\u5e7f\u897f\u536b\u89c6").setChannelNum(47).setId(52));
                list.add(new Channel().setImgResName("weishi_aoya").setName("\u6fb3\u4e9a\u536b\u89c6").setChannelNum(52).setId(167));
                list.add(new Channel().setImgResName("shenzhen").setName("\u6df1\u5733\u536b\u89c6").setChannelNum(32).setId(59));
                list.add(new Channel().setImgResName("hunan").setName("\u6e56\u5357\u536b\u89c6").setChannelNum(43).setId(51));
                list.add(new Channel().setImgResName("ws_xinjiang").setName("\u65b0\u7586\u536b\u89c6").setChannelNum(144).setId(65));
                list.add(new Channel().setImgResName("ws_beijing").setName("\u5317\u4eac\u536b\u89c6").setChannelNum(33).setId(157));
                list.add(new Channel().setImgResName("ws_gansu").setName("\u7518\u8083\u536b\u89c6").setChannelNum(45).setId(157));
                return list;
            }
            case 4: {
                list.add(new Channel().setImgResName("dst_yihegouwu").setName("\u5b9c\u548c\u8d2d\u7269").setChannelNum(8).setId(142));
                list.add(new Channel().setImgResName("dst_kuailegouwu").setName("\u5feb\u4e50\u8d2d\u7269").setChannelNum(7).setId(127));
                list.add(new Channel().setImgResName("ch_yggw").setName("\u592e\u5e7f\u8d2d\u7269").setChannelNum(26).setId(143));
                list.add(new Channel().setImgResName("yougouwu").setName("\u4f18\u8d2d\u7269").setChannelNum(28).setId(221));
                return list;
            }
            case 5: {
                list.add(new Channel().setImgResName("tv_cpd").setName("\u8336\u9891\u9053").setChannelNum(147).setId(120));
                list.add(new Channel().setImgResName("tv_rwpd").setName("\u4eba\u7269\u9891\u9053").setChannelNum(146).setId(121));
                list.add(new Channel().setImgResName("tv_wssj").setName("\u6b66\u672f\u4e16\u754c").setChannelNum(60).setId(122));
                list.add(new Channel().setImgResName("tv_ly").setName("\u68a8\u56ed\u9891\u9053").setChannelNum(148).setId(123));
                list.add(new Channel().setImgResName("tv_jbty").setName("\u52b2\u7206\u4f53\u80b2").setChannelNum(62).setId(124));
                list.add(new Channel().setImgResName("tv_gef").setName("\u9ad8\u5c14\u592b").setChannelNum(63).setId(125));
                list.add(new Channel().setImgResName("tv_tywq").setName("\u5929\u5143\u56f4\u68cb").setChannelNum(64).setId(126));
                list.add(new Channel().setImgResName("tv_fyzq").setName("\u98ce\u4e91\u8db3\u7403").setChannelNum(79).setId(128));
                list.add(new Channel().setImgResName("tv_ozzq").setName("\u6b27\u6d32\u8db3\u7403").setChannelNum(61).setId(129));
                list.add(new Channel().setImgResName("tv_shdy").setName("\u56db\u6d77\u9493\u9c7c").setChannelNum(80).setId(131));
                list.add(new Channel().setImgResName("tv_dfcj").setName("\u4e1c\u65b9\u8d22\u7ecf").setChannelNum(65).setId(132));
                list.add(new Channel().setImgResName("tv_cftx").setName("\u8d22\u5bcc\u5929\u4e0b").setChannelNum(66).setId(133));
                list.add(new Channel().setImgResName("tv_sh").setName("\u4e66\u753b").setChannelNum(67).setId(134));
                list.add(new Channel().setImgResName("tv_ysjp").setName("\u592e\u89c6\u7cbe\u54c1").setChannelNum(68).setId(135));
                list.add(new Channel().setImgResName("tv_yybb").setName("\u4f18\u4f18\u5b9d\u8d1d").setChannelNum(69).setId(136));
                list.add(new Channel().setImgResName("tv_yyfd").setName("\u82f1\u8bed\u8f85\u5bfc").setChannelNum(70).setId(137));
                list.add(new Channel().setImgResName("tv_lxsj").setName("\u7559\u5b66\u4e16\u754c").setChannelNum(81).setId(138));
                list.add(new Channel().setImgResName("tv_kszx").setName("\u8003\u8bd5\u5728\u7ebf").setChannelNum(82).setId(180));
                list.add(new Channel().setImgResName("tv_gxpd").setName("\u56fd\u5b66\u9891\u9053").setChannelNum(83).setId(182));
                list.add(new Channel().setImgResName("tv_zgqx").setName("\u4e2d\u56fd\u6c14\u8c61").setChannelNum(71).setId(219));
                list.add(new Channel().setImgResName("tv_wsjk").setName("\u536b\u751f\u5065\u5eb7").setChannelNum(72).setId(185));
                list.add(new Channel().setImgResName("tv_lnl").setName("\u8001\u5e74\u798f").setChannelNum(73).setId(188));
                list.add(new Channel().setImgResName("tv_klcw").setName("\u5feb\u4e50\u5ba0\u7269").setChannelNum(84).setId(194));
                list.add(new Channel().setImgResName("tv_mlyy").setName("\u9b45\u529b\u97f3\u4e50").setChannelNum(74).setId(190));
                list.add(new Channel().setImgResName("tv_shss").setName("\u751f\u6d3b\u65f6\u5c1a").setChannelNum(75).setId(189));
                list.add(new Channel().setImgResName("tv_jsqc").setName("\u6781\u901f\u6c7d\u8f66").setChannelNum(76).setId(193));
                list.add(new Channel().setImgResName("tv_fyyy").setName("\u98ce\u4e91\u97f3\u4e50").setChannelNum(85).setId(195));
                list.add(new Channel().setImgResName("tv_nxss").setName("\u5973\u6027\u65f6\u5c1a").setChannelNum(86).setId(196));
                list.add(new Channel().setImgResName("tv_cctvyy").setName("CCTV-\u97f3\u4e50").setChannelNum(87).setId(198));
                list.add(new Channel().setImgResName("tv_sjdl").setName("\u4e16\u754c\u5730\u7406").setChannelNum(77).setId(200));
                list.add(new Channel().setImgResName("tv_gfjs").setName("\u56fd\u9632\u519b\u4e8b").setChannelNum(78).setId(191));
                list.add(new Channel().setImgResName("tv_fxzl").setName("\u53d1\u73b0\u4e4b\u65c5").setChannelNum(88).setId(202));
                list.add(new Channel().setImgResName("tv_sctx").setName("\u6536\u85cf\u5929\u4e0b").setChannelNum(89).setId(201));
                list.add(new Channel().setImgResName("tv_qjs").setName("\u5168\u7eaa\u5b9e").setChannelNum(90).setId(199));
                list.add(new Channel().setImgResName("tv_lgs").setName("\u8001\u6545\u4e8b").setChannelNum(91).setId(204));
                list.add(new Channel().setImgResName("tv_dyjc").setName("\u7b2c\u4e00\u5267\u573a").setChannelNum(92).setId(184));
                list.add(new Channel().setImgResName("tv_fyjc").setName("\u98ce\u4e91\u5267\u573a").setChannelNum(93).setId(206));
                list.add(new Channel().setImgResName("tv_hjjc").setName("\u6000\u65e7\u5267\u573a").setChannelNum(94).setId(192));
                list.add(new Channel().setImgResName("tv_dujc").setName("\u90fd\u5e02\u5267\u573a").setChannelNum(95).setId(187));
                list.add(new Channel().setImgResName("tv_doxtv").setName("DOXTV").setChannelNum(96).setId(205));
                list.add(new Channel().setImgResName("tv_xkdm").setName("\u65b0\u79d1\u52a8\u6f2b").setChannelNum(97).setId(210));
                list.add(new Channel().setImgResName("tv_dmxc").setName("\u52a8\u6f2b\u79c0\u573a").setChannelNum(98).setId(186));
                list.add(new Channel().setImgResName("tv_yxjj").setName("\u6e38\u620f\u7ade\u6280").setChannelNum(99).setId(217));
                list.add(new Channel().setImgResName("tv_yxfy").setName("\u6e38\u620f\u98ce\u4e91").setChannelNum(100).setId(216));
                return list;
            }
            case 6: {
                list.add(new Channel().setImgResName("cctv5_plus").setName("CCTV 5+").setChannelNum(101).setId(222));
                list.add(new Channel().setImgResName("tv_cctv1gq").setName("CCTV-1\u9ad8\u6e05").setChannelNum(102).setId(218));
                list.add(new Channel().setImgResName("tv_cctv3gq").setName("CCTV3\u9ad8\u6e05").setChannelNum(124).setId(215));
                list.add(new Channel().setImgResName("tv_cctv5gq").setName("CCTV-5\u9ad8\u6e05").setChannelNum(103).setId(215));
                list.add(new Channel().setImgResName("tv_cctv6gq").setName("CCTV6\u9ad8\u6e05").setChannelNum(127).setId(214));
                list.add(new Channel().setImgResName("tv_doxyx").setName("DOX\u9662\u7ebf(\u4e9a\u6d32)").setChannelNum(104).setId(213));
                list.add(new Channel().setImgResName("tv_xsjjs").setName("\u65b0\u89c6\u89c9\u7eaa\u5b9e").setChannelNum(105).setId(212));
                list.add(new Channel().setImgResName("tv_mlyyhd").setName("\u9b45\u529b\u97f3\u4e50HD").setChannelNum(106).setId(209));
                list.add(new Channel().setImgResName("tv_xsjjc").setName("\u6b22\u7b11\u5267\u573a").setChannelNum(107).setId(208));
                list.add(new Channel().setImgResName("tv_gdtygq").setName("\u5e7f\u4e1c\u4f53\u80b2\u9ad8\u6e05").setChannelNum(126).setId(207));
                list.add(new Channel().setImgResName("tv_bjwsgq").setName("\u5317\u4eac\u536b\u89c6\u9ad8\u6e05").setChannelNum(108).setId(181));
                list.add(new Channel().setImgResName("tv_zjwsgq").setName("\u6d59\u6c5f\u536b\u89c6\u9ad8\u6e05").setChannelNum(109).setId(183));
                list.add(new Channel().setImgResName("tv_jswsgq").setName("\u6c5f\u82cf\u536b\u89c6\u9ad8\u6e05").setChannelNum(110).setId(197));
                list.add(new Channel().setImgResName("tv_dfwsgq").setName("\u4e1c\u65b9\u536b\u89c6\u9ad8\u6e05").setChannelNum(111).setId(203));
                list.add(new Channel().setImgResName("tv_hnwsgq").setName("\u6e56\u5357\u536b\u89c6\u9ad8\u6e05").setChannelNum(112).setId(153));
                list.add(new Channel().setImgResName("tv_gdwsgq").setName("\u5e7f\u4e1c\u536b\u89c6\u9ad8\u6e05").setChannelNum(115).setId(144));
                list.add(new Channel().setImgResName("tv_szwsgq").setName("\u6df1\u5733\u536b\u89c6\u9ad8\u6e05").setChannelNum(116).setId(147));
                list.add(new Channel().setImgResName("tv_ahwsgq").setName("\u5b89\u5fbd\u536b\u89c6\u9ad8\u6e05").setChannelNum(117).setId(148));
                list.add(new Channel().setImgResName("tv_tjwsgq").setName("\u5929\u6d25\u536b\u89c6\u9ad8\u6e05").setChannelNum(128).setId(154));
                list.add(new Channel().setImgResName("tv_cctv3d").setName("CCTV-3D").setChannelNum(113).setId(141));
                list.add(new Channel().setImgResName("tv_gqyc").setName("\u9ad8\u6e05\u82f1\u8d85").setChannelNum(114).setId(19));
                list.add(new Channel().setImgResName("tv_jbtyhd").setName("\u52b2\u7206\u4f53\u80b2HD").setChannelNum(123).setId(109));
                list.add(new Channel().setImgResName("tv_doxyj").setName("DOX\u6021\u5bb6").setChannelNum(118).setId(223));
                list.add(new Channel().setImgResName("tv_doxyh").setName("DOX\u6620\u753b(\u6b27\u7f8e)").setChannelNum(119).setId(224));
                list.add(new Channel().setImgResName("tv_doxjc").setName("DOX\u5267\u573a").setChannelNum(120).setId(225));
                list.add(new Channel().setImgResName("tv_doxxz").setName("DOX\u65b0\u77e5").setChannelNum(121).setId(226));
                list.add(new Channel().setImgResName("tv_doxxy").setName("DOX\u65b0\u827a").setChannelNum(122).setId(227));
                list.add(new Channel().setImgResName("tv_chcgqdy").setName("CHC\u9ad8\u6e05\u7535\u5f71").setChannelNum(125).setId(228));
                return list;
            }
        }
    }
    
    public static ArrayList<Channel> Malaysia(final int n) {
        final ArrayList<Channel> list = new ArrayList<Channel>();
        switch (n) {
            default: {
                return list;
            }
            case 0: {
                list.add(new Channel().setImgResName("ch101").setName("tv1").setChannelNum(101).setId(101));
                list.add(new Channel().setImgResName("ch102").setName("tv2").setChannelNum(102).setId(102));
                list.add(new Channel().setImgResName("ch103").setName("tv3").setChannelNum(103).setId(103));
                list.add(new Channel().setImgResName("ch104").setName("Astro Ria").setChannelNum(104).setId(104));
                list.add(new Channel().setImgResName("ch105").setName("Astro Prima").setChannelNum(105).setId(105));
                list.add(new Channel().setImgResName("ch106").setName("Astro Oasis").setChannelNum(106).setId(106));
                list.add(new Channel().setImgResName("ch107").setName("NTV7").setChannelNum(107).setId(107));
                list.add(new Channel().setImgResName("ch108").setName("ZEE VARIASI").setChannelNum(108).setId(108));
                list.add(new Channel().setImgResName("ch111").setName("Arabic Radio & TV Variety").setChannelNum(111).setId(111));
                list.add(new Channel().setImgResName("ch114").setName("Al-Hijrah").setChannelNum(114).setId(114));
                list.add(new Channel().setImgResName("ch119").setName("TV 9").setChannelNum(119).setId(119));
                list.add(new Channel().setImgResName("ch132").setName("Astro Warna").setChannelNum(132).setId(132));
                list.add(new Channel().setImgResName("ch133").setName("Astro Bella").setChannelNum(133).setId(133));
                list.add(new Channel().setImgResName("ch141").setName("Bintang").setChannelNum(141).setId(141));
                list.add(new Channel().setImgResName("ch142").setName("Pelangi").setChannelNum(142).setId(142));
                list.add(new Channel().setImgResName("ch201").setName("Astro Vaanavil").setChannelNum(201).setId(201));
                list.add(new Channel().setImgResName("ch202").setName("Astro Vellithirai").setChannelNum(202).setId(202));
                list.add(new Channel().setImgResName("ch203").setName("Makkal TV").setChannelNum(203).setId(203));
                list.add(new Channel().setImgResName("ch211").setName("SUN TV").setChannelNum(211).setId(211));
                list.add(new Channel().setImgResName("ch214").setName("Adithya").setChannelNum(214).setId(214));
                list.add(new Channel().setImgResName("ch221").setName("Jaya TV").setChannelNum(221).setId(221));
                list.add(new Channel().setImgResName("ch222").setName("Raj TV").setChannelNum(222).setId(222));
                list.add(new Channel().setImgResName("ch223").setName("Kalaignar TV").setChannelNum(223).setId(223));
                list.add(new Channel().setImgResName("ch224").setName("Star Vijay").setChannelNum(224).setId(224));
                list.add(new Channel().setImgResName("ch301").setName("Astro AEC").setChannelNum(301).setId(301));
                list.add(new Channel().setImgResName("ch302").setName("iView").setChannelNum(302).setId(302));
                list.add(new Channel().setImgResName("ch304").setName("Kah Lai Toi").setChannelNum(304).setId(304));
                list.add(new Channel().setImgResName("ch305").setName("TVB Classic").setChannelNum(305).setId(305));
                list.add(new Channel().setImgResName("ch311").setName("Astro Wah Lai Toi").setChannelNum(311).setId(311));
                list.add(new Channel().setImgResName("ch312").setName("Phoenix Chinese Channel").setChannelNum(312).setId(312));
                list.add(new Channel().setImgResName("ch313").setName("TVBE News").setChannelNum(313).setId(313));
                list.add(new Channel().setImgResName("ch314").setName("TVB Xing He").setChannelNum(314).setId(314));
                list.add(new Channel().setImgResName("ch316").setName("CTI Asia").setChannelNum(316).setId(316));
                list.add(new Channel().setImgResName("ch317").setName("TVBS Asia").setChannelNum(317).setId(317));
                list.add(new Channel().setImgResName("ch324").setName("Astro Shuang Xing").setChannelNum(324).setId(324));
                list.add(new Channel().setImgResName("ch333").setName("Astro HUA HEE DAI").setChannelNum(333).setId(333));
                list.add(new Channel().setImgResName("ch334").setName("CCTV4").setChannelNum(334).setId(334));
                list.add(new Channel().setImgResName("ch391").setName("KBS World").setChannelNum(391).setId(391));
                list.add(new Channel().setImgResName("ch398").setName("NHK World").setChannelNum(398).setId(398));
                list.add(new Channel().setImgResName("ch471").setName("Astro Best SD 1").setChannelNum(471).setId(471));
                list.add(new Channel().setImgResName("ch472").setName("Astro Best SD 2").setChannelNum(472).setId(472));
                list.add(new Channel().setImgResName("ch473").setName("Astro Best SD 3").setChannelNum(473).setId(473));
                list.add(new Channel().setImgResName("ch474").setName("Astro Best SD 4").setChannelNum(474).setId(474));
                list.add(new Channel().setImgResName("ch477").setName("Astro Best SD 10").setChannelNum(477).setId(477));
                list.add(new Channel().setImgResName("ch478").setName("Astro Best SD 7").setChannelNum(478).setId(478));
                list.add(new Channel().setImgResName("ch479").setName("Astro Best SD 8").setChannelNum(479).setId(479));
                list.add(new Channel().setImgResName("ch494").setName("Astro First SD10").setChannelNum(494).setId(494));
                list.add(new Channel().setImgResName("ch701").setName("AXN").setChannelNum(701).setId(701));
                list.add(new Channel().setImgResName("ch702").setName("Diva Universal").setChannelNum(702).setId(702));
                list.add(new Channel().setImgResName("ch703").setName("Asian Food Channel").setChannelNum(703).setId(703));
                list.add(new Channel().setImgResName("ch707").setName("TLC").setChannelNum(707).setId(707));
                list.add(new Channel().setImgResName("ch708").setName("8TV").setChannelNum(708).setId(708));
                list.add(new Channel().setImgResName("ch709").setName("Lifetime Channel").setChannelNum(709).setId(709));
                list.add(new Channel().setImgResName("ch710").setName("FOX").setChannelNum(710).setId(710));
                list.add(new Channel().setImgResName("ch711").setName("STAR WORLD").setChannelNum(711).setId(711));
                list.add(new Channel().setImgResName("ch712").setName("E! Entertainment").setChannelNum(712).setId(712));
                list.add(new Channel().setImgResName("ch715").setName("Animax").setChannelNum(715).setId(715));
                list.add(new Channel().setImgResName("ch735").setName("ITV Choice").setChannelNum(735).setId(735));
                return list;
            }
            case 1: {
                list.add(new Channel().setImgResName("ch134").setName("Mustika HD").setChannelNum(134).setId(134));
                list.add(new Channel().setImgResName("ch135").setName("Maya HD").setChannelNum(135).setId(135));
                list.add(new Channel().setImgResName("ch308").setName("Astro Quan Jia HD").setChannelNum(308).setId(308));
                list.add(new Channel().setImgResName("ch309").setName("Celestial Movies HD").setChannelNum(309).setId(309));
                list.add(new Channel().setImgResName("ch310").setName("Astro Zhi Zun HD").setChannelNum(310).setId(310));
                list.add(new Channel().setImgResName("ch332").setName("Hua Hee Dai HD").setChannelNum(332).setId(332));
                list.add(new Channel().setImgResName("ch350").setName("Astro On Demand 350 HD").setChannelNum(350).setId(350));
                list.add(new Channel().setImgResName("ch392").setName("KBS World HD").setChannelNum(392).setId(392));
                list.add(new Channel().setImgResName("ch393").setName("One HD").setChannelNum(393).setId(393));
                list.add(new Channel().setImgResName("ch431").setName("HBO HD").setChannelNum(431).setId(431));
                list.add(new Channel().setImgResName("ch433").setName("Fox Movies Premium HD").setChannelNum(433).setId(433));
                list.add(new Channel().setImgResName("ch438").setName("Sundance HD").setChannelNum(438).setId(438));
                list.add(new Channel().setImgResName("ch475").setName("Astro Best HD1").setChannelNum(475).setId(475));
                list.add(new Channel().setImgResName("ch637").setName("Disney XD").setChannelNum(637).setId(637));
                list.add(new Channel().setImgResName("ch721").setName("AXN HD").setChannelNum(721).setId(721));
                list.add(new Channel().setImgResName("ch722").setName("Star World HD").setChannelNum(722).setId(722));
                list.add(new Channel().setImgResName("ch726").setName("FX HD").setChannelNum(726).setId(726));
                list.add(new Channel().setImgResName("ch727").setName("Food Network Asia HD").setChannelNum(727).setId(727));
                list.add(new Channel().setImgResName("ch728").setName("Life Inspired HD").setChannelNum(728).setId(728));
                list.add(new Channel().setImgResName("ch729").setName("KIX HD").setChannelNum(729).setId(729));
                list.add(new Channel().setImgResName("ch831").setName("Astro SuperSport HD").setChannelNum(831).setId(831));
                list.add(new Channel().setImgResName("ch832").setName("FOX Sports Plus HD").setChannelNum(832).setId(832));
                list.add(new Channel().setImgResName("ch833").setName("Astro SuperSport HD 2").setChannelNum(833).setId(833));
                list.add(new Channel().setImgResName("ch834").setName("Astro SuperSport 3 HD").setChannelNum(834).setId(834));
                list.add(new Channel().setImgResName("ch841").setName("FWC1 HD").setChannelNum(841).setId(841));
                list.add(new Channel().setImgResName("ch842").setName("FWC2 HD").setChannelNum(842).setId(842));
                list.add(new Channel().setImgResName("ch843").setName("FWC3 HD").setChannelNum(843).setId(843));
                list.add(new Channel().setImgResName("ch844").setName("FWC4 HD").setChannelNum(844).setId(844));
                return list;
            }
            case 2: {
                list.add(new Channel().setImgResName("ch213").setName("Chutti TV").setChannelNum(213).setId(213));
                list.add(new Channel().setImgResName("ch325").setName("Astro Xiao Tai Yang").setChannelNum(325).setId(325));
                list.add(new Channel().setImgResName("ch601").setName("Astro Tutor TV UPSR").setChannelNum(601).setId(601));
                list.add(new Channel().setImgResName("ch602").setName("Astro Tutor TV MR").setChannelNum(602).setId(602));
                list.add(new Channel().setImgResName("ch603").setName("Tutor TV SPM").setChannelNum(603).setId(603));
                list.add(new Channel().setImgResName("ch610").setName("Astro TVIQ").setChannelNum(610).setId(610));
                list.add(new Channel().setImgResName("ch611").setName("Astro Ceria").setChannelNum(611).setId(611));
                list.add(new Channel().setImgResName("ch612").setName("Nickelodeon").setChannelNum(612).setId(612));
                list.add(new Channel().setImgResName("ch613").setName("Disney Junior").setChannelNum(613).setId(613));
                list.add(new Channel().setImgResName("ch615").setName("Disney Channel").setChannelNum(615).setId(615));
                list.add(new Channel().setImgResName("ch616").setName("Cartoon Network").setChannelNum(616).setId(616));
                list.add(new Channel().setImgResName("ch617").setName("Disney XD").setChannelNum(617).setId(617));
                list.add(new Channel().setImgResName("ch618").setName("BabyTV").setChannelNum(618).setId(618));
                return list;
            }
            case 3: {
                list.add(new Channel().setImgResName("ch180").setName("TVi").setChannelNum(180).setId(180));
                list.add(new Channel().setImgResName("ch212").setName("Sun Music").setChannelNum(212).setId(212));
                list.add(new Channel().setImgResName("ch705").setName("Astro Hitz").setChannelNum(705).setId(705));
                list.add(new Channel().setImgResName("ch713").setName("MTV Asia").setChannelNum(713).setId(713));
                return list;
            }
            case 4: {
                list.add(new Channel().setImgResName("ch323").setName("Phoenix Info News").setChannelNum(323).setId(323));
                list.add(new Channel().setImgResName("ch501").setName("Astro Awani").setChannelNum(501).setId(501));
                list.add(new Channel().setImgResName("ch502").setName("Bernama TV").setChannelNum(502).setId(502));
                list.add(new Channel().setImgResName("ch509").setName("CCTV News").setChannelNum(509).setId(509));
                list.add(new Channel().setImgResName("ch511").setName("CNN").setChannelNum(511).setId(511));
                list.add(new Channel().setImgResName("ch512").setName("BBC World").setChannelNum(512).setId(512));
                list.add(new Channel().setImgResName("ch513").setName("Al Jazeera English").setChannelNum(513).setId(513));
                list.add(new Channel().setImgResName("ch518").setName("CNBC Asia").setChannelNum(518).setId(518));
                list.add(new Channel().setImgResName("ch519").setName("Bloomberg Television").setChannelNum(519).setId(519));
                return list;
            }
            case 5: {
                list.add(new Channel().setImgResName("ch130").setName("ASTRO BOX OFFICE MOVIES TAYANGAN HEBAT").setChannelNum(130).setId(130));
                list.add(new Channel().setImgResName("ch241").setName("ASTRO BOX OFFICE MOVIE THANGATHIRAI").setChannelNum(241).setId(241));
                list.add(new Channel().setImgResName("ch351").setName("Astro On Demand 351").setChannelNum(351).setId(351));
                list.add(new Channel().setImgResName("ch352").setName("Astro On Demand 352").setChannelNum(352).setId(352));
                list.add(new Channel().setImgResName("ch353").setName("Astro On Demand 353").setChannelNum(353).setId(353));
                list.add(new Channel().setImgResName("ch354").setName("Astro On Demand 354").setChannelNum(354).setId(354));
                list.add(new Channel().setImgResName("ch361").setName("Astro On Demand 361").setChannelNum(361).setId(361));
                list.add(new Channel().setImgResName("ch362").setName("Astro On Demand 362").setChannelNum(362).setId(362));
                list.add(new Channel().setImgResName("ch363").setName("Astro On Demand 363").setChannelNum(363).setId(363));
                list.add(new Channel().setImgResName("ch364").setName("Astro On Demand 364").setChannelNum(364).setId(364));
                list.add(new Channel().setImgResName("ch456").setName("A-List").setChannelNum(456).setId(456));
                list.add(new Channel().setImgResName("ch971").setName("Astro Box Office Sport 1").setChannelNum(971).setId(971));
                list.add(new Channel().setImgResName("ch972").setName("Astro Box Office Sport 2").setChannelNum(972).setId(972));
                return list;
            }
            case 6: {
                list.add(new Channel().setImgResName("ch852").setName("hitz.fm").setChannelNum(852).setId(852));
                list.add(new Channel().setImgResName("ch853").setName("MY FM").setChannelNum(853).setId(853));
                list.add(new Channel().setImgResName("ch854").setName("LITE FM").setChannelNum(854).setId(854));
                list.add(new Channel().setImgResName("ch855").setName("MIX FM").setChannelNum(855).setId(855));
                list.add(new Channel().setImgResName("ch856").setName("ERA").setChannelNum(856).setId(856));
                list.add(new Channel().setImgResName("ch857").setName("Sinar FM").setChannelNum(857).setId(857));
                list.add(new Channel().setImgResName("ch858").setName("Melody FM").setChannelNum(858).setId(858));
                list.add(new Channel().setImgResName("ch859").setName("THR Raaga").setChannelNum(859).setId(859));
                list.add(new Channel().setImgResName("ch860").setName("Classic Rock").setChannelNum(860).setId(860));
                list.add(new Channel().setImgResName("ch861").setName("Gold").setChannelNum(861).setId(861));
                list.add(new Channel().setImgResName("ch862").setName("Opus").setChannelNum(862).setId(862));
                list.add(new Channel().setImgResName("ch863").setName("THR Gegar").setChannelNum(863).setId(863));
                list.add(new Channel().setImgResName("ch864").setName("India Beat").setChannelNum(864).setId(864));
                list.add(new Channel().setImgResName("ch865").setName("Jazz").setChannelNum(865).setId(865));
                list.add(new Channel().setImgResName("ch866").setName("Osai FM").setChannelNum(866).setId(866));
                list.add(new Channel().setImgResName("ch867").setName("Bayu FM").setChannelNum(867).setId(867));
                list.add(new Channel().setImgResName("ch868").setName("Kenyalang").setChannelNum(868).setId(868));
                list.add(new Channel().setImgResName("ch869").setName("NAS FM").setChannelNum(869).setId(869));
                list.add(new Channel().setImgResName("ch870").setName("V FM").setChannelNum(870).setId(870));
                list.add(new Channel().setImgResName("ch871").setName("Wai FM").setChannelNum(871).setId(871));
                return list;
            }
            case 7: {
                list.add(new Channel().setImgResName("ch801").setName("Astro Arena").setChannelNum(801).setId(801));
                list.add(new Channel().setImgResName("ch810").setName("Astro Super Sport").setChannelNum(810).setId(810));
                list.add(new Channel().setImgResName("ch811").setName("Astro Super Sport 2").setChannelNum(811).setId(811));
                list.add(new Channel().setImgResName("ch812").setName("FOX Sports").setChannelNum(812).setId(812));
                list.add(new Channel().setImgResName("ch813").setName("Start Sports").setChannelNum(813).setId(813));
                list.add(new Channel().setImgResName("ch814").setName("EURO Sport").setChannelNum(814).setId(814));
                list.add(new Channel().setImgResName("ch815").setName("Golf").setChannelNum(815).setId(815));
                list.add(new Channel().setImgResName("ch816").setName("Astro Super Sport 3").setChannelNum(816).setId(816));
                list.add(new Channel().setImgResName("ch817").setName("Astro Super Sport 4").setChannelNum(817).setId(817));
                list.add(new Channel().setImgResName("ch818").setName("FOX Sports News").setChannelNum(818).setId(818));
                list.add(new Channel().setImgResName("ch810").setName("Astro Super Sport 5").setChannelNum(821).setId(821));
                list.add(new Channel().setImgResName("ch810").setName("Astro Super Sport 6").setChannelNum(822).setId(822));
                list.add(new Channel().setImgResName("ch810").setName("Astro Super Sport 7").setChannelNum(823).setId(823));
                list.add(new Channel().setImgResName("ch810").setName("Astro Super Sport 8").setChannelNum(824).setId(824));
                return list;
            }
        }
    }
    
    public static ArrayList<Channel> SouthAfrica(final int n) {
        final ArrayList<Channel> list = new ArrayList<Channel>();
        switch (n) {
            default: {
                return list;
            }
            case 0: {
                list.add(new Channel().setImgResName("ch180sa").setName("Discovery HD").setChannelNum(180).setId(180));
                list.add(new Channel().setImgResName("ch181sa").setName("National Geographic").setChannelNum(181).setId(181));
                list.add(new Channel().setImgResName("ch182sa").setName("Nat Geo Wild").setChannelNum(182).setId(182));
                list.add(new Channel().setImgResName("ch183sa").setName("Animal Planet").setChannelNum(183).setId(183));
                list.add(new Channel().setImgResName("ch184sa").setName("BBC Knowledge").setChannelNum(184).setId(184));
                list.add(new Channel().setImgResName("ch186sa").setName("HISTORY").setChannelNum(186).setId(186));
                list.add(new Channel().setImgResName("ch187sa").setName("Discovery World").setChannelNum(187).setId(187));
                list.add(new Channel().setImgResName("ch188sa").setName("TRACE Sport Stars").setChannelNum(188).setId(188));
                list.add(new Channel().setImgResName("ch189sa").setName("Ignition").setChannelNum(189).setId(189));
                return list;
            }
            case 1: {
                list.add(new Channel().setImgResName("ch200sa").setName("SuperSport Blitz").setChannelNum(200).setId(200));
                list.add(new Channel().setImgResName("ch201sa").setName("SuperSport 1").setChannelNum(201).setId(201));
                list.add(new Channel().setImgResName("ch202sa").setName("SuperSport 2").setChannelNum(202).setId(202));
                list.add(new Channel().setImgResName("ch203sa").setName("SuperSport 3").setChannelNum(203).setId(203));
                list.add(new Channel().setImgResName("ch204sa").setName("SuperSport 4").setChannelNum(204).setId(204));
                list.add(new Channel().setImgResName("ch205sa").setName("SuperSport 5").setChannelNum(205).setId(205));
                list.add(new Channel().setImgResName("ch206sa").setName("SuperSport 6").setChannelNum(206).setId(206));
                list.add(new Channel().setImgResName("ch207sa").setName("SuperSport 7").setChannelNum(207).setId(207));
                list.add(new Channel().setImgResName("ch208sa").setName("SuperSport 8").setChannelNum(208).setId(208));
                list.add(new Channel().setImgResName("ch209sa").setName("SuperSport 9").setChannelNum(209).setId(209));
                list.add(new Channel().setImgResName("ch210sa").setName("SuperSport Select").setChannelNum(210).setId(210));
                list.add(new Channel().setImgResName("ch211sa").setName("SuperSport HD 1").setChannelNum(211).setId(211));
                list.add(new Channel().setImgResName("ch212sa").setName("SuperSport HD 2").setChannelNum(212).setId(212));
                list.add(new Channel().setImgResName("ch213sa").setName("SuperSport HD 3").setChannelNum(213).setId(213));
                list.add(new Channel().setImgResName("ch214sa").setName("SuperSport HD 4").setChannelNum(214).setId(214));
                list.add(new Channel().setImgResName("ch215sa").setName("SuperSport HD 5").setChannelNum(215).setId(215));
                list.add(new Channel().setImgResName("ch216sa").setName("SuperSport HD 6").setChannelNum(216).setId(216));
                list.add(new Channel().setImgResName("ch217sa").setName("SuperSport HD 7").setChannelNum(217).setId(217));
                list.add(new Channel().setImgResName("ch221sa").setName("SuperSport MaXimo 1").setChannelNum(221).setId(221));
                list.add(new Channel().setImgResName("ch239sa").setName("TellyTrack").setChannelNum(239).setId(239));
                return list;
            }
            case 2: {
                list.add(new Channel().setImgResName("ch170sa").setName("Crime Investigation").setChannelNum(170).setId(170));
                list.add(new Channel().setImgResName("ch171sa").setName("Discovery ID").setChannelNum(171).setId(171));
                list.add(new Channel().setImgResName("ch172sa").setName("TLC Entertainment").setChannelNum(172).setId(172));
                list.add(new Channel().setImgResName("ch173sa").setName("Style").setChannelNum(173).setId(173));
                list.add(new Channel().setImgResName("ch174sa").setName("BBC Lifestyle").setChannelNum(174).setId(174));
                list.add(new Channel().setImgResName("ch176sa").setName("The Home Channel").setChannelNum(176).setId(176));
                list.add(new Channel().setImgResName("ch178sa").setName("Fashion TV").setChannelNum(178).setId(178));
                list.add(new Channel().setImgResName("ch179sa").setName("Travel Channel").setChannelNum(179).setId(179));
                return list;
            }
            case 3: {
                list.add(new Channel().setImgResName("ch320sa").setName("Channel O i").setChannelNum(320).setId(320));
                list.add(new Channel().setImgResName("ch321sa").setName("Mzansi Magic Music").setChannelNum(321).setId(321));
                list.add(new Channel().setImgResName("ch322sa").setName("MTV Base").setChannelNum(322).setId(322));
                list.add(new Channel().setImgResName("ch323sa").setName("VH1 Classic").setChannelNum(323).setId(323));
                list.add(new Channel().setImgResName("ch325sa").setName("TRACE Urban").setChannelNum(325).setId(325));
                list.add(new Channel().setImgResName("ch331sa").setName("One Gospel").setChannelNum(331).setId(331));
                return list;
            }
            case 4: {
                list.add(new Channel().setImgResName("ch301sa").setName("Cartoon Network").setChannelNum(301).setId(301));
                list.add(new Channel().setImgResName("ch302sa").setName("Boomerang").setChannelNum(302).setId(302));
                list.add(new Channel().setImgResName("ch303sa").setName("Disney Channel").setChannelNum(303).setId(303));
                list.add(new Channel().setImgResName("ch304sa").setName("Disney XD").setChannelNum(304).setId(304));
                list.add(new Channel().setImgResName("ch305sa").setName("Nickelodeon").setChannelNum(305).setId(305));
                list.add(new Channel().setImgResName("ch306sa").setName("Cbeebies").setChannelNum(306).setId(306));
                list.add(new Channel().setImgResName("ch309sa").setName("Disney Junior").setChannelNum(309).setId(309));
                list.add(new Channel().setImgResName("ch310sa").setName("JimJam").setChannelNum(310).setId(310));
                list.add(new Channel().setImgResName("ch319sa").setName("Mindset").setChannelNum(319).setId(319));
                return list;
            }
            case 5: {
                list.add(new Channel().setImgResName("ch199sa").setName("The Oscar Pistorius Trial").setChannelNum(199).setId(199));
                list.add(new Channel().setImgResName("ch400sa").setName("BBC World News").setChannelNum(400).setId(400));
                list.add(new Channel().setImgResName("ch401sa").setName("CNN International").setChannelNum(401).setId(401));
                list.add(new Channel().setImgResName("ch402sa").setName("Sky News").setChannelNum(402).setId(402));
                list.add(new Channel().setImgResName("ch403sa").setName("eNews Channel Africa").setChannelNum(403).setId(403));
                list.add(new Channel().setImgResName("ch404sa").setName("SABC News").setChannelNum(404).setId(404));
                list.add(new Channel().setImgResName("ch405sa").setName("ANN7").setChannelNum(405).setId(405));
                list.add(new Channel().setImgResName("ch406sa").setName("Al Jazeera").setChannelNum(406).setId(406));
                list.add(new Channel().setImgResName("ch407sa").setName("Russia Today").setChannelNum(407).setId(407));
                list.add(new Channel().setImgResName("ch408sa").setName("Parliamentary Service").setChannelNum(408).setId(408));
                list.add(new Channel().setImgResName("ch409sa").setName("CCTV News").setChannelNum(409).setId(409));
                list.add(new Channel().setImgResName("ch410sa").setName("CNBC Africa").setChannelNum(410).setId(410));
                list.add(new Channel().setImgResName("ch411sa").setName("Bloomberg").setChannelNum(411).setId(411));
                list.add(new Channel().setImgResName("ch412sa").setName("Business Day TV").setChannelNum(412).setId(412));
                list.add(new Channel().setImgResName("ch413sa").setName("NDTV247").setChannelNum(413).setId(413));
                list.add(new Channel().setImgResName("ch414sa").setName("Euro News").setChannelNum(414).setId(414));
                list.add(new Channel().setImgResName("ch415sa").setName("People\u2019s Weather").setChannelNum(415).setId(415));
                list.add(new Channel().setImgResName("ch446sa").setName("Deutsche Welle").setChannelNum(446).setId(446));
                return list;
            }
            case 6: {
                list.add(new Channel().setImgResName("ch340sa").setName("Dumisa").setChannelNum(340).setId(340));
                list.add(new Channel().setImgResName("ch341sa").setName("TBN").setChannelNum(341).setId(341));
                list.add(new Channel().setImgResName("ch343sa").setName("Rhema TV").setChannelNum(343).setId(343));
                list.add(new Channel().setImgResName("ch347sa").setName("iTV").setChannelNum(347).setId(347));
                list.add(new Channel().setImgResName("ch882sa").setName("Radio Pulpit").setChannelNum(882).setId(882));
                return list;
            }
            case 7: {
                list.add(new Channel().setImgResName("ch431sa").setName("BVN").setChannelNum(431).setId(431));
                list.add(new Channel().setImgResName("ch432sa").setName("ERT").setChannelNum(432).setId(432));
                return list;
            }
            case 8: {
                list.add(new Channel().setImgResName("ch101sa").setName("M-Net").setChannelNum(101).setId(101));
                list.add(new Channel().setImgResName("ch103sa").setName("M-Net Movies Premiere").setChannelNum(103).setId(103));
                list.add(new Channel().setImgResName("ch104sa").setName("M-Net Movies Comedy").setChannelNum(104).setId(104));
                list.add(new Channel().setImgResName("ch105sa").setName("M-Net Movies Family").setChannelNum(105).setId(105));
                list.add(new Channel().setImgResName("ch106sa").setName("M-Net Movies Action+").setChannelNum(106).setId(106));
                list.add(new Channel().setImgResName("ch107sa").setName("M-Net Movies Romance").setChannelNum(107).setId(107));
                list.add(new Channel().setImgResName("ch108sa").setName("M-Net Movies Showcase").setChannelNum(108).setId(108));
                list.add(new Channel().setImgResName("ch110sa").setName("M-Net Movies Action").setChannelNum(110).setId(110));
                list.add(new Channel().setImgResName("ch111sa").setName("M-Net Movies Stars").setChannelNum(111).setId(111));
                list.add(new Channel().setImgResName("ch112sa").setName("Studio Universal").setChannelNum(112).setId(112));
                list.add(new Channel().setImgResName("ch113sa").setName("M-Net Series Showcase").setChannelNum(113).setId(113));
                list.add(new Channel().setImgResName("ch114sa").setName("M-Net Series Reality").setChannelNum(114).setId(114));
                list.add(new Channel().setImgResName("ch115sa").setName("M-Net Series Zone").setChannelNum(115).setId(115));
                list.add(new Channel().setImgResName("ch116sa").setName("Vuzu").setChannelNum(116).setId(116));
                list.add(new Channel().setImgResName("ch117sa").setName("Universal Channel").setChannelNum(117).setId(117));
                list.add(new Channel().setImgResName("ch118sa").setName("Telemundo").setChannelNum(118).setId(118));
                list.add(new Channel().setImgResName("ch120sa").setName("BBC Entertainment").setChannelNum(120).setId(120));
                list.add(new Channel().setImgResName("ch121sa").setName("Discovery Channel").setChannelNum(121).setId(121));
                list.add(new Channel().setImgResName("ch122sa").setName("Comedy Central").setChannelNum(122).setId(122));
                list.add(new Channel().setImgResName("ch124sa").setName("E! Entertainment Television").setChannelNum(124).setId(124));
                list.add(new Channel().setImgResName("ch125sa").setName("FOX").setChannelNum(125).setId(125));
                list.add(new Channel().setImgResName("ch126sa").setName("FOX Crime").setChannelNum(126).setId(126));
                list.add(new Channel().setImgResName("ch127sa").setName("SONY").setChannelNum(127).setId(127));
                list.add(new Channel().setImgResName("ch128sa").setName("SONY MAX").setChannelNum(128).setId(128));
                list.add(new Channel().setImgResName("ch129sa").setName("Blackbelt TV").setChannelNum(129).setId(129));
                list.add(new Channel().setImgResName("ch130sa").setName("MTV").setChannelNum(130).setId(130));
                list.add(new Channel().setImgResName("ch132sa").setName("CBS Reality").setChannelNum(132).setId(132));
                list.add(new Channel().setImgResName("ch133sa").setName("CBS Action").setChannelNum(133).setId(133));
                list.add(new Channel().setImgResName("ch134sa").setName("CBS Drama").setChannelNum(134).setId(134));
                list.add(new Channel().setImgResName("ch137sa").setName("Turner Classic Movies").setChannelNum(137).setId(137));
                list.add(new Channel().setImgResName("ch138sa").setName("True Movies 1").setChannelNum(138).setId(138));
                list.add(new Channel().setImgResName("ch139sa").setName("M-Net Movies Zone").setChannelNum(139).setId(139));
                list.add(new Channel().setImgResName("ch140sa").setName("MGM").setChannelNum(140).setId(140));
                list.add(new Channel().setImgResName("ch144sa").setName("kykNET").setChannelNum(144).setId(144));
                list.add(new Channel().setImgResName("ch145sa").setName("kykNET & Kie").setChannelNum(145).setId(145));
                list.add(new Channel().setImgResName("ch146sa").setName("KykNET Musiek").setChannelNum(146).setId(146));
                list.add(new Channel().setImgResName("ch150sa").setName("AfricaMagic Entertainment").setChannelNum(150).setId(150));
                list.add(new Channel().setImgResName("ch152sa").setName("AfricaMagic Movies").setChannelNum(152).setId(152));
                list.add(new Channel().setImgResName("ch153sa").setName("AfricaMagic Movies 1").setChannelNum(153).setId(153));
                list.add(new Channel().setImgResName("ch154sa").setName("AfricaMagic").setChannelNum(154).setId(154));
                list.add(new Channel().setImgResName("ch155sa").setName("AfricaMagic World").setChannelNum(155).setId(155));
                list.add(new Channel().setImgResName("ch161sa").setName("Mzansi Magic").setChannelNum(161).setId(161));
                list.add(new Channel().setImgResName("ch162sa").setName("Magic World").setChannelNum(162).setId(162));
                list.add(new Channel().setImgResName("ch163sa").setName("Mzansi Wethu").setChannelNum(163).setId(163));
                list.add(new Channel().setImgResName("ch164sa").setName("Mzansi Bioskop").setChannelNum(164).setId(164));
                list.add(new Channel().setImgResName("ch175sa").setName("Food Network").setChannelNum(175).setId(175));
                list.add(new Channel().setImgResName("ch190sa").setName("Channel ED").setChannelNum(190).setId(190));
                list.add(new Channel().setImgResName("ch191sa").setName("SABC 1").setChannelNum(191).setId(191));
                list.add(new Channel().setImgResName("ch192sa").setName("SABC 2").setChannelNum(192).setId(192));
                list.add(new Channel().setImgResName("ch193sa").setName("SABC 3").setChannelNum(193).setId(193));
                list.add(new Channel().setImgResName("ch194sa").setName("e.tv").setChannelNum(194).setId(194));
                list.add(new Channel().setImgResName("ch251sa").setName("Soweto TV").setChannelNum(251).setId(251));
                list.add(new Channel().setImgResName("ch260sa").setName("Bay TV").setChannelNum(260).setId(260));
                list.add(new Channel().setImgResName("ch261sa").setName("1 KZN").setChannelNum(261).setId(261));
                list.add(new Channel().setImgResName("ch262sa").setName("Tshwane TV").setChannelNum(262).setId(262));
                list.add(new Channel().setImgResName("ch263sa").setName("Cape Town TV").setChannelNum(263).setId(263));
                list.add(new Channel().setImgResName("ch430sa").setName("RAI Italia").setChannelNum(430).setId(430));
                list.add(new Channel().setImgResName("ch435sa").setName("RTP International").setChannelNum(435).setId(435));
                list.add(new Channel().setImgResName("ch437sa").setName("TV5 Monde Afrique").setChannelNum(437).setId(437));
                list.add(new Channel().setImgResName("ch447sa").setName("CCTV 4").setChannelNum(447).setId(447));
                return list;
            }
        }
    }
}
