package com.gk.zhangzhongbao.ui.fragment;

import android.support.v4.app.*;
import com.gk.zhangzhongbao.ui.bean.*;
import com.gk.zhangzhongbao.ui.adapter.*;
import android.support.v4.content.*;
import android.util.*;
import com.gk.wifictr.net.command.*;
import android.content.*;
import android.view.*;
import com.gk.zhangzhongbao.ui.app.*;
import com.gk.wifictr.net.*;
import com.gk.wifictr.net.command.station.*;
import android.widget.*;
import com.gk.zhangzhongbao.ui.activity.*;
import android.os.*;
import java.util.*;

public class FavoriteChFragment extends Fragment
{
    public static final int MSG_ADD_CH = 1;
    public static final int MSG_REMOVE_CH = 2;
    public static Handler handler;
    public static List<Channel> list;
    private String SN;
    private GridViewAdapter adapter;
    private MyApplication app;
    private GridView gridView;
    LocalBroadcastManager lbcManager;
    BroadcastReceiver receiver;
    
    public FavoriteChFragment() {
        this.receiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final int int1 = intent.getExtras().getInt("cmd");
                Log.e("favor receive", "cmd:" + int1);
                switch (int1) {
                    case 4: {
                        final ReadData readData = (ReadData)intent.getParcelableExtra("data");
                        if (readData != null) {
                            Log.e("data body", ByteUtil.byte2hex(readData.body));
                            Log.e("type", ByteUtil.byte2hex(new byte[] { readData.type }));
                            return;
                        }
                        break;
                    }
                }
            }
        };
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.app = (MyApplication)this.getActivity().getApplication();
        FavoriteChFragment.list = new ArrayList<Channel>();
        FavoriteChFragment.handler = new FavoriteChHandler(Looper.myLooper());
        this.SN = ConnectService.defWifi.SN;
        while (true) {
            try {
                FavoriteChFragment.list = DataBase.db.getChlList(this.SN);
                this.adapter = new GridViewAdapter((Context)this.getActivity(), FavoriteChFragment.list);
                this.lbcManager = LocalBroadcastManager.getInstance((Context)this.getActivity());
                final IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("com.gk.wifictr.net");
                this.lbcManager.registerReceiver(this.receiver, intentFilter);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                continue;
            }
            break;
        }
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2130903063, viewGroup, false);
        (this.gridView = (GridView)inflate.findViewById(2131230871)).setAdapter((ListAdapter)this.adapter);
        this.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                Util.vibrator((Context)FavoriteChFragment.this.getActivity());
                final Channel channel = FavoriteChFragment.list.get(n);
                final int[] chCodes = channel.getChCodes();
                Log.e("ChannelNum", new StringBuilder(String.valueOf(channel.getChannelNum())).toString());
                Log.e("codes", String.valueOf(chCodes[0]) + " " + chCodes[1] + " " + chCodes[2]);
                Log.e("stb sn", FavoriteChFragment.this.app.stbDevice.devSN);
                new BroadcastHelper().writeData(new Out_53H().make(FavoriteChFragment.this.app.stbDevice.devSN, FavoriteChFragment.this.app.stbDevice.devType, (byte)channel.getBtnCode(chCodes[0]), (byte)channel.getBtnCode(chCodes[1]), (byte)channel.getBtnCode(chCodes[2]), 0, 0));
            }
        });
        this.gridView.setOnItemLongClickListener((AdapterView.OnItemLongClickListener)new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                final Intent intent = new Intent((Context)FavoriteChFragment.this.getActivity(), (Class)FavoriteActivity.class);
                intent.putExtra("gv_index", n);
                FavoriteChFragment.this.startActivity(intent);
                return true;
            }
        });
        return inflate;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.lbcManager.unregisterReceiver(this.receiver);
    }
    
    protected class FavoriteChHandler extends Handler
    {
        Looper mLooper;
        
        public FavoriteChHandler(final Looper mLooper) {
            this.mLooper = mLooper;
        }
        
        public void handleMessage(final Message message) {
            Label_0028: {
                switch (message.what) {
                    case 1: {
                        final Channel channel = (Channel)message.obj;
                        if (channel != null) {
                            for (final Channel channel2 : FavoriteChFragment.list) {
                                if (channel2.getId() == channel.getId()) {
                                    if (channel.getChannelNum() != channel2.getChannelNum()) {
                                        channel2.setChannelNum(channel.getChannelNum());
                                        DataBase.db.updateChannelNum(channel.getChannelNum(), new StringBuilder(String.valueOf(channel.getId())).toString(), FavoriteChFragment.this.SN);
                                        FavoriteChFragment.this.adapter.notifyDataSetChanged();
                                        return;
                                    }
                                    break Label_0028;
                                }
                            }
                            FavoriteChFragment.list.add(channel);
                            FavoriteChFragment.this.adapter.notifyDataSetChanged();
                            DataBase.db.insert(channel, FavoriteChFragment.this.SN);
                            return;
                        }
                        break;
                    }
                    case 2: {
                        final Channel channel3 = (Channel)message.obj;
                        if (channel3 != null) {
                            for (final Channel channel4 : FavoriteChFragment.list) {
                                if (channel4.getId() == channel3.getId()) {
                                    channel4.timerCancle();
                                    channel4.saveFavor(false);
                                    FavoriteChFragment.list.remove(channel4);
                                    DataBase.db.deleteData("channel_table", "channel_id", new StringBuilder(String.valueOf(channel3.getId())).toString(), FavoriteChFragment.this.SN);
                                    FavoriteChFragment.this.adapter.notifyDataSetChanged();
                                    return;
                                }
                            }
                            break;
                        }
                        break;
                    }
                }
            }
        }
    }
}
