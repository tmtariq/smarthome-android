package com.gk.zhangzhongbao.ui.fragment;

import android.support.v4.app.*;
import com.gk.zhangzhongbao.ui.bean.*;
import android.support.v4.content.*;
import com.gk.zhangzhongbao.ui.adapter.*;
import java.util.*;
import android.util.*;
import com.gk.wifictr.net.command.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.content.*;
import android.view.*;
import com.gk.wifictr.net.*;
import com.gk.wifictr.net.command.station.*;
import android.widget.*;
import com.gk.zhangzhongbao.ui.activity.*;
import android.os.*;

public class CommonChFragment extends Fragment
{
    public static int MSG_REFLASH = 10;
    public static int gvLayoutState;
    public static List<Channel> gvList;
    public static Handler handler;
    public static int index;
    MyApplication app;
    private GridView gridView;
    private GridViewAdapter gvAdapter;
    private FrameLayout gvLayout;
    LocalBroadcastManager lbcManager;
    private ListView listView;
    private CommonChAdapter lvAdapter;
    private List<String> lvList;
    BroadcastReceiver receiver;
    
    static {
        CommonChFragment.gvLayoutState = 8;
    }
    
    public CommonChFragment() {
        this.lvList = new ArrayList<String>();
        this.receiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                int int1 = intent.getExtras().getInt("cmd");
                Log.e("stb receive", "cmd:" + int1);
                switch (int1) {
                    case 4: {
                        ReadData readData = (ReadData)intent.getParcelableExtra("data");
                        if (readData != null) {
                            Log.e("data body", ByteUtil.byte2hex(readData.body));
                            Log.e("type", ByteUtil.byte2hex(new byte[] { readData.type }));
                            return;
                        }
                        break;
                    }
                }
            }
        };
    }
    
    private void initList() {
        if (Util.packageName.equals("com.gk.smarthome") || Util.packageName.equals("com.gk.duo.smarthome.plus") || Util.packageName.equals("com.gk.duo.smarthome") || Util.packageName.equals("com.gk.duo.smarthome.noabout") || Util.packageName.equals("com.gk.generic.smarthome") || Util.packageName.equals("com.gk.hb.smarthome")) {
            this.lvList.add(this.getString(2131296331));
            this.lvList.add(this.getString(2131296332));
            this.lvList.add(this.getString(2131296333));
            this.lvList.add(this.getString(2131296334));
            this.lvList.add(this.getString(2131296335));
            this.lvList.add(this.getString(2131296336));
            this.lvList.add(this.getString(2131296337));
        }
        else if (Util.packageName.equals("com.gk.ma.smarthome")) {
            this.lvList.add("ENTERTAINMENT");
            this.lvList.add("HD");
            this.lvList.add("KIDS");
            this.lvList.add("MUSIC");
            this.lvList.add("NEWS");
            this.lvList.add("PREMIUM");
            this.lvList.add("RADIO");
            this.lvList.add("SPORTS");
        }
        else if (Util.packageName.equals("com.gk.sa.smarthome")) {
            this.lvList.add("Documentary");
            this.lvList.add("Sport");
            this.lvList.add("Lifestyle");
            this.lvList.add("Music");
            this.lvList.add("Children");
            this.lvList.add("News & Commerce");
            this.lvList.add("Religion");
            this.lvList.add("Specialist");
            this.lvList.add("General Entertainemnt & Movies");
        }
        this.lvAdapter = new CommonChAdapter(this.getActivity(), this.lvList);
    }
    
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.app = (MyApplication)this.getActivity().getApplication();
        CommonChFragment.handler = new CommonChHandler(Looper.myLooper());
        this.initList();
        this.lbcManager = LocalBroadcastManager.getInstance(this.getActivity());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.gk.wifictr.net");
        this.lbcManager.registerReceiver(this.receiver, intentFilter);
    }
    
    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(2130903055, viewGroup, false);
        (this.listView = (ListView)inflate.findViewById(2131230851)).setAdapter(this.lvAdapter);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int gridView, long n) {
                setGridView(gridView);
                setGvVisibility(0);
            }
        });
        (this.gvLayout = (FrameLayout)inflate.findViewById(2131230852)).setVisibility(CommonChFragment.gvLayoutState);
        this.gridView = (GridView)inflate.findViewById(2131230853);
        this.setGridView(CommonChFragment.index);
        this.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int n, long n2) {
                Util.vibrator(getActivity());
                Channel channel = CommonChFragment.gvList.get(n);
                int[] chCodes = channel.getChCodes();
                Log.e("ChannelNum", new StringBuilder(String.valueOf(channel.getChannelNum())).toString());
                Log.e("codes", String.valueOf(chCodes[0]) + " " + chCodes[1] + " " + chCodes[2]);
                Log.e("stb sn", app.stbDevice.devSN);
                Log.e("codes", String.valueOf(channel.getBtnCode(chCodes[0])) + " " + channel.getBtnCode(chCodes[1]) + " " + channel.getBtnCode(chCodes[2]) + " ");
                new BroadcastHelper().writeData(new Out_53H().make(app.stbDevice.devSN, app.stbDevice.devType, (byte)channel.getBtnCode(chCodes[0]), (byte)channel.getBtnCode(chCodes[1]), (byte)channel.getBtnCode(chCodes[2]), 0, 0));
            }
        });
        this.gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int n, long n2) {
                Intent intent = new Intent(getActivity(), ChannelActivity.class);
                intent.putExtra("gv_index", n);
                startActivity(intent);
                return true;
            }
        });
        return inflate;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroyView();
        this.lbcManager.unregisterReceiver(this.receiver);
    }
    
    public void setGridView(int index) {
        CommonChFragment.index = index;
        if (Util.packageName.equals("com.gk.smarthome") || Util.packageName.equals("com.gk.duo.smarthome.plus") || Util.packageName.equals("com.gk.duo.smarthome") || Util.packageName.equals("com.gk.duo.smarthome.noabout") || Util.packageName.equals("com.gk.generic.smarthome") || Util.packageName.equals("com.gk.hb.smarthome")) {
            CommonChFragment.gvList = TVStation.Chinese(index);
        }
        else if (Util.packageName.equals("com.gk.ma.smarthome")) {
            CommonChFragment.gvList = TVStation.Malaysia(index);
        }
        else if (Util.packageName.equals("com.gk.sa.smarthome")) {
            CommonChFragment.gvList = TVStation.SouthAfrica(index);
        }
        this.gvAdapter = new GridViewAdapter(this.getActivity(), CommonChFragment.gvList);
        this.gridView.setAdapter(this.gvAdapter);
    }
    
    public void setGridViewGone() {
        this.gvLayout.setVisibility(8);
        CommonChFragment.gvLayoutState = 8;
    }
    
    public void setGvVisibility(int n) {
        this.gvLayout.setVisibility(n);
        CommonChFragment.gvLayoutState = n;
    }
    
    public class CommonChHandler extends Handler
    {
        Looper mLooper;
        
        public CommonChHandler(Looper mLooper) {
            this.mLooper = mLooper;
        }
        
        public void handleMessage(Message message) {
            switch (message.what) {
                case 10: {
                    gvAdapter.notifyDataSetChanged();
                    
                }
            }
        }
    }
}
