package com.gk.zhangzhongbao.ui.view;

import android.content.*;
import android.util.*;
import android.widget.*;
import android.os.*;
import android.view.*;
import android.graphics.*;

public class SwitchView extends FrameLayout
{
    private static final int ARGUMENT_SHAKE = 0;
    private static final int FRAMES = 15;
    private static final int MAX_HEIGHT = 800;
    private static final int MAX_WIDTH = 2048;
    private static final String TEXT_LEFT_SWITCH = "";
    private static final String TEXT_RIGHT_SWITCH = "";
    public static final int TYPE_LEFT = 0;
    public static final int TYPE_RIGHT = 1;
    private boolean isOpenClick;
    private GestureDetector mDetector;
    private ImageView mFloatingLayer;
    private int mFloatingLayerWidth;
    private TextView mLeftSwitch;
    private int mNowType;
    private int mOffsetX;
    private TextView mRightSwitch;
    private OnSwitchListener mSwitchListener;
    private LinearLayout mSwitchParent;
    private int mTotalWidth;
    private boolean rightIsClicked;
    
    public SwitchView(final Context context) {
        super(context);
        this.mOffsetX = 0;
        this.rightIsClicked = false;
        this.mNowType = 0;
        this.mDetector = null;
        this.isOpenClick = true;
        this.init();
    }
    
    public SwitchView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mOffsetX = 0;
        this.rightIsClicked = false;
        this.mNowType = 0;
        this.mDetector = null;
        this.isOpenClick = true;
        this.init();
    }
    
    static /* synthetic */ void access$2(final SwitchView switchView, final int mOffsetX) {
        switchView.mOffsetX = mOffsetX;
    }
    
    private int getBestHeight(final int n) {
        int n2 = n;
        if (n > 800) {
            n2 = 800;
        }
        return n2;
    }
    
    private int getBestWidth(final int n) {
        int n2 = n;
        if (n > 2048) {
            n2 = 2048;
        }
        return n2;
    }
    
    private int getFlowWidth(final int n) {
        return n / 2 + n / 15;
    }
    
    private int getFlowXWithLimit(final double n, final double n2) {
        if (n < 0.0) {
            return 0;
        }
        if (n > this.mTotalWidth - this.mFloatingLayerWidth) {
            if (!this.rightIsClicked) {
                Log.e("ok", "ok");
                this.performOnCheck(1);
                this.rightIsClicked = true;
            }
            return this.mTotalWidth - this.mFloatingLayerWidth;
        }
        if (n < this.mTotalWidth / 8) {
            this.rightIsClicked = false;
        }
        return (int)n2;
    }
    
    private int getType() {
        if (this.mOffsetX == 0) {
            return 0;
        }
        return 1;
    }
    
    private int getX(final int n) {
        if (n != 0 && n == 1) {
            return this.mTotalWidth - this.mFloatingLayerWidth;
        }
        return 0;
    }
    
    private void init() {
        (this.mSwitchParent = new LinearLayout(this.getContext())).setOrientation(0);
        this.setBackgroundResource(2130838270);
        (this.mFloatingLayer = new ImageView(this.getContext())).setBackgroundResource(2130838171);
        this.addView((View)this.mFloatingLayer, new FrameLayout.LayoutParams(-1, -1));
        this.mLeftSwitch = new TextView(this.getContext());
        this.mRightSwitch = new TextView(this.getContext());
        this.mLeftSwitch.setText((CharSequence)"");
        this.mRightSwitch.setText((CharSequence)"");
        this.mLeftSwitch.setBackgroundColor(0);
        this.mRightSwitch.setBackgroundColor(0);
        this.mLeftSwitch.setTextColor(-16777216);
        this.mRightSwitch.setTextColor(-16777216);
        this.mLeftSwitch.setGravity(17);
        this.mRightSwitch.setGravity(17);
        final LinearLayout.LayoutParams linearLayout$LayoutParams = new LinearLayout.LayoutParams(-1, -1);
        linearLayout$LayoutParams.weight = 1.0f;
        this.mLeftSwitch.setLayoutParams(linearLayout$LayoutParams);
        this.mRightSwitch.setLayoutParams(linearLayout$LayoutParams);
        this.mSwitchParent.addView((View)this.mLeftSwitch);
        this.mSwitchParent.addView((View)this.mRightSwitch);
        this.addView((View)this.mSwitchParent, -1, -1);
        this.mDetector = new GestureDetector(this.getContext(), (GestureDetector.OnGestureListener)new FlowGesture());
    }
    
    private void performOnCheck(final int n) {
        if (this.mSwitchListener == null) {
            return;
        }
        if (n == 0) {
            this.mSwitchListener.onCheck(this, true, false);
            return;
        }
        if (n == 1) {
            this.mSwitchListener.onCheck(this, false, true);
            return;
        }
        this.mSwitchListener.onCheck(this, false, false);
    }
    
    private void setup(final View view, final int n) {
        view.layout(n, 0, view.getMeasuredWidth() + n, view.getMeasuredHeight());
    }
    
    private void shake(final int n) {
        if (n != 0) {
            if (n == 1) {}
        }
        this.mOffsetX += 0;
        this.requestLayout();
    }
    
    private void sliding(final int mNowType) {
        final int mOffsetX = this.mOffsetX;
        final int x = this.getX(mNowType);
        if (mOffsetX == x) {
            return;
        }
        new Handler().post((Runnable)new Runnable() {
            private final /* synthetic */ FakeAnimation val$fake = new FakeAnimation(x - mOffsetX, 20L);
            
            @Override
            public void run() {
                if (this.val$fake.isEnd()) {
                    SwitchView.access$2(SwitchView.this, x);
                    SwitchView.this.requestLayout();
                    return;
                }
                SwitchView.access$2(SwitchView.this, this.val$fake.getValue() + SwitchView.this.mOffsetX);
                SwitchView.this.requestLayout();
                SwitchView.this.postDelayed((Runnable)this, 15L);
            }
        });
        this.performOnCheck(this.mNowType = mNowType);
    }
    
    private void sliding(final int mNowType, final int mOffsetX) {
        //mOffsetX = this.mOffsetX;
        final int x = this.getX(mNowType);
        if (mOffsetX == x) {
            return;
        }
        new Handler().post((Runnable)new Runnable() {
            private final /* synthetic */ FakeAnimation val$fake = new FakeAnimation(x - mOffsetX, 150L);
            
            @Override
            public void run() {
                if (this.val$fake.isEnd()) {
                    SwitchView.access$2(SwitchView.this, x);
                    SwitchView.this.requestLayout();
                    return;
                }
                SwitchView.access$2(SwitchView.this, this.val$fake.getValue() + SwitchView.this.mOffsetX);
                SwitchView.this.requestLayout();
                SwitchView.this.postDelayed((Runnable)this, 15L);
            }
        });
        this.performOnCheck(this.mNowType = mNowType);
    }
    
    public void closeClickEvent() {
        this.isOpenClick = false;
    }
    
    public TextView getLeftView() {
        return this.mLeftSwitch;
    }
    
    public TextView getRightView() {
        return this.mRightSwitch;
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        this.setup((View)this.mFloatingLayer, this.mOffsetX);
    }
    
    protected void onMeasure(int mode, int n) {
        super.onMeasure(mode, n);
        final int size = View.MeasureSpec.getSize(mode);
        mode = View.MeasureSpec.getMode(mode);
        final int size2 = View.MeasureSpec.getSize(n);
        n = View.MeasureSpec.getMode(n);
        final int bestWidth = this.getBestWidth(size);
        final int bestHeight = this.getBestHeight(size2);
        this.mTotalWidth = bestWidth;
        final int measureSpec = View.MeasureSpec.makeMeasureSpec(bestWidth, mode);
        n = View.MeasureSpec.makeMeasureSpec(bestHeight, n);
        this.mFloatingLayerWidth = this.getFlowWidth(bestWidth);
        this.mFloatingLayer.measure(View.MeasureSpec.makeMeasureSpec(this.mFloatingLayerWidth, mode), n);
        this.mSwitchParent.measure(measureSpec, n);
        this.setMeasuredDimension(measureSpec, n);
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        final int n = (int)motionEvent.getX();
        if (motionEvent.getAction() == 1 || motionEvent.getAction() == 3) {
            Log.v("test", "getit");
            this.rightIsClicked = false;
            this.sliding(0);
        }
        return this.mDetector.onTouchEvent(motionEvent);
    }
    
    public void openClickEvent() {
        this.isOpenClick = true;
    }
    
    public void setOnSwitchListener(final OnSwitchListener mSwitchListener) {
        this.mSwitchListener = mSwitchListener;
    }
    
    public void switching() {
        if (this.getType() == 0) {
            this.sliding(1);
            return;
        }
        this.sliding(0);
    }
    
    public void switching(final int n) {
        this.sliding(n);
    }
    
    public void switching(final boolean b) {
        System.out.println(b);
        int n;
        if (b) {
            n = 0;
        }
        else {
            n = 1;
        }
        this.sliding(n);
    }
    
    private static class FakeAnimation
    {
        private int ceil;
        private int nowTimes;
        private int times;
        
        FakeAnimation(final int n, final long n2) {
            this.times = (int)(n2 / 15L);
            this.ceil = n / this.times;
            this.nowTimes = 0;
        }
        
        public int getValue() {
            ++this.nowTimes;
            return this.ceil;
        }
        
        public boolean isEnd() {
            return this.nowTimes == this.times;
        }
    }
    
    class FlowGesture extends GestureDetector.SimpleOnGestureListener
    {
        public boolean onDown(final MotionEvent motionEvent) {
            final int n = (int)motionEvent.getX();
            final int n2 = (int)motionEvent.getY();
            final Rect rect = new Rect();
            SwitchView.this.mFloatingLayer.getHitRect(rect);
            return rect.contains(n, n2);
        }
        
        public boolean onScroll(final MotionEvent motionEvent, final MotionEvent motionEvent2, final float n, final float n2) {
            SwitchView.access$2(SwitchView.this, SwitchView.this.getFlowXWithLimit(SwitchView.this.mOffsetX, SwitchView.this.mOffsetX - n));
            SwitchView.this.requestLayout();
            return true;
        }
        
        public boolean onSingleTapUp(final MotionEvent motionEvent) {
            return super.onSingleTapUp(motionEvent);
        }
    }
    
    public interface OnSwitchListener
    {
        void onCheck(SwitchView p0, boolean p1, boolean p2);
    }
}
