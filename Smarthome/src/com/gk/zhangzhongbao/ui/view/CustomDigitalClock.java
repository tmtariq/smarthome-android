package com.gk.zhangzhongbao.ui.view;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.provider.Settings.System;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.widget.DigitalClock;
import java.util.Calendar;

public class CustomDigitalClock
  extends DigitalClock
{
  public static long distanceTime = 0L;
  private static boolean isFirst = false;
  private static final String m12 = "h:mm aa";
  private static final String m24 = "k:mm";
  private long endTime;
  Calendar mCalendar;
  private ClockListener mClockListener;
  private String mFormat;
  private FormatChangeObserver mFormatChangeObserver;
  private Handler mHandler;
  private Runnable mTicker;
  private boolean mTickerStopped;
  
  public CustomDigitalClock(Context paramContext)
  {
    super(paramContext);
    initClock(paramContext);
  }
  
  public CustomDigitalClock(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    initClock(paramContext);
  }
  
  public static Spanned dealTime(long paramLong)
  {
    Object localObject = new StringBuffer();
    long l1 = paramLong / 86400L;
    long l2 = paramLong % 86400L / 3600L;
    long l3 = paramLong % 86400L % 3600L / 60L;
    String str1 = timeStrFormat(String.valueOf(l2));
    String str2 = timeStrFormat(String.valueOf(l3));
    String str3 = timeStrFormat(String.valueOf(paramLong % 86400L % 3600L % 60L));
    ((StringBuffer)localObject).append(String.valueOf(l1)).append(":").append(str1).append(":").append(str2).append(":").append(str3).append(":");
    localObject = Html.fromHtml(((StringBuffer)localObject).toString());
    if (l1 >= 10L)
    {
      ((Spannable)localObject).setSpan(new AbsoluteSizeSpan(16), 2, 3, 33);
      ((Spannable)localObject).setSpan(new AbsoluteSizeSpan(16), 5, 7, 33);
      ((Spannable)localObject).setSpan(new AbsoluteSizeSpan(16), 9, 11, 33);
      ((Spannable)localObject).setSpan(new AbsoluteSizeSpan(16), 13, 14, 33);
      return (Spanned)localObject;
    }
    ((Spannable)localObject).setSpan(new AbsoluteSizeSpan(16), 1, 2, 33);
    ((Spannable)localObject).setSpan(new AbsoluteSizeSpan(16), 4, 6, 33);
    ((Spannable)localObject).setSpan(new AbsoluteSizeSpan(16), 8, 10, 33);
    ((Spannable)localObject).setSpan(new AbsoluteSizeSpan(16), 12, 13, 33);
    return (Spanned)localObject;
  }
  
  private boolean get24HourMode()
  {
    return DateFormat.is24HourFormat(getContext());
  }
  
  private void initClock(Context paramContext)
  {
    if (mCalendar == null) {
      mCalendar = Calendar.getInstance();
    }
    mFormatChangeObserver = new FormatChangeObserver();
    getContext().getContentResolver().registerContentObserver(Settings.System.CONTENT_URI, true, mFormatChangeObserver);
    setFormat();
  }
  
  private void setFormat()
  {
    if (get24HourMode())
    {
      mFormat = "k:mm";
      return;
    }
    mFormat = "h:mm aa";
  }
  
  private static String timeStrFormat(String string) {
      switch (string.length()) {
          default: {
              return string;
          }
          case 1: 
      }
      return "0" + string;
  }
  
  protected void onAttachedToWindow() {
      this.mTickerStopped = false;
      super.onAttachedToWindow();
      this.mHandler = new Handler();
      this.mTicker = new Runnable(){

          /*
           * Enabled aggressive block sorting
           */
          @Override
          public void run() {
              if (CustomDigitalClock.this.mTickerStopped) {
                  return;
              }
              long l = Calendar.getInstance().getTimeInMillis();
              if (l / 1000 == CustomDigitalClock.this.endTime / 1000 - 300) {
                  CustomDigitalClock.this.mClockListener.remainFiveMinutes();
              }
              CustomDigitalClock.distanceTime = CustomDigitalClock.this.endTime - l;
              if ((CustomDigitalClock.distanceTime/=1000) == 0) {
                  CustomDigitalClock.this.setText((CharSequence)"00:00:00");
                  CustomDigitalClock.this.onDetachedFromWindow();
              } else if (CustomDigitalClock.distanceTime < 0) {
                  CustomDigitalClock.this.setText((CharSequence)"00:00:00");
                  CustomDigitalClock.this.onDetachedFromWindow();
              } else {
                  CustomDigitalClock.this.setText((CharSequence)CustomDigitalClock.dealTime(CustomDigitalClock.distanceTime));
              }
              CustomDigitalClock.this.invalidate();
              l = SystemClock.uptimeMillis();
              CustomDigitalClock.this.mHandler.postAtTime(CustomDigitalClock.this.mTicker, l + (1000 - l % 1000));
          }
      };
      this.mTicker.run();
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    mTickerStopped = true;
  }
  
  public void setClockListener(ClockListener paramClockListener)
  {
    mClockListener = paramClockListener;
  }
  
  public void setEndTime(long paramLong)
  {
    endTime = paramLong;
  }
  
  public static abstract interface ClockListener
  {
    public abstract void remainFiveMinutes();
    
    public abstract void timeEnd();
  }
  
  private class FormatChangeObserver
  extends ContentObserver {
      public FormatChangeObserver() {
          super(new Handler());
      }

      public void onChange(boolean bl) {
          CustomDigitalClock.this.setFormat();
      }
  }
}


/* Location:              /Users/tariq/Downloads/output_jar.jar!/com/gk/zhangzhongbao/ui/view/CustomDigitalClock.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */