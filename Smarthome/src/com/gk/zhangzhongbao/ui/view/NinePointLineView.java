package com.gk.zhangzhongbao.ui.view;

import android.content.*;
import android.util.*;
import android.view.*;
import android.graphics.*;

public class NinePointLineView extends View
{
    Bitmap defaultBitmap;
    int defaultBitmapRadius;
    int height;
    boolean isUp;
    Paint linePaint;
    StringBuffer lockString;
    CallBack lockStringListener;
    int moveX;
    int moveY;
    PointInfo[] points;
    Bitmap selectedBitmap;
    int selectedBitmapDiameter;
    int selectedBitmapRadius;
    PointInfo startPoint;
    private int startX;
    private int startY;
    Paint textPaint;
    Paint whiteLinePaint;
    int width;
    
    public NinePointLineView(final Context context) {
        super(context);
        this.linePaint = new Paint();
        this.whiteLinePaint = new Paint();
        this.textPaint = new Paint();
        this.defaultBitmap = BitmapFactory.decodeResource(this.getResources(), 2130838169);
        this.defaultBitmapRadius = this.defaultBitmap.getWidth() / 2;
        this.selectedBitmap = BitmapFactory.decodeResource(this.getResources(), 2130838107);
        this.selectedBitmapDiameter = this.selectedBitmap.getWidth();
        this.selectedBitmapRadius = this.selectedBitmapDiameter / 2;
        this.points = new PointInfo[9];
        this.startPoint = null;
        this.isUp = false;
        this.lockString = new StringBuffer();
        this.startX = 0;
        this.startY = 0;
        this.setBackgroundResource(2130837571);
        this.initPaint();
    }
    
    public NinePointLineView(final Context context, final AttributeSet set) {
        super(context, set);
        this.linePaint = new Paint();
        this.whiteLinePaint = new Paint();
        this.textPaint = new Paint();
        this.defaultBitmap = BitmapFactory.decodeResource(this.getResources(), 2130838169);
        this.defaultBitmapRadius = this.defaultBitmap.getWidth() / 2;
        this.selectedBitmap = BitmapFactory.decodeResource(this.getResources(), 2130838107);
        this.selectedBitmapDiameter = this.selectedBitmap.getWidth();
        this.selectedBitmapRadius = this.selectedBitmapDiameter / 2;
        this.points = new PointInfo[9];
        this.startPoint = null;
        this.isUp = false;
        this.lockString = new StringBuffer();
        this.startX = 0;
        this.startY = 0;
        this.setBackgroundResource(2130837571);
        this.initPaint();
    }
    
    private void drawEachLine(final Canvas canvas, final PointInfo pointInfo) {
        if (pointInfo.hasNextId()) {
            final int nextId = pointInfo.getNextId();
            this.drawLine(canvas, pointInfo.getCenterX(), pointInfo.getCenterY(), this.points[nextId].getCenterX(), this.points[nextId].getCenterY());
            this.drawEachLine(canvas, this.points[nextId]);
        }
    }
    
    private void drawLine(final Canvas canvas, final float n, final float n2, final float n3, final float n4) {
        canvas.drawLine(n, n2, n3, n4, this.linePaint);
        canvas.drawLine(n, n2, n3, n4, this.whiteLinePaint);
    }
    
    private void drawNinePoint(final Canvas canvas) {
        if (this.startPoint != null) {
            this.drawEachLine(canvas, this.startPoint);
        }
        final PointInfo[] points = this.points;
        for (int length = points.length, i = 0; i < length; ++i) {
            final PointInfo pointInfo = points[i];
            if (pointInfo != null) {
                if (pointInfo.isSelected()) {
                    canvas.drawBitmap(this.selectedBitmap, (float)pointInfo.getSeletedX(), (float)pointInfo.getSeletedY(), (Paint)null);
                }
                canvas.drawBitmap(this.defaultBitmap, (float)pointInfo.getDefaultX(), (float)pointInfo.getDefaultY(), (Paint)null);
            }
        }
    }
    
    private void finishDraw() {
        final PointInfo[] points = this.points;
        for (int length = points.length, i = 0; i < length; ++i) {
            final PointInfo pointInfo = points[i];
            pointInfo.setSelected(false);
            pointInfo.setNextId(pointInfo.getId());
        }
        this.lockString.delete(0, this.lockString.length());
        this.isUp = false;
        this.invalidate();
    }
    
    private void handlingEvent(final MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            default: {}
            case 2: {
                this.moveX = (int)motionEvent.getX();
                this.moveY = (int)motionEvent.getY();
                final PointInfo[] points = this.points;
                for (int length = points.length, i = 0; i < length; ++i) {
                    final PointInfo pointInfo = points[i];
                    if (pointInfo.isInMyPlace(this.moveX, this.moveY) && !pointInfo.isSelected()) {
                        pointInfo.setSelected(true);
                        this.startX = pointInfo.getCenterX();
                        this.startY = pointInfo.getCenterY();
                        final int length2 = this.lockString.length();
                        if (length2 != 0) {
                            this.points[this.lockString.charAt(length2 - 1) - '0'].setNextId(pointInfo.getId());
                        }
                        this.lockString.append(pointInfo.getId());
                        break;
                    }
                }
                this.invalidate(0, this.height - this.width, this.width, this.height);
            }
            case 0: {
                final int n = (int)motionEvent.getX();
                final int n2 = (int)motionEvent.getY();
                final PointInfo[] points2 = this.points;
                for (int length3 = points2.length, j = 0; j < length3; ++j) {
                    final PointInfo startPoint = points2[j];
                    if (startPoint.isInMyPlace(n, n2)) {
                        startPoint.setSelected(true);
                        this.startPoint = startPoint;
                        this.startX = startPoint.getCenterX();
                        this.startY = startPoint.getCenterY();
                        this.lockString.append(startPoint.getId());
                        break;
                    }
                }
                this.invalidate(0, this.height - this.width, this.width, this.height);
            }
            case 1: {
                this.moveY = 0;
                this.moveX = 0;
                this.startY = 0;
                this.startX = 0;
                this.isUp = true;
                this.invalidate();
                this.lockStringListener.onActionUp(this.lockString);
            }
        }
    }
    
    private void initLinePaint(final Paint paint) {
        paint.setColor(-7829368);
        paint.setStrokeWidth((float)this.defaultBitmap.getWidth());
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.Cap.ROUND);
    }
    
    private void initPaint() {
        this.initLinePaint(this.linePaint);
        this.initTextPaint(this.textPaint);
        this.initWhiteLinePaint(this.whiteLinePaint);
    }
    
    private void initPoints(final PointInfo[] array) {
        final int length = array.length;
        int n2;
        final int n = n2 = (this.width - this.selectedBitmapDiameter * 3) / 4;
        int n3 = this.height - this.width;
        int n4 = this.selectedBitmapRadius + n2 - this.defaultBitmapRadius;
        int n5 = this.selectedBitmapRadius + n3 - this.defaultBitmapRadius;
        int n6 = 0;
        int n7 = 0;
        for (int i = 0; i < length; ++i, n5 = n6, n3 = n7) {
            Label_0130: {
                if (i != 3) {
                    n6 = n5;
                    n7 = n3;
                    if (i != 6) {
                        break Label_0130;
                    }
                }
                n2 = n;
                n7 = n3 + (this.selectedBitmapDiameter + n);
                n4 = this.selectedBitmapRadius + n2 - this.defaultBitmapRadius;
                n6 = n5 + (this.selectedBitmapDiameter + n);
            }
            array[i] = new PointInfo(i, n4, n6, n2, n7);
            n2 += this.selectedBitmapDiameter + n;
            n4 += this.selectedBitmapDiameter + n;
        }
    }
    
    private void initTextPaint(final Paint paint) {
        this.textPaint.setTextSize(30.0f);
        this.textPaint.setAntiAlias(true);
        this.textPaint.setTypeface(Typeface.MONOSPACE);
    }
    
    private void initWhiteLinePaint(final Paint paint) {
        paint.setColor(-1);
        paint.setStrokeWidth((float)(this.defaultBitmap.getWidth() - 5));
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.Cap.ROUND);
    }
    
    protected void onDraw(final Canvas canvas) {
        while (true) {
            if (this.moveX == 0 || this.moveY == 0 || this.startX != 0) {
                this.drawNinePoint(canvas);
                super.onDraw(canvas);
                return;
            }
            continue;
        }
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
    }
    
    protected void onMeasure(final int n, final int n2) {
        this.width = this.getWidth();
        this.height = this.getHeight();
        if (this.width != 0 && this.height != 0) {
            this.initPoints(this.points);
        }
        super.onMeasure(n, n2);
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        if (this.isUp) {
            this.finishDraw();
            return false;
        }
        this.handlingEvent(motionEvent);
        return true;
    }
    
    public void setOnCallBack(final CallBack lockStringListener) {
        this.lockStringListener = lockStringListener;
    }
    
    public interface CallBack
    {
        void onActionUp(StringBuffer p0);
    }
    
    private class PointInfo
    {
        private int defaultX;
        private int defaultY;
        private int id;
        private int nextId;
        private boolean selected;
        private int seletedX;
        private int seletedY;
        
        public PointInfo(final int n, final int defaultX, final int defaultY, final int seletedX, final int seletedY) {
            this.id = n;
            this.nextId = n;
            this.defaultX = defaultX;
            this.defaultY = defaultY;
            this.seletedX = seletedX;
            this.seletedY = seletedY;
        }
        
        public int getCenterX() {
            return this.seletedX + NinePointLineView.this.selectedBitmapRadius;
        }
        
        public int getCenterY() {
            return this.seletedY + NinePointLineView.this.selectedBitmapRadius;
        }
        
        public int getDefaultX() {
            return this.defaultX;
        }
        
        public int getDefaultY() {
            return this.defaultY;
        }
        
        public int getId() {
            return this.id;
        }
        
        public int getNextId() {
            return this.nextId;
        }
        
        public int getSeletedX() {
            return this.seletedX;
        }
        
        public int getSeletedY() {
            return this.seletedY;
        }
        
        public boolean hasNextId() {
            return this.nextId != this.id;
        }
        
        public boolean isInMyPlace(int n, int n2) {
            if (n > this.seletedX && n < this.seletedX + NinePointLineView.this.selectedBitmapDiameter) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (n2 > this.seletedY && n2 < this.seletedY + NinePointLineView.this.selectedBitmapDiameter) {
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            return n != 0 && n2 != 0;
        }
        
        public boolean isSelected() {
            return this.selected;
        }
        
        public void setNextId(final int nextId) {
            this.nextId = nextId;
        }
        
        public void setSelected(final boolean selected) {
            this.selected = selected;
        }
    }
}
