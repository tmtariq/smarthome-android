package com.gk.zhangzhongbao.ui.view;

import com.gk.zhangzhongbao.ui.bean.*;
import android.app.*;
import android.widget.*;
import android.content.*;
import com.gk.wifictr.net.*;
import android.util.*;
import android.view.*;
import com.gk.zhangzhongbao.ui.app.*;
import com.gk.wifictr.net.command.station.*;
import com.gk.wifictr.net.data.*;

public class LightView extends RelativeLayout
{
    int[] imgsOff;
    int[] imgsOn;
    int[] indexs;
    private boolean isSend;
    private LightItem light;
    private ImageView light1;
    private ImageView light2;
    private ImageView light3;
    private ImageView lightSwitch;
    private LinearLayout ll_one;
    private LinearLayout ll_three;
    private Context mContext;
    private TextView name;
    private ImageView oneLight;
    
    public LightView(final Context context, final AttributeSet set) {
        super(context, set);
        this.indexs = new int[] { 0, 1, 2 };
        this.imgsOn = new int[] { 2130838147, 2130838149, 2130838151 };
        this.imgsOff = new int[] { 2130838146, 2130838148, 2130838150 };
        this.init(context);
    }
    
    public LightView(final Context context, final LightItem light, final boolean isSend) {
        super(context);
        this.indexs = new int[] { 0, 1, 2 };
        this.imgsOn = new int[] { 2130838147, 2130838149, 2130838151 };
        this.imgsOff = new int[] { 2130838146, 2130838148, 2130838150 };
        this.light = light;
        this.isSend = isSend;
        this.init(context);
    }
    
    private void init(final Context mContext) {
        this.mContext = mContext;
        final View inflate = LayoutInflater.from(mContext).inflate(2130903076, (ViewGroup)this, true);
        this.name = (TextView)inflate.findViewById(2131231005);
        this.lightSwitch = (ImageView)inflate.findViewById(2131231003);
        this.oneLight = (ImageView)inflate.findViewById(2131231007);
        this.light1 = (ImageView)inflate.findViewById(2131231009);
        this.light2 = (ImageView)inflate.findViewById(2131231010);
        this.light3 = (ImageView)inflate.findViewById(2131231011);
        this.ll_one = (LinearLayout)inflate.findViewById(2131231006);
        this.ll_three = (LinearLayout)inflate.findViewById(2131231008);
        this.name.setText((CharSequence)this.light.getName());
        this.light.lights[0] = this.light1;
        this.light.lights[1] = this.light2;
        this.light.lights[2] = this.light3;
        this.light.lightSwitch = this.lightSwitch;
        if (this.light.longClickable) {
            this.name.setOnLongClickListener((View.OnLongClickListener)new View.OnLongClickListener() {
                public boolean onLongClick(final View view) {
                    final AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder(LightView.this.mContext);
                    alertDialog$Builder.setTitle((CharSequence)"Modify the name");
                    final EditText view2 = new EditText(LightView.this.mContext);
                    view2.setSingleLine();
                    alertDialog$Builder.setView((View)view2);
                    alertDialog$Builder.setPositiveButton((CharSequence)"Yes", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialogInterface, final int n) {
                            final String string = view2.getText().toString();
                            if (string != null && !string.equals("") && LightView.this.isSend) {
                                new BroadcastHelper().writeData(new Out_25H().make(LightView.this.light.getDevNo(), string, LightView.this.light.getSnCode(), (byte)16, LightView.this.light.getCount()));
                            }
                        }
                    });
                    alertDialog$Builder.setNegativeButton((CharSequence)"Cancle", null);
                    alertDialog$Builder.show();
                    return true;
                }
            });
            this.lightSwitch.setOnLongClickListener((View.OnLongClickListener)new View.OnLongClickListener() {
                public boolean onLongClick(final View view) {
                    final AlertDialog.Builder alertDialog$Builder = new AlertDialog.Builder(LightView.this.mContext);
                    alertDialog$Builder.setTitle((CharSequence)"Delete the device?");
                    alertDialog$Builder.setPositiveButton((CharSequence)"Yes", (DialogInterface.OnClickListener)new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialogInterface, final int n) {
                            Log.e("\u8bbe\u5907\u53f7", new StringBuilder(String.valueOf(LightView.this.light.getDevNo())).toString());
                            Log.e("\u8bbe\u5907sn", new StringBuilder(String.valueOf(LightView.this.light.getSnCode())).toString());
                            if (LightView.this.isSend) {
                                new BroadcastHelper().writeData(new Out_24H().make(LightView.this.light.getDevNo(), LightView.this.light.getSnCode(), (byte)16));
                            }
                        }
                    });
                    alertDialog$Builder.setNegativeButton((CharSequence)"Cancle", (DialogInterface.OnClickListener)null);
                    alertDialog$Builder.show();
                    return true;
                }
            });
        }
        if (this.light.getCount() == 1) {
            this.ll_one.setVisibility(0);
            this.light.setLightOn(this.light.state_one[0]);
            if (this.light.isLightOn()) {
                this.lightSwitch.setImageResource(2130838158);
                this.oneLight.setImageResource(2130838018);
            }
            else {
                this.lightSwitch.setImageResource(2130838157);
                this.oneLight.setImageResource(2130838017);
            }
            this.lightSwitch.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    Util.vibrator(LightView.this.mContext, 50L);
                    Util.clickSound(LightView.this.mContext);
                    if (LightView.this.light.isLightOn()) {
                        LightView.this.lightSwitch.setImageResource(2130838157);
                        LightView.this.light.setLightOn(false);
                        LightView.this.oneLight.setImageResource(2130838017);
                        LightView.this.light.state_one[0] = false;
                        if (LightView.this.isSend) {
                            new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(LightView.this.light.getDevNo(), LightView.this.light.getName(), LightView.this.light.getSnCode(), (byte)16, LightView.this.light.state_one), LightView.this.light.state_one));
                        }
                    }
                    else {
                        LightView.this.lightSwitch.setImageResource(2130838158);
                        LightView.this.light.setLightOn(true);
                        LightView.this.oneLight.setImageResource(2130838018);
                        LightView.this.light.state_one[0] = true;
                        if (LightView.this.isSend) {
                            new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(LightView.this.light.getDevNo(), LightView.this.light.getName(), LightView.this.light.getSnCode(), (byte)16, LightView.this.light.state_one), LightView.this.light.state_one));
                        }
                    }
                }
            });
        }
        else if (this.light.getCount() == 3) {
            this.name.setBackgroundResource(2130838152);
            this.ll_three.setVisibility(0);
            if (this.light.allLightOff()) {
                this.light.setLightOn(false);
            }
            else {
                this.light.setLightOn(true);
            }
            if (this.light.isLightOn()) {
                this.lightSwitch.setImageResource(2130838158);
            }
            else {
                this.lightSwitch.setImageResource(2130838157);
            }
            this.lightSwitch.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                public void onClick(final View view) {
                    Util.vibrator(LightView.this.mContext, 50L);
                    Util.clickSound(LightView.this.mContext);
                    if (LightView.this.light.isLightOn()) {
                        LightView.this.light.setAllLightOff();
                        return;
                    }
                    LightView.this.light.setAllLightOn();
                }
            });
            final int[] indexs = this.indexs;
            for (int length = indexs.length, i = 0; i < length; ++i) {
                final int n = indexs[i];
                if (this.light.status_three[n]) {
                    this.light.lights[n].setImageResource(this.imgsOn[n]);
                }
                else {
                    this.light.lights[n].setImageResource(this.imgsOff[n]);
                }
                this.light.lights[n].setOnClickListener((View.OnClickListener)new View.OnClickListener() {
                    public void onClick(final View view) {
                        Util.vibrator(LightView.this.mContext);
                        Util.clickSound(LightView.this.mContext);
                        if (LightView.this.light.status_three[n]) {
                            LightView.this.light.lights[n].setImageResource(LightView.this.imgsOff[n]);
                            LightView.this.light.status_three[n] = false;
                            if (LightView.this.light.allLightOff()) {
                                LightView.this.light.setLightOn(false);
                                LightView.this.lightSwitch.setImageResource(2130838157);
                            }
                        }
                        else {
                            LightView.this.light.lights[n].setImageResource(LightView.this.imgsOn[n]);
                            LightView.this.light.status_three[n] = true;
                            if (!LightView.this.light.isLightOn()) {
                                LightView.this.light.setLightOn(true);
                                LightView.this.lightSwitch.setImageResource(2130838158);
                            }
                        }
                        if (LightView.this.isSend) {
                            new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(LightView.this.light.getDevNo(), LightView.this.light.getName(), LightView.this.light.getSnCode(), (byte)16, LightView.this.light.status_three), LightView.this.light.status_three));
                        }
                    }
                });
            }
        }
    }
}
