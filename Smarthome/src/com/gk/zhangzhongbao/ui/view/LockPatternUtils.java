package com.gk.zhangzhongbao.ui.view;

import android.preference.*;
import java.util.*;
import android.content.*;

public class LockPatternUtils
{
    private static final String KEY_LOCK_PWD = "lock_pwd";
    private static Context mContext;
    private static SharedPreferences preference;
    
    public LockPatternUtils(final Context mContext) {
        LockPatternUtils.mContext = mContext;
        LockPatternUtils.preference = PreferenceManager.getDefaultSharedPreferences(LockPatternUtils.mContext);
    }
    
    public static String patternToString(final List<LockPatternView.Cell> list) {
        if (list == null) {
            return "";
        }
        final int size = list.size();
        final byte[] array = new byte[size];
        for (int i = 0; i < size; ++i) {
            final LockPatternView.Cell cell = list.get(i);
            array[i] = (byte)(cell.getRow() * 3 + cell.getColumn());
        }
        return Arrays.toString(array);
    }
    
    public static List<LockPatternView.Cell> stringToPattern(final String s) {
        final ArrayList<LockPatternView.Cell> list = new ArrayList<LockPatternView.Cell>();
        final byte[] bytes = s.getBytes();
        for (int i = 0; i < bytes.length; ++i) {
            final byte b = bytes[i];
            list.add(LockPatternView.Cell.of(b / 3, b % 3));
        }
        return list;
    }
    
    public int checkPattern(final List<LockPatternView.Cell> list) {
        final String lockPaternString = this.getLockPaternString();
        if (lockPaternString.isEmpty()) {
            return -1;
        }
        if (lockPaternString.equals(patternToString(list))) {
            return 1;
        }
        return 0;
    }
    
    public void clearLock() {
        this.saveLockPattern(null);
    }
    
    public String getLockPaternString() {
        return LockPatternUtils.preference.getString("lock_pwd", "");
    }
    
    public void saveLockPattern(final List<LockPatternView.Cell> list) {
        final SharedPreferences.Editor edit = LockPatternUtils.preference.edit();
        edit.putString("lock_pwd", patternToString(list));
        edit.commit();
    }
}
