package com.gk.zhangzhongbao.ui.view;

import android.app.*;
import android.content.*;
import android.os.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.widget.*;
import android.view.*;

public class MyAlertDialog extends Dialog implements View.OnClickListener
{
    private RelativeLayout center;
    private Context context;
    private Button okButton;
    private Runnable runnable;
    private TextView title;
    
    public MyAlertDialog(final Context context, final int n, final Runnable runnable) {
        super(context, n);
        this.context = context;
        this.runnable = runnable;
        this.setCanceledOnTouchOutside(false);
    }
    
    public void addView(final View view) {
        this.center.addView(view);
    }
    
    public void onBackPressed() {
    }
    
    public void onClick(final View view) {
        if (view == this.okButton) {
            this.runnable.run();
        }
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final View inflate = View.inflate(this.context, 2130903049, (ViewGroup)null);
        this.setContentView(inflate, new RelativeLayout.LayoutParams(Util.dip(this.context, 450.0f), -2));
        this.title = (TextView)inflate.findViewById(2131230820);
        this.center = (RelativeLayout)inflate.findViewById(2131230822);
        (this.okButton = (Button)inflate.findViewById(2131230824)).setOnClickListener((View.OnClickListener)this);
    }
    
    public void removeAllView() {
        this.center.removeAllViews();
    }
    
    public void setTitle(final String text) {
        this.title.setText((CharSequence)text);
    }
}
