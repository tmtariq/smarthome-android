package com.gk.zhangzhongbao.ui.view;

import android.content.*;
import android.graphics.*;
import com.gk.wifictr.net.data.*;
import java.util.*;
import android.os.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.gk.wifictr.net.*;
import com.gk.wifictr.net.command.station.*;
import com.gk.zhangzhongbao.ui.activity.*;

public class CurtainGarageView extends RelativeLayout implements View.OnClickListener
{
    public static final int TYPE_C_H = 3;
    public static final int TYPE_C_V = 4;
    public static final int TYPE_G_H = 1;
    public static final int TYPE_G_V = 2;
    private Context context;
    private ImageView curtainImageView;
    private int[] curtain_h;
    private int[] garage_h;
    private int[] garage_v;
    Handler handler;
    private int[] images;
    private int index;
    private boolean isLongClick;
    private boolean isSend;
    private Button left;
    public Bitmap mBitmap;
    private String nameString;
    private TextView nameTextView;
    private boolean open;
    private Button right;
    private Button stop;
    public SubDevice subDevice;
    private Timer timer;
    private TimerTask timerTask;
    public int type;
    
    public CurtainGarageView(final Context context) {
        super(context);
        this.index = 0;
        this.open = true;
        this.garage_v = new int[] { 2130838058, 2130838060, 2130838061, 2130838062, 2130838063, 2130838064, 2130838065, 2130838066, 2130838067, 2130838059 };
        this.garage_h = new int[] { 2130838521, 2130838523, 2130838524, 2130838525, 2130838526, 2130838527, 2130838528, 2130838529, 2130838530, 2130838522 };
        this.curtain_h = new int[] { 2130838000, 2130838001, 2130838002, 2130838003, 2130838004, 2130838005, 2130838006 };
        this.isSend = false;
        this.isLongClick = false;
        this.nameString = "";
        this.handler = new Handler() {
            public void handleMessage(final Message message) {
                final Bitmap mBitmap = CurtainGarageView.this.mBitmap;
                CurtainGarageView.this.mBitmap = Util.readBitMap(CurtainGarageView.this.context, CurtainGarageView.this.images[CurtainGarageView.this.index]);
                CurtainGarageView.this.curtainImageView.setImageBitmap(CurtainGarageView.this.mBitmap);
                if (mBitmap == null) {
                    Log.e("debug", "null");
                    return;
                }
                if (!mBitmap.isRecycled()) {
                    mBitmap.recycle();
                    Log.e("debug", "recycle");
                    return;
                }
                Log.e("debug", "is recycled");
            }
        };
        this.images = this.garage_v;
        this.type = 2;
        this.init(context);
    }
    
    public CurtainGarageView(final Context context, final int n, final String nameString, final boolean isSend, final boolean isLongClick) {
        super(context);
        this.index = 0;
        this.open = true;
        this.garage_v = new int[] { 2130838058, 2130838060, 2130838061, 2130838062, 2130838063, 2130838064, 2130838065, 2130838066, 2130838067, 2130838059 };
        this.garage_h = new int[] { 2130838521, 2130838523, 2130838524, 2130838525, 2130838526, 2130838527, 2130838528, 2130838529, 2130838530, 2130838522 };
        this.curtain_h = new int[] { 2130838000, 2130838001, 2130838002, 2130838003, 2130838004, 2130838005, 2130838006 };
        this.isSend = false;
        this.isLongClick = false;
        this.nameString = "";
        this.handler = new Handler() {
            public void handleMessage(final Message message) {
                final Bitmap mBitmap = CurtainGarageView.this.mBitmap;
                CurtainGarageView.this.mBitmap = Util.readBitMap(CurtainGarageView.this.context, CurtainGarageView.this.images[CurtainGarageView.this.index]);
                CurtainGarageView.this.curtainImageView.setImageBitmap(CurtainGarageView.this.mBitmap);
                if (mBitmap == null) {
                    Log.e("debug", "null");
                    return;
                }
                if (!mBitmap.isRecycled()) {
                    mBitmap.recycle();
                    Log.e("debug", "recycle");
                    return;
                }
                Log.e("debug", "is recycled");
            }
        };
        this.isSend = isSend;
        this.isLongClick = isLongClick;
        this.nameString = nameString;
        switch (n) {
            case 1: {
                this.images = this.garage_h;
                this.type = 1;
                break;
            }
            case 2: {
                this.images = this.garage_v;
                this.type = 2;
                break;
            }
            case 3: {
                this.images = this.curtain_h;
                this.type = 3;
                break;
            }
            case 4: {
                this.type = 4;
                break;
            }
        }
        this.init(context);
    }
    
    public CurtainGarageView(final Context context, final AttributeSet set) {
        super(context, set);
        this.index = 0;
        this.open = true;
        this.garage_v = new int[] { 2130838058, 2130838060, 2130838061, 2130838062, 2130838063, 2130838064, 2130838065, 2130838066, 2130838067, 2130838059 };
        this.garage_h = new int[] { 2130838521, 2130838523, 2130838524, 2130838525, 2130838526, 2130838527, 2130838528, 2130838529, 2130838530, 2130838522 };
        this.curtain_h = new int[] { 2130838000, 2130838001, 2130838002, 2130838003, 2130838004, 2130838005, 2130838006 };
        this.isSend = false;
        this.isLongClick = false;
        this.nameString = "";
        this.handler = new Handler() {
            public void handleMessage(final Message message) {
                final Bitmap mBitmap = CurtainGarageView.this.mBitmap;
                CurtainGarageView.this.mBitmap = Util.readBitMap(CurtainGarageView.this.context, CurtainGarageView.this.images[CurtainGarageView.this.index]);
                CurtainGarageView.this.curtainImageView.setImageBitmap(CurtainGarageView.this.mBitmap);
                if (mBitmap == null) {
                    Log.e("debug", "null");
                    return;
                }
                if (!mBitmap.isRecycled()) {
                    mBitmap.recycle();
                    Log.e("debug", "recycle");
                    return;
                }
                Log.e("debug", "is recycled");
            }
        };
        this.images = this.garage_v;
        this.type = 2;
        this.init(context);
    }
    
    static /* synthetic */ void access$6(final CurtainGarageView curtainGarageView, final int index) {
        curtainGarageView.index = index;
    }
    
    private void curtainStart() {
        if (this.timer == null && this.timerTask == null) {
            this.timer = new Timer();
            this.timerTask = new TimerTask() {
                @Override
                public void run() {
                    if (CurtainGarageView.this.open) {
                        final CurtainGarageView this$0 = CurtainGarageView.this;
                        CurtainGarageView.access$6(this$0, this$0.index - 1);
                        if (CurtainGarageView.this.index < 0) {
                            CurtainGarageView.access$6(CurtainGarageView.this, 0);
                            CurtainGarageView.this.cancle();
                            return;
                        }
                        CurtainGarageView.this.handler.sendEmptyMessage(1);
                    }
                    else {
                        final CurtainGarageView this$ = CurtainGarageView.this;
                        CurtainGarageView.access$6(this$, this$.index + 1);
                        if (CurtainGarageView.this.index >= CurtainGarageView.this.images.length) {
                            CurtainGarageView.access$6(CurtainGarageView.this, CurtainGarageView.this.images.length - 1);
                            CurtainGarageView.this.cancle();
                            return;
                        }
                        CurtainGarageView.this.handler.sendEmptyMessage(1);
                    }
                }
            };
            this.timer.schedule(this.timerTask, 0L, 200L);
        }
    }
    
    private void init(final Context context) {
        this.context = context;
        final View inflate = LayoutInflater.from(context).inflate(2130903065, (ViewGroup)this, true);
        this.nameTextView = (TextView)inflate.findViewById(2131230733);
        this.curtainImageView = (ImageView)inflate.findViewById(2131230858);
        this.left = (Button)inflate.findViewById(2131230855);
        this.right = (Button)inflate.findViewById(2131230857);
        this.stop = (Button)inflate.findViewById(2131230856);
        this.left.setOnClickListener((View.OnClickListener)this);
        this.right.setOnClickListener((View.OnClickListener)this);
        this.stop.setOnClickListener((View.OnClickListener)this);
        this.nameTextView.setText((CharSequence)this.nameString);
        this.mBitmap = Util.readBitMap(context, this.images[this.index]);
        this.curtainImageView.setImageBitmap(this.mBitmap);
        switch (this.type) {
            case 1: {
                this.left.setBackgroundResource(2130838068);
                this.right.setBackgroundResource(2130838074);
                break;
            }
            case 2: {
                this.left.setBackgroundResource(2130838072);
                this.right.setBackgroundResource(2130838070);
                break;
            }
            case 3: {
                this.left.setBackgroundResource(2130838009);
                this.right.setBackgroundResource(2130838012);
                break;
            }
        }
        if (this.isLongClick) {
            this.nameTextView.setOnLongClickListener((View.OnLongClickListener)new View.OnLongClickListener() {
                public boolean onLongClick(final View view) {
                    final EditText editText = new EditText(context);
                    Util.alertDlg(context, context.getString(2131296287), null, (View)editText, new Runnable() {
                        @Override
                        public void run() {
                            final String string = editText.getText().toString();
                            if (string != null && !string.equals("")) {
                                CurtainGarageView.this.nameTextView.setText((CharSequence)string);
                                CurtainGarageView.this.subDevice.devName = string;
                                new BroadcastHelper().writeData(new Out_25H().make(CurtainGarageView.this.subDevice.devNo, string, CurtainGarageView.this.subDevice.devSN, CurtainGarageView.this.subDevice.devType, CurtainGarageView.this.subDevice.devCode));
                            }
                        }
                    });
                    return true;
                }
            });
        }
    }
    
    private void send(final int n) {
        new BroadcastHelper().writeData(new Out_27H().make((byte)n, this.subDevice.devSN, this.subDevice.devType));
    }
    
    private void send(final int n, final int n2, final int n3) {
        switch (this.type) {
            default: {}
            case 1: {
                new BroadcastHelper().writeData(new Out_27H().make((byte)n, this.subDevice.devSN));
            }
            case 2: {
                new BroadcastHelper().writeData(new Out_27H().make((byte)n2, this.subDevice.devSN));
            }
            case 3: {
                new BroadcastHelper().writeData(new Out_27H().make((byte)n3, this.subDevice.devSN));
            }
        }
    }
    
    public void cancle() {
        if (this.timer != null) {
            this.timer.cancel();
            this.timer = null;
        }
        if (this.timerTask != null) {
            this.timerTask = null;
        }
    }
    
    public void onClick(final View view) {
        Util.vibrator(this.context);
        Util.clickBeepSound(this.context);
        switch (view.getId()) {
            case 2131230855: {
                this.open = false;
                this.index = 0;
                this.curtainStart();
                if (!this.isSend) {
                    break;
                }
                if (this.subDevice.devType == 96 || this.subDevice.devType == 97) {
                    this.send(1);
                    return;
                }
                if (this.subDevice.devType == 80) {
                    this.send(145, 161, 18);
                    return;
                }
                break;
            }
            case 2131230857: {
                this.open = true;
                this.index = this.images.length - 1;
                this.curtainStart();
                if (!this.isSend) {
                    break;
                }
                if (this.subDevice.devType == 96 || this.subDevice.devType == 97) {
                    this.send(2);
                    return;
                }
                if (this.subDevice.devType == 80) {
                    this.send(146, 162, 17);
                    return;
                }
                break;
            }
            case 2131230856: {
                this.cancle();
                if (!this.isSend) {
                    break;
                }
                if (this.subDevice.devType == 96 || this.subDevice.devType == 97) {
                    this.send(3);
                    return;
                }
                if (this.subDevice.devType == 80) {
                    this.send(147, 163, 19);
                    return;
                }
                break;
            }
        }
    }
    
    public void recycle() {
        if (this.mBitmap == null) {
            Log.e("debug", "null");
            return;
        }
        if (!this.mBitmap.isRecycled()) {
            this.mBitmap.recycle();
            this.mBitmap = null;
            Log.e("debug", "recycle");
            return;
        }
        Log.e("debug", "is recycled");
    }
    
    public void setDevice(final SubDevice subDevice) {
        this.subDevice = subDevice;
        this.nameTextView.setText((CharSequence)subDevice.devName);
        if (this.subDevice.devType == 96 || this.subDevice.devType == 97) {
            this.left.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(final View view) {
                    CurtainGarageView.this.subDevice.actionCode = 1;
                    final Message message = new Message();
                    message.what = 10;
                    message.obj = CurtainGarageView.this.subDevice;
                    CurtainActivity.handler.sendMessage(message);
                    return true;
                }
            });
            this.right.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(final View view) {
                    CurtainGarageView.this.subDevice.actionCode = 2;
                    final Message message = new Message();
                    message.what = 10;
                    message.obj = CurtainGarageView.this.subDevice;
                    CurtainActivity.handler.sendMessage(message);
                    return true;
                }
            });
            this.stop.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(final View view) {
                    CurtainGarageView.this.subDevice.actionCode = 3;
                    final Message message = new Message();
                    message.what = 10;
                    message.obj = CurtainGarageView.this.subDevice;
                    CurtainActivity.handler.sendMessage(message);
                    return true;
                }
            });
        }
    }
}
