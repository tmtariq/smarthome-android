package com.gk.zhangzhongbao.ui.view;

import android.support.v4.view.*;
import android.content.*;
import android.util.*;
import android.view.*;

public class MyViewPager extends ViewPager
{
    private boolean enabled;
    
    public MyViewPager(final Context context) {
        super(context);
    }
    
    public MyViewPager(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    @Override
    public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        return this.enabled && super.onInterceptTouchEvent(motionEvent);
    }
    
    @Override
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        return this.enabled && super.onTouchEvent(motionEvent);
    }
    
    public void setScrollEnabled(final boolean enabled) {
        this.enabled = enabled;
    }
}
