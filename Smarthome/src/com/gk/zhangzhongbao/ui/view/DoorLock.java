package com.gk.zhangzhongbao.ui.view;

import com.gk.wifictr.net.data.*;
import android.content.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.gk.wifictr.net.*;
import com.gk.zhangzhongbao.ui.app.*;
import com.gk.wifictr.net.command.station.*;

public class DoorLock extends LinearLayout
{
    SwitchView lockSwitchView;
    Callback mCallback;
    TextView nameTextView;
    public SubDevice subDevice;
    
    public DoorLock(final Context context, final AttributeSet set, final SubDevice subDevice) {
        super(context, set);
        this.subDevice = subDevice;
        this.init(context);
    }
    
    public DoorLock(final Context context, final SubDevice subDevice) {
        super(context);
        this.subDevice = subDevice;
        this.init(context);
    }
    
    private void init(final Context context) {
        final View inflate = View.inflate(context, 2130903079, (ViewGroup)this);
        this.nameTextView = (TextView)inflate.findViewById(2131231017);
        this.lockSwitchView = (SwitchView)inflate.findViewById(2131231018);
        this.nameTextView.setText((CharSequence)this.subDevice.devName);
        this.nameTextView.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(final View view) {
                final EditText editText = new EditText(context);
                Util.alertDlg(context, context.getString(2131296287), null, (View)editText, new Runnable() {
                    @Override
                    public void run() {
                        final String string = editText.getText().toString();
                        if (string != null && !string.equals("")) {
                            DoorLock.this.nameTextView.setText((CharSequence)string);
                            DoorLock.this.subDevice.devName = string;
                            new BroadcastHelper().writeData(new Out_25H().make(DoorLock.this.subDevice));
                        }
                    }
                });
                return true;
            }
        });
        this.lockSwitchView.setOnSwitchListener((SwitchView.OnSwitchListener)new SwitchView.OnSwitchListener() {
            @Override
            public void onCheck(final SwitchView switchView, final boolean b, final boolean b2) {
                if (b2) {
                    Util.vibrator(context);
                    Util.clickSound(context);
                    new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(DoorLock.this.subDevice.devNo, DoorLock.this.subDevice.devName, DoorLock.this.subDevice.devSN, (byte)64, new boolean[] { true }), new boolean[] { true }));
                }
            }
        });
    }
    
    public void setCallback(final Callback mCallback) {
        this.mCallback = mCallback;
    }
    
    public void setDevice(final SubDevice subDevice) {
        this.subDevice = subDevice;
        this.nameTextView.setText((CharSequence)subDevice.devName);
    }
    
    public interface Callback
    {
        void deleteDevice();
        
        void nameChange(String p0);
    }
}
