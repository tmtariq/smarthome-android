package com.gk.zhangzhongbao.ui.view;

import android.annotation.SuppressLint;
import android.content.*;

import java.lang.reflect.*;

import android.util.*;
import android.graphics.*;

import java.util.*;

import com.gk.ma.smarthome.noabout.R;

import android.view.*;
import android.os.*;

public class LockPatternView extends View
{
    private static int ASPECT_LOCK_HEIGHT = 2;
    private static int ASPECT_LOCK_WIDTH = 1;
    private static int ASPECT_SQUARE = 0;
    private static int MILLIS_PER_CIRCLE_ANIMATING = 700;
    private static boolean PROFILE_DRAWING = false;
    static int STATUS_BAR_HEIGHT = 25;
    private long mAnimatingPeriodStart;
    private Matrix mArrowMatrix;
    private int mAspect;
    private Bitmap mBitmapArrowGreenUp;
    private Bitmap mBitmapArrowRedUp;
    private Bitmap mBitmapBtnDefault;
    private Bitmap mBitmapBtnTouched;
    private Bitmap mBitmapCircleDefault;
    private Bitmap mBitmapCircleGreen;
    private Bitmap mBitmapCircleRed;
    private int mBitmapHeight;
    private int mBitmapWidth;
    private Matrix mCircleMatrix;
    private Path mCurrentPath;
    private float mDiameterFactor;
    private boolean mDrawingProfilingStarted;
    private boolean mEnableHapticFeedback;
    private float mHitFactor;
    private float mInProgressX;
    private float mInProgressY;
    private boolean mInStealthMode;
    private boolean mInputEnabled;
    private Rect mInvalidate;
    private OnPatternListener mOnPatternListener;
    private Paint mPaint;
    private Paint mPathPaint;
    private ArrayList<Cell> mPattern;
    private DisplayMode mPatternDisplayMode;
    private boolean[][] mPatternDrawLookup;
    private boolean mPatternInProgress;
    private float mSquareHeight;
    private float mSquareWidth;
    private int mStrokeAlpha;
    
    public LockPatternView(Context context) {
        this(context, null);
    }
    
    public LockPatternView(Context context, AttributeSet set) {
        super(context, set);
        mDrawingProfilingStarted = false;
        mPaint = new Paint();
        mPathPaint = new Paint();
        mPattern = new ArrayList<Cell>(9);
        mPatternDrawLookup = (boolean[][])Array.newInstance(Boolean.TYPE, 3, 3);
        mInProgressX = -1.0f;
        mInProgressY = -1.0f;
        mPatternDisplayMode = DisplayMode.Correct;
        mInputEnabled = true;
        mInStealthMode = false;
        mEnableHapticFeedback = true;
        mPatternInProgress = false;
        mDiameterFactor = 0.1f;
        mStrokeAlpha = 128;
        mHitFactor = 0.6f;
        mCurrentPath = new Path();
        mInvalidate = new Rect();
        mArrowMatrix = new Matrix();
        mCircleMatrix = new Matrix();
        String string = context.obtainStyledAttributes(set, R.styleable.LockPatternView).getString(0);
        if ("square".equals(string)) {
            mAspect = 0;
        }
        else if ("lock_width".equals(string)) {
            mAspect = 1;
        }
        else if ("lock_height".equals(string)) {
            mAspect = 2;
        }
        else {
            mAspect = 0;
        }
        setClickable(true);
        mPathPaint.setAntiAlias(true);
        mPathPaint.setDither(true);
        mPathPaint.setColor(-1);
        mPathPaint.setAlpha(128);
        mPathPaint.setStyle(Paint.Style.STROKE);
        mPathPaint.setStrokeJoin(Paint.Join.ROUND);
        mPathPaint.setStrokeCap(Paint.Cap.ROUND);
        mBitmapBtnDefault = getBitmapFor(2130837630);
        mBitmapBtnTouched = getBitmapFor(2130837632);
        mBitmapCircleDefault = getBitmapFor(2130838102);
        mBitmapCircleGreen = getBitmapFor(2130838104);
        mBitmapCircleRed = getBitmapFor(2130838106);
        mBitmapArrowGreenUp = getBitmapFor(2130838098);
        mBitmapArrowRedUp = getBitmapFor(2130838100);
        Bitmap[] array = { mBitmapBtnDefault, mBitmapBtnTouched, mBitmapCircleDefault, mBitmapCircleGreen, mBitmapCircleRed };
        for (int length = array.length, i = 0; i < length; ++i) {
            Bitmap bitmap = array[i];
            mBitmapWidth = Math.max(mBitmapWidth, bitmap.getWidth());
            mBitmapHeight = Math.max(mBitmapHeight, bitmap.getHeight());
        }
    }
    
    private void addCellToPattern(Cell cell) {
        mPatternDrawLookup[cell.getRow()][cell.getColumn()] = true;
        mPattern.add(cell);
        notifyCellAdded();
    }
    
    private Cell checkForNewHit(float n, float n2) {
        int rowHit = getRowHit(n2);
        if (rowHit >= 0) {
            int columnHit = getColumnHit(n);
            if (columnHit >= 0 && !mPatternDrawLookup[rowHit][columnHit]) {
                return Cell.of(rowHit, columnHit);
            }
        }
        return null;
    }
    
    private void clearPatternDrawLookup() {
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                mPatternDrawLookup[i][j] = false;
            }
        }
    }
    
    @SuppressLint("NewApi")
	private Cell detectAndAddHit(float n, float n2) {
        int n3 = -1;
        Cell checkForNewHit = checkForNewHit(n, n2);
        if (checkForNewHit != null) {
            Cell of = null;
            //ArrayList<Cell> mPattern = mPattern;
            if (!mPattern.isEmpty()) {
                Cell cell = mPattern.get(mPattern.size() - 1);
                int n4 = checkForNewHit.row - cell.row;
                int n5 = checkForNewHit.column - cell.column;
                int row = cell.row;
                int column = cell.column;
                int n6 = row;
                if (Math.abs(n4) == 2) {
                    n6 = row;
                    if (Math.abs(n5) != 1) {
                        int row2 = cell.row;
                        int n7;
                        if (n4 > 0) {
                            n7 = 1;
                        }
                        else {
                            n7 = -1;
                        }
                        n6 = row2 + n7;
                    }
                }
                int n8 = column;
                if (Math.abs(n5) == 2) {
                    n8 = column;
                    if (Math.abs(n4) != 1) {
                        int column2 = cell.column;
                        int n9 = n3;
                        if (n5 > 0) {
                            n9 = 1;
                        }
                        n8 = column2 + n9;
                    }
                }
                of = Cell.of(n6, n8);
            }
            if (of != null && !mPatternDrawLookup[of.row][of.column]) {
                addCellToPattern(of);
            }
            addCellToPattern(checkForNewHit);
            if (mEnableHapticFeedback) {
                performHapticFeedback(1, 3);
            }
            return checkForNewHit;
        }
        return null;
    }
    
    private void drawArrow(Canvas canvas, float n, float n2, Cell cell, Cell cell2) {
        int n3;
        if (mPatternDisplayMode != DisplayMode.Wrong) {
            n3 = 1;
        }
        else {
            n3 = 0;
        }
        int row = cell2.row;
        int row2 = cell.row;
        int column = cell2.column;
        int column2 = cell.column;
        int n4 = ((int)mSquareWidth - mBitmapWidth) / 2;
        int n5 = ((int)mSquareHeight - mBitmapHeight) / 2;
        Log.v("--->>", String.valueOf(n4) + " " + n5 + " offset");
        Bitmap bitmap;
        if (n3 != 0) {
            bitmap = mBitmapArrowGreenUp;
        }
        else {
            bitmap = mBitmapArrowRedUp;
        }
        //int mBitmapWidth = mBitmapWidth;
        //int mBitmapHeight = mBitmapHeight;
        float n6 = (float)Math.toDegrees((float)Math.atan2(row - row2, column - column2));
        float min = Math.min(mSquareWidth / mBitmapWidth, 1.0f);
        float min2 = Math.min(mSquareHeight / mBitmapHeight, 1.0f);
        mArrowMatrix.setTranslate(n4 + n, n5 + n2);
        mArrowMatrix.preTranslate((float)(mBitmapWidth / 2), (float)(mBitmapHeight / 2));
        mArrowMatrix.preScale(min, min2);
        mArrowMatrix.preTranslate((float)(-mBitmapWidth / 2), (float)(-mBitmapHeight / 2));
        mArrowMatrix.preRotate(n6 + 90.0f, mBitmapWidth / 2.0f, mBitmapHeight / 2.0f);
        mArrowMatrix.preTranslate((mBitmapWidth - bitmap.getWidth()) / 2.0f, 0.0f);
        canvas.drawBitmap(bitmap, mArrowMatrix, mPaint);
    }
    
    private void drawCircle(Canvas canvas, int n, int n2, boolean b) {
        Bitmap bitmap;
        Bitmap bitmap2;
        if (!b || (mInStealthMode && mPatternDisplayMode != DisplayMode.Wrong)) {
            bitmap = mBitmapCircleDefault;
            bitmap2 = mBitmapBtnDefault;
        }
        else if (mPatternInProgress) {
            bitmap = mBitmapCircleGreen;
            bitmap2 = mBitmapBtnTouched;
        }
        else if (mPatternDisplayMode == DisplayMode.Wrong) {
            bitmap = mBitmapCircleRed;
            bitmap2 = mBitmapBtnDefault;
        }
        else {
            if (mPatternDisplayMode != DisplayMode.Correct && mPatternDisplayMode != DisplayMode.Animate) {
                throw new IllegalStateException("unknown display mode " + mPatternDisplayMode);
            }
            bitmap = mBitmapCircleGreen;
            bitmap2 = mBitmapBtnDefault;
        }
        //int mBitmapWidth = mBitmapWidth;
        //int mBitmapHeight = mBitmapHeight;
        //float mSquareWidth = mSquareWidth;
        //float mSquareHeight = mSquareHeight;
        int n3 = (int)((mSquareWidth - mBitmapWidth) / 2.0f);
        int n4 = (int)((mSquareHeight - mBitmapHeight) / 2.0f);
        float min = Math.min(mSquareWidth / mBitmapWidth, 1.0f);
        float min2 = Math.min(mSquareHeight / mBitmapHeight, 1.0f);
        mCircleMatrix.setTranslate((float)(n + n3), (float)(n2 + n4));
        mCircleMatrix.preTranslate((float)(mBitmapWidth / 2), (float)(mBitmapHeight / 2));
        mCircleMatrix.preScale(min, min2);
        mCircleMatrix.preTranslate((float)(-mBitmapWidth / 2), (float)(-mBitmapHeight / 2));
        canvas.drawBitmap(bitmap, mCircleMatrix, mPaint);
        canvas.drawBitmap(bitmap2, mCircleMatrix, mPaint);
    }
    
    private Bitmap getBitmapFor(int n) {
        return BitmapFactory.decodeResource(getContext().getResources(), n);
    }
    
    private float getCenterXForColumn(int n) {
        return getPaddingLeft() + n * mSquareWidth + mSquareWidth / 2.0f;
    }
    
    private float getCenterYForRow(int n) {
        return getPaddingTop() + n * mSquareHeight + mSquareHeight / 2.0f;
    }
    
    private int getColumnHit(float n) {
        //float mSquareWidth = mSquareWidth;
        float n2 = mSquareWidth * mHitFactor;
        float n3 = getPaddingLeft();
        float n4 = (mSquareWidth - n2) / 2.0f;
        for (int i = 0; i < 3; ++i) {
            float n5 = n3 + n4 + i * mSquareWidth;
            if (n >= n5) {
                int n6 = i;
                if (n <= n5 + n2) {
                    return n6;
                }
            }
        }
        return -1;
    }
    
    private int getRowHit(float n) {
        //float mSquareHeight = mSquareHeight;
        float n2 = mSquareHeight * mHitFactor;
        float n3 = getPaddingTop();
        float n4 = (mSquareHeight - n2) / 2.0f;
        for (int i = 0; i < 3; ++i) {
            float n5 = n3 + n4 + i * mSquareHeight;
            if (n >= n5) {
                int n6 = i;
                if (n <= n5 + n2) {
                    return n6;
                }
            }
        }
        return -1;
    }
    
    private void handleActionDown(MotionEvent motionEvent) {
        resetPattern();
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        Cell detectAndAddHit = detectAndAddHit(x, y);
        if (detectAndAddHit != null) {
            mPatternInProgress = true;
            mPatternDisplayMode = DisplayMode.Correct;
            notifyPatternStarted();
        }
        else {
            mPatternInProgress = false;
            notifyPatternCleared();
        }
        if (detectAndAddHit != null) {
            float centerXForColumn = getCenterXForColumn(detectAndAddHit.column);
            float centerYForRow = getCenterYForRow(detectAndAddHit.row);
            float n = mSquareWidth / 2.0f;
            float n2 = mSquareHeight / 2.0f;
            invalidate((int)(centerXForColumn - n), (int)(centerYForRow - n2), (int)(centerXForColumn + n), (int)(centerYForRow + n2));
        }
        mInProgressX = x;
        mInProgressY = y;
    }
    
    private void handleActionMove(MotionEvent motionEvent) {
        for (int historySize = motionEvent.getHistorySize(), i = 0; i < historySize + 1; ++i) {
            float mInProgressX;
            if (i < historySize) {
                mInProgressX = motionEvent.getHistoricalX(i);
            }
            else {
                mInProgressX = motionEvent.getX();
            }
            float mInProgressY;
            if (i < historySize) {
                mInProgressY = motionEvent.getHistoricalY(i);
            }
            else {
                mInProgressY = motionEvent.getY();
            }
            int size = mPattern.size();
            Cell detectAndAddHit = detectAndAddHit(mInProgressX, mInProgressY);
            int size2 = mPattern.size();
            if (detectAndAddHit != null && size2 == 1) {
                mPatternInProgress = true;
                notifyPatternStarted();
            }
            if (Math.abs(mInProgressX - mInProgressX) + Math.abs(mInProgressY - mInProgressY) > mSquareWidth * 0.01f) {
                float mInProgressX2 = mInProgressX;
                float mInProgressY2 = mInProgressY;
                this.mInProgressX = mInProgressX;
                this.mInProgressY = mInProgressY;
                if (mPatternInProgress && size2 > 0) {
                    //ArrayList<Cell> mPattern = mPattern;
                    float n = mSquareWidth * mDiameterFactor * 0.5f;
                    Cell cell = mPattern.get(size2 - 1);
                    float centerXForColumn = getCenterXForColumn(cell.column);
                    float centerYForRow = getCenterYForRow(cell.row);
                    //Rect mInvalidate = mInvalidate;
                    float n3;
                    if (centerXForColumn < mInProgressX) {
                        float n2 = centerXForColumn;
                        n3 = mInProgressX;
                        mInProgressX = n2;
                    }
                    else {
                        n3 = centerXForColumn;
                    }
                    float n4;
                    if (centerYForRow < mInProgressY) {
                        n4 = centerYForRow;
                    }
                    else {
                        n4 = mInProgressY;
                        mInProgressY = centerYForRow;
                    }
                    mInvalidate.set((int)(mInProgressX - n), (int)(n4 - n), (int)(n3 + n), (int)(mInProgressY + n));
                    float n5;
                    if (centerXForColumn < mInProgressX2) {
                        n5 = mInProgressX2;
                    }
                    else {
                        n5 = centerXForColumn;
                        centerXForColumn = mInProgressX2;
                    }
                    float n6;
                    if (centerYForRow < mInProgressY2) {
                        n6 = centerYForRow;
                        centerYForRow = mInProgressY2;
                    }
                    else {
                        n6 = mInProgressY2;
                    }
                    mInvalidate.union((int)(centerXForColumn - n), (int)(n6 - n), (int)(n5 + n), (int)(centerYForRow + n));
                    if (detectAndAddHit != null) {
                        float centerXForColumn2 = getCenterXForColumn(detectAndAddHit.column);
                        float centerYForRow2 = getCenterYForRow(detectAndAddHit.row);
                        float centerXForColumn3;
                        float centerYForRow3;
                        if (size2 >= 2) {
                            Cell cell2 = mPattern.get(size2 - 1 - (size2 - size));
                            centerXForColumn3 = getCenterXForColumn(cell2.column);
                            centerYForRow3 = getCenterYForRow(cell2.row);
                            if (centerXForColumn2 >= centerXForColumn3) {
                                float n7 = centerXForColumn2;
                                centerXForColumn2 = centerXForColumn3;
                                centerXForColumn3 = n7;
                            }
                            if (centerYForRow2 < centerYForRow3) {
                                float n8 = centerYForRow2;
                                centerYForRow2 = centerYForRow3;
                                centerYForRow3 = n8;
                            }
                        }
                        else {
                            centerXForColumn3 = centerXForColumn2;
                            float n9 = centerYForRow2;
                            centerYForRow3 = centerYForRow2;
                            centerYForRow2 = n9;
                        }
                        float n10 = mSquareWidth / 2.0f;
                        float n11 = mSquareHeight / 2.0f;
                        mInvalidate.set((int)(centerXForColumn2 - n10), (int)(centerYForRow3 - n11), (int)(centerXForColumn3 + n10), (int)(centerYForRow2 + n11));
                    }
                    invalidate(mInvalidate);
                }
                else {
                    invalidate();
                }
            }
        }
    }
    
    private void handleActionUp(MotionEvent motionEvent) {
        if (!mPattern.isEmpty()) {
            mPatternInProgress = false;
            notifyPatternDetected();
            invalidate();
        }
    }
    
    private void notifyCellAdded() {
        if (mOnPatternListener != null) {
            mOnPatternListener.onPatternCellAdded(mPattern);
        }
        sendAccessEvent(2131296262);
    }
    
    private void notifyPatternCleared() {
        if (mOnPatternListener != null) {
            mOnPatternListener.onPatternCleared();
        }
        sendAccessEvent(2131296261);
    }
    
    private void notifyPatternDetected() {
        if (mOnPatternListener != null) {
            mOnPatternListener.onPatternDetected(mPattern);
        }
        sendAccessEvent(2131296263);
    }
    
    private void notifyPatternStarted() {
        if (mOnPatternListener != null) {
            mOnPatternListener.onPatternStart();
        }
        sendAccessEvent(2131296260);
    }
    
    private void resetPattern() {
        mPattern.clear();
        clearPatternDrawLookup();
        mPatternDisplayMode = DisplayMode.Correct;
        invalidate();
    }
    
    private int resolveMeasured(int n, int n2) {
        int size = View.MeasureSpec.getSize(n);
        switch (View.MeasureSpec.getMode(n)) {
            default: {
                return size;
            }
            case 0: {
                return n2;
            }
            case Integer.MIN_VALUE: {
                return Math.min(size, n2);
            }
        }
    }
    
    @SuppressLint("NewApi")
	private void sendAccessEvent(int n) {
        setContentDescription((CharSequence)getContext().getString(n));
        sendAccessibilityEvent(4);
        setContentDescription((CharSequence)null);
    }
    
    public void clearPattern() {
        resetPattern();
    }
    
    public void disableInput() {
        mInputEnabled = false;
    }
    
    public void enableInput() {
        mInputEnabled = true;
    }
    
    protected int getSuggestedMinimumHeight() {
        return mBitmapHeight * 3;
    }
    
    protected int getSuggestedMinimumWidth() {
        return mBitmapWidth * 3;
    }
    
    public boolean isInStealthMode() {
        return mInStealthMode;
    }
    
    public boolean isTactileFeedbackEnabled() {
        return mEnableHapticFeedback;
    }
    
    protected void onDraw(Canvas canvas) {
        //ArrayList<Cell> mPattern = mPattern;
        int size = mPattern.size();
        //boolean[][] mPatternDrawLookup = mPatternDrawLookup;
        if (mPatternDisplayMode == DisplayMode.Animate) {
            int n = (int)(SystemClock.elapsedRealtime() - mAnimatingPeriodStart) % ((size + 1) * 700);
            int n2 = n / 700;
            clearPatternDrawLookup();
            for (int i = 0; i < n2; ++i) {
                Cell cell = mPattern.get(i);
                mPatternDrawLookup[cell.getRow()][cell.getColumn()] = true;
            }
            int n3;
            if (n2 > 0 && n2 < size) {
                n3 = 1;
            }
            else {
                n3 = 0;
            }
            if (n3 != 0) {
                float n4 = n % 700 / 700.0f;
                Cell cell2 = mPattern.get(n2 - 1);
                float centerXForColumn = getCenterXForColumn(cell2.column);
                float centerYForRow = getCenterYForRow(cell2.row);
                Cell cell3 = mPattern.get(n2);
                float centerXForColumn2 = getCenterXForColumn(cell3.column);
                float centerYForRow2 = getCenterYForRow(cell3.row);
                mInProgressX = centerXForColumn + n4 * (centerXForColumn2 - centerXForColumn);
                mInProgressY = centerYForRow + n4 * (centerYForRow2 - centerYForRow);
            }
            invalidate();
        }
        //float mSquareWidth = mSquareWidth;
        //float mSquareHeight = mSquareHeight;
        mPathPaint.setStrokeWidth(mDiameterFactor * mSquareWidth * 0.5f);
        //Path mCurrentPath = mCurrentPath;
        mCurrentPath.rewind();
        int paddingTop = getPaddingTop();
        int paddingLeft = getPaddingLeft();
        for (int j = 0; j < 3; ++j) {
            float n5 = paddingTop;
            float n6 = j;
            for (int k = 0; k < 3; ++k) {
                drawCircle(canvas, (int)(paddingLeft + k * mSquareWidth), (int)(n5 + n6 * mSquareHeight), mPatternDrawLookup[j][k]);
            }
        }
        boolean b;
        if (mInStealthMode && mPatternDisplayMode != DisplayMode.Wrong) {
            b = false;
        }
        else {
            b = true;
        }
        boolean filterBitmap = (mPaint.getFlags() & 0x2) != 0x0;
        mPaint.setFilterBitmap(true);
        if (b) {
            for (int l = 0; l < size - 1; ++l) {
                Cell cell4 = mPattern.get(l);
                Cell cell5 = mPattern.get(l + 1);
                if (!mPatternDrawLookup[cell5.row][cell5.column]) {
                    break;
                }
                drawArrow(canvas, paddingLeft + cell4.column * mSquareWidth, paddingTop + cell4.row * mSquareHeight, cell4, cell5);
            }
        }
        if (b) {
            boolean b2 = false;
            for (int n7 = 0; n7 < size; ++n7) {
                Cell cell6 = mPattern.get(n7);
                if (!mPatternDrawLookup[cell6.row][cell6.column]) {
                    break;
                }
                b2 = true;
                float centerXForColumn3 = getCenterXForColumn(cell6.column);
                float centerYForRow3 = getCenterYForRow(cell6.row);
                if (n7 == 0) {
                    mCurrentPath.moveTo(centerXForColumn3, centerYForRow3);
                }
                else {
                    mCurrentPath.lineTo(centerXForColumn3, centerYForRow3);
                }
            }
            if ((mPatternInProgress || mPatternDisplayMode == DisplayMode.Animate) && b2) {
                mCurrentPath.lineTo(mInProgressX, mInProgressY);
            }
            canvas.drawPath(mCurrentPath, mPathPaint);
        }
        mPaint.setFilterBitmap(filterBitmap);
    }
    
    protected void onMeasure(int n, int n2) {
        int suggestedMinimumWidth = getSuggestedMinimumWidth();
        int suggestedMinimumHeight = getSuggestedMinimumHeight();
        int resolveMeasured = resolveMeasured(n, suggestedMinimumWidth);
        n = resolveMeasured(n2, suggestedMinimumHeight);
        switch (mAspect) {
            default: {
                n2 = resolveMeasured;
                break;
            }
            case 0: {
                n = (n2 = Math.min(resolveMeasured, n));
                break;
            }
            case 1: {
                n = Math.min(resolveMeasured, n);
                n2 = resolveMeasured;
                break;
            }
            case 2: {
                n2 = Math.min(resolveMeasured, n);
                break;
            }
        }
        setMeasuredDimension(n2, n);
    }
    
    protected void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState)parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setPattern(DisplayMode.Correct, LockPatternUtils.stringToPattern(savedState.getSerializedPattern()));
        mPatternDisplayMode = DisplayMode.values()[savedState.getDisplayMode()];
        mInputEnabled = savedState.isInputEnabled();
        mInStealthMode = savedState.isInStealthMode();
        mEnableHapticFeedback = savedState.isTactileFeedbackEnabled();
    }
    
    protected Parcelable onSaveInstanceState() {
        return new SavedState(super.onSaveInstanceState(), LockPatternUtils.patternToString(mPattern), mPatternDisplayMode.ordinal(), mInputEnabled, mInStealthMode, mEnableHapticFeedback);
    }
    
    protected void onSizeChanged(int n, int n2, int n3, int n4) {
        mSquareWidth = (n - getPaddingLeft() - getPaddingRight()) / 3.0f;
        mSquareHeight = (n2 - getPaddingTop() - getPaddingBottom()) / 3.0f;
    }
    
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!mInputEnabled || !isEnabled()) {
            return false;
        }
        switch (motionEvent.getAction()) {
            default: {
                return false;
            }
            case 0: {
                handleActionDown(motionEvent);
                return true;
            }
            case 1: {
                handleActionUp(motionEvent);
                return true;
            }
            case 2: {
                handleActionMove(motionEvent);
                return true;
            }
            case 3: {
                resetPattern();
                mPatternInProgress = false;
                notifyPatternCleared();
                return true;
            }
        }
    }
    
    public void setDisplayMode(DisplayMode mPatternDisplayMode) {
        this.mPatternDisplayMode = mPatternDisplayMode;
        if (mPatternDisplayMode == DisplayMode.Animate) {
            if (mPattern.size() == 0) {
                throw new IllegalStateException("you must have a pattern to animate if you want to set the display mode to animate");
            }
            mAnimatingPeriodStart = SystemClock.elapsedRealtime();
            Cell cell = mPattern.get(0);
            mInProgressX = getCenterXForColumn(cell.getColumn());
            mInProgressY = getCenterYForRow(cell.getRow());
            clearPatternDrawLookup();
        }
        invalidate();
    }
    
    public void setInStealthMode(boolean mInStealthMode) {
        this.mInStealthMode = mInStealthMode;
    }
    
    public void setOnPatternListener(OnPatternListener mOnPatternListener) {
        this.mOnPatternListener = mOnPatternListener;
    }
    
    public void setPattern(DisplayMode displayMode, List<Cell> list) {
        mPattern.clear();
        mPattern.addAll(list);
        clearPatternDrawLookup();
        for (Cell cell : list) {
            mPatternDrawLookup[cell.getRow()][cell.getColumn()] = true;
        }
        setDisplayMode(displayMode);
    }
    
    public void setTactileFeedbackEnabled(boolean mEnableHapticFeedback) {
        this.mEnableHapticFeedback = mEnableHapticFeedback;
    }
    
    public static class Cell
    {
        static Cell[][] sCells;
        int column;
        int row;
        
        static {
            Cell.sCells = (Cell[][])Array.newInstance(Cell.class, 3, 3);
            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j < 3; ++j) {
                    Cell.sCells[i][j] = new Cell(i, j);
                }
            }
        }
        
        private Cell(int row, int column) {
            checkRange(row, column);
            this.row = row;
            this.column = column;
        }
        
        private static void checkRange(int n, int n2) {
            if (n < 0 || n > 2) {
                throw new IllegalArgumentException("row must be in range 0-2");
            }
            if (n2 < 0 || n2 > 2) {
                throw new IllegalArgumentException("column must be in range 0-2");
            }
        }
        
        public static Cell of(int n, int n2) {
            synchronized (Cell.class) {
                checkRange(n, n2);
                return Cell.sCells[n][n2];
            }
        }
        
        public int getColumn() {
            return column;
        }
        
        public int getRow() {
            return row;
        }
        
        @Override
        public String toString() {
            return "(row=" + row + ",clmn=" + column + ")";
        }
    }
    
    public enum DisplayMode
    {
        Animate, 
        Correct, 
        Wrong
    }
    
    public interface OnPatternListener
    {
        void onPatternCellAdded(List<Cell> p0);
        
        void onPatternCleared();
        
        void onPatternDetected(List<Cell> p0);
        
        void onPatternStart();
    }
    
    private static class SavedState extends View.BaseSavedState
    {
        private int mDisplayMode;
        private boolean mInStealthMode;
        private boolean mInputEnabled;
        private String mSerializedPattern;
        private boolean mTactileFeedbackEnabled;
        
        private SavedState(Parcelable parcelable, String mSerializedPattern, int mDisplayMode, boolean mInputEnabled, boolean mInStealthMode, boolean mTactileFeedbackEnabled) {
            super(parcelable);
            this.mSerializedPattern = mSerializedPattern;
            this.mDisplayMode = mDisplayMode;
            this.mInputEnabled = mInputEnabled;
            this.mInStealthMode = mInStealthMode;
            this.mTactileFeedbackEnabled = mTactileFeedbackEnabled;
        }
        
        private SavedState(Parcel parcel) {
            super(parcel);
            mSerializedPattern = parcel.readString();
            mDisplayMode = parcel.readInt();
            mInputEnabled = (Boolean)parcel.readValue(null);
            mInStealthMode = (Boolean)parcel.readValue(null);
            mTactileFeedbackEnabled = (Boolean)parcel.readValue(null);
        }
        
        public int getDisplayMode() {
            return mDisplayMode;
        }
        
        public String getSerializedPattern() {
            return mSerializedPattern;
        }
        
        public boolean isInStealthMode() {
            return mInStealthMode;
        }
        
        public boolean isInputEnabled() {
            return mInputEnabled;
        }
        
        public boolean isTactileFeedbackEnabled() {
            return mTactileFeedbackEnabled;
        }
        
        public void writeToParcel(Parcel parcel, int n) {
            super.writeToParcel(parcel, n);
            parcel.writeString(mSerializedPattern);
            parcel.writeInt(mDisplayMode);
            parcel.writeValue((Object)mInputEnabled);
            parcel.writeValue((Object)mInStealthMode);
            parcel.writeValue((Object)mTactileFeedbackEnabled);
        }
    }
}
