package com.gk.zhangzhongbao.ui.view;

import android.widget.*;
import android.content.*;
import android.util.*;
import android.view.*;

public class MainButton extends RelativeLayout
{
    public int isAdd;
    public String key;
    private RelativeLayout rLayout;
    private TextView textView;
    
    public MainButton(final Context context) {
        super(context);
        this.key = "";
        this.isAdd = 0;
        this.init(context);
    }
    
    public MainButton(final Context context, final AttributeSet set) {
        super(context, set);
        this.key = "";
        this.isAdd = 0;
        this.init(context);
    }
    
    private void init(final Context context) {
        final View inflate = View.inflate(context, 2130903083, (ViewGroup)this);
        this.rLayout = (RelativeLayout)inflate.findViewById(2131231032);
        this.textView = (TextView)inflate.findViewById(2131231033);
    }
    
    public void setBackground(final int backgroundResource) {
        this.rLayout.setBackgroundResource(backgroundResource);
    }
    
    public void setText(final String text) {
        this.textView.setText((CharSequence)text);
    }
}
