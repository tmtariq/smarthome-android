package com.gk.zhangzhongbao.ui.view;

import android.app.*;
import java.util.*;
import android.content.*;
import android.os.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.view.*;
import android.util.*;
import android.widget.*;
import android.graphics.drawable.*;

public class BrandDialog extends Dialog
{
    public List<Map<String, Object>> ablist;
    private SimpleAdapter adapter;
    private ListView airBrandLv;
    public TextView brandName;
    public TextView brandNum;
    public Button cancleButton;
    private Context context;
    public Button left;
    ClickListener mListener;
    public Button okButton;
    private PopupWindow popupWindow;
    public Button right;
    public TextView rlBrandName;
    public TextView rlBrandNum;
    
    public BrandDialog(final Context context, final List<Map<String, Object>> ablist, final int n) {
        super(context, n);
        this.context = context;
        this.ablist = ablist;
    }
    
    public BrandDialog(final Context context, final boolean b, final DialogInterface.OnCancelListener dialogInterface$OnCancelListener) {
        super(context, b, dialogInterface$OnCancelListener);
    }
    
    static /* synthetic */ void access$2(final BrandDialog brandDialog, final ListView airBrandLv) {
        brandDialog.airBrandLv = airBrandLv;
    }
    
    static /* synthetic */ void access$5(final BrandDialog brandDialog, final PopupWindow popupWindow) {
        brandDialog.popupWindow = popupWindow;
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final View inflate = View.inflate(this.context, 2130903051, (ViewGroup)null);
        this.setContentView(inflate, new RelativeLayout.LayoutParams(Util.dip(this.context, 450.0f), Util.dip(this.context, 600.0f)));
        this.brandName = (TextView)inflate.findViewById(2131230833);
        this.brandNum = (TextView)inflate.findViewById(2131230837);
        this.rlBrandName = (TextView)inflate.findViewById(2131230832);
        this.rlBrandNum = (TextView)inflate.findViewById(2131230835);
        this.left = (Button)inflate.findViewById(2131230838);
        this.right = (Button)inflate.findViewById(2131230836);
        this.okButton = (Button)inflate.findViewById(2131230839);
        this.cancleButton = (Button)inflate.findViewById(2131230840);
        this.brandName.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                Log.e("", "click");
                if (BrandDialog.this.airBrandLv == null) {
                    BrandDialog.access$2(BrandDialog.this, new ListView(BrandDialog.this.context));
                    BrandDialog.this.airBrandLv.setAdapter((ListAdapter)BrandDialog.this.adapter);
                    BrandDialog.this.airBrandLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
                            Util.vibrator(BrandDialog.this.context);
                            BrandDialog.this.mListener.itemClick(n);
                            BrandDialog.this.popupWindow.dismiss();
                        }
                    });
                }
                if (BrandDialog.this.popupWindow == null) {
                    BrandDialog.access$5(BrandDialog.this, new PopupWindow((View)BrandDialog.this.airBrandLv, Util.dip(BrandDialog.this.context, 230.0f), Util.dip(BrandDialog.this.context, 400.0f)));
                }
                BrandDialog.this.popupWindow.setBackgroundDrawable((Drawable)new BitmapDrawable());
                BrandDialog.this.popupWindow.showAsDropDown((View)BrandDialog.this.brandName, Util.dip(BrandDialog.this.context, -20.0f), 0);
                BrandDialog.this.popupWindow.setFocusable(true);
                BrandDialog.this.popupWindow.setOutsideTouchable(true);
                BrandDialog.this.popupWindow.update();
            }
        });
        this.left.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                BrandDialog.this.mListener.leftClick();
            }
        });
        this.right.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                BrandDialog.this.mListener.rightClick();
            }
        });
        this.okButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                BrandDialog.this.mListener.okClick();
            }
        });
        this.cancleButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                BrandDialog.this.dismiss();
            }
        });
        this.adapter = new SimpleAdapter(this.context, (List)this.ablist, 2130903052, new String[] { "brand" }, new int[] { 2131230832 });
    }
    
    public void setListener(final ClickListener mListener) {
        this.mListener = mListener;
    }
    
    public interface ClickListener
    {
        void itemClick(int p0);
        
        void leftClick();
        
        void okClick();
        
        void rightClick();
    }
}
