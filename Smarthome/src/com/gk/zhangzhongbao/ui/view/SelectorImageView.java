package com.gk.zhangzhongbao.ui.view;

import android.widget.*;
import android.graphics.*;
import android.content.*;
import android.util.*;
import android.view.*;
import com.gk.zhangzhongbao.ui.app.*;

public class SelectorImageView extends ImageView
{
    public Bitmap bitmap_n;
    public Bitmap bitmap_p;
    Context context;
    float subx;
    float suby;
    float x;
    float y;
    
    public SelectorImageView(final Context context, final int n, final int n2) {
        super(context);
        this.bitmap_n = null;
        this.bitmap_p = null;
        this.x = 0.0f;
        this.y = 0.0f;
        this.context = context;
        this.setRes(n, n2);
    }
    
    public SelectorImageView(final Context context, final AttributeSet set) {
        super(context, set);
        this.bitmap_n = null;
        this.bitmap_p = null;
        this.x = 0.0f;
        this.y = 0.0f;
        this.context = context;
    }
    
    public boolean dispatchTouchEvent(final MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0: {
                this.x = motionEvent.getX();
                this.y = motionEvent.getY();
                this.setImageBitmap(this.bitmap_p);
                break;
            }
            case 1: {
                this.setImageBitmap(this.bitmap_n);
                break;
            }
            case 2: {
                this.subx = motionEvent.getX() - this.x;
                this.suby = motionEvent.getY() - this.y;
                if (this.subx > 15.0f || this.subx < -15.0f || this.suby > 1.0f || this.suby < -1.0f) {
                    this.setImageBitmap(this.bitmap_n);
                    break;
                }
                break;
            }
        }
        return super.dispatchTouchEvent(motionEvent);
    }
    
    public Bitmap getBitmap() {
        return this.bitmap_n;
    }
    
    public void setImage(final int n) {
        if (n > 0) {
            if (this.bitmap_n == null) {
                this.bitmap_n = Util.readBitMap(this.context, n);
            }
            this.setImageBitmap(this.bitmap_n);
        }
    }
    
    public void setRes(final int n, final int n2) {
        if (n > 0) {
            if (this.bitmap_n == null) {
                this.bitmap_n = Util.readBitMap(this.context, n);
            }
            this.setImageBitmap(this.bitmap_n);
        }
        if (n2 > 0 && this.bitmap_p == null) {
            this.bitmap_p = Util.readBitMap(this.context, n2);
        }
    }
}
