package com.gk.zhangzhongbao.ui.view;

import android.graphics.*;
import android.content.*;
import android.support.v4.view.*;
import android.widget.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.view.*;
import android.util.*;
import java.util.*;

public class GuideViewPager extends ViewPager
{
    protected ArrayList<Bitmap> bmpList;
    private Context context;
    private OnPageChangeListener mLinstener;
    OnPageTouchLisenter mTouchLisenter;
    protected int page;
    protected int sh;
    float subx;
    float suby;
    protected int sw;
    private ArrayList<View> views;
    private ViewPagerAdapter vpAdapter;
    float x;
    float y;
    
    public GuideViewPager(Context context) {
        super(context);
        this.views = new ArrayList<View>();
        this.mLinstener = new OnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(int n) {
            }
            
            @Override
            public void onPageScrolled(int n, float n2, int n3) {
            }
            
            @Override
            public void onPageSelected(int page) {
                GuideViewPager.this.page = page;
            }
        };
        this.x = 0.0f;
        this.y = 0.0f;
        this.context = context;
        this.initViews();
        this.setAdapter(this.vpAdapter = new ViewPagerAdapter());
        this.setOnPageChangeListener(this.mLinstener);
    }
    
    public GuideViewPager(Context context, AttributeSet set) {
        super(context, set);
        this.views = new ArrayList<View>();
        this.mLinstener = new OnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(int n) {
            }
            
            @Override
            public void onPageScrolled(int n, float n2, int n3) {
            }
            
            @Override
            public void onPageSelected(int page) {
                GuideViewPager.this.page = page;
            }
        };
        this.x = 0.0f;
        this.y = 0.0f;
        this.context = context;
        this.initViews();
        this.setAdapter(this.vpAdapter = new ViewPagerAdapter());
        this.setOnPageChangeListener(this.mLinstener);
    }
    
    private void initViews() {
        this.setBackgroundColor(-16777216);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager)this.context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        this.sw = displayMetrics.widthPixels;
        this.sh = displayMetrics.heightPixels - Util.getStatusBarHeight(this.context);
        this.bmpList = new ArrayList<Bitmap>();
        if (Util.lan.equals("zh")) {
            this.addGuide(2130838081);
            this.addGuide(2130838083);
            this.addGuide(2130838085);
        }
        else if (Util.lan.equals("en")) {
            this.addGuide(2130838082);
            this.addGuide(2130838084);
            this.addGuide(2130838086);
        }
    }
    
    public void addGuide(int n) {
        FrameLayout frameLayout = new FrameLayout(this.context);
        ImageView imageView = new ImageView(this.context);
        frameLayout.addView((View)imageView, new FrameLayout.LayoutParams(-1, -1));
        Bitmap createBitmap = MakeBmp.CreateBitmap(this.context, n, this.sw, this.sh);
        imageView.setImageBitmap(createBitmap);
        this.bmpList.add(createBitmap);
        this.views.add((View)frameLayout);
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0: {
                this.x = motionEvent.getX();
                this.y = motionEvent.getY();
                break;
            }
            case 2: {
                this.subx = motionEvent.getX() - this.x;
                break;
            }
            case 1: {
                if (this.subx < -60.0f && this.page == 2 && this.mTouchLisenter != null) {
                    this.mTouchLisenter.onLastPage();
                    break;
                }
                break;
            }
        }
        return super.onTouchEvent(motionEvent);
    }
    
    public void recycle() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.e("", "recycle");
                    ArrayList<Object> list = new ArrayList<Object>();
                    for (Bitmap bitmap : GuideViewPager.this.bmpList) {
                        if (bitmap != null && !bitmap.isRecycled()) {
                            Log.e("", "recycle");
                            bitmap.recycle();
                            list.add(bitmap);
                        }
                    }
                    GuideViewPager.this.bmpList.removeAll(list);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }
    
    public void setPageTouchLisenter(OnPageTouchLisenter mTouchLisenter) {
        this.mTouchLisenter = mTouchLisenter;
    }
    
    public interface OnPageTouchLisenter
    {
        void onLastPage();
    }
    
    private class ViewPagerAdapter extends PagerAdapter
    {
        @Override
        public void destroyItem(View view, int n, Object o) {
            ((ViewPager)view).removeView(GuideViewPager.this.views.get(n));
        }
        
        @Override
        public int getCount() {
            if (GuideViewPager.this.views != null) {
                return GuideViewPager.this.views.size();
            }
            return 0;
        }
        
        @Override
        public Object instantiateItem(View view, int n) {
            ((ViewPager)view).addView((View)GuideViewPager.this.views.get(n), 0);
            return GuideViewPager.this.views.get(n);
        }
        
        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }
    }
}
