package com.gk.zhangzhongbao.ui.view;

import android.widget.*;
import android.content.*;
import android.util.*;
import android.view.*;

public class MyScrollView extends ScrollView
{
    public MyScrollView(final Context context) {
        super(context);
    }
    
    public MyScrollView(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public MyScrollView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        return false;
    }
}
