package com.gk.zhangzhongbao.ui.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.gk.wifictr.net.BroadcastHelper;
import com.gk.wifictr.net.command.station.Out_25H;
import com.gk.wifictr.net.data.SubDevice;
import com.gk.zhangzhongbao.ui.app.Util;
import java.util.Timer;
import java.util.TimerTask;

public class CurtainView
  extends RelativeLayout
  implements View.OnClickListener
{
  private Context context;
  private ImageView curtainImageView;
  private Button curtainOff;
  private Button curtainOn;
  private Button curtainStop;
  @SuppressLint({"HandlerLeak"})
  Handler handler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      curtainImageView.setImageResource(images[index]);
    }
  };
  private int[] images = { 2130838035, 2130838036, 2130838037, 2130838038, 2130838039, 2130838040, 2130838041 };
  private int index = 0;
  private boolean isLongClick = false;
  private boolean isSend = false;
  private TextView nameTextView;
  private boolean open = true;
  public SubDevice subDevice;
  private Timer timer;
  private TimerTask timerTask;
  
  public CurtainView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public CurtainView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  public CurtainView(Context paramContext, boolean paramBoolean1, boolean paramBoolean2)
  {
    super(paramContext);
    isSend = paramBoolean1;
    isLongClick = paramBoolean2;
    init(paramContext);
  }
  
  private void curtainStart() {
      if (this.timer == null && this.timerTask == null) {
          this.timer = new Timer();
          this.timerTask = new TimerTask(){

              @Override
              public void run() {
                  if (CurtainView.this.open) {
                      CurtainView curtainView = CurtainView.this;
                      index -= 1;
                      if (CurtainView.this.index < 0) {
                    	  CurtainView.this.index =  0;
                          CurtainView.this.cancle();
                          return;
                      }
                      CurtainView.this.handler.sendEmptyMessage(1);
                      return;
                  }
                  CurtainView curtainView = CurtainView.this;
                  index += 1;
                  if (CurtainView.this.index > 6) {
                	  CurtainView.this.index = 6;
                      CurtainView.this.cancle();
                      return;
                  }
                  CurtainView.this.handler.sendEmptyMessage(1);
              }
          };
          this.timer.schedule(this.timerTask, 0, 300);
      }
  }
  
  private void init(final Context context) {
      this.context = context;
      View view = LayoutInflater.from(context).inflate(2130903055, this, true);
      this.nameTextView = (TextView)view.findViewById(2131230736);
      this.curtainImageView = (ImageView)view.findViewById(2131230829);
      this.curtainOff = (Button)view.findViewById(2131230826);
      this.curtainOn = (Button)view.findViewById(2131230828);
      this.curtainStop = (Button)view.findViewById(2131230827);
      this.curtainOff.setOnClickListener((View.OnClickListener)this);
      this.curtainOn.setOnClickListener((View.OnClickListener)this);
      this.curtainStop.setOnClickListener((View.OnClickListener)this);
      this.curtainImageView.setImageResource(this.images[this.index]);
      if (this.isLongClick) {
          this.nameTextView.setOnLongClickListener(new View.OnLongClickListener(){

              public boolean onLongClick(View view) {
                  final EditText val$nameEditText = new EditText(context);
                  Util.alertDlg(context, "Modfy", null, view, new Runnable(){

                      @Override
                      public void run() {
                          String string = val$nameEditText.getText().toString();
                          if (!(string == null || string.equals(""))) {
                              nameTextView.setText(string);
                              subDevice.devName = string;
                              new BroadcastHelper().writeData(new Out_25H().make(subDevice));
                          }
                      }
                  });
                  return true;
              }

          });
      }
  }
  
  public void cancle()
  {
    if (timer != null)
    {
      timer.cancel();
      timer = null;
    }
    if (timerTask != null) {
      timerTask = null;
    }
  }
  
  public void onClick(View view) {
      Util.vibrator(this.context);
      Util.clickBeepSound(this.context);
      switch (view.getId()) {
          default: {
              return;
          }
          case 2131230826: {
              this.open = false;
              this.curtainStart();
              return;
          }
          case 2131230828: {
              this.open = true;
              this.curtainStart();
              return;
          }
          case 2131230827: 
      }
      this.cancle();
  }
  
  public void setDevice(SubDevice subDevice) {
      this.subDevice = subDevice;
      this.nameTextView.setText((CharSequence)subDevice.devName);
  }
  
  public void setIsSend(boolean paramBoolean)
  {
    isSend = paramBoolean;
  }
}


/* Location:              /Users/tariq/Downloads/output_jar.jar!/com/gk/zhangzhongbao/ui/view/CurtainView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */