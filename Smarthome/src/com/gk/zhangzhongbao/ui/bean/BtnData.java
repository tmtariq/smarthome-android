package com.gk.zhangzhongbao.ui.bean;

import android.widget.*;
import com.gk.wifictr.net.command.*;

public class BtnData
{
    public byte No;
    public Button button;
    public byte[] data;
    public String name;
    public byte[] name_byte;
    
    public byte[] make() {
        final byte[] arraycat = ByteUtil.arraycat(new byte[] { this.No }, this.name_byte);
        if (this.data != null) {
            return ByteUtil.arraycat(ByteUtil.arraycat(arraycat, ByteUtil.getLengthByte((short)this.data.length)), this.data);
        }
        return ByteUtil.arraycat(arraycat, new byte[2]);
    }
}
