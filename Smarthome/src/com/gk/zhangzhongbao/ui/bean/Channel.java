package com.gk.zhangzhongbao.ui.bean;

import android.graphics.*;
import android.os.*;
import java.util.*;
import com.gk.wifictr.net.*;
import com.gk.zhangzhongbao.ui.app.*;
import android.content.*;
import android.widget.*;

public class Channel
{
    private final String CH_FAVOR;
    private final String CH_NO;
    private String SN;
    private String adTime;
    private Bitmap bitmap;
    private int channelNum;
    private Handler handler;
    public int hour;
    private int id;
    private String imgResName;
    private boolean isFavorite;
    private String logoPath;
    public int min;
    private String name;
    private int resId;
    public int second;
    private TextView textView;
    private Timer timer;
    private TimerTask timerTask;
    
    public Channel() {
        this.name = "";
        this.logoPath = "";
        this.isFavorite = false;
        this.adTime = "00:00:00";
        this.CH_FAVOR = "ch_favor";
        this.CH_NO = "ch_no";
        this.SN = ConnectService.defWifi.SN;
    }
    
    private void setTime(final int n, final int n2, final int n3) {
        String s;
        if (n2 < 10) {
            s = "0" + n2;
        }
        else {
            s = new StringBuilder().append(n2).toString();
        }
        String s2;
        if (n3 < 10) {
            s2 = "0" + n3;
        }
        else {
            s2 = new StringBuilder().append(n3).toString();
        }
        this.adTime = String.valueOf(s) + ":" + s2;
    }
    
    public String getAdTime() {
        return this.adTime;
    }
    
    public Bitmap getBitmap() {
        return this.bitmap;
    }
    
    public int getBtnCode(final int n) {
        switch (n) {
            default: {
                return 34;
            }
            case 0: {
                return 34;
            }
            case 1: {
                return 24;
            }
            case 2: {
                return 25;
            }
            case 3: {
                return 26;
            }
            case 4: {
                return 27;
            }
            case 5: {
                return 28;
            }
            case 6: {
                return 29;
            }
            case 7: {
                return 30;
            }
            case 8: {
                return 31;
            }
            case 9: {
                return 32;
            }
        }
    }
    
    public int[] getChCodes() {
        final int[] array = new int[3];
        if (this.channelNum < 10) {
            array[1] = (array[0] = 0);
            array[2] = this.channelNum;
        }
        else {
            if (this.channelNum >= 10 && this.channelNum < 100) {
                array[0] = 0;
                array[1] = this.channelNum / 10;
                array[2] = this.channelNum % 10;
                return array;
            }
            if (this.channelNum >= 100) {
                array[0] = this.channelNum / 100;
                array[1] = this.channelNum / 10 % 10;
                array[2] = this.channelNum % 100 % 10;
                return array;
            }
        }
        return array;
    }
    
    public int getChNumValue() {
        return Util.getIntValue("ch_no" + this.id + this.SN);
    }
    
    public int getChannelNum() {
        return this.channelNum;
    }
    
    public boolean getFavorValue() {
        return Util.getBooleanValue("ch_favor" + this.id + this.SN, false);
    }
    
    public int getHour() {
        return this.hour;
    }
    
    public int getId() {
        return this.id;
    }
    
    public String getImgResName() {
        return this.imgResName;
    }
    
    public String getLogoPath() {
        return this.logoPath;
    }
    
    public int getMin() {
        return this.min;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int getResId() {
        return this.resId;
    }
    
    public int getSecond() {
        return this.second;
    }
    
    public boolean isFavorite() {
        return this.isFavorite;
    }
    
    public void saveChNum(final int n) {
        Util.saveValue("ch_no" + this.id + this.SN, n);
    }
    
    public void saveFavor(final boolean b) {
        Util.saveValue("ch_favor" + this.id + this.SN, Boolean.valueOf(b));
    }
    
    public Channel setAdTime(final String adTime) {
        this.adTime = adTime;
        return this;
    }
    
    public void setAdTime(final Context context, final int hour, final int min, final int second, final TextView textView, final Handler handler) {
        this.hour = hour;
        this.min = min;
        this.second = second;
        this.textView = textView;
        this.handler = handler;
        this.setTime(hour, min, second);
        if (this.timer == null && this.timerTask == null) {
            this.timer = new Timer();
            this.timerTask = new TimerTask() {
                @Override
                public void run() {
                    final Channel this$0 = Channel.this;
                    --this$0.second;
                    if (Channel.this.second < 0) {
                        Channel.this.second = 59;
                        final Channel this$ = Channel.this;
                        --this$.min;
                        if (Channel.this.min < 0) {
                            Channel.this.min = 0;
                        }
                    }
                    Channel.this.setTime(Channel.this.hour, Channel.this.min, Channel.this.second);
                    Channel.this.handler.post((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            Channel.this.textView.setText((CharSequence)Channel.this.adTime);
                            if (Channel.this.second <= 0 && Channel.this.min <= 0 && Channel.this.hour <= 0) {
                                Toast.makeText(context, (CharSequence)"times up", 0).show();
                                Channel.this.timerCancle();
                            }
                        }
                    });
                }
            };
            this.timer.schedule(this.timerTask, 0L, 1000L);
        }
    }
    
    public Channel setBitmap(final Bitmap bitmap) {
        this.bitmap = bitmap;
        return this;
    }
    
    public Channel setChannelNum(final int channelNum) {
        this.channelNum = channelNum;
        return this;
    }
    
    public Channel setFavorite(final boolean isFavorite) {
        this.isFavorite = isFavorite;
        return this;
    }
    
    public void setHour(final int hour) {
        this.hour = hour;
    }
    
    public Channel setId(final int id) {
        this.id = id;
        return this;
    }
    
    public Channel setImgResName(final String imgResName) {
        this.imgResName = imgResName;
        return this;
    }
    
    public Channel setLogoPath(final String logoPath) {
        this.logoPath = logoPath;
        return this;
    }
    
    public void setMin(final int min) {
        this.min = min;
    }
    
    public Channel setName(final String name) {
        this.name = name;
        return this;
    }
    
    public Channel setResId(final int resId) {
        this.resId = resId;
        return this;
    }
    
    public void setSecond(final int second) {
        this.second = second;
    }
    
    public void setTime(final TextView textView) {
        this.setTime(this.hour, this.min, this.second);
        this.textView = textView;
        if (this.textView != null) {
            this.textView.setText((CharSequence)this.adTime);
        }
    }
    
    public void timerCancle() {
        if (this.timer != null) {
            this.timer.cancel();
            this.timer = null;
        }
        if (this.timerTask != null) {
            this.timerTask.cancel();
            this.timerTask = null;
        }
    }
}
