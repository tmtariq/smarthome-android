package com.gk.zhangzhongbao.ui.bean;

import android.widget.*;
import com.gk.wifictr.net.*;
import com.gk.wifictr.net.command.station.*;
import com.gk.wifictr.net.data.*;

public class LightItem
{
    public static final int ONE_LIGHT = 1;
    public static final int THREE_LIGHT = 3;
    public int actionCode;
    private int devNo;
    private int devType;
    public int id;
    int[] imgsOff;
    int[] imgsOn;
    int[] indexs;
    private int lightCount;
    private boolean lightOn;
    public ImageView lightSwitch;
    public ImageView[] lights;
    public boolean longClickable;
    public boolean mainLightOn;
    private String name;
    private String snCode;
    public boolean[] state_one;
    public boolean[] status_three;
    private boolean temp_one;
    private boolean[] temp_three;
    
    public LightItem() {
        this.name = "";
        this.snCode = "";
        this.lightOn = true;
        this.state_one = new boolean[1];
        this.status_three = new boolean[3];
        this.temp_three = new boolean[3];
        this.lights = new ImageView[3];
        this.mainLightOn = true;
        this.indexs = new int[] { 0, 1, 2 };
        this.imgsOn = new int[] { 2130838147, 2130838149, 2130838151 };
        this.imgsOff = new int[] { 2130838146, 2130838148, 2130838150 };
        this.longClickable = true;
        this.actionCode = 0;
        this.temp_three[0] = true;
        this.temp_three[1] = true;
        this.temp_three[2] = true;
    }
    
    public LightItem(final int devNo, final String name, final String snCode, final int lightCount, final boolean lightOn) {
        this.name = "";
        this.snCode = "";
        this.lightOn = true;
        this.state_one = new boolean[1];
        this.status_three = new boolean[3];
        this.temp_three = new boolean[3];
        this.lights = new ImageView[3];
        this.mainLightOn = true;
        this.indexs = new int[] { 0, 1, 2 };
        this.imgsOn = new int[] { 2130838147, 2130838149, 2130838151 };
        this.imgsOff = new int[] { 2130838146, 2130838148, 2130838150 };
        this.longClickable = true;
        this.actionCode = 0;
        this.name = name;
        this.snCode = snCode;
        this.lightCount = lightCount;
        this.devNo = devNo;
        this.lightOn = lightOn;
        this.state_one[0] = lightOn;
    }
    
    public LightItem(final int devNo, final String name, final String snCode, final int lightCount, final boolean b, final boolean b2, final boolean b3) {
        this.name = "";
        this.snCode = "";
        this.lightOn = true;
        this.state_one = new boolean[1];
        this.status_three = new boolean[3];
        this.temp_three = new boolean[3];
        this.lights = new ImageView[3];
        this.mainLightOn = true;
        this.indexs = new int[] { 0, 1, 2 };
        this.imgsOn = new int[] { 2130838147, 2130838149, 2130838151 };
        this.imgsOff = new int[] { 2130838146, 2130838148, 2130838150 };
        this.longClickable = true;
        this.actionCode = 0;
        this.name = name;
        this.snCode = snCode;
        this.lightCount = lightCount;
        this.devNo = devNo;
        this.status_three[0] = b;
        this.status_three[1] = b2;
        this.status_three[2] = b3;
    }
    
    public boolean allLightOff() {
        if (this.lightCount == 1) {
            if (!this.state_one[0]) {
                return true;
            }
        }
        else if (!this.status_three[0] && !this.status_three[1] && !this.status_three[2]) {
            return true;
        }
        return false;
    }
    
    public int[] get3LightStatus() {
        final int[] array = new int[3];
        if (this.status_three[0]) {
            array[0] = 1;
        }
        else {
            array[0] = 0;
        }
        if (this.status_three[1]) {
            array[1] = 1;
        }
        else {
            array[1] = 0;
        }
        if (this.status_three[2]) {
            array[2] = 1;
            return array;
        }
        array[2] = 0;
        return array;
    }
    
    public int getCount() {
        return this.lightCount;
    }
    
    public int getDevNo() {
        return this.devNo;
    }
    
    public int getDevType() {
        return this.devType;
    }
    
    public int getLightStatus() {
        if (this.state_one[0]) {
            return 1;
        }
        return 0;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getSnCode() {
        return this.snCode;
    }
    
    public boolean isLightOn() {
        return this.lightOn;
    }
    
    public boolean ismainLightOn() {
        return this.mainLightOn;
    }
    
    public LightItem set3LightStatus(final boolean b, final boolean b2, final boolean b3) {
        this.status_three[0] = b;
        this.status_three[1] = b2;
        this.status_three[2] = b3;
        return this;
    }
    
    public void setAllLightOff() {
        int i = 0;
        if (this.lightCount == 1) {
            this.temp_one = this.lightOn;
            this.lightOn = false;
            this.state_one[0] = false;
            new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(this.getDevNo(), this.getName(), this.getSnCode(), (byte)16, this.state_one), this.state_one));
            return;
        }
        this.lightOn = false;
        if (this.lightSwitch != null) {
            this.lightSwitch.setImageResource(2130838157);
        }
        this.temp_three[0] = this.status_three[0];
        this.temp_three[1] = this.status_three[1];
        this.temp_three[2] = this.status_three[2];
        this.status_three[0] = false;
        this.status_three[1] = false;
        this.status_three[2] = false;
        for (int[] indexs = this.indexs; i < indexs.length; ++i) {
            final int n = indexs[i];
            this.lights[n].setImageResource(this.imgsOff[n]);
        }
        new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(this.getDevNo(), this.getName(), this.getSnCode(), (byte)16, this.status_three), this.status_three));
    }
    
    public void setAllLightOn() {
        int i = 0;
        if (this.lightCount == 1) {
            this.lightOn = this.temp_one;
            this.state_one[0] = this.temp_one;
            new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(this.getDevNo(), this.getName(), this.getSnCode(), (byte)16, this.state_one), this.state_one));
            return;
        }
        this.lightOn = true;
        if (this.lightSwitch != null) {
            this.lightSwitch.setImageResource(2130838158);
        }
        this.status_three[0] = this.temp_three[0];
        this.status_three[1] = this.temp_three[1];
        this.status_three[2] = this.temp_three[2];
        for (int[] indexs = this.indexs; i < indexs.length; ++i) {
            final int n = indexs[i];
            if (this.status_three[n]) {
                this.lights[n].setImageResource(this.imgsOn[n]);
            }
            else {
                this.lights[n].setImageResource(this.imgsOff[n]);
            }
        }
        new BroadcastHelper().writeData(new Out_27H().make(new SubDevice(this.getDevNo(), this.getName(), this.getSnCode(), (byte)16, this.status_three), this.status_three));
    }
    
    public LightItem setCount(final int lightCount) {
        this.lightCount = lightCount;
        return this;
    }
    
    public LightItem setDevNo(final int devNo) {
        this.devNo = devNo;
        return this;
    }
    
    public LightItem setDevType(final int devType) {
        this.devType = devType;
        return this;
    }
    
    public void setLight1(final int n) {
        if (n == 0) {
            this.status_three[0] = false;
            return;
        }
        this.status_three[0] = true;
    }
    
    public void setLight2(final int n) {
        if (n == 0) {
            this.status_three[1] = false;
            return;
        }
        this.status_three[1] = true;
    }
    
    public void setLight3(final int n) {
        if (n == 0) {
            this.status_three[2] = false;
            return;
        }
        this.status_three[2] = true;
    }
    
    public void setLightOn(final boolean lightOn) {
        this.lightOn = lightOn;
    }
    
    public LightItem setName(final String name) {
        this.name = name;
        return this;
    }
    
    public void setOneLight(final int n) {
        if (n == 0) {
            this.state_one[0] = false;
            return;
        }
        this.state_one[0] = true;
    }
    
    public LightItem setSnCode(final String snCode) {
        this.snCode = snCode;
        return this;
    }
    
    public void setmainLightOn(final boolean mainLightOn) {
        this.mainLightOn = mainLightOn;
    }
}
