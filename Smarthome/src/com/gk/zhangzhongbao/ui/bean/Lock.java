package com.gk.zhangzhongbao.ui.bean;

public class Lock
{
  private int divNum;
  private boolean lockOn;
  private String name;
  private String snCode;
  private int type;
  
  public int getDivNum()
  {
    return divNum;
  }
  
  public String getName()
  {
    return name;
  }
  
  public String getSnCode()
  {
    return snCode;
  }
  
  public int getType()
  {
    return type;
  }
  
  public boolean isLockOn()
  {
    return lockOn;
  }
  
  public Lock setDivNum(int paramInt)
  {
    divNum = paramInt;
    return this;
  }
  
  public Lock setLockOn(boolean paramBoolean)
  {
    lockOn = paramBoolean;
    return this;
  }
  
  public Lock setName(String paramString)
  {
    name = paramString;
    return this;
  }
  
  public Lock setSnCode(String paramString)
  {
    snCode = paramString;
    return this;
  }
  
  public void setType(int paramInt)
  {
    type = paramInt;
  }
}


/* Location:              /Users/tariq/Downloads/output_jar.jar!/com/gk/zhangzhongbao/ui/bean/Lock.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */