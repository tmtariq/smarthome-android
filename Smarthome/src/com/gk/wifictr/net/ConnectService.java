package com.gk.wifictr.net;

import com.gk.wifictr.net.command.WriteData;
import com.gk.wifictr.net.command.station.Out_33H;
import com.gk.wifictr.net.data.StationWifi;
import com.gk.wifictr.net.easylink.EasylinkConfig;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;


public class ConnectService extends Service
{
    public static int APP_TYPE = 0;
    public static String AP_SERVER_IP;
    public static String AP_SERVER_PORT;
    public static int CHANGE_AP_MODE = 6;
    public static boolean D = true;
    public static int EASY_LINK = 8;
    public static int GOT_SN = 9;
    public static int LAN_CONF_CHANGE = 1;
    public static String LAN_SERVER_IP;
    public static String LAN_SERVER_PORT;
    public static int LINK_STATE_CHANGE = 0;
    public static String Net_Send_Filter = "com.gk.wifictr.net";
    public static int READ_DATA = 4;
    public static int SEND_LOG = 7;
    public static int TYPE_SMART_HOME = 2;
    public static int TYPE_WIFI_BATH = 1;
    public static int TYPE_WIFI_CTR = 0;
    public static String UDP_READ_PORT;
    public static int UDP_SERACH = 3;
    public static String UDP_WRITE_IP;
    public static String UDP_WRITE_PORT;
    public static String UI_Send_Filter = "com.gk.wifictr.ui";
    public static int WAN_CONF_CHANGE = 2;
    public static String WAN_SERVER_IP;
    public static String WAN_SERVER_PORT;
    public static int WRITE_DATA = 5;
    private static Context applicationContext;
    public static StationWifi defWifi;
    public static int defWifinum;
    public static boolean getSnSuccess;
    public static ConnectService mConnectService;
    public static UdpThread mUdpThread;
    public static boolean useAP;
    SocketThread mApThread;
    public EasylinkConfig mEasylinkConfig;
    SocketThread mLanThread;
    NetHelper mNetHelper;
    public NetworkReceiver mNetworkReceiver;
    public OwnReceiver mOwnReceiver;
    SocketThread mWanThread;
    
    static {
        getSnSuccess = false;
        APP_TYPE = 0;
        UDP_WRITE_IP = "255.255.255.255";
        UDP_WRITE_PORT = "12306";
        UDP_READ_PORT = "12306";
        AP_SERVER_IP = "20.20.20.1";
        AP_SERVER_PORT = "8080";
        LAN_SERVER_IP = "192.168.1.113";
        LAN_SERVER_PORT = "8080";
        WAN_SERVER_IP = "112.124.48.89";
        WAN_SERVER_PORT = "10004";
        defWifinum = 1;
        useAP = false;
    }
    
    private void closeAP() {
        if (mApThread != null) {
            log("\u5e72\u6389\u90a3\u4e2aAP\u8fdb\u7a0b");
            mApThread.interrupt();
            mApThread.cancel();
            mApThread = null;
        }
    }
    
    private void closeLan() {
        if (mLanThread != null) {
            log("\u5e72\u6389\u90a3\u4e2aLan\u8fdb\u7a0b");
            mLanThread.interrupt();
            mLanThread.cancel();
            mLanThread = null;
        }
    }
    
    private void closeWan() {
        if (mWanThread != null) {
            log("\u5e72\u6389\u90a3\u4e2aWan\u8fdb\u7a0b");
            mWanThread.interrupt();
            mWanThread.cancel();
            mWanThread = null;
        }
    }
    
    public static Context getInstance() {
        return applicationContext;
    }
    
    private void log(String s) {
        Log.v("ConnectService", s);
    }
    
    private void setNetReceiver() {
        mNetworkReceiver = new NetworkReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver((BroadcastReceiver)mNetworkReceiver, intentFilter);
    }
    
    private void setOwnReceiver() {
        mOwnReceiver = new OwnReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.gk.wifictr.ui");
        LocalBroadcastManager.getInstance((Context)this).registerReceiver(mOwnReceiver, intentFilter);
    }
    
    private void startAp() {
        closeAP();
        mApThread = new SocketThread("mApThread", AP_SERVER_IP, AP_SERVER_PORT);
        mApThread.type = "AP";
        mApThread.AUTO_RELINK_MAX_TIME = 2;
        mApThread.start();
    }
    
    private void startLan() {
        closeLan();
        mLanThread = new SocketThread("mLanThread", LAN_SERVER_IP, LAN_SERVER_PORT);
        mLanThread.type = "LAN";
        mLanThread.start();
    }
    
    private void startWan() {
        closeWan();
        mWanThread = new SocketThread("mWanThread", WAN_SERVER_IP, WAN_SERVER_PORT);
        mWanThread.type = "WAN";
        mWanThread.start();
    }
    
    private void toast(String s) {
        Toast.makeText(getApplicationContext(), (CharSequence)s, 0).show();
    }
    
    public void changeAPmode(boolean useAP) {
        ConnectService.useAP = useAP;
        if (useAP) {
            startAp();
            closeLan();
            closeWan();
            return;
        }
        closeAP();
        startLan();
        startWan();
        startUdp(0);
    }
    
    public void changeDefWifiNum(int defWifinum) {
    	ConnectService.defWifinum = defWifinum;
        defWifi = new StationWifi(defWifinum);
        changeAPmode(useAP);
        write(new Out_33H().make());
    }
    
    public boolean isAP() {
        return mApThread != null && mApThread.isLink;
    }
    
    public boolean isLan() {
        return (mApThread != null && mApThread.isLink) || (mLanThread != null && mLanThread.isLink);
    }
    
    public boolean isLink() {
        return (mApThread != null && mApThread.isLink) || (mLanThread != null && mLanThread.isLink) || (mWanThread != null && mWanThread.isLink);
    }
    
    public IBinder onBind(Intent intent) {
        return null;
    }
    
    public void onCreate() {
        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        }
        mConnectService = this;
        applicationContext = getApplicationContext();
        defWifi = new StationWifi(defWifinum);
        mNetHelper = new NetHelper(getApplicationContext());
        changeAPmode(useAP);
        write(new Out_33H().make());
        setNetReceiver();
        setOwnReceiver();
        super.onCreate();
    }
    
    public void onDestroy() {
        if (mOwnReceiver != null) {
            LocalBroadcastManager.getInstance((Context)this).unregisterReceiver(mOwnReceiver);
        }
        if (mNetworkReceiver != null) {
            unregisterReceiver((BroadcastReceiver)mNetworkReceiver);
        }
        if (mLanThread != null) {
            mLanThread.cancel();
        }
        if (mWanThread != null) {
            mWanThread.cancel();
        }
        super.onDestroy();
    }
    
    public void startUdp(int n) {
        if (mUdpThread != null) {
            log("\u5e72\u6389\u90a3\u4e2aUdp\u8fdb\u7a0b");
            mUdpThread.interrupt();
            mUdpThread.cancel();
            mUdpThread = null;
        }
        (mUdpThread = new UdpThread((WifiManager)getApplication().getSystemService("wifi"), getApplicationContext(), n)).start();
    }
    
    public boolean write(WriteData writeData) {
        if (mApThread != null && writeData.AP_CAN_SEND) {
            if (mApThread.write(writeData.LanBody)) {
                log("AP\u53d1\u9001\u6210\u529f");
                return true;
            }
            log("AP\u53d1\u9001\u5931\u8d25");
        }
        if (mLanThread != null && writeData.LAN_CAN_SEND) {
            if (mLanThread.write(writeData.LanBody)) {
                log("LAN\u53d1\u9001\u6210\u529f");
                return true;
            }
            log("LAN\u53d1\u9001\u5931\u8d25");
        }
        if (mWanThread != null && writeData.WAN_CAN_SEND) {
            if (mWanThread.write(writeData.WanBody)) {
                log("WAN\u53d1\u9001\u6210\u529f");
                return true;
            }
            log("WAN\u53d1\u9001\u5931\u8d25");
        }
        log("SEND->" + writeData.AP_CAN_SEND + writeData.LAN_CAN_SEND + writeData.WAN_CAN_SEND);
        return false;
    }
    
    public boolean write(WriteData writeData, int n) {
        if (mApThread != null && mApThread.isLink() && writeData.AP_CAN_SEND && mApThread.write(writeData.LanBody)) {
            log("\u8d70AP");
            return true;
        }
        if (mLanThread != null && mLanThread.isLink() && writeData.LAN_CAN_SEND && mLanThread.write(writeData.LanBody)) {
            log("\u8d70LAN");
            return true;
        }
        if (mWanThread != null && mWanThread.isLink() && writeData.WAN_CAN_SEND && mWanThread.write(writeData.WanBody)) {
            log("\u8d70WAN");
            return true;
        }
        toast("\u672a\u8fde\u63a5\u8bbe\u5907");
        log("SEND->" + writeData.AP_CAN_SEND + writeData.LAN_CAN_SEND + writeData.WAN_CAN_SEND);
        log("Link->" + mApThread.isLink() + mLanThread.isLink() + mWanThread.isLink());
        return false;
    }
    
    class NetworkReceiver extends BroadcastReceiver
    {
        public void onReceive(Context context, Intent intent) {
            if (APP_TYPE == 2 || APP_TYPE == 1) {
                if (mNetHelper.getWifiSSID().indexOf("XinYangAP_") != -1) {
                    changeAPmode(true);
                }
                else {
                    changeAPmode(false);
                }
            }
            if (mNetHelper.isWifiConnected() && !useAP && !mLanThread.isLink) {
                startUdp(0);
                write(new Out_33H().make());
            }
        }
    }
    
    private class OwnReceiver extends BroadcastReceiver
    {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.gk.wifictr.ui")) {
                switch (intent.getExtras().getInt("cmd")) {
                    case 1: {
                        if (!mLanThread.isLink) {
                            startLan();
                            startUdp(0);
                            write(new Out_33H().make());
                            return;
                        }
                        log("\u6536\u5230\u8fde\u63a5\u53d8\u5316\u8bf7\u6c42\nmLanThread.isLink=" + mLanThread.isLink + "\n\u4e0d\u4e88\u52a8\u4f5c");
                        break;
                    }
                    case 2: {
                        startWan();
                        break;
                    }
                    case 3: {
                        startUdp(intent.getIntExtra("UDP_TYPE", 0));
                        break;
                    }
                    case 5: {
                        log("WRITE_DATA");
                        write((WriteData)intent.getParcelableExtra("data"));
                        break;
                    }
                    case 6: {
                        log("CHANGE_AP_MODE");
                        changeAPmode(intent.getBooleanExtra("mode", false));
                        break;
                    }
                    case 8: {
                        log("EASY_LINK");
                        String stringExtra = intent.getStringExtra("status");
                        if (stringExtra.equals("START")) {
                            log("EASY_LINK START");
                            String stringExtra2 = intent.getStringExtra("SSID");
                            String stringExtra3 = intent.getStringExtra("PASS");
                            try {
                                (mEasylinkConfig = new EasylinkConfig()).send(stringExtra2, stringExtra3);
                                startUdp(62);
                                return;
                            }
                            catch (Exception ex) {
                                ex.printStackTrace();
                                return;
                            }
                        }
                        if (!stringExtra.equals("STOP")) {
                            break;
                        }
                        log("EASY_LINK STOP");
                        if (mEasylinkConfig != null) {
                            mEasylinkConfig.stopSend();
                            return;
                        }
                        break;
                    }
                }
            }
        }
    }
}
