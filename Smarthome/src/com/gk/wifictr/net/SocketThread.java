package com.gk.wifictr.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;

import com.gk.wifictr.net.command.ByteUtil;
import com.gk.wifictr.net.command.ReadData;
import com.gk.wifictr.net.command.station.In_43H;
import com.gk.wifictr.net.command.station.Out_33H;

import android.util.Log;


public class SocketThread extends Thread
{
    public boolean AUTO_RELINK;
    public int[] AUTO_RELINK_DELAY;
    public int AUTO_RELINK_MAX_TIME;
    public int AUTO_RELINK_TIME;
    public boolean isLink;
    private BroadcastHelper mBroadcastHelper;
    private DataInputStream reader;
    private Socket socketClient;
    public String targetAddress;
    public String targetPort;
    public String type;
    private DataOutputStream writer;
    
    public SocketThread(String targetAddress, String targetPort) {
        this.AUTO_RELINK = true;
        this.AUTO_RELINK_DELAY = new int[] { 0, 500, 1000, 2000, 3000, 5000, 7000, 10000 };
        this.AUTO_RELINK_TIME = 0;
        this.AUTO_RELINK_MAX_TIME = Integer.MAX_VALUE;
        this.type = "LAN";
        this.isLink = false;
        this.setName("SocketThread");
        this.targetAddress = targetAddress;
        this.targetPort = targetPort;
        this.mBroadcastHelper = new BroadcastHelper();
    }
    
    public SocketThread(String name, String targetAddress, String targetPort) {
        this.AUTO_RELINK = true;
        this.AUTO_RELINK_DELAY = new int[] { 0, 500, 1000, 2000, 3000, 5000, 7000, 10000 };
        this.AUTO_RELINK_TIME = 0;
        this.AUTO_RELINK_MAX_TIME = Integer.MAX_VALUE;
        this.type = "LAN";
        this.isLink = false;
        this.setName(name);
        this.targetAddress = targetAddress;
        this.targetPort = targetPort;
        this.mBroadcastHelper = new BroadcastHelper();
    }
    
    public void cancel() {
        this.log("cancel()");
        this.disconnect();
    }
    
    public void connect() {
        this.log("connect()");
        //while (true) {
            try {
                if (this.socketClient != null) {
                    this.disconnect();
                }
                (this.socketClient = new Socket()).connect(new InetSocketAddress(this.targetAddress, Integer.parseInt(this.targetPort)), 5000);
                this.socketClient.setKeepAlive(true);
                this.socketClient.setSoLinger(true, 0);
                this.socketClient.setSoTimeout(30000);
                this.socketClient.setReceiveBufferSize(1024);
                this.reader = new DataInputStream(this.socketClient.getInputStream());
                this.writer = new DataOutputStream(this.socketClient.getOutputStream());
                this.AUTO_RELINK_TIME = 0;
                this.setLink();
                if (this.type.equals("WAN")) {
                    this.write(new Out_33H().make().WanBody);
                }
            } catch (Exception ex) {
                //continue;
            }
            //break;
        //}
    }
    
    public void disconnect() {
        log("disconnect()");
        try {
            if (socketClient != null) {
                if (!socketClient.isInputShutdown()) {
                    socketClient.shutdownInput();
                }
                if (!socketClient.isOutputShutdown()) {
                    socketClient.shutdownOutput();
                }
            }
            if (!(socketClient == null || socketClient.isClosed())) {
                socketClient.close();
            }
            if (writer != null) {
                writer.close();
            }
            if (reader != null) {
                reader.close();
            }
        }
        catch (Exception var1_1) {
            var1_1.printStackTrace();
        }
        finally {
            writer = null;
            reader = null;
            socketClient = null;
        }
        //do {
            try {
                SocketThread.sleep(300);
            }
            catch (InterruptedException var1_3) {}
            setLink();
        //} while (true);
        
    }
    
    public int getAUTO_RELINK_DELAY() {
        if (this.AUTO_RELINK_TIME < 0) {
            this.AUTO_RELINK_TIME = 0;
        }
        if (this.AUTO_RELINK_TIME >= this.AUTO_RELINK_DELAY.length) {
            return this.AUTO_RELINK_DELAY[this.AUTO_RELINK_DELAY.length - 1];
        }
        return this.AUTO_RELINK_DELAY[this.AUTO_RELINK_TIME];
    }
    
    public boolean isLink() {
        return this.isLink;
    }
    
    public void log(String s) {
        Log.v(this.getName(), s);
    }
    
    @Override
    public void run() {
        this.log("run()");
        this.connect();
        while (!this.isInterrupted()) {
            try {
                Thread.sleep(this.getAUTO_RELINK_DELAY());
                if (this.isLink) {
                    byte[] array = new byte[1024];
                    try {
                        int read = this.reader.read(array);
                        if (read < 10) {
                            continue;
                        }
                        byte[] copyOfRange = ByteUtil.copyOfRange(array, 0, read);
                        this.log(ByteUtil.byte2hex(copyOfRange));
                        ReadData readData = new ReadData(copyOfRange);
                        if (readData.goodData != 0) {
                            //goto Label_0145;
                        	continue;
                        }
                        if (readData.type == 67) {
                            new In_43H(readData.body);
                        }
                        new BroadcastHelper().readData(readData);
                    }
                    catch (SocketException ex) {
                        this.setLink();
                    }
                    catch (Exception ex2) {
                        this.setLink();
                    }
                    continue;
                }
            }
            catch (InterruptedException ex3) {}
            if (!this.AUTO_RELINK) {
                this.log("\u867e\u7c73");
                break;
            }
            if (this.AUTO_RELINK_TIME >= this.AUTO_RELINK_MAX_TIME) {
                this.log("\u8d85\u8fc7\u91cd\u8fde\u6b21\u6570\u9650\u5236");
                break;
            }
            this.log("\u91cd\u8fde");
            this.connect();
            ++this.AUTO_RELINK_TIME;
        }
        super.run();
        this.disconnect();
    }
    
    public boolean setLink() {
    	boolean isLink;
        while (true) {
            if (this.socketClient == null) {
                isLink = false;
                break;
            }
            if (!this.socketClient.isConnected()) {
                isLink = false;
                break;
            }
            Label_0126: {
                if (this.writer == null) {
                    break Label_0126;
                	//break;
                }
                try {
                    if (this.type.equals("WAN")) {
                        this.socketClient.sendUrgentData(255);
                        this.writer.flush();
                    }
                    else {
                        this.writer.write(255);
                        this.writer.flush();
                    }
                    isLink = true;
                    this.isLink = isLink;
                    this.log(String.valueOf(this.getName()) + "'s link state -> " + isLink);
                    this.mBroadcastHelper.linkStateChange();
                    return isLink;
                }
                catch (IOException ex) {
                    isLink = false;
                    continue;
                }
            }
            isLink = false;
            //continue;
        }
        return isLink;
    }
    
    @Override
    public void start() {
        this.log("start()");
        super.start();
    }
    
    public boolean write(byte[] array) {
        this.log("write()");
        if (this.isLink()) {
            try {
                this.writer.write(array);
                this.writer.flush();
                this.log("write()");
                return true;
            }
            catch (IOException ex) {
                this.log("\u53d1\u9001\u5931\u8d25");
                this.setLink();
                return false;
            }
        }
        this.setLink();
        return false;
    }
}
