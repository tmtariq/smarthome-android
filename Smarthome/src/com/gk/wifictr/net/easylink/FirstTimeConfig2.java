package com.gk.wifictr.net.easylink;

import java.util.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import android.annotation.*;
import com.gk.wifictr.net.*;
import android.util.*;
import java.net.*;

public class FirstTimeConfig2
{
    Thread ackWaitThread;
    private byte[] encryptionKey;
    private String ip;
    private String key;
    int listenPort;
    private MulticastSocket listenSocket;
    int localPort;
    Thread mDnsThread;
    private FirstTimeConfigListener m_listener;
    Thread sendingThread;
    InetSocketAddress sockAddr;
    private String ssid;
    private boolean stopSending;
    private String syncHString;
    private String syncLString;
    int waitForAckSocketTimeout;
    
    public FirstTimeConfig2(final FirstTimeConfigListener firstTimeConfigListener, final String s, final byte[] array, final String s2, final String s3) throws Exception {
        this(firstTimeConfigListener, s, array, s2, s3, "EMW3161");
    }
    
    public FirstTimeConfig2(final FirstTimeConfigListener firstTimeConfigListener, final String s, final byte[] array, final String s2, final String s3, final String s4) throws Exception {
        this(firstTimeConfigListener, s, array, s2, s3, s4, 5353);
    }
    
    public FirstTimeConfig2(final FirstTimeConfigListener firstTimeConfigListener, final String s, final byte[] array, final String s2, final String s3, final String s4, final int n) throws Exception {
        this(firstTimeConfigListener, s, array, s2, s3, s4, n, 15000);
    }
    
    public FirstTimeConfig2(final FirstTimeConfigListener firstTimeConfigListener, final String s, final byte[] array, final String s2, final String s3, final String s4, final int n, final int n2) throws Exception {
        this(firstTimeConfigListener, s, array, s2, s3, s4, n, n2, 300000);
    }
    
    public FirstTimeConfig2(final FirstTimeConfigListener firstTimeConfigListener, final String s, final byte[] array, final String s2, final String s3, final String s4, final int n, final int n2, final int n3) throws Exception {
        this(firstTimeConfigListener, s, array, s2, s3, s4, n, n2, n3, 4, 10, "abc", "abcdefghijklmnopqrstuvw");
    }
    
    public FirstTimeConfig2(final FirstTimeConfigListener listener, final String key, final byte[] encryptionKey, final String ip, final String ssid, final String s, final int n, final int n2, final int waitForAckSocketTimeout, final int n3, final int n4, final String syncLString, final String syncHString) throws Exception {
        this.m_listener = null;
        this.m_listener = listener;
        if (encryptionKey != null && encryptionKey.length != 0 && encryptionKey.length != 16) {
            throw new Exception("Encryption key must have 16 characters!");
        }
        if (key.length() > 32) {
            throw new Exception("Password is too long! Maximum length is 32 characters.");
        }
        if (ssid.length() > 32) {
            throw new Exception("Network name (SSID) is too long! Maximum length is 32 characters.");
        }
        this.stopSending = true;
        this.listenSocket = null;
        this.ip = ip;
        this.ssid = ssid;
        this.key = key;
        this.syncLString = syncLString;
        this.syncHString = syncHString;
        this.encryptionKey = encryptionKey;
        this.createBroadcastUDPSocket(n);
        this.localPort = new Random().nextInt(65535);
        this.listenPort = 5353;
        this.waitForAckSocketTimeout = waitForAckSocketTimeout;
        this.sockAddr = new InetSocketAddress(this.ip, this.localPort);
        final byte[] array = new byte[this.key.length()];
        System.arraycopy(this.key.getBytes(), 0, array, 0, this.key.length());
        new FtcEncode(this.ssid, this.encryptData(array));
    }
    
    private void createBroadcastUDPSocket(final int n) throws Exception {
        final InetSocketAddress inetSocketAddress = new InetSocketAddress((InetAddress)null, n);
        (this.listenSocket = new MulticastSocket((SocketAddress)null)).setReuseAddress(true);
        this.listenSocket.bind(inetSocketAddress);
        this.listenSocket.setTimeToLive(255);
        this.listenSocket.joinGroup(InetAddress.getByName("224.0.0.251"));
        this.listenSocket.setBroadcast(true);
    }
    
    @SuppressLint({ "TrulyRandom" })
    private byte[] encryptData(final byte[] array) throws Exception {
        if (this.encryptionKey != null && this.encryptionKey.length != 0) {
            final byte[] array2 = new byte[32];
            final byte[] array3 = new byte[16];
            for (int i = 0; i < 16; ++i) {
                if (i < this.encryptionKey.length) {
                    array3[i] = this.encryptionKey[i];
                }
                else {
                    array3[i] = 0;
                }
            }
            int n = 0;
            if (array.length < 32) {
                array2[0] = (byte)array.length;
                n = 0 + 1;
            }
            System.arraycopy(array, 0, array2, n, array.length);
            for (int j = n + array.length; j < 32; ++j) {
                array2[j] = 0;
            }
            final Cipher instance = Cipher.getInstance("AES/ECB/NoPadding");
            instance.init(1, new SecretKeySpec(array3, "AES"));
            return instance.doFinal(array2);
        }
        return array;
    }
    
    private void send() throws Exception {
        while (!this.stopSending) {
            this.sendSync();
            Thread.sleep(50L);
        }
    }
    
    private void sendData(final DatagramPacket datagramPacket) throws Exception {
        final MulticastSocket multicastSocket = new MulticastSocket(10000);
        multicastSocket.joinGroup(InetAddress.getByName(this.ip));
        multicastSocket.send(datagramPacket);
        multicastSocket.close();
    }
    
    private void sendSync() throws SocketException, Exception {
        this.syncLString.getBytes();
        final byte[] bytes = this.syncHString.getBytes();
        final byte[] array = new byte[this.key.length()];
        final byte[] array2 = new byte[this.ssid.getBytes("UTF-8").length];
        final String ip = new NetHelper().getIP();
        Log.d("qqaa", ip);
        final int length = ip.getBytes().length;
        final byte[] array3 = new byte[length];
        System.arraycopy(ip.getBytes(), 0, array3, 0, length);
        System.arraycopy(this.key.getBytes(), 0, array, 0, this.key.length());
        System.arraycopy(this.ssid.getBytes("UTF-8"), 0, array2, 0, this.ssid.getBytes("UTF-8").length);
        final byte[] byteMerger = Helper.byteMerger(new byte[] { (byte)array2.length, (byte)array.length }, Helper.byteMerger(array2, array));
        this.ip = "239.118.0.0";
        for (int i = 0; i < 5; ++i) {
            this.sockAddr = new InetSocketAddress(InetAddress.getByName(this.ip), 10000);
            this.sendData(new DatagramPacket(bytes, 20, this.sockAddr));
        }
        int n;
        if ((n = length) == 0) {
            n = length + 1;
        }
        byte[] array4;
        if (byteMerger.length % 2 == 0) {
            if (array3.length == 0) {
                array4 = Helper.byteMerger(byteMerger, new byte[] { (byte)n, 0, 0 });
            }
            else {
                array4 = Helper.byteMerger(byteMerger, new byte[] { (byte)n, 0 });
            }
        }
        else {
            array4 = Helper.byteMerger(byteMerger, new byte[] { 0, (byte)n, 0, 0 });
        }
        final byte[] byteMerger2 = Helper.byteMerger(array4, array3);
        for (int j = 0; j < byteMerger2.length; j += 2) {
            if (j + 1 < byteMerger2.length) {
                this.ip = "239.126." + (byteMerger2[j] & 0xFF) + "." + (byteMerger2[j + 1] & 0xFF);
            }
            else {
                this.ip = "239.126." + (byteMerger2[j] & 0xFF) + ".0";
            }
            this.sockAddr = new InetSocketAddress(InetAddress.getByName(this.ip), 10000);
            this.sendData(new DatagramPacket(new byte[j / 2 + 20], j / 2 + 20, this.sockAddr));
        }
    }
    
    public void stopTransmitting() throws Exception {
        this.listenSocket.close();
        this.stopSending = true;
        if (Thread.currentThread() != this.sendingThread) {
            this.sendingThread.join();
        }
    }
    
    public void transmitSettings() throws Exception {
        this.stopSending = false;
        (this.sendingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FirstTimeConfig2.this.send();
                }
                catch (Exception ex) {
                    new NotifyThread(FirstTimeConfig2.this.m_listener, ex);
                }
            }
        })).start();
    }
    
    private class NotifyThread implements Runnable
    {
        public NotifyThread(final FirstTimeConfigListener firstTimeConfigListener, final Exception ex) {
        }
        
        @Override
        public void run() {
        }
    }
}
