package com.gk.wifictr.net.easylink;

public interface FirstTimeConfigListener
{
    void onFirstTimeConfigEvent(FtcEvent p0, Exception p1);
    
    public enum FtcEvent
    {
    	FTC_ERROR, 
        FTC_SUCCESS, 
        FTC_TIMEOUT
    	
        //FTC_ERROR("FTC_ERROR", 1), 
        //FTC_SUCCESS("FTC_SUCCESS", 0), 
        //FTC_TIMEOUT("FTC_TIMEOUT", 2);
    }
}
