package com.gk.wifictr.net.easylink;

import com.gk.wifictr.net.*;
import android.util.*;

public class EasylinkConfig implements FirstTimeConfigListener
{
    private FirstTimeConfig2 firstConfig2;
    private boolean inSend;
    private NetHelper mNetHelper;
    
    public EasylinkConfig() {
        this.firstConfig2 = null;
        this.inSend = false;
    }
    
    @Override
    public void onFirstTimeConfigEvent(final FtcEvent ftcEvent, final Exception ex) {
    }
    
    public void send(final String s, final String s2) throws Exception {
        this.sendPacketData2(s, s2);
        this.inSend = true;
    }
    
    public void sendPacketData2(final String s, final String s2) throws Exception {
        this.mNetHelper = new NetHelper();
        final String ip = new NetHelper().getIP();
        Log.d("qq", ip);
        (this.firstConfig2 = new FirstTimeConfig2(this, s2, null, this.mNetHelper.getGatewayIP(), s, ip)).transmitSettings();
    }
    
    public void stopSend() {
        if (this.inSend) {
            if (this.firstConfig2 == null) {
                return;
            }
            try {
                this.firstConfig2.stopTransmitting();
                return;
            }
            catch (Exception ex) {
                ex.printStackTrace();
                Log.e("EasyLink", "Stop EasyLink failed");
                return;
            }
        }
        Log.e("EasyLink", "now no send");
    }
}
