package com.gk.wifictr.net.easylink;

public class EasyLinkConstants
{
    public static final int DLG_CONNECTION_FAILURE = 7;
    public static final int DLG_CONNECTION_SUCCESS = 6;
    public static final int DLG_CONNECTION_TIMEOUT = 8;
    public static final int DLG_GATEWAY_IP_INVALID = 4;
    public static final int DLG_KEY_INVALID = 5;
    public static final int DLG_NO_WIFI_AVAILABLE = 1;
    public static final int DLG_PASSWORD_INVALID = 3;
    public static final int DLG_SSID_INVALID = 2;
    public static final int SPLASH_DELAY = 1500;
}
