package com.gk.wifictr.net.easylink;

import java.util.*;

public class Ftc20
{
    private ArrayList<Integer> mData;
    private byte[] mKey;
    private String mSsid;
    
    private void constructKey() throws Exception {
        this.mData.add(this.mKey.length + 1 + 27);
        this.encodeKeyString(this.mKey);
    }
    
    private void constructSsid() throws Exception {
        this.mData.add(this.mSsid.length() + 1 + 27);
        this.encodeSsidString(this.mSsid);
    }
    
    private byte[] convertStringToBytes(final String s) throws Exception {
        return s.getBytes();
    }
    
    private void encodeKeyString(final byte[] array) throws Exception {
        byte b = 0;
        byte b2 = 0;
        for (int i = 0; i < array.length; ++i) {
            final int intToUint8 = this.intToUint8(array[i]);
            final int n = intToUint8 & 0xF;
            final int n2 = intToUint8 >> 4;
            final ArrayList<Integer> mData = this.mData;
            final byte b3 = (byte)(b2 + 1);
            mData.add(((b ^ b2) << 4 | n2) + 593);
            this.mData.add((((byte)n2 ^ b3) << 4 | n) + 593);
            b = (byte)n;
            b2 = (byte)(b3 + 1 & 0xF);
        }
    }
    
    private void encodeSsidString(final String s) throws Exception {
        byte b = 0;
        byte b2 = 0;
        final byte[] array = new byte[s.length()];
        final byte[] convertStringToBytes = this.convertStringToBytes(s);
        for (int i = 0; i < s.length(); ++i) {
            final byte b3 = convertStringToBytes[i];
            final byte b4 = (byte)(b3 & 0xF);
            final int n = b3 >> 4;
            final ArrayList<Integer> mData = this.mData;
            final byte b5 = (byte)(b2 + 1);
            mData.add(((b ^ b2) << 4 | n) + 593);
            this.mData.add((((byte)n ^ b5) << 4 | b4) + 593);
            b = b4;
            b2 = (byte)(b5 + 1 & 0xF);
        }
    }
    
    private int intToUint8(final int n) throws Exception {
        return n & 0xFF;
    }
    
    public void encodePackets() throws Exception {
        (this.mData = new ArrayList<Integer>()).add(1399);
        this.constructSsid();
        this.mData.add(1459);
        this.constructKey();
    }
    
    public ArrayList<Integer> getmData() throws Exception {
        return this.mData;
    }
    
    public byte[] getmKey() throws Exception {
        return this.mKey;
    }
    
    public String getmSsid() throws Exception {
        return this.mSsid;
    }
    
    public void setmKey(final byte[] mKey) throws Exception {
        this.mKey = mKey;
    }
    
    public void setmSsid(final String mSsid) throws Exception {
        this.mSsid = mSsid;
    }
}
