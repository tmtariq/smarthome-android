package com.gk.wifictr.net.easylink;

import android.content.*;
import android.app.*;

public class EasyLinkUtils
{
    public static boolean isScreenXLarge(final Context context) {
        return (context.getResources().getConfiguration().screenLayout & 0xF) >= 3;
    }
    
    public static void setProtraitOrientationEnabled(final Activity activity) {
        if (!isScreenXLarge((Context)activity)) {
            activity.setRequestedOrientation(1);
        }
    }
}
