package com.gk.wifictr.net.easylink;

import android.content.*;
import android.net.wifi.*;
import android.os.*;
import android.net.*;

public class EasyLinkWifiManager
{
    private static final int BUILD_VERSION_JELLYBEAN = 17;
    private Context mContext;
    private WifiInfo mWifiInfo;
    private WifiManager mWifiManager;
    
    public EasyLinkWifiManager(final Context mContext) {
        this.mWifiManager = null;
        this.mWifiInfo = null;
        this.mContext = null;
        this.mContext = mContext;
        this.mWifiManager = (WifiManager)mContext.getSystemService("wifi");
        this.mWifiInfo = this.mWifiManager.getConnectionInfo();
    }
    
    public static String removeSSIDQuotes(final String s) {
        String substring = s;
        if (Build.VERSION.SDK_INT >= 17) {
            substring = s;
            if (s.startsWith("\"")) {
                substring = s;
                if (s.endsWith("\"")) {
                    substring = s.substring(1, s.length() - 1);
                }
            }
        }
        return substring;
    }
    
    public String getBaseSSID() {
        return this.mWifiInfo.getBSSID();
    }
    
    public String getCurrentIpAddressConnected() {
        final int ipAddress = this.mWifiInfo.getIpAddress();
        return String.format("%d.%d.%d.%d", ipAddress & 0xFF, ipAddress >> 8 & 0xFF, ipAddress >> 16 & 0xFF, ipAddress >> 24 & 0xFF).toString();
    }
    
    public String getCurrentSSID() {
        return removeSSIDQuotes(this.mWifiInfo.getSSID());
    }
    
    public String getGatewayIpAddress() {
        final int gateway = this.mWifiManager.getDhcpInfo().gateway;
        return String.format("%d.%d.%d.%d", gateway & 0xFF, gateway >> 8 & 0xFF, gateway >> 16 & 0xFF, gateway >> 24 & 0xFF).toString();
    }
    
    public boolean isWifiConnected() {
        return ((ConnectivityManager)this.mContext.getSystemService("connectivity")).getNetworkInfo(1).isConnected();
    }
}
