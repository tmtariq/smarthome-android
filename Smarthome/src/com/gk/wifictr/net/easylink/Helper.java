package com.gk.wifictr.net.easylink;

public class Helper
{
    public static int Convert2bytesHexaFormatToInt(final byte[] array) {
        int n;
        if (array[1] <= -1) {
            n = 0 + (array[1] + 256);
        }
        else {
            n = 0 + array[1];
        }
        if (array[0] <= -1) {
            return n + (array[0] * 256 + 256);
        }
        return n + array[0] * 256;
    }
    
    public static String ConvertHexByteArrayToString(final byte[] array) {
        String s = "";
        for (int i = 0; i < array.length; ++i) {
            if (array[i] < 0) {
                s = String.valueOf(s) + Integer.toString(array[i] + 256, 16) + " ";
            }
            else if (array[i] <= 15) {
                s = String.valueOf(s) + "0" + Integer.toString(array[i], 16) + " ";
            }
            else {
                s = String.valueOf(s) + Integer.toString(array[i], 16) + " ";
            }
        }
        return s;
    }
    
    public static String ConvertHexByteToString(final byte b) {
        if (b < 0) {
            return String.valueOf("") + Integer.toString(b + 256, 16) + " ";
        }
        if (b <= 15) {
            return String.valueOf("") + "0" + Integer.toString(b, 16) + " ";
        }
        return String.valueOf("") + Integer.toString(b, 16) + " ";
    }
    
    public static byte[] ConvertIntTo2bytesHexaFormat(final int n) {
        return new byte[] { (byte)(n / 256), (byte)(n - n / 256 * 256) };
    }
    
    public static String ConvertIntToHexFormatString(final int n) {
        return ConvertHexByteArrayToString(ConvertIntTo2bytesHexaFormat(n)).replaceAll(" ", "");
    }
    
    public static byte ConvertStringToHexByte(final String s) {
        final char[] charArray = s.toUpperCase().toCharArray();
        int n = 0;
        for (int i = 0; i <= 1; ++i) {
            int n2;
            int n3;
            for (int j = 0; j <= 15; j = n2 + 1, n = n3) {
                n2 = j;
                n3 = n;
                if (charArray[i] == (new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' })[j]) {
                    if (i == 1) {
                        n3 = n + j;
                        n2 = 15;
                    }
                    else {
                        n2 = j;
                        n3 = n;
                        if (i == 0) {
                            n3 = n + j * 16;
                            n2 = 15;
                        }
                    }
                }
            }
        }
        return (byte)n;
    }
    
    public static byte[] ConvertStringToHexBytes(final String s) {
        final char[] charArray = s.toCharArray();
        final char[] array2;
        final char[] array = array2 = new char[16];
        array2[0] = '0';
        array2[1] = '1';
        array2[2] = '2';
        array2[3] = '3';
        array2[4] = '4';
        array2[5] = '5';
        array2[6] = '6';
        array2[7] = '7';
        array2[8] = '8';
        array2[9] = '9';
        array2[10] = 'A';
        array2[11] = 'B';
        array2[12] = 'C';
        array2[13] = 'D';
        array2[14] = 'E';
        array2[15] = 'F';
        int n = 0;
        for (int i = 0; i <= 1; ++i) {
            int n2;
            int n3;
            for (int j = 0; j <= 15; j = n2 + 1, n = n3) {
                n2 = j;
                n3 = n;
                if (charArray[i] == array[j]) {
                    if (i == 1) {
                        n3 = n + j;
                        n2 = 15;
                    }
                    else {
                        n2 = j;
                        n3 = n;
                        if (i == 0) {
                            n3 = n + j * 16;
                            n2 = 15;
                        }
                    }
                }
            }
        }
        final byte b = (byte)n;
        int n4 = 0;
        for (int k = 2; k <= 3; ++k) {
            int n5;
            int n6;
            for (int l = 0; l <= 15; l = n5 + 1, n4 = n6) {
                n5 = l;
                n6 = n4;
                if (charArray[k] == array[l]) {
                    if (k == 3) {
                        n6 = n4 + l;
                        n5 = 15;
                    }
                    else {
                        n5 = l;
                        n6 = n4;
                        if (k == 2) {
                            n6 = n4 + l * 16;
                            n5 = 15;
                        }
                    }
                }
            }
        }
        return new byte[] { b, (byte)n4 };
    }
    
    public static byte[] ConvertStringToHexBytesArray(String replaceAll) {
        replaceAll = replaceAll.toUpperCase().replaceAll(" ", "");
        final char[] charArray = replaceAll.toCharArray();
        int n = 0;
        final byte[] array = new byte[replaceAll.length() / 2];
        int n4;
        for (int length = replaceAll.length(), i = 0; i < length; ++i, n = n4) {
            int n2;
            int n3;
            for (int j = 0; j <= 15; j = n2 + 1, n = n3) {
                n2 = j;
                n3 = n;
                if (charArray[i] == (new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' })[j]) {
                    if (i % 2 == 1) {
                        n3 = n + j;
                        n2 = 15;
                    }
                    else {
                        n2 = j;
                        n3 = n;
                        if (i % 2 == 0) {
                            n3 = n + j * 16;
                            n2 = 15;
                        }
                    }
                }
            }
            n4 = n;
            if (i % 2 == 1) {
                array[i / 2] = (byte)n;
                n4 = 0;
            }
        }
        return array;
    }
    
    public static int ConvertStringToInt(final String s) {
        if (s.length() > 2) {
            return Integer.parseInt(s.substring(2, 4), 16) + Integer.parseInt(s.substring(0, 2), 16) * 256;
        }
        return Integer.parseInt(s.substring(0, 2), 16);
    }
    
    public static String FormatValueByteWrite(final String s) {
        return castHexKeyboard(StringForceDigit(s, 2)).toUpperCase();
    }
    
    public static String StringForceDigit(String s, final int n) {
        s = s.replaceAll(" ", "");
        if (s.length() == 4) {
            return s;
        }
        String s2 = s;
        if (s.length() < n) {
            while (s.length() != n) {
                s = "0".concat(s);
            }
            s2 = s;
        }
        return s2;
    }
    
    public static String[] buildArrayBlocks(final byte[] array, final int n) {
        final String[] array2 = new String[n];
        int n2 = array[1];
        if (array[1] < 0) {
            n2 = array[1] + 256;
        }
        int n3;
        if (array[0] < 0) {
            n3 = n2 + (array[0] + 256) * 256;
        }
        else {
            n3 = n2 + array[0] * 256;
        }
        int n4;
        for (int i = 0; i < n; i = n4 + 1) {
            if ((n4 = i) == 14) {
                n4 = 14;
            }
            array2[n4] = "Block  " + ConvertIntToHexFormatString(n4 + n3).toUpperCase();
        }
        return array2;
    }
    
    public static String[] buildArrayValueBlocks(final byte[] array, final int n) {
        final String[] array2 = new String[n];
        int n2 = 1;
        for (int i = 0; i < n; ++i) {
            array2[i] = "";
            array2[i] = String.valueOf(array2[i]) + ConvertHexByteToString(array[n2]).toUpperCase();
            array2[i] = String.valueOf(array2[i]) + " ";
            array2[i] = String.valueOf(array2[i]) + ConvertHexByteToString(array[n2 + 1]).toUpperCase();
            array2[i] = String.valueOf(array2[i]) + " ";
            array2[i] = String.valueOf(array2[i]) + ConvertHexByteToString(array[n2 + 2]).toUpperCase();
            array2[i] = String.valueOf(array2[i]) + " ";
            array2[i] = String.valueOf(array2[i]) + ConvertHexByteToString(array[n2 + 3]).toUpperCase();
            n2 += 4;
        }
        return array2;
    }
    
    public static byte[] byteMerger(final byte[] array, final byte[] array2) {
        final byte[] array3 = new byte[array.length + array2.length];
        System.arraycopy(array, 0, array3, 0, array.length);
        System.arraycopy(array2, 0, array3, array.length, array2.length);
        return array3;
    }
    
    public static String castHexKeyboard(String string) {
        final String s = "";
        final String upperCase = string.toUpperCase();
        final char[] charArray = upperCase.toCharArray();
        int i = 0;
        string = s;
        while (i < upperCase.length()) {
            if (charArray[i] != '0' && charArray[i] != '1' && charArray[i] != '2' && charArray[i] != '3' && charArray[i] != '4' && charArray[i] != '5' && charArray[i] != '6' && charArray[i] != '7' && charArray[i] != '8' && charArray[i] != '9' && charArray[i] != 'A' && charArray[i] != 'B' && charArray[i] != 'C' && charArray[i] != 'D' && charArray[i] != 'E') {
                charArray[i] = 'F';
            }
            string = String.valueOf(string) + charArray[i];
            ++i;
        }
        return string;
    }
    
    private static byte charToByte(final char c) {
        return (byte)"0123456789ABCDEF".indexOf(c);
    }
    
    public static String checkAndChangeDataHexa(String s) {
        final String s2 = "";
        final String upperCase = s.toUpperCase();
        final char[] charArray = upperCase.toCharArray();
        int i = 0;
        s = s2;
        while (i < upperCase.length()) {
            String string = null;
            Label_0196: {
                if (charArray[i] != '0' && charArray[i] != '1' && charArray[i] != '2' && charArray[i] != '3' && charArray[i] != '4' && charArray[i] != '5' && charArray[i] != '6' && charArray[i] != '7' && charArray[i] != '8' && charArray[i] != '9' && charArray[i] != 'A' && charArray[i] != 'B' && charArray[i] != 'C' && charArray[i] != 'D' && charArray[i] != 'E') {
                    string = s;
                    if (charArray[i] != 'F') {
                        break Label_0196;
                    }
                }
                string = String.valueOf(s) + charArray[i];
            }
            ++i;
            s = string;
        }
        return s;
    }
    
    public static String checkAndChangeFileName(final String s) {
        String s2 = "";
        final char[] charArray = s.toCharArray();
        String string;
        for (int i = 0; i < s.length(); ++i, s2 = string) {
            if (charArray[i] != '0' && charArray[i] != '1' && charArray[i] != '2' && charArray[i] != '3' && charArray[i] != '4' && charArray[i] != '5' && charArray[i] != '6' && charArray[i] != '7' && charArray[i] != '8' && charArray[i] != '9' && charArray[i] != 'a' && charArray[i] != 'b' && charArray[i] != 'c' && charArray[i] != 'd' && charArray[i] != 'e' && charArray[i] != 'f' && charArray[i] != 'g' && charArray[i] != 'h' && charArray[i] != 'i' && charArray[i] != 'j' && charArray[i] != 'k' && charArray[i] != 'l' && charArray[i] != 'm' && charArray[i] != 'n' && charArray[i] != 'o' && charArray[i] != 'p' && charArray[i] != 'q' && charArray[i] != 'r' && charArray[i] != 's' && charArray[i] != 't' && charArray[i] != 'u' && charArray[i] != 'v' && charArray[i] != 'w' && charArray[i] != 'x' && charArray[i] != 'y' && charArray[i] != 'z' && charArray[i] != 'A' && charArray[i] != 'B' && charArray[i] != 'C' && charArray[i] != 'D' && charArray[i] != 'E' && charArray[i] != 'F' && charArray[i] != 'G' && charArray[i] != 'H' && charArray[i] != 'I' && charArray[i] != 'J' && charArray[i] != 'K' && charArray[i] != 'L' && charArray[i] != 'M' && charArray[i] != 'N' && charArray[i] != 'O' && charArray[i] != 'P' && charArray[i] != 'Q' && charArray[i] != 'R' && charArray[i] != 'S' && charArray[i] != 'T' && charArray[i] != 'U' && charArray[i] != 'V' && charArray[i] != 'W' && charArray[i] != 'X' && charArray[i] != 'Y' && charArray[i] != 'Z' && charArray[i] != '.') {
                string = s2;
                if (charArray[i] != '_') {
                    continue;
                }
            }
            string = String.valueOf(s2) + charArray[i];
        }
        return s2;
    }
    
    public static boolean checkDataHexa(String upperCase) {
        boolean b = true;
        upperCase = upperCase.toUpperCase();
        final char[] charArray = upperCase.toCharArray();
        boolean b2;
        for (int i = 0; i < upperCase.length(); ++i, b = b2) {
            b2 = b;
            if (charArray[i] != '0') {
                b2 = b;
                if (charArray[i] != '1') {
                    b2 = b;
                    if (charArray[i] != '2') {
                        b2 = b;
                        if (charArray[i] != '3') {
                            b2 = b;
                            if (charArray[i] != '4') {
                                b2 = b;
                                if (charArray[i] != '5') {
                                    b2 = b;
                                    if (charArray[i] != '6') {
                                        b2 = b;
                                        if (charArray[i] != '7') {
                                            b2 = b;
                                            if (charArray[i] != '8') {
                                                b2 = b;
                                                if (charArray[i] != '9') {
                                                    b2 = b;
                                                    if (charArray[i] != 'A') {
                                                        b2 = b;
                                                        if (charArray[i] != 'B') {
                                                            b2 = b;
                                                            if (charArray[i] != 'C') {
                                                                b2 = b;
                                                                if (charArray[i] != 'D') {
                                                                    b2 = b;
                                                                    if (charArray[i] != 'E') {
                                                                        b2 = b;
                                                                        if (charArray[i] != 'F') {
                                                                            b2 = false;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return b;
    }
    
    public static boolean checkFileName(final String s) {
        boolean b = true;
        final char[] charArray = s.toCharArray();
        boolean b2;
        for (int i = 0; i < s.length(); ++i, b = b2) {
            b2 = b;
            if (charArray[i] != '0') {
                b2 = b;
                if (charArray[i] != '1') {
                    b2 = b;
                    if (charArray[i] != '2') {
                        b2 = b;
                        if (charArray[i] != '3') {
                            b2 = b;
                            if (charArray[i] != '4') {
                                b2 = b;
                                if (charArray[i] != '5') {
                                    b2 = b;
                                    if (charArray[i] != '6') {
                                        b2 = b;
                                        if (charArray[i] != '7') {
                                            b2 = b;
                                            if (charArray[i] != '8') {
                                                b2 = b;
                                                if (charArray[i] != '9') {
                                                    b2 = b;
                                                    if (charArray[i] != 'a') {
                                                        b2 = b;
                                                        if (charArray[i] != 'b') {
                                                            b2 = b;
                                                            if (charArray[i] != 'c') {
                                                                b2 = b;
                                                                if (charArray[i] != 'd') {
                                                                    b2 = b;
                                                                    if (charArray[i] != 'e') {
                                                                        b2 = b;
                                                                        if (charArray[i] != 'f') {
                                                                            b2 = b;
                                                                            if (charArray[i] != 'g') {
                                                                                b2 = b;
                                                                                if (charArray[i] != 'h') {
                                                                                    b2 = b;
                                                                                    if (charArray[i] != 'i') {
                                                                                        b2 = b;
                                                                                        if (charArray[i] != 'j') {
                                                                                            b2 = b;
                                                                                            if (charArray[i] != 'k') {
                                                                                                b2 = b;
                                                                                                if (charArray[i] != 'l') {
                                                                                                    b2 = b;
                                                                                                    if (charArray[i] != 'm') {
                                                                                                        b2 = b;
                                                                                                        if (charArray[i] != 'n') {
                                                                                                            b2 = b;
                                                                                                            if (charArray[i] != 'o') {
                                                                                                                b2 = b;
                                                                                                                if (charArray[i] != 'p') {
                                                                                                                    b2 = b;
                                                                                                                    if (charArray[i] != 'q') {
                                                                                                                        b2 = b;
                                                                                                                        if (charArray[i] != 'r') {
                                                                                                                            b2 = b;
                                                                                                                            if (charArray[i] != 's') {
                                                                                                                                b2 = b;
                                                                                                                                if (charArray[i] != 't') {
                                                                                                                                    b2 = b;
                                                                                                                                    if (charArray[i] != 'u') {
                                                                                                                                        b2 = b;
                                                                                                                                        if (charArray[i] != 'v') {
                                                                                                                                            b2 = b;
                                                                                                                                            if (charArray[i] != 'w') {
                                                                                                                                                b2 = b;
                                                                                                                                                if (charArray[i] != 'x') {
                                                                                                                                                    b2 = b;
                                                                                                                                                    if (charArray[i] != 'y') {
                                                                                                                                                        b2 = b;
                                                                                                                                                        if (charArray[i] != 'z') {
                                                                                                                                                            b2 = b;
                                                                                                                                                            if (charArray[i] != 'A') {
                                                                                                                                                                b2 = b;
                                                                                                                                                                if (charArray[i] != 'B') {
                                                                                                                                                                    b2 = b;
                                                                                                                                                                    if (charArray[i] != 'C') {
                                                                                                                                                                        b2 = b;
                                                                                                                                                                        if (charArray[i] != 'D') {
                                                                                                                                                                            b2 = b;
                                                                                                                                                                            if (charArray[i] != 'E') {
                                                                                                                                                                                b2 = b;
                                                                                                                                                                                if (charArray[i] != 'F') {
                                                                                                                                                                                    b2 = b;
                                                                                                                                                                                    if (charArray[i] != 'G') {
                                                                                                                                                                                        b2 = b;
                                                                                                                                                                                        if (charArray[i] != 'H') {
                                                                                                                                                                                            b2 = b;
                                                                                                                                                                                            if (charArray[i] != 'I') {
                                                                                                                                                                                                b2 = b;
                                                                                                                                                                                                if (charArray[i] != 'J') {
                                                                                                                                                                                                    b2 = b;
                                                                                                                                                                                                    if (charArray[i] != 'K') {
                                                                                                                                                                                                        b2 = b;
                                                                                                                                                                                                        if (charArray[i] != 'L') {
                                                                                                                                                                                                            b2 = b;
                                                                                                                                                                                                            if (charArray[i] != 'M') {
                                                                                                                                                                                                                b2 = b;
                                                                                                                                                                                                                if (charArray[i] != 'N') {
                                                                                                                                                                                                                    b2 = b;
                                                                                                                                                                                                                    if (charArray[i] != 'O') {
                                                                                                                                                                                                                        b2 = b;
                                                                                                                                                                                                                        if (charArray[i] != 'P') {
                                                                                                                                                                                                                            b2 = b;
                                                                                                                                                                                                                            if (charArray[i] != 'Q') {
                                                                                                                                                                                                                                b2 = b;
                                                                                                                                                                                                                                if (charArray[i] != 'R') {
                                                                                                                                                                                                                                    b2 = b;
                                                                                                                                                                                                                                    if (charArray[i] != 'S') {
                                                                                                                                                                                                                                        b2 = b;
                                                                                                                                                                                                                                        if (charArray[i] != 'T') {
                                                                                                                                                                                                                                            b2 = b;
                                                                                                                                                                                                                                            if (charArray[i] != 'U') {
                                                                                                                                                                                                                                                b2 = b;
                                                                                                                                                                                                                                                if (charArray[i] != 'V') {
                                                                                                                                                                                                                                                    b2 = b;
                                                                                                                                                                                                                                                    if (charArray[i] != 'W') {
                                                                                                                                                                                                                                                        b2 = b;
                                                                                                                                                                                                                                                        if (charArray[i] != 'X') {
                                                                                                                                                                                                                                                            b2 = b;
                                                                                                                                                                                                                                                            if (charArray[i] != 'Y') {
                                                                                                                                                                                                                                                                b2 = b;
                                                                                                                                                                                                                                                                if (charArray[i] != 'Z') {
                                                                                                                                                                                                                                                                    b2 = b;
                                                                                                                                                                                                                                                                    if (charArray[i] != '.') {
                                                                                                                                                                                                                                                                        b2 = b;
                                                                                                                                                                                                                                                                        if (charArray[i] != '_') {
                                                                                                                                                                                                                                                                            b2 = false;
                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                            }
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                }
                                                                                                                                                                                                            }
                                                                                                                                                                                                        }
                                                                                                                                                                                                    }
                                                                                                                                                                                                }
                                                                                                                                                                                            }
                                                                                                                                                                                        }
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return b;
    }
    
    public static byte[] fillbyte(final int n, final byte[] array) {
        final byte[] array2 = new byte[n];
        for (int i = 0; i < n; ++i) {
            if (i < array.length) {
                array2[i] = array[i];
            }
            else {
                array2[i] = 0;
            }
        }
        return array2;
    }
    
    public static byte[] hexStringToBytes(String upperCase) {
        byte[] array;
        if (upperCase == null || upperCase.equals("")) {
            array = null;
        }
        else {
            upperCase = upperCase.toUpperCase();
            final int n = upperCase.length() / 2;
            final char[] charArray = upperCase.toCharArray();
            final byte[] array2 = new byte[n];
            int n2 = 0;
            while (true) {
                array = array2;
                if (n2 >= n) {
                    break;
                }
                final int n3 = n2 * 2;
                array2[n2] = (byte)(charToByte(charArray[n3]) << 4 | charToByte(charArray[n3 + 1]));
                ++n2;
            }
        }
        return array;
    }
}
