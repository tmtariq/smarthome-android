package com.gk.wifictr.net.easylink;

import java.util.*;

public class FtcEncode
{
    private ArrayList<Integer> mData;
    
    public FtcEncode(final String s, final byte[] array) throws Exception {
        final Ftc20 ftc20 = new Ftc20();
        ftc20.setmSsid(s);
        ftc20.setmKey(array);
        ftc20.encodePackets();
        this.mData = ftc20.getmData();
    }
    
    public ArrayList<Integer> getmData() throws Exception {
        return this.mData;
    }
}
