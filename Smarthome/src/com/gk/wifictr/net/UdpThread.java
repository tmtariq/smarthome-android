package com.gk.wifictr.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import com.gk.wifictr.net.command.ByteUtil;
import com.gk.wifictr.net.command.ReadData;
import com.gk.wifictr.net.command.station.In_11H;
import com.gk.wifictr.net.command.station.In_61H;
import com.gk.wifictr.net.command.station.Out_21H;

import android.content.Context;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;



public class UdpThread extends Thread
{
    public static int UDP_ANY = 0;
    public static int UDP_EASYLINK = 62;
    public static int UDP_IP = 11;
    public static int UDP_MAX_RETRYT = 0;
    public static int UDP_SN = 61;
    public static int[] UDP_TIME_OUT;
    public Boolean IsThreadDisable;
    public int UDPType;
    public int UDP_TIME_OUT_TIME;
    Context context;
    DatagramSocket datagramSocket;
    private WifiManager.MulticastLock lock;
    BroadcastHelper mBroadcastHelper;
    InetAddress mInetAddress;
    NetHelper mNetHelper;
    
    static {
        UdpThread.UDP_TIME_OUT = new int[] { 10000, 5000, 5000, 5000, 3000, 5000, 10000, 20000 };
        UdpThread.UDP_MAX_RETRYT = 8;
    }
    
    public UdpThread(WifiManager wifiManager, Context context) {
        UDPType = 0;
        UDP_TIME_OUT_TIME = 0;
        IsThreadDisable = false;
        lock = wifiManager.createMulticastLock("UDPwifi");
        setName("UdpThread");
        context = context;
        mNetHelper = new NetHelper(ConnectService.getInstance());
        mBroadcastHelper = new BroadcastHelper();
    }
    
    public UdpThread(WifiManager wifiManager, Context context, int udpType) {
        UDPType = 0;
        UDP_TIME_OUT_TIME = 0;
        IsThreadDisable = false;
        lock = wifiManager.createMulticastLock("UDPwifi");
        setName("UdpThread");
        context = context;
        UDPType = udpType;
        mNetHelper = new NetHelper(ConnectService.getInstance());
        mBroadcastHelper = new BroadcastHelper();
        if (udpType == 62) {
            UdpThread.UDP_TIME_OUT = new int[] { 10000, 10000, 10000, 10000, 10000, 10000 };
            UdpThread.UDP_MAX_RETRYT = 6;
        }
    }
    
    public void StartListen() {
		if (mNetHelper.getWifiState() != NetworkInfo.State.CONNECTED)
			log("不在wifi发毛线udp");
		int i=0;
		label1046: label1051: label1054: while (true) {
			if (datagramSocket == null)
				newSocket();
			lock.acquire();
			
			if ((UDPType == 0) || (UDPType == 11)) {
				send(new Out_21H().make(Integer
						.parseInt(ConnectService.UDP_READ_PORT)));
				i = 0;
			}
			while (true) {
				DatagramPacket localDatagramPacket = new DatagramPacket(new byte['ÿ'], 255);
				byte[] arrayOfByte = ByteUtil.copyOfRange(localDatagramPacket.getData(), 0, localDatagramPacket.getLength());
				while (true) {
					if ((IsThreadDisable) || (isInterrupted()))
						break label1054;
					try {
						log("监听 " + datagramSocket.getLocalPort() + " 端口ing");
						//localDatagramPacket = new DatagramPacket(new byte['ÿ'], 255);
						datagramSocket.receive(localDatagramPacket);
						arrayOfByte = ByteUtil.copyOfRange(localDatagramPacket.getData(), 0, localDatagramPacket.getLength());
						if (arrayOfByte.length > 20)
							 continue; //break label368;
						log("太短");
					} catch (IOException localIOException) {
						i++;
						log("这尼玛已经是第" + i + "次接收超时了\n现在的超时阀值为"
								+ getTIMEOUT_DELAY());
						if (i >= UDP_MAX_RETRYT) {
							log("收不到数据，哥不玩儿了");
							IsThreadDisable = Boolean.valueOf(true);
							datagramSocket.close();
							lock.release();
						}
						UDP_TIME_OUT_TIME ++;
					}
					
					label298:
					try {
						datagramSocket.setSoTimeout(getTIMEOUT_DELAY());
						log("现在的超时阀值为" + getTIMEOUT_DELAY());
						if ((UDPType == 0) || (UDPType == 11)) {
							send(new Out_21H().make(Integer.parseInt(ConnectService.UDP_READ_PORT)));
							i=0;
							continue;
						}
							log("当前的UDPType=" + UDPType + "，所以不发送21H");
							//break;
							log("长度：" + localDatagramPacket.getLength());
							if (localDatagramPacket.getAddress().getHostAddress().toString().equals(mNetHelper.getIP())) {
								log("自家数据");
								//continue;
							}
							log(String.valueOf(localDatagramPacket.getAddress().getHostAddress().toString()) + ":" + localDatagramPacket.getPort() + " >> " + ByteUtil.byte2hex(arrayOfByte));
							ReadData localReadData = new ReadData(localDatagramPacket.getAddress().getHostAddress().toString(),String.valueOf(localDatagramPacket.getPort()), arrayOfByte);
							localReadData.fromIP = localDatagramPacket.getAddress().getHostAddress().toString();
							localReadData.fromPort = String.valueOf(localDatagramPacket.getPort());
							log("好数据？ " + localReadData.goodData);
							log("类型？ " + ByteUtil.byte2hex(localReadData.type));
							if (localReadData.goodData == 0) {
								if (localReadData.type == 17) {
									if ((UDPType == 0)|| (UDPType == 11)) {
										log("收到IP/端口数据，解析");
										new In_11H(localReadData.body);
										if (UDPType != 11)
											continue;
										datagramSocket.close();
										lock.release();
										IsThreadDisable = Boolean.valueOf(true);
										UDP_TIME_OUT_TIME = 0;
										//continue;
									}
									log("收到IP/端口数据,但当前的UDPType=" + UDPType + "，所以未解析");
									//continue;
								}
								if (localReadData.type == 98) {
									if ((UDPType == 0) || (UDPType == 62)) {
										log("Easylink完成");
										new BroadcastHelper().doneEasyLink();
										if (UDPType != 62)
											break label1046;
										datagramSocket.close();
										lock.release();
										IsThreadDisable = Boolean
												.valueOf(true);
										UDP_TIME_OUT_TIME = 0;
										//continue;
									}
									log("收到Easylink完成信号,但当前的UDPType="
											+ UDPType + "，所以未解析");
									//continue;
								}
								if (localReadData.type == 97) {
									if ((UDPType == 0)
											|| (UDPType == 61)) {
										log("收到主机广播的SN号和密码");
										new In_61H(localReadData.body);
										if (UDPType != 61)
											break label1051;
										datagramSocket.close();
										lock.release();
										IsThreadDisable = Boolean
												.valueOf(true);
										UDP_TIME_OUT_TIME = 0;
										//continue;
									}
									log("收到主机广播的SN号和密码,但当前的UDPType="
											+ UDPType + "，所以未解析");
									continue;
								}
								i++;
								log("数据不对,起来重收，当前的UDPType=" + UDPType);
								continue;
							}
							log("数据格式不正确，忽略");
							//continue;
						log("当前的UDPType=" + UDPType + "，所以不发送21H");
					} catch (SocketException localSocketException) {
						i = 0;
						break label298;
					}
				}
				//label368: continue;
				//i = 0;
			}
		}
	}
    
    public void cancel() {
        IsThreadDisable = true;
    }
    
    public int getTIMEOUT_DELAY() {
        if (UDP_TIME_OUT_TIME < 0) {
            UDP_TIME_OUT_TIME = 0;
        }
        if (UDP_TIME_OUT_TIME >= UdpThread.UDP_TIME_OUT.length) {
            return UdpThread.UDP_TIME_OUT[UdpThread.UDP_TIME_OUT.length - 1];
        }
        return UdpThread.UDP_TIME_OUT[UDP_TIME_OUT_TIME];
    }
    
    public void log(String s) {
        Log.v(getName(), s);
        mBroadcastHelper.sendLog(s);
    }
    
    public void newSocket() {
        int int1 = Integer.parseInt(ConnectService.UDP_READ_PORT);
        if (datagramSocket != null) {
            return;
        }
        try {
            (datagramSocket = new DatagramSocket(null)).setReuseAddress(true);
            datagramSocket.bind(new InetSocketAddress(int1));
            datagramSocket.setSoTimeout(getTIMEOUT_DELAY());
            log("\u8d85\u65f6\u9600\u503c\u4e3a" + getTIMEOUT_DELAY());
            datagramSocket.setBroadcast(true);
        }
        catch (SocketException ex) {}
    }
    
    @Override
    public void run() {
        StartListen();
    }
    
    public void send(byte[] array) {
        int int1 = Integer.parseInt(ConnectService.UDP_WRITE_PORT);
        Log.d("UDP Demo", "UDP\u53d1\u9001\u6570\u636e:" + array);
        if (datagramSocket == null) {
            newSocket();
        }
        InetAddress inetAddress = null;
        while (true) {
            try {
                inetAddress = InetAddress.getByName(ConnectService.UDP_WRITE_IP);
                inetAddress = InetAddress.getByName(mNetHelper.getBroadcastAdd());
                //array = (byte[])(Object)new DatagramPacket(array, array.length, inetAddress, int1);
                //UdpThread udpThread = this;
                //DatagramSocket datagramSocket = udpThread.datagramSocket;
                //byte[] array2 = array;
                datagramSocket.send(new DatagramPacket(array, array.length, inetAddress, int1));
                return;
            }
            catch (UnknownHostException ex) {
                ex.printStackTrace();
                continue;
            }
            catch (IOException ex2) {
                ex2.printStackTrace();
            }
            break;
        }
        try {
            UdpThread udpThread = this;
            DatagramSocket datagramSocket = udpThread.datagramSocket;
            byte[] array2 = array;
            datagramSocket.send((DatagramPacket)(Object)array2);
        }
        catch (IOException ex2) {
            ex2.printStackTrace();
        }
    }
    
    @Override
    public void start() {
        super.start();
    }
    
    public void toast(String s) {
        Looper.prepare();
        Toast.makeText(context, (CharSequence)s, 0).show();
        Looper.loop();
    }
}
