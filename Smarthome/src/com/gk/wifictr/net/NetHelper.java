package com.gk.wifictr.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;


public class NetHelper
{
    Context context;
    ConnectivityManager mConnectivityManager;
    DhcpInfo mDhcpInfo;
    WifiInfo mWifiInfo;
    WifiManager mWifiManager;
    
    public NetHelper() {
        this.context = (Context)ConnectService.mConnectService;
        this.mWifiManager = (WifiManager)this.context.getSystemService("wifi");
        this.mConnectivityManager = (ConnectivityManager)this.context.getSystemService("connectivity");
        this.mWifiInfo = this.mWifiManager.getConnectionInfo();
    }
    
    public NetHelper(final Context context) {
        this.context = context;
        this.mWifiManager = (WifiManager)this.context.getSystemService("wifi");
        this.mConnectivityManager = (ConnectivityManager)this.context.getSystemService("connectivity");
        this.mWifiInfo = this.mWifiManager.getConnectionInfo();
    }
    
    public String getAll() {
        return String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf("")).append("\u7f51\u5173\uff1a").append(this.getGatewayIP()).append("\n").toString())).append("IP\uff1a").append(this.getIP()).append("\n").toString())).append("\u5e7f\u64ad\u5730\u5740\uff1a").append(this.getBroadcastAdd()).append("\n").toString())).append("MAC\uff1a").append(this.getMAC()).append("\n").toString())).append("\u79fb\u52a8\u7f51\u7edc\uff1a").append(this.getDataState().toString()).append("\n").toString())).append("WIFI\uff1a").append(this.getWifiState().toString()).append("\n").toString()) + "WIFI\u5f00\u5173\uff1a" + this.isWifiEnabled() + "\n";
    }
    
    public String getBaseSSID() {
        this.mWifiInfo = this.mWifiManager.getConnectionInfo();
        return this.mWifiInfo.getBSSID();
    }
    
    public String getBroadcastAdd() {
        this.mDhcpInfo = this.mWifiManager.getDhcpInfo();
        final int ipAddress = this.mDhcpInfo.ipAddress;
        final int netmask = this.mDhcpInfo.netmask;
        return this.intIP2String((ipAddress & netmask) | ~netmask);
    }
    
    public NetworkInfo.State getDataState() {
        return this.mConnectivityManager.getNetworkInfo(0).getState();
    }
    
    public String getGatewayIP() {
        this.mDhcpInfo = this.mWifiManager.getDhcpInfo();
        return this.intIP2String(this.mDhcpInfo.gateway);
    }
    
    public String getIP() {
        this.mWifiInfo = this.mWifiManager.getConnectionInfo();
        return this.intIP2String(this.mWifiInfo.getIpAddress());
    }
    
    public String getMAC() {
        this.mWifiInfo = this.mWifiManager.getConnectionInfo();
        return this.mWifiInfo.getMacAddress();
    }
    
    public String getWifiSSID() {
        if (this.isWifiEnabled()) {
            this.mWifiInfo = this.mWifiManager.getConnectionInfo();
            String s2;
            final String s = s2 = this.mWifiInfo.getSSID();
            if (Build.VERSION.SDK_INT >= 17) {
                s2 = s;
                if (s.startsWith("\"")) {
                    s2 = s;
                    if (s.endsWith("\"")) {
                        s2 = s.substring(1, s.length() - 1);
                    }
                }
            }
            return s2;
        }
        return "";
    }
    
    public NetworkInfo.State getWifiState() {
        return this.mConnectivityManager.getNetworkInfo(1).getState();
    }
    
    public String intIP2String(final int n) {
        return String.valueOf(n & 0xFF) + "." + (n >> 8 & 0xFF) + "." + (n >> 16 & 0xFF) + "." + (n >> 24 & 0xFF);
    }
    
    public boolean isWifiConnected() {
        return this.mConnectivityManager.getNetworkInfo(1).isConnected();
    }
    
    public boolean isWifiEnabled() {
        return this.mWifiManager.isWifiEnabled();
    }
}
