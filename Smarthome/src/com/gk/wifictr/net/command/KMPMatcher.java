package com.gk.wifictr.net.command;

class KMPMatcher
{
    private byte[] bytePattern;
    private int[] failure;
    private int matchPoint;
    
    public void computeFailure4Byte(byte[] bytePattern) {
        this.bytePattern = bytePattern;
        int n = 0;
        int length = this.bytePattern.length;
        this.failure = new int[length];
        int n2 = 1;
        while (true) {
            int n3 = n;
            if (n2 >= length) {
                break;
            }
            while (n3 > 0 && this.bytePattern[n3] != this.bytePattern[n2]) {
                n3 = this.failure[n3 - 1];
            }
            n = n3;
            if (this.bytePattern[n3] == this.bytePattern[n2]) {
                n = n3 + 1;
            }
            this.failure[n2] = n;
            ++n2;
        }
    }
    
    public int indexOf(byte[] array, int n) {
        int n2 = 0;
        if (array.length != 0 && n <= array.length) {
            int i = n;
            n = n2;
            while (i < array.length) {
                int n3;
                for (n3 = n; n3 > 0 && this.bytePattern[n3] != array[i]; n3 = this.failure[n3 - 1]) {}
                n = n3;
                if (this.bytePattern[n3] == array[i]) {
                    n = n3 + 1;
                }
                if (n == this.bytePattern.length) {
                    return this.matchPoint = i - this.bytePattern.length + 1;
                }
                ++i;
            }
        }
        return -1;
    }
    
    public int lastIndexOf(byte[] array, int n) {
        this.matchPoint = -1;
        int n2 = 0;
        if (array.length == 0 || n > array.length) {
            return -1;
        }
        int n4;
        int n5;
        int n6;
        for (int length = array.length, i = n; i < length; i = n5 + 1, length = n6, n = n4) {
            while (n2 > 0 && this.bytePattern[n2] != array[i]) {
                n2 = this.failure[n2 - 1];
            }
            int n3 = n2;
            if (this.bytePattern[n2] == array[i]) {
                n3 = n2 + 1;
            }
            if (n3 == this.bytePattern.length) {
                this.matchPoint = i - this.bytePattern.length + 1;
                if (array.length - i <= this.bytePattern.length) {
                    return this.matchPoint;
                }
                n2 = 0;
                n4 = n;
                n5 = i;
                n6 = length;
            }
            else {
                n6 = length;
                n5 = i;
                n2 = n3;
                if ((n4 = n) != 0) {
                    n6 = length;
                    n5 = i;
                    n2 = n3;
                    n4 = n;
                    if (i + 1 == length) {
                        n5 = -1;
                        n4 = 0;
                        n6 = n;
                        n2 = n3;
                    }
                }
            }
        }
        return this.matchPoint;
    }
    
    public int lastIndexOfWithNoLoop(byte[] array, int n) {
        this.matchPoint = -1;
        int n2 = 0;
        if (array.length == 0 || n > array.length) {
            return -1;
        }
        for (int i = n; i < array.length; ++i) {
            while (n2 > 0 && this.bytePattern[n2] != array[i]) {
                n2 = this.failure[n2 - 1];
            }
            n = n2;
            if (this.bytePattern[n2] == array[i]) {
                n = n2 + 1;
            }
            if ((n2 = n) == this.bytePattern.length) {
                this.matchPoint = i - this.bytePattern.length + 1;
                if (array.length - i <= this.bytePattern.length) {
                    return this.matchPoint;
                }
                n2 = 0;
            }
        }
        return this.matchPoint;
    }
}
