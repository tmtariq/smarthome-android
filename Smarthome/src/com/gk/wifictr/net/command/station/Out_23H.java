package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.data.*;
import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.*;
import java.io.*;

public class Out_23H extends Order
{
    public byte[] ORDER_TYPE_CODE;
    
    public Out_23H() {
        this.ORDER_TYPE_CODE = new byte[] { 35 };
    }
    
    private byte getDevStatusByte(int n) {
        int[] array = new int[8];
        for (int n2 = 0; n2 < n && n2 <= 3; ++n2) {
            array[n2 + 4] = 1;
        }
        return ByteUtil.getBit(array);
    }
    
    private byte getDevStatusByte(int[] array) {
        return ByteUtil.getBit(array);
    }
    
    public WriteData make(SubDevice subDevice) {
        return this.make(subDevice.devName, subDevice.devSN, subDevice.devType, subDevice.lightCount);
    }
    
    public WriteData make(String s, String hex2byte, byte b, byte b2) {
        this.routerAP = true;
        this.routerLAN = true;
        this.routerWAN = true;
        byte[] byteSN = new byte[16];
        while (true) {
            try {
                byte[] bytes = s.getBytes("UTF-8");
                byteSN = ConnectService.defWifi.getByteSN();
                byte[] bytePassword = ConnectService.defWifi.getBytePassword();
                byte[] byteWithLength = ByteUtil.getByteWithLength(bytes, 16);
                //hex2byte = (String)(Object)ByteUtil.hex2byte(hex2byte);
                byte[] arraycat = ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(new byte[0], this.ORDER_TYPE_CODE), byteSN), bytePassword), byteWithLength), ByteUtil.hex2byte(hex2byte)), new byte[] { b }), new byte[] { b2 });
                this.log("body\uff1a" + ByteUtil.byte2hex(arraycat));
                return new WriteData(this.makeOrder(arraycat, "LAN"), this.makeOrder(arraycat, "WAN"), this.routerAP, this.routerLAN, this.routerWAN);
            }
            catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
                byte[] bytes = byteSN;
                continue;
            }
            //break;
        }
    }
    
    public WriteData make(String s, String hex2byte, byte b, int n) {
        this.routerAP = true;
        this.routerLAN = true;
        this.routerWAN = true;
        byte[] byteSN = new byte[16];
        while (true) {
            try {
                byte[] bytes = s.getBytes("UTF-8");
                byteSN = ConnectService.defWifi.getByteSN();
                byte[] bytePassword = ConnectService.defWifi.getBytePassword();
                byte[] byteWithLength = ByteUtil.getByteWithLength(bytes, 16);
                hex2byte = (String)(Object)ByteUtil.hex2byte(hex2byte);
                byte[] arraycat = ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(new byte[0], this.ORDER_TYPE_CODE), byteSN), bytePassword), byteWithLength), (byte[])(Object)hex2byte), new byte[] { b }), new byte[] { this.getDevStatusByte(n) });
                this.log("body\uff1a" + ByteUtil.byte2hex(arraycat));
                return new WriteData(this.makeOrder(arraycat, "LAN"), this.makeOrder(arraycat, "WAN"), this.routerAP, this.routerLAN, this.routerWAN);
            }
            catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
                byte[] bytes = byteSN;
                continue;
            }
            //break;
        }
    }
}
