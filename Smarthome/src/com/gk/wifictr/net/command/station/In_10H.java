package com.gk.wifictr.net.command.station;

public class In_10H extends Order
{
    public static final byte TYPE = 16;
    String SN;
    byte[] body;
    String linkStatus;
    String returnStatus;
    
    public In_10H(final byte[] body) {
        this.body = body;
    }
}
