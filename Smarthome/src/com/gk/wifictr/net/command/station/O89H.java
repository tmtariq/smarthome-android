package com.gk.wifictr.net.command.station;

import com.gk.zhangzhongbao.ui.bean.*;
import com.gk.wifictr.net.command.*;
import android.util.*;
import java.io.*;
import java.util.*;

public class O89H
{
    final byte[] end;
    final byte[] head;
    final byte[] version;
    
    public O89H() {
        this.head = new byte[] { -18, -1 };
        this.version = new byte[] { 1 };
        this.end = new byte[] { 13, -17 };
    }
    
    public ArrayList<BtnData> decodeIn(byte[] check) {
        final ArrayList<BtnData> list = new ArrayList<BtnData>();
        check = O88H.check((byte[])check);
        if (check == null) {
            return null;
        }
        final byte[] copyOfRange = ByteUtil.copyOfRange((byte[])check, 4, check.length);
        Log.e("decode body", ByteUtil.byte2hex(copyOfRange));
        int n = 0;
        int i = 1;
        while (i <= 59) {
            final BtnData btnData = new BtnData();
            btnData.No = copyOfRange[n];
            Log.e("-------------", "\u7f16\u53f7:" + btnData.No);
            final byte[] copyOfRange2 = ByteUtil.copyOfRange(copyOfRange, n + 1, n + 17);
            //check = "";
            String str = "";
            while (true) {
                try {
                	str = new String(copyOfRange2, "UTF-8");
                    btnData.name_byte = copyOfRange2;
                    btnData.name = str.replaceAll(new String(new char[1]), "");
                    Log.e("-------------", "\u540d\u79f0:" + btnData.name + " " + ByteUtil.byte2hex(copyOfRange2));
                    final short shortFromByte = ByteUtil.getShortFromByte(ByteUtil.copyOfRange(copyOfRange, n + 17, n + 19));
                    Log.e("-------------", "\u5b66\u7801\u6570\u636e\u957f\u5ea6:" + shortFromByte);
                    if (shortFromByte > 0) {
                        btnData.data = ByteUtil.copyOfRange(copyOfRange, n + 19, n + 19 + shortFromByte);
                        Log.e("-------------", "\u5b66\u7801\u6570\u636e:" + ByteUtil.byte2hex(btnData.data));
                    }
                    n = n + 1 + 16 + 2 + shortFromByte;
                    Log.e("-------------", "index:" + n);
                    list.add(btnData);
                    ++i;
                }
                catch (UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                    continue;
                }
                break;
            }
        }
        Log.e("-------------", "list.size():" + list.size());
        return list;
    }
    
    public byte[] makeOut(byte[] array) {
        array = ByteUtil.arraycat(new byte[] { -119, 59 }, array);
        final Random random = new Random();
        final byte[] array2 = { (byte)random.nextInt(), (byte)random.nextInt() };
        array2[0] = -70;
        array2[1] = -58;
        for (int i = 0; i < array.length; ++i) {
            array[i] = (byte)((array[i] + array2[0]) % 256);
            array[i] ^= array2[1];
        }
        array = ByteUtil.arraycat(array2, ByteUtil.arraycat(ByteUtil.getLengthByte((short)array.length), array));
        array = ByteUtil.arraycat(this.version, array);
        array = ByteUtil.arraycat(this.head, array);
        return ByteUtil.arraycat(ByteUtil.arraycat(array, ByteUtil.getCRC(array)), this.end);
    }
}
