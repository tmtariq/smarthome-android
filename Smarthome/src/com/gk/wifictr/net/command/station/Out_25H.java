package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.*;
import java.io.*;
import com.gk.wifictr.net.data.*;

public class Out_25H extends Order
{
    public final byte[] ORDER_TYPE_CODE;
    
    public Out_25H() {
        this.ORDER_TYPE_CODE = new byte[] { 37 };
    }
    
    private byte getDevStatusByte(final int n) {
        final int[] array = new int[8];
        for (int n2 = 0; n2 < n && n2 <= 3; ++n2) {
            array[n2 + 4] = 1;
        }
        return ByteUtil.getBit(array);
    }
    
    public WriteData make(final int n, final String s, String hex2byte, final byte b, final byte b2) {
        this.routerAP = true;
        this.routerLAN = true;
        this.routerWAN = true;
        byte[] byteSN = new byte[16];
        while (true) {
            try {
                final byte[] bytes = s.getBytes("UTF-8");
                byteSN = ConnectService.defWifi.getByteSN();
                final byte[] bytePassword = ConnectService.defWifi.getBytePassword();
                final byte b3 = (byte)n;
                final byte[] byteWithLength = ByteUtil.getByteWithLength(bytes, 16);
                hex2byte = (String)(Object)ByteUtil.hex2byte(hex2byte);
                final byte[] arraycat = ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(new byte[0], this.ORDER_TYPE_CODE), byteSN), bytePassword), new byte[] { b3 }), byteWithLength), (byte[])(Object)hex2byte), new byte[] { b }), new byte[] { b2 });
                this.log("body\uff1a" + ByteUtil.byte2hex(arraycat));
                return new WriteData(this.makeOrder(arraycat, "LAN"), this.makeOrder(arraycat, "WAN"), this.routerAP, this.routerLAN, this.routerWAN);
            }
            catch (UnsupportedEncodingException ex) {
                final byte[] bytes = byteSN;
                continue;
            }
            //break;
        }
    }
    
    public WriteData make(final int n, final String s, String hex2byte, final byte b, final int n2) {
        this.routerAP = true;
        this.routerLAN = true;
        this.routerWAN = true;
        byte[] byteSN = new byte[16];
        while (true) {
            try {
                final byte[] bytes = s.getBytes("UTF-8");
                byteSN = ConnectService.defWifi.getByteSN();
                final byte[] bytePassword = ConnectService.defWifi.getBytePassword();
                final byte b2 = (byte)n;
                final byte[] byteWithLength = ByteUtil.getByteWithLength(bytes, 16);
                hex2byte = (String)(Object)ByteUtil.hex2byte(hex2byte);
                final byte[] arraycat = ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(new byte[0], this.ORDER_TYPE_CODE), byteSN), bytePassword), new byte[] { b2 }), byteWithLength), (byte[])(Object)hex2byte), new byte[] { b }), new byte[] { this.getDevStatusByte(n2) });
                this.log("body\uff1a" + ByteUtil.byte2hex(arraycat));
                return new WriteData(this.makeOrder(arraycat, "LAN"), this.makeOrder(arraycat, "WAN"), this.routerAP, this.routerLAN, this.routerWAN);
            }
            catch (UnsupportedEncodingException ex) {
                final byte[] bytes = byteSN;
                continue;
            }
            //break;
        }
    }
    
    public WriteData make(final SubDevice subDevice) {
        return this.make(subDevice.devNo, subDevice.devName, subDevice.devSN, subDevice.devType, subDevice.lightCount);
    }
}
