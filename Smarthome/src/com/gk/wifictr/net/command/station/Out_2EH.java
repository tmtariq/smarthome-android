package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.*;

public class Out_2EH extends Order
{
    public final byte[] ORDER_TYPE_CODE;
    
    public Out_2EH() {
        this.ORDER_TYPE_CODE = new byte[] { 46 };
    }
    
    public WriteData make(final byte b, final byte b2) {
        this.routerAP = true;
        this.routerLAN = true;
        this.routerWAN = false;
        final byte[] arraycat = ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(new byte[0], this.ORDER_TYPE_CODE), ConnectService.defWifi.getByteSN()), ConnectService.defWifi.getBytePassword()), new byte[] { b }), new byte[] { b2 });
        this.log("body\uff1a" + ByteUtil.byte2hex(arraycat));
        return new WriteData(this.makeOrder(arraycat, "LAN"), this.makeOrder(arraycat, "WAN"), this.routerAP, this.routerLAN, this.routerWAN);
    }
}
