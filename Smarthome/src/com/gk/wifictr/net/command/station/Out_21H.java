package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.*;

public class Out_21H extends Order
{
    public final byte[] ORDER_TYPE_CODE;
    
    public Out_21H() {
        this.ORDER_TYPE_CODE = new byte[] { 33 };
    }
    
    public byte[] make(final int n) {
        final byte[] arraycat = ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(new byte[0], this.ORDER_TYPE_CODE), ConnectService.defWifi.getByteSN()), ConnectService.defWifi.getBytePassword()), ByteUtil.swapByte(ByteUtil.getByte((short)n)));
        this.log("body\uff1a" + ByteUtil.byte2hex(arraycat));
        final byte[] order = this.makeOrder(arraycat, "LAN");
        this.log("order\uff1a" + ByteUtil.byte2hex(order));
        return order;
    }
}
