package com.gk.wifictr.net.command.station;

import android.util.*;
import com.gk.wifictr.net.command.*;
import java.io.*;
import java.util.*;

public class O88H
{
    byte[] end;
    byte[] head;
    byte[] version;
    
    public O88H() {
        this.head = new byte[] { -18, -1 };
        this.version = new byte[] { 1 };
        this.end = new byte[] { 13, -17 };
    }
    
    public static byte[] check(byte[] array) {
        byte[] array3;
        byte[] array2 = array3 = new byte[2];
        array3[0] = -18;
        array3[1] = -1;
        byte[] array5;
        byte[] array4 = array5 = new byte[2];
        array5[0] = 13;
        array5[1] = -17;
        byte[] array7;
        byte[] array6 = array7 = new byte[2];
        array7[0] = 1;
        array7[1] = 2;
        if (array[0] != array2[0] || array[1] != array2[1]) {
            Log.e("decode", "\u5934\u90e8\u9519\u8bef");
            return null;
        }
        if (array[array.length - 2] != array4[0] || array[array.length - 1] != array4[1]) {
            Log.e("decode", "\u5c3e\u90e8\u9519\u8bef");
            return null;
        }
        if (array[2] != (new byte[] { 1 })[0]) {
            Log.e("decode", "\u7248\u672c\u9519\u8bef");
            return null;
        }
        byte[] crc = ByteUtil.getCRC(ByteUtil.copyOfRange(array, 0, array.length - 4));
        if (crc[0] != array[array.length - 4] || crc[1] != array[array.length - 3]) {
            Log.e("decode", "CRC\u9519\u8bef");
            Log.e("decode", "CRC\u5e94\u4e3a" + ByteUtil.byte2hex(crc));
        }
        array6[0] = array[3];
        array6[1] = array[4];
        for (int i = 7; i < array.length - 4; ++i) {
            array[i] ^= array6[1];
            array[i] = (byte)((array[i] + (256 - array6[0])) % 256);
        }
        Log.d("decode", "\u89e3\u5bc6\u540e:" + ByteUtil.byte2hex(array));
        return ByteUtil.copyOfRange(array, 5, array.length - 4);
    }
    
    public String decodeIn(byte[] array) {
        byte[] check = check(array);
        Log.e("body[2]", new StringBuilder(String.valueOf(check[2])).toString());
        String replaceAll = "";
        //array = (byte[])(replaceAll = "");
        if (check == null) {
            return "";
        }
        //replaceAll = array;
        if (check[2] != -120) {
            return (String)replaceAll;
        }
        byte[] copyOfRange = ByteUtil.copyOfRange(check, 3, 19);
        try {
            array = (byte[])(Object)new String(copyOfRange, "UTF-8");
            replaceAll = ((String)(Object)array).replaceAll(new String(new char[1]), "");
            return (String)replaceAll;
        }
        catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            return ((String)(Object)array).replaceAll(new String(new char[1]), "");
        }
    }
    
    public byte[] makeOut(String s) {
        byte[] o = new byte[16];
        while (true) {
            try {
                byte[] bytes = s.getBytes("UTF-8");
                byte[] arraycat = ByteUtil.arraycat(new byte[] { -120 }, ByteUtil.getByteWithLength((byte[])bytes, 16));
                Random random = new Random();
                byte[] array = { (byte)random.nextInt(), (byte)(random).nextInt() };
                array[0] = -70;
                array[1] = -58;
                for (int i = 0; i < arraycat.length; ++i) {
                    arraycat[i] = (byte)((arraycat[i] + array[0]) % 256);
                    arraycat[i] ^= array[1];
                }
                byte[] arraycat2 = ByteUtil.arraycat(this.head, ByteUtil.arraycat(this.version, ByteUtil.arraycat(array, ByteUtil.arraycat(ByteUtil.getLengthByte((short)arraycat.length), arraycat))));
                return ByteUtil.arraycat(ByteUtil.arraycat(arraycat2, ByteUtil.getCRC(arraycat2)), this.end);
            }
            catch (UnsupportedEncodingException ex) {
                //Object bytes = o;
                continue;
            }
            //break;
        }
    }
}
