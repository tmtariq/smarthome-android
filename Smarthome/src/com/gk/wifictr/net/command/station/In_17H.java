package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;
import android.util.*;

public class In_17H extends Order
{
    public static final byte TYPE = 23;
    byte[] body;
    public boolean isGoodSubSn;
    String sn;
    public String subsn;
    
    public In_17H(byte[] copyOfRange) {
        this.isGoodSubSn = false;
        this.body = copyOfRange;
        Log.e("body", ByteUtil.byte2hex(copyOfRange));
        final byte[] copyOfRange2 = ByteUtil.copyOfRange(copyOfRange, 3, 15);
        copyOfRange = ByteUtil.copyOfRange(copyOfRange, 15, 19);
        Log.e("sn", ByteUtil.byte2hex(copyOfRange2));
        Log.e("subsn", ByteUtil.byte2hex(copyOfRange));
        if (copyOfRange[3] == 85) {
            this.isGoodSubSn = true;
        }
        this.sn = ByteUtil.getStringFromByte(copyOfRange2);
        this.subsn = ByteUtil.byte2hex(copyOfRange, true);
        this.sn = this.sn.replaceAll(new String(new char[1]), "");
        this.log("\u63d0\u53d6\u51faSN\u53f7\u4e3a" + this.sn);
        this.log("\u63d0\u53d6\u51fa\u5b50\u673aSN\u4e3a" + this.subsn);
    }
}
