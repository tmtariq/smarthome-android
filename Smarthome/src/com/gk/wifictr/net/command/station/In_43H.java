package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.*;

public class In_43H extends Order
{
    public static final byte TYPE = 67;
    String IP;
    String PORT;
    String SN;
    String SSID;
    byte[] body;
    String linkStatus;
    NetHelper mNetHelper;
    String returnStatus;
    
    public In_43H(final byte[] body) {
        this.body = body;
        this.SSID = ByteUtil.getStringFromByte(ByteUtil.copyOfRange(body, 15, 46));
        this.SSID = this.SSID.replaceAll(new String(new char[1]), "");
        this.log("\u63d0\u53d6\u51faSSID\u4e3a" + this.SSID);
        this.IP = ByteUtil.getStringFromByte(ByteUtil.copyOfRange(body, 47, 62));
        this.IP = this.IP.replaceAll(new String(new char[1]), "");
        this.log("\u63d0\u53d6\u51faIP\u4e3a" + this.IP);
        this.PORT = Short.toString(ByteUtil.getShortFromByte(ByteUtil.copyOfRange(body, 63, 65)));
        this.log("\u63d0\u53d6\u51fa\u7aef\u53e3\u4e3a" + this.PORT);
        if (this.IP.equals(ConnectService.LAN_SERVER_IP) && this.PORT.equals(ConnectService.LAN_SERVER_PORT)) {
            this.log("\u7aef\u53e3IP\u90fd\u6ca1\u53d8\u5316");
            return;
        }
        this.mNetHelper = new NetHelper();
        if (!this.mNetHelper.isWifiConnected()) {
            this.log("\u5f53\u524d\u7684wifi\u8fde\u63a5\u72b6\u6001\u4e3a" + this.mNetHelper.isWifiConnected());
            return;
        }
        if (this.mNetHelper.getWifiSSID().equals(this.SSID)) {
            new BroadcastHelper().changeLanIP(this.IP, this.PORT);
            this.log("IP\u4fee\u6539\u4e3a\uff1a" + this.IP + "\n\u7aef\u53e3\u4fee\u6539\u4e3a" + this.PORT);
            return;
        }
        this.log("\u5f53\u524d\u624b\u673a\u8fde\u63a5\u7684SSID\u662f" + this.mNetHelper.getWifiSSID() + "\n\u5f53\u524d\u4e3b\u673a\u8fde\u63a5\u7684SSID\u662f" + this.SSID);
    }
}
