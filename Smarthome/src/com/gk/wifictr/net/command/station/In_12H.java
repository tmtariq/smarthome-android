package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;

public class In_12H extends Order
{
    public static final byte TYPE = 18;
    byte[] body;
    String name;
    String routerPasswd;
    String routerSSID;
    String wanAdd;
    int wanPort;
    byte wlanSecurity;
    
    public In_12H(final byte[] body) {
        this.body = body;
        final byte[] copyOfLength = ByteUtil.copyOfLength(body, 15, 64);
        final byte[] copyOfLength2 = ByteUtil.copyOfLength(body, 79, 2);
        final byte[] copyOfLength3 = ByteUtil.copyOfLength(body, 81, 16);
        final byte[] copyOfLength4 = ByteUtil.copyOfLength(body, 97, 32);
        final byte[] copyOfLength5 = ByteUtil.copyOfLength(body, 130, body[129]);
        final byte wlanSecurity = body[162];
        this.wanAdd = ByteUtil.getStringFromByte(copyOfLength);
        this.wanPort = ByteUtil.getShortFromByte(copyOfLength2);
        this.name = ByteUtil.getStringFromByte(copyOfLength3);
        this.routerSSID = ByteUtil.getStringFromByte(copyOfLength4);
        this.routerPasswd = ByteUtil.getStringFromByte(copyOfLength5);
        this.wlanSecurity = wlanSecurity;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getRouterPasswd() {
        return this.routerPasswd;
    }
    
    public String getRouterSSID() {
        return this.routerSSID;
    }
    
    public String getWanAdd() {
        return this.wanAdd;
    }
    
    public int getWanPort() {
        return this.wanPort;
    }
    
    public byte getWlanSecurity() {
        return this.wlanSecurity;
    }
}
