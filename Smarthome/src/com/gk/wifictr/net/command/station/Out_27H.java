package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.data.*;
import com.gk.wifictr.net.*;
import android.util.*;

public class Out_27H extends Order
{
    public byte[] ORDER_TYPE_CODE;
    
    public Out_27H() {
        this.ORDER_TYPE_CODE = new byte[] { 39 };
    }
    
    private byte getDevCodeByte(boolean[] array) {
        int[] array2 = new int[8];
        for (int n = 0; n < array.length && n <= 3; ++n) {
            array2[n + 4] = 1;
            if (array[n]) {
                array2[n] = 1;
            }
        }
        return ByteUtil.getBit(array2);
    }
    
    public WriteData make(byte b, String s) {
        return this.make(s, (byte)80, b, ByteUtil.getEmptyByte(14));
    }
    
    public WriteData make(byte b, String s, int n) {
        return this.make(s, (byte)n, b, ByteUtil.getEmptyByte(14));
    }
    
    public WriteData make(SubDevice subDevice, byte b) {
        return this.make(subDevice.devSN, subDevice.devType, b, ByteUtil.getEmptyByte(14));
    }
    
    public WriteData make(SubDevice subDevice, byte b, byte[] array) {
        return this.make(subDevice.devSN, subDevice.devType, b, array);
    }
    
    public WriteData make(SubDevice subDevice, boolean[] array) {
        return this.make(subDevice.devSN, subDevice.devType, this.getDevCodeByte(array), ByteUtil.getEmptyByte(14));
    }
    
    public WriteData make(String s, byte b, byte b2) {
        return this.make(s, b, b2, ByteUtil.getEmptyByte(14));
    }
    
    public WriteData make(String s, byte b, byte b2, byte[] array) {
        this.routerAP = true;
        this.routerLAN = true;
        this.routerWAN = true;
        byte[] byteSN = ConnectService.defWifi.getByteSN();
        byte[] bytePassword = ConnectService.defWifi.getBytePassword();
        byte[] byteWithLength = ByteUtil.getByteWithLength(ByteUtil.hex2byte(s), 3);
        byte[] array2 = { b };
        byte[] array3 = { b2 };
        Log.e("27", "SN_NUM=" + ByteUtil.byte2hex(byteSN));
        Log.e("27", "PASSWORD=" + ByteUtil.byte2hex(bytePassword));
        Log.e("27", "DEV_SN=" + ByteUtil.byte2hex(byteWithLength));
        Log.e("27", "DEV_TYPE=" + ByteUtil.byte2hex(array2));
        Log.e("27", "DEV_CODE=" + ByteUtil.byte2hex(array3));
        Log.e("27", "AC_CODE=" + ByteUtil.byte2hex(array));
        byte[] arraycat = ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(new byte[0], this.ORDER_TYPE_CODE), byteSN), bytePassword), byteWithLength), array2), array3), array);
        this.log("body\uff1a" + ByteUtil.byte2hex(arraycat));
        return new WriteData(this.makeOrder(arraycat, "LAN"), this.makeOrder(arraycat, "WAN"), this.routerAP, this.routerLAN, this.routerWAN);
    }
    
    public WriteData make(String s, byte b, boolean[] array) {
        return this.make(s, b, this.getDevCodeByte(array), ByteUtil.getEmptyByte(14));
    }
    
    public WriteData makeBath(int n) {
        return this.make("000000", (byte)48, (byte)n, ByteUtil.getEmptyByte(14));
    }
}
