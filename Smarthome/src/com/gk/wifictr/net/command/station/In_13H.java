package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.data.*;
import com.gk.wifictr.net.command.*;
import android.util.*;
import java.io.*;
import java.util.*;
import java.lang.reflect.*;

public class In_13H extends Order
{
    public static byte TYPE = 19;
    public byte[] body;
    public ArrayList<SubDevice> devices;
    
    public In_13H() {
    }
    
    public In_13H(byte[] array) {
        this.body = ByteUtil.copyOfRange(array, 0, array.length);
    }
    
    public ArrayList<SubDevice> decode(ReadData readData) {
        if (readData.goodData == 0) {
            this.devices = new ArrayList<SubDevice>();
            byte[] body = readData.body;
            Log.e("\u540e\u9762\u6570\u636e\u957f\u5ea6", new StringBuilder(String.valueOf(ByteUtil.getShortFromByte(new byte[] { body[0], body[1] }))).toString());
            Log.e("\u6307\u4ee4\u7c7b\u578b", ByteUtil.byte2hex(body[2]));
            byte[] array = new byte[12];
            for (int i = 0; i < 12; ++i) {
                array[i] = body[i + 3];
            }
            String stringFromByte = ByteUtil.getStringFromByte(array);
            Log.e("\u4e3b\u673asn", ByteUtil.byte2hex(array));
            Log.e("\u4e3b\u673asn", stringFromByte);
            byte b = body[15];
            Log.e("\u8bbe\u5907\u6570", String.valueOf(ByteUtil.byte2hex(body[15])) + "  " + b);
            //readData = (ReadData)(Object)ByteUtil.copyOfRange(body, 16, (b - 1) * 22 + 16 + 22);
            byte[] btar1 = ByteUtil.copyOfRange(body, 16, (b - 1) * 22 + 16 + 22);
            Log.e("divices", ByteUtil.byte2hex(btar1));
            ArrayList<byte[]> list = new ArrayList<byte[]>();
            for (byte b2 = 0; b2 < b; ++b2) {
                list.add(ByteUtil.copyOfRange(btar1, b2 * 22, b2 * 22 + 22));
            }
            Log.d("", "-------------------------");
            for (byte[] array2 : list) {
                if (array2.length > 0) {
                    byte b3 = array2[0];
                    Log.e("\u8bbe\u5907\u7f16\u53f7", ByteUtil.byte2hex(array2[0]));
                    byte[] array3 = new byte[16];
                    int n = 1;
                    while (true) {
                        //Label_0498: {
                            
                            //readData = (ReadData)"";
                            String str = "";
                            while (true) {
                                try {
                                	str = new String(array3, "UTF-8");
                                	str = str.replaceAll(new String(new char[1]), "");
                                    Log.e("\u8bbe\u5907\u6635\u79f0", (String)str);
                                    String byte2hex = ByteUtil.byte2hex(new byte[] { array2[17], array2[18], array2[19] }, true);
                                    Log.e("\u8bbe\u5907SN\u53f7", byte2hex);
                                    byte b4 = array2[20];
                                    Log.e("\u8bbe\u5907\u7c7b\u578b", ByteUtil.byte2hex(array2[20]));
                                    byte b5 = array2[21];
                                    Log.e("\u8bbe\u5907\u72b6\u6001", ByteUtil.byte2hex(array2[21]));
                                    this.devices.add(new SubDevice(b3, (String)str, byte2hex, b4, b5));
                                    Log.i("", "---------------");
                                    //break;
                                    array3[n - 1] = array2[n];
                                    ++n;
                                }
                                catch (UnsupportedEncodingException ex) {
                                    ex.printStackTrace();
                                    continue;
                                }
                                break;
                            }
                            
                            if (n <= 16) {
                                break;// Label_0498;
                            }
                        //}
                    }
                }
            }
        }
        return this.devices;
    }
    
    public ArrayList<SubDevice> getDevices() {
        this.devices = new ArrayList<SubDevice>();
        byte b = this.body[15];
        Object copyOfRange = ByteUtil.copyOfRange(this.body, 17, (b - 1) * 22 + 17 + 21);
        byte[][] array = (byte[][])Array.newInstance(Byte.TYPE, b, 21);
        for (byte b2 = 0; b2 < b; ++b2) {
            array[b2] = ByteUtil.copyOfRange((byte[])copyOfRange, b2 * 22, b2 * 22 + 21);
        }
        for (int length = array.length, i = 0; i < length; ++i) {
            byte[] array2 = array[i];
            byte b3 = array2[0];
            byte[] array3 = new byte[16];
            int n = 0;
            while (true) {
                Label_0244: {
                    if (n < 17) {
                        break Label_0244;
                    }
                    copyOfRange = "";
                    while (true) {
                        try {
                            copyOfRange = new String(array3, "UTF-8");
                            this.devices.add(new SubDevice(b3, (String)copyOfRange, ByteUtil.byte2hex(new byte[] { array2[17], array2[18], array2[19] }), array2[20], array2[21]));
                            //break;
                            array3[n] = array2[n + 1];
                            ++n;
                        }
                        catch (UnsupportedEncodingException ex) {
                            ex.printStackTrace();
                            continue;
                        }
                        break;
                    }
                }
            }
        }
        return this.devices;
    }
}
