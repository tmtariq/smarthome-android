package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.*;

public class In_61H extends Order
{
    public static final byte TYPE = 97;
    byte[] body;
    
    public In_61H(byte[] copyOfRange) {
        this.body = copyOfRange;
        final byte[] copyOfRange2 = ByteUtil.copyOfRange(copyOfRange, 3, 15);
        copyOfRange = ByteUtil.copyOfRange(copyOfRange, 15, 30);
        final String stringFromByte = ByteUtil.getStringFromByte(copyOfRange2);
        final String stringFromByte2 = ByteUtil.getStringFromByte(copyOfRange);
        final String replaceAll = stringFromByte.replaceAll(new String(new char[1]), "");
        final String replaceAll2 = stringFromByte2.replaceAll(new String(new char[1]), "");
        ConnectService.defWifi.SN = replaceAll;
        ConnectService.defWifi.password = replaceAll2;
        ConnectService.defWifi.save(ConnectService.defWifinum);
        this.log("\u63d0\u53d6\u51faSN\u53f7\u4e3a" + replaceAll);
        this.log("\u63d0\u53d6\u51fa\u5bc6\u7801\u4e3a" + replaceAll2);
        new BroadcastHelper().gotSN(replaceAll, replaceAll2);
    }
}
