package com.gk.wifictr.net.command.station;

import android.util.*;
import com.gk.wifictr.net.command.*;
import java.util.*;

public class Order
{
    public static byte[] IN_LAN_END_CODE;
    public static byte[] IN_WAN_END_CODE;
    public static byte[] IN_WAN_HEAD_CODE;
    public static byte[] IN_lAN_HEAD_CODE;
    public static byte[] OUT_LAN_END_CODE;
    public static byte[] OUT_LAN_HEAD_CODE;
    public static byte[] OUT_WAN_END_CODE;
    public static byte[] OUT_WAN_HEAD_CODE;
    public static byte[] VERSION_CODE;
    public boolean routerAP;
    public boolean routerLAN;
    public boolean routerUDP;
    public boolean routerWAN;
    
    static {
        VERSION_CODE = new byte[] { 1 };
        OUT_WAN_HEAD_CODE = new byte[] { 46, 46 };
        OUT_LAN_HEAD_CODE = new byte[] { 62, 62 };
        IN_lAN_HEAD_CODE = new byte[] { -30, -30 };
        IN_WAN_HEAD_CODE = new byte[] { -29, -29 };
        OUT_LAN_END_CODE = new byte[] { 13, -17 };
        OUT_WAN_END_CODE = new byte[] { 13, -17 };
        IN_LAN_END_CODE = new byte[] { 13, -17 };
        IN_WAN_END_CODE = new byte[] { 13, -17 };
    }
    
    public Order() {
        this.routerAP = true;
        this.routerLAN = true;
        this.routerWAN = true;
        this.routerUDP = true;
    }
    
    public void log(String s) {
        Log.v("MakeOrder", s);
    }
    
    public byte[] makeOrder(byte[] array, String s) {
        array = ByteUtil.copyOfRange(array, 0, array.length);
        Random random = new Random();
        byte[] array2 = { (byte)random.nextInt(), (byte)random.nextInt() };
        array2[0] = -70;
        array2[1] = -58;
        for (int i = 0; i < array.length; ++i) {
            array[i] = (byte)((array[i] + array2[0]) % 256);
            array[i] ^= array2[1];
        }
        array = ByteUtil.arraycat(array2, ByteUtil.arraycat(ByteUtil.getLengthByte((short)array.length), array));
        array = ByteUtil.arraycat(Order.VERSION_CODE, array);
        if (s.equals("WAN")) {
            array = ByteUtil.arraycat(Order.OUT_WAN_HEAD_CODE, array);
        }
        else {
            array = ByteUtil.arraycat(Order.OUT_LAN_HEAD_CODE, array);
        }
        array = ByteUtil.arraycat(array, ByteUtil.getCRC(array));
        if (s.equals("WAN")) {
            array = ByteUtil.arraycat(array, Order.OUT_WAN_END_CODE);
        }
        else {
            array = ByteUtil.arraycat(array, Order.OUT_LAN_END_CODE);
        }
        this.log(String.valueOf(s) + " Order\uff1a" + ByteUtil.byte2hex(array));
        return array;
    }
}
