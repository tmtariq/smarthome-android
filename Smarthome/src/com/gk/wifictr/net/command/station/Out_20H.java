package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.*;
import com.gk.wifictr.net.command.*;

public class Out_20H extends Order
{
    public final byte[] ORDER_TYPE_CODE;
    
    public Out_20H() {
        this.ORDER_TYPE_CODE = new byte[] { 32 };
    }
    
    public WriteData make(final String s) {
        return this.make(ConnectService.defWifi.password, s);
    }
    
    public WriteData make(final String s, final int n, final String s2, final String s3, final String s4, final byte b) {
        this.routerAP = true;
        this.routerLAN = true;
        this.routerWAN = true;
        final byte[] arraycat = ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(new byte[0], this.ORDER_TYPE_CODE), ConnectService.defWifi.getByteSN()), ConnectService.defWifi.getBytePassword()), ByteUtil.getAsciiByte(s, 64)), ByteUtil.swapByte(ByteUtil.getByte((short)n))), ByteUtil.getAsciiByte(s2, 16)), ConnectService.defWifi.getBytePassword()), ByteUtil.getAsciiByte(s3, 32)), new byte[] { (byte)s4.length() }), ByteUtil.getAsciiByte(s4, 16)), new byte[] { b });
        this.log("body\uff1a" + ByteUtil.byte2hex(arraycat));
        return new WriteData(this.makeOrder(arraycat, "LAN"), this.makeOrder(arraycat, "WAN"), this.routerAP, this.routerLAN, this.routerWAN);
    }
    
    public WriteData make(final String s, final String s2) {
        this.routerAP = true;
        this.routerLAN = true;
        this.routerWAN = true;
        final byte[] arraycat = ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(new byte[0], this.ORDER_TYPE_CODE), ConnectService.defWifi.getByteSN()), ByteUtil.getAsciiByte(s, 16)), ByteUtil.getEmptyByte(64)), ByteUtil.getEmptyByte(2)), ByteUtil.getEmptyByte(16)), ByteUtil.getAsciiByte(s2, 16)), ByteUtil.getEmptyByte(32)), ByteUtil.getEmptyByte(1)), ByteUtil.getEmptyByte(16)), ByteUtil.getEmptyByte(1));
        this.log("body\uff1a" + ByteUtil.byte2hex(arraycat));
        return new WriteData(this.makeOrder(arraycat, "LAN"), this.makeOrder(arraycat, "WAN"), this.routerAP, this.routerLAN, this.routerWAN);
    }
    
    public WriteData make(final String s, final String s2, final String s3, final byte b) {
        return this.make("", 0, s, s2, s3, b);
    }
    
    public WriteData make(final String s, final String s2, final String s3, final String s4, final int n, final byte b) {
        return this.make(s4, n, s, s2, s3, b);
    }
}
