package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.*;

public class Out_EEH extends Order
{
    public final byte[] ORDER_TYPE_CODE;
    
    public Out_EEH() {
        this.ORDER_TYPE_CODE = new byte[] { -18 };
    }
    
    public WriteData make() {
        this.routerAP = true;
        this.routerLAN = false;
        this.routerWAN = false;
        final byte[] arraycat = ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(new byte[0], this.ORDER_TYPE_CODE), ConnectService.defWifi.getByteSN()), ConnectService.defWifi.getByteSuperPassword());
        this.log("body\uff1a" + ByteUtil.byte2hex(arraycat));
        return new WriteData(this.makeOrder(arraycat, "LAN"), this.makeOrder(arraycat, "WAN"), this.routerAP, this.routerLAN, this.routerWAN);
    }
}
