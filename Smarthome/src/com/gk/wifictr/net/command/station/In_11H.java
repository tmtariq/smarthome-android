package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.*;

public class In_11H extends Order
{
    public static final byte TYPE = 17;
    String IP;
    String PORT;
    String SN;
    byte[] body;
    String linkStatus;
    String returnStatus;
    
    public In_11H(final byte[] body) {
        this.body = body;
        this.IP = ByteUtil.getStringFromByte(ByteUtil.copyOfRange(body, 15, 30));
        this.IP = this.IP.replaceAll(new String(new char[1]), "");
        this.log("\u63d0\u53d6\u51faIP\u4e3a" + this.IP);
        this.log("\u8f6c\u6362\u540e\u7684IP\u4e3a" + this.IP);
        this.PORT = Short.toString(ByteUtil.getShortFromByte(ByteUtil.copyOfRange(body, 31, 33)));
        this.log("\u63d0\u53d6\u51fa\u7aef\u53e3\u4e3a" + this.PORT);
        if (this.IP.equals(ConnectService.LAN_SERVER_IP) && this.PORT.equals(ConnectService.LAN_SERVER_PORT)) {
            this.log("\u7aef\u53e3IP\u90fd\u6ca1\u53d8\u5316");
            return;
        }
        new BroadcastHelper().changeLanIP(this.IP, this.PORT);
    }
}
