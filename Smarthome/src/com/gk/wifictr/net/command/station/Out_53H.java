package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.*;

public class Out_53H extends Order
{
    public final byte[] ORDER_TYPE_CODE;
    
    public Out_53H() {
        this.ORDER_TYPE_CODE = new byte[] { 83 };
    }
    
    public WriteData make(final String s, final byte b, final byte b2, final byte b3, final byte b4, final int n, final int n2) {
        this.routerAP = true;
        this.routerLAN = true;
        this.routerWAN = true;
        final byte[] arraycat = ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(new byte[0], this.ORDER_TYPE_CODE), ConnectService.defWifi.getByteSN()), ConnectService.defWifi.getBytePassword()), ByteUtil.getByteWithLength(ByteUtil.hex2byte(s), 3)), new byte[] { b }), new byte[] { b2, b3, b4 }), new byte[] { (byte)n, (byte)n2 });
        this.log("body\uff1a" + ByteUtil.byte2hex(arraycat));
        return new WriteData(this.makeOrder(arraycat, "LAN"), this.makeOrder(arraycat, "WAN"), this.routerAP, this.routerLAN, this.routerWAN);
    }
}
