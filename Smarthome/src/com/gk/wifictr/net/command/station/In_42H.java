package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;

public class In_42H extends Order
{
    public static final byte DEVICE_OFF_LINE = 0;
    public static final byte DEVICE_ON_LINE = 1;
    public static final byte ONLINE_BUT_ERROR = 2;
    public static final byte TYPE = 66;
    public byte onlineType;
    
    public In_42H(final ReadData readData) {
        this.onlineType = readData.returnbyte;
    }
    
    public byte devIsOnline() {
        return this.onlineType;
    }
}
