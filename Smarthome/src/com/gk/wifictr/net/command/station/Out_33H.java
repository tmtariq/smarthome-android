package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.*;

public class Out_33H extends Order
{
    public final byte[] ORDER_TYPE_CODE;
    
    public Out_33H() {
        this.ORDER_TYPE_CODE = new byte[] { 51 };
    }
    
    public WriteData make() {
        this.routerAP = false;
        this.routerLAN = false;
        this.routerWAN = true;
        final byte[] arraycat = ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(new byte[0], this.ORDER_TYPE_CODE), ConnectService.defWifi.getByteSN()), ConnectService.defWifi.getBytePassword());
        this.log("33Hbody\uff1a" + ByteUtil.byte2hex(arraycat));
        return new WriteData(this.makeOrder(arraycat, "LAN"), this.makeOrder(arraycat, "WAN"), this.routerAP, this.routerLAN, this.routerWAN);
    }
}
