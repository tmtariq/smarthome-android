package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.*;

public class Out_24H extends Order
{
    public final byte[] ORDER_TYPE_CODE;
    
    public Out_24H() {
        this.ORDER_TYPE_CODE = new byte[] { 36 };
    }
    
    public WriteData make(final int n, final String s, final byte b) {
        this.routerAP = true;
        this.routerLAN = true;
        this.routerWAN = true;
        final byte[] arraycat = ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(new byte[0], this.ORDER_TYPE_CODE), ConnectService.defWifi.getByteSN()), ConnectService.defWifi.getBytePassword()), new byte[] { (byte)n }), ByteUtil.hex2byte(s)), new byte[] { b });
        this.log("body\uff1a" + ByteUtil.byte2hex(arraycat));
        return new WriteData(this.makeOrder(arraycat, "LAN"), this.makeOrder(arraycat, "WAN"), this.routerAP, this.routerLAN, this.routerWAN);
    }
}
