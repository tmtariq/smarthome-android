package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.command.*;

public class In_16H extends Order
{
    public static final byte TYPE = 22;
    byte[] IR_code;
    byte[] body;
    int code_length;
    int code_num;
    public byte code_type;
    short length;
    
    public In_16H(final byte[] body) {
        this.body = body;
        this.length = ByteUtil.getShortFromByte(new byte[] { body[0], body[1] });
        this.code_length = this.length - 33 + 16;
        this.code_type = body[15];
        this.code_num = body[16];
        this.IR_code = ByteUtil.copyOfRange(body, 17, body.length - 2);
    }
    
    public byte[] get_IR_code() {
        return this.IR_code = ByteUtil.copyOfRange(this.body, 17, this.body.length - 2);
    }
    
    public int get_code_num() {
        return this.code_num = this.body[16];
    }
}
