package com.gk.wifictr.net.command.station;

import com.gk.wifictr.net.*;
import com.gk.wifictr.net.command.*;

public class Out_55H extends Order
{
    public final byte[] ORDER_TYPE_CODE;
    
    public Out_55H() {
        this.ORDER_TYPE_CODE = new byte[] { 85 };
    }
    
    public WriteData make(final boolean b) {
        this.routerAP = true;
        this.routerLAN = true;
        this.routerWAN = true;
        final byte[] byteSN = ConnectService.defWifi.getByteSN();
        final byte[] bytePassword = ConnectService.defWifi.getBytePassword();
        final byte[] array = { 0 };
        if (b) {
            array[0] = -1;
        }
        else {
            array[0] = 0;
        }
        final byte[] arraycat = ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(ByteUtil.arraycat(new byte[0], this.ORDER_TYPE_CODE), byteSN), bytePassword), array);
        this.log("body\uff1a" + ByteUtil.byte2hex(arraycat));
        return new WriteData(this.makeOrder(arraycat, "LAN"), this.makeOrder(arraycat, "WAN"), this.routerAP, this.routerLAN, this.routerWAN);
    }
}
