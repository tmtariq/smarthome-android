package com.gk.wifictr.net.command;

import android.util.*;
import android.annotation.*;

public class ByteUtil
{
    public static final byte[] LAN_END_CODE;
    public static final byte[] LAN_HEAD_CODE;
    public static final byte[] VERSION_CODE;
    public static final byte[] WAN_END_CODE;
    public static final byte[] WAN_HEAD_CODE;
    
    static {
        WAN_HEAD_CODE = new byte[] { 62, 62 };
        LAN_HEAD_CODE = new byte[] { 62, 62 };
        VERSION_CODE = new byte[] { 1 };
        WAN_END_CODE = new byte[] { 13, -17 };
        LAN_END_CODE = new byte[] { 13, -17 };
    }
    
    public static byte[] arraycat(final byte[] array, final byte[] array2) {
        Object o = null;
        int length = 0;
        int length2 = 0;
        if (array != null) {
            length = array.length;
        }
        if (array2 != null) {
            length2 = array2.length;
        }
        if (length + length2 > 0) {
            o = new byte[length + length2];
        }
        if (length > 0) {
            System.arraycopy(array, 0, o, 0, length);
        }
        if (length2 > 0) {
            System.arraycopy(array2, 0, o, length, length2);
        }
        return (byte[])o;
    }
    
    public static boolean[] byte2bit(final byte b) {
        final boolean[] array = new boolean[8];
        for (int i = 0; i < array.length; ++i) {
            array[i] = ((b >> i & 0x1) == 0x1);
        }
        return array;
    }
    
    public static String byte2hex(final byte b) {
        return byte2hex(new byte[] { b });
    }
    
    public static String byte2hex(final byte[] array) {
        String string = "";
        for (int i = 0; i < array.length; ++i) {
            String s2;
            final String s = s2 = Integer.toHexString(array[i] & 0xFF);
            if (s.length() == 1) {
                s2 = "0" + s;
            }
            string = String.valueOf(string) + " " + s2;
        }
        return string;
    }
    
    public static String byte2hex(final byte[] array, final boolean b) {
        if (b) {
            String string = "";
            for (int i = 0; i < array.length; ++i) {
                String s2;
                final String s = s2 = Integer.toHexString(array[i] & 0xFF);
                if (s.length() == 1) {
                    s2 = "0" + s;
                }
                string = String.valueOf(string) + s2;
            }
            return string;
        }
        return byte2hex(array);
    }
    
    private static byte charToByte(final char c) {
        return (byte)"0123456789ABCDEF".indexOf(c);
    }
    
    public static byte[] copyOfLength(final byte[] array, final int n, final int n2) {
        if (n2 < 0 || n + n2 > array.length || n > array.length) {
            return new byte[0];
        }
        final byte[] array2 = new byte[n2];
        System.arraycopy(array, n, array2, 0, Math.min(array.length - n, n2));
        return array2;
    }
    
    public static byte[] copyOfRange(final byte[] array, final int n, final int n2) {
        final int n3 = n2 - n;
        if (n3 < 0 || n2 > array.length || n > array.length) {
            return new byte[0];
        }
        final byte[] array2 = new byte[n3];
        System.arraycopy(array, n, array2, 0, Math.min(array.length - n, n3));
        return array2;
    }
    
    public static byte[] getAsciiByte(final String s) {
        final byte[] array = new byte[s.length()];
        for (int length = array.length, i = 0; i < length; ++i) {
            array[array[i]] = 0;
        }
        final char[] charArray = s.toCharArray();
        for (int j = 0; j < charArray.length; ++j) {
            array[j] = (byte)charArray[j];
        }
        return array;
    }
    
    public static byte[] getAsciiByte(final String s, final int n) {
        final byte[] array = new byte[n];
        for (int length = array.length, i = 0; i < length; ++i) {
            array[array[i]] = 0;
        }
        final char[] charArray = s.toCharArray();
        for (int n2 = 0; n2 < charArray.length && n2 < n; ++n2) {
            array[n2] = (byte)charArray[n2];
        }
        return array;
    }
    
    public static byte getBit(final int[] array) {
        byte b = 0;
        final int[] array2 = new int[8];
        for (int i = 0; i < array2.length; ++i) {
            if (i < array.length) {
                array2[i] = array[i];
            }
            else {
                array2[i] = 0;
            }
        }
        for (int j = 7; j >= 0; --j) {
            final byte b2 = b <<= 1;
            if (array2[j] == 1) {
                b = (byte)(b2 | 0x1);
            }
        }
        return b;
    }
    
    public static byte[] getByte(final short n) {
        return new byte[] { (byte)(n >> 8), (byte)(n >> 0) };
    }
    
    public static byte[] getByteWithCRC(final byte[] array) {
        return arraycat(arraycat(array, getCRC(array)), new byte[] { 13, -17 });
    }
    
    public static byte[] getByteWithLength(final byte[] array, int n) {
        byte[] emptyByte;
        if (array == null) {
            emptyByte = getEmptyByte(n);
        }
        else {
            final byte[] array2 = new byte[n];
            n = 0;
            while (true) {
                emptyByte = array2;
                if (n >= array.length) {
                    break;
                }
                if (n < array2.length) {
                    array2[n] = array[n];
                }
                ++n;
            }
        }
        return emptyByte;
    }
    
    public static byte[] getCRC(final byte[] array) {
        int n = 65535;
        for (int i = 0; i < array.length; ++i) {
            n ^= (array[i] & 0xFF);
            for (int j = 0; j < 8; ++j) {
                if ((n & 0x1) != 0x0) {
                    n = (n >> 1 ^ 0xA001);
                }
                else {
                    n >>= 1;
                }
            }
        }
        return new byte[] { (byte)(n & 0xFF), (byte)(n >> 8 & 0xFF) };
    }
    
    public static byte[] getEmptyByte(int i) {
        final byte[] array = new byte[i];
        int length;
        for (length = array.length, i = 0; i < length; ++i) {
            array[array[i]] = 0;
        }
        return array;
    }
    
    public static byte[] getLengthByte(final short n) {
        final byte[] array = { (byte)(n & 0xFF), (byte)(n >> 8 & 0xFF) };
        Log.d("getLengthByte", String.valueOf(array[0]) + "&&" + array[1]);
        return array;
    }
    
    public static short getShortFromByte(final byte[] array) {
        return (short)(array[1] << 8 | (array[0] & 0xFF));
    }
    
    public static String getStringFromByte(final byte[] array) {
        final char[] array2 = new char[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = (char)array[i];
        }
        return new String(array2);
    }
    
    @SuppressLint({ "DefaultLocale" })
    public static byte[] hex2byte(String upperCase) {
        byte[] array;
        if (upperCase == null || upperCase.equals("")) {
            array = null;
        }
        else {
            upperCase = upperCase.replaceAll(" ", "").toUpperCase();
            final int n = upperCase.length() / 2;
            final char[] charArray = upperCase.toCharArray();
            final byte[] array2 = new byte[n];
            int n2 = 0;
            while (true) {
                array = array2;
                if (n2 >= n) {
                    break;
                }
                final int n3 = n2 * 2;
                array2[n2] = (byte)(charToByte(charArray[n3]) << 4 | charToByte(charArray[n3 + 1]));
                ++n2;
            }
        }
        return array;
    }
    
    public static byte[] swapByte(final byte[] array) {
        return new byte[] { array[1], array[0] };
    }
}
