package com.gk.wifictr.net.command;

import android.os.*;
import android.util.*;

public class ReadData implements Parcelable
{
    public static final Parcelable.Creator<ReadData> CREATOR;
    public static final int DATA_CRC_ERR = 1;
    public static final int DATA_END_ERR = 3;
    public static final int DATA_GOOD = 0;
    public static final int DATA_HEAD_ERR = 2;
    public static final int DATA_TYPE_ERR = 5;
    public static final int DATA_VERSION_ERR = 4;
    public static final byte[] LAN_END_CODE;
    public static final byte[] READ_LAN_HEAD_CODE;
    public static final byte[] READ_WAN_HEAD_CODE;
    public static final byte[] VERSION_CODE;
    public static final byte[] WAN_END_CODE;
    public boolean ERR_CRC_WRONG;
    public boolean ERR_OPERATION_FAILURE;
    public boolean ERR_OUTOF_MAXTCPLINK;
    public boolean ERR_PASSWD_WRONG;
    public boolean ERR_SN_WRONG;
    public boolean ERR_VERSION_NO_WRONG;
    public boolean ROUTER_IS_LINK;
    public boolean SERVER_IS_LINK;
    public byte[] body;
    public String fromIP;
    public String fromPort;
    public String fromType;
    public int goodData;
    public boolean[] linkByte;
    public byte[] raw;
    public boolean[] returnByte;
    public byte returnbyte;
    public byte type;
    
    static {
        READ_WAN_HEAD_CODE = new byte[] { -30, -30 };
        READ_LAN_HEAD_CODE = new byte[] { -29, -29 };
        VERSION_CODE = new byte[] { 1 };
        LAN_END_CODE = new byte[] { 13, -17 };
        WAN_END_CODE = new byte[] { 13, -17 };
        CREATOR = new Parcelable.Creator<ReadData>() {
            public ReadData createFromParcel(final Parcel parcel) {
                return new ReadData(parcel);
            }
            
            public ReadData[] newArray(final int n) {
                return new ReadData[n];
            }
        };
    }
    
    public ReadData(final Parcel parcel) {
        this.goodData = 0;
        parcel.setDataPosition(1);
        this.fromIP = parcel.readString();
        parcel.setDataPosition(2);
        this.fromPort = parcel.readString();
        parcel.setDataPosition(3);
        parcel.readByteArray(this.raw);
        this.decode();
    }
    
    public ReadData(final String fromIP, final String fromPort, final byte[] raw) {
        this.goodData = 0;
        this.fromIP = fromIP;
        this.fromPort = fromPort;
        this.raw = raw;
        this.decode();
    }
    
    public ReadData(final byte[] raw) {
        this.goodData = 0;
        this.raw = raw;
        this.decode();
    }
    
    public byte[] decode() {
        final byte[] raw = this.raw;
        final byte[] array2;
        final byte[] array = array2 = new byte[2];
        array2[0] = 1;
        array2[1] = 2;
        if (raw[0] == ReadData.READ_WAN_HEAD_CODE[0] && raw[1] == ReadData.READ_WAN_HEAD_CODE[1]) {
            this.fromType = "WAN";
        }
        else {
            if (raw[0] != ReadData.READ_LAN_HEAD_CODE[0] || raw[1] != ReadData.READ_LAN_HEAD_CODE[1]) {
                this.goodData = 2;
                Log.e("decode", "\u5934\u90e8\u9519\u8bef");
                return this.body = this.raw;
            }
            this.fromType = "LAN";
        }
        if (raw[raw.length - 2] != ReadData.LAN_END_CODE[0] || raw[raw.length - 1] != ReadData.LAN_END_CODE[1]) {
            this.goodData = 3;
            Log.e("decode", "\u5c3e\u90e8\u9519\u8bef");
            return this.body = this.raw;
        }
        if (raw[2] != ReadData.VERSION_CODE[0]) {
            this.goodData = 4;
            Log.e("decode", "\u7248\u672c\u9519\u8bef");
            return this.body = this.raw;
        }
        final byte[] crc = ByteUtil.getCRC(ByteUtil.copyOfRange(raw, 0, raw.length - 4));
        if (crc[0] != raw[raw.length - 4] || crc[1] != raw[raw.length - 3]) {
            this.goodData = 1;
            Log.e("decode", "CRC\u9519\u8bef");
            Log.e("decode", "CRC\u5e94\u4e3a" + ByteUtil.byte2hex(crc));
        }
        array[0] = raw[3];
        array[1] = raw[4];
        for (int i = 7; i < raw.length - 4; ++i) {
            raw[i] ^= array[1];
            raw[i] = (byte)((raw[i] + (256 - array[0])) % 256);
        }
        Log.d("decode", "\u89e3\u5bc6\u540e:" + ByteUtil.byte2hex(raw));
        this.body = ByteUtil.copyOfRange(raw, 5, raw.length - 4);
        Log.d("decode", "body:" + ByteUtil.byte2hex(this.body));
        this.type = this.body[2];
        Log.d("decode", "type:" + ByteUtil.byte2hex(this.type));
        this.linkByte = ByteUtil.byte2bit(this.body[this.body.length - 2]);
        this.returnByte = ByteUtil.byte2bit(this.body[this.body.length - 1]);
        this.ROUTER_IS_LINK = this.linkByte[0];
        this.SERVER_IS_LINK = this.linkByte[1];
        this.ERR_CRC_WRONG = this.returnByte[0];
        this.ERR_PASSWD_WRONG = this.returnByte[1];
        this.ERR_SN_WRONG = this.returnByte[2];
        this.ERR_VERSION_NO_WRONG = this.returnByte[3];
        this.ERR_OUTOF_MAXTCPLINK = this.returnByte[4];
        this.ERR_OPERATION_FAILURE = this.returnByte[5];
        return this.body;
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public String toString() {
        return ByteUtil.byte2hex(this.raw);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.setDataPosition(1);
        parcel.writeString(this.fromIP);
        parcel.setDataPosition(2);
        parcel.writeString(this.fromPort);
        parcel.setDataPosition(3);
        parcel.writeByteArray(this.raw);
    }
}
