package com.gk.wifictr.net.command;

import android.os.*;

public class WriteData implements Parcelable
{
    public static Parcelable.Creator<WriteData> CREATOR;
    public boolean AP_CAN_SEND;
    public boolean LAN_CAN_SEND;
    public byte[] LanBody;
    public boolean WAN_CAN_SEND;
    public byte[] WanBody;
    public boolean[] router;
    
    static {
        CREATOR = new Parcelable.Creator<WriteData>() {
            public WriteData createFromParcel(Parcel parcel) {
                return new WriteData(parcel);
            }
            
            public WriteData[] newArray(int n) {
                return new WriteData[n];
            }
        };
    }
    
    public WriteData(Parcel parcel) {
        parcel.setDataPosition(1);
        parcel.readByteArray(this.LanBody);
        parcel.setDataPosition(2);
        parcel.readByteArray(this.WanBody);
        parcel.setDataPosition(3);
        parcel.readBooleanArray(this.router);
        this.AP_CAN_SEND = this.router[0];
        this.LAN_CAN_SEND = this.router[1];
        this.WAN_CAN_SEND = this.router[2];
    }
    
    public WriteData(byte[] lanBody, byte[] wanBody, boolean ap_CAN_SEND, boolean lan_CAN_SEND, boolean wan_CAN_SEND) {
        this.LanBody = lanBody;
        this.WanBody = wanBody;
        this.AP_CAN_SEND = ap_CAN_SEND;
        this.LAN_CAN_SEND = lan_CAN_SEND;
        this.WAN_CAN_SEND = wan_CAN_SEND;
        this.router = new boolean[] { ap_CAN_SEND, lan_CAN_SEND, wan_CAN_SEND };
    }
    
    public WriteData(byte[] lanBody, byte[] wanBody, boolean[] router) {
        this.LanBody = lanBody;
        this.WanBody = wanBody;
        this.router = router;
        this.AP_CAN_SEND = router[0];
        this.LAN_CAN_SEND = router[1];
        this.WAN_CAN_SEND = router[2];
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void writeToParcel(Parcel parcel, int n) {
        parcel.setDataPosition(1);
        parcel.writeByteArray(this.LanBody);
        parcel.setDataPosition(2);
        parcel.writeByteArray(this.WanBody);
        parcel.setDataPosition(3);
        parcel.writeBooleanArray(this.router = new boolean[] { this.AP_CAN_SEND, this.LAN_CAN_SEND, this.WAN_CAN_SEND });
    }
}
