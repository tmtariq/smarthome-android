package com.gk.wifictr.net.data;

import android.preference.*;
import com.gk.wifictr.net.command.*;
import com.gk.wifictr.net.*;
import android.content.*;

public class StationWifi
{
    public static final byte WLAN_SECURITY_AUTO = 5;
    public static final byte WLAN_SECURITY_NONE = 0;
    public static final byte WLAN_SECURITY_WEP_OPEN = 1;
    public static final byte WLAN_SECURITY_WEP_SHARED = 2;
    public static final byte WLAN_SECURITY_WPA = 3;
    public static final byte WLAN_SECURITY_WPA12 = 7;
    public static final byte WLAN_SECURITY_WPA2 = 4;
    public static final byte WLAN_SECURITY_WPS = 6;
    public String SN;
    public String name;
    public String password;
    public boolean power;
    public String routerPassword;
    public String routerSSID;
    public byte routerSecurityType;
    public String routerState;
    public String serverAdd;
    public short serverPort;
    
    public StationWifi(final int n) {
        this.password = "123456";
        this.serverAdd = "112.124.48.49";
        this.serverPort = 10003;
        this.power = false;
        this.routerSecurityType = 7;
        final SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ConnectService.getInstance());
        this.SN = defaultSharedPreferences.getString("SN*No." + n, "");
        this.password = defaultSharedPreferences.getString("password*No." + n, "12345678");
        this.serverAdd = defaultSharedPreferences.getString("serverAdd*No." + n, "112.124.48.49");
        this.serverPort = (short)defaultSharedPreferences.getInt("serverPort*No." + n, 10003);
        this.name = defaultSharedPreferences.getString("name*No." + n, "");
        this.routerSSID = defaultSharedPreferences.getString("routerSSID*No." + n, "");
        this.routerPassword = defaultSharedPreferences.getString("routerPassword*No." + n, "");
        this.routerSecurityType = (byte)defaultSharedPreferences.getInt("routerSecurityType*No." + n, 7);
        ConnectService.LAN_SERVER_IP = defaultSharedPreferences.getString("LAN_SERVER_IP*No." + n, "192.168.0.1");
        ConnectService.LAN_SERVER_PORT = defaultSharedPreferences.getString("LAN_SERVER_PORT*No." + n, "8080");
    }
    
    public byte[] getByteName() {
        return ByteUtil.getAsciiByte(this.name, 16);
    }
    
    public byte[] getBytePassword() {
        return ByteUtil.getAsciiByte(this.password, 16);
    }
    
    public byte[] getByteSN() {
        return ByteUtil.getAsciiByte(this.SN, 12);
    }
    
    public byte[] getByteServerAdd() {
        return ByteUtil.getAsciiByte(this.serverAdd, 64);
    }
    
    public byte[] getByteServerPort() {
        return ByteUtil.swapByte(ByteUtil.getByte(this.serverPort));
    }
    
    public byte[] getByteSuperPassword() {
        return ByteUtil.getAsciiByte("EEEEEEEEEEEEEEEE", 16);
    }
    
    public byte[] getRouterPassword() {
        return ByteUtil.getAsciiByte(this.routerPassword, 16);
    }
    
    public byte[] getRouterPasswordLength() {
        return new byte[] { (byte)this.routerPassword.length() };
    }
    
    public byte[] getRouterSSID() {
        return ByteUtil.getAsciiByte(this.routerSSID, 32);
    }
    
    public byte[] routerSecurityType() {
        return new byte[] { this.routerSecurityType };
    }
    
    public void save(final int n) {
        final SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(ConnectService.getInstance()).edit();
        edit.putString("SN*No." + n, this.SN);
        edit.putString("password*No." + n, this.password);
        edit.putString("serverAdd*No." + n, this.serverAdd);
        edit.putInt("serverPort*No." + n, (int)this.serverPort);
        edit.putString("name*No." + n, this.name);
        edit.putString("routerSSID*No." + n, this.routerSSID);
        edit.putString("routerPassword*No." + n, this.routerPassword);
        edit.putInt("routerSecurityType*No." + n, (int)this.routerSecurityType);
        edit.putString("LAN_SERVER_IP*No." + n, ConnectService.LAN_SERVER_IP);
        edit.putString("LAN_SERVER_PORT*No." + n, ConnectService.LAN_SERVER_PORT);
        edit.commit();
        new BroadcastHelper().startUDPSearch();
    }
}
