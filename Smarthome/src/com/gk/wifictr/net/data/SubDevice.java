package com.gk.wifictr.net.data;

import com.gk.wifictr.net.command.*;
import java.util.*;
import android.util.*;

public class SubDevice
{
    public int actionCode;
    public int cg_type;
    public byte devCode;
    public String devName;
    public int devNo;
    public String devSN;
    public byte devType;
    public int lightCount;
    public boolean[] lightStatus;
    
    public SubDevice(final int devNo, final String devName, final String devSN, final byte devType) {
        this.lightCount = 0;
        this.actionCode = 0;
        this.devNo = devNo;
        this.devName = devName;
        this.devSN = devSN;
        this.devType = devType;
        this.lightCount = 0;
    }
    
    public SubDevice(int i, final String devName, final String devSN, final byte devType, final byte devCode) {
        this.lightCount = 0;
        this.actionCode = 0;
        this.devNo = i;
        this.devName = devName;
        this.devSN = devSN;
        this.devType = devType;
        final boolean[] byte2bit = ByteUtil.byte2bit(devCode);
        final ArrayList<Integer> list = new ArrayList<Integer>();
        for (i = 0; i < 4; ++i) {
            if (byte2bit[i + 4]) {
                list.add(i);
            }
        }
        this.lightStatus = new boolean[list.size()];
        for (i = 0; i < list.size(); ++i) {
            this.lightStatus[i] = byte2bit[list.get(i)];
        }
        this.lightCount = this.lightStatus.length;
        Log.e("\u706f\u6570\u76ee", new StringBuilder(String.valueOf(this.lightCount)).toString());
        this.devCode = devCode;
    }
    
    public SubDevice(final int devNo, final String devName, final String devSN, final byte devType, final boolean[] lightStatus) {
        this.lightCount = 0;
        this.actionCode = 0;
        this.devNo = devNo;
        this.devName = devName;
        this.devSN = devSN;
        this.devType = devType;
        this.lightCount = lightStatus.length;
        this.lightStatus = lightStatus;
    }
}
