package com.gk.wifictr.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import android.util.Log;

import com.gk.wifictr.net.command.ByteUtil;
import com.gk.wifictr.net.command.ReadData;



public class MSocket extends Thread
{
    public boolean AUTO_RELINK;
    public int[] AUTO_RELINK_DELAY;
    public int AUTO_RELINK_MAX_TIME;
    public int AUTO_RELINK_TIME;
    public boolean isLink;
    private BroadcastHelper mBroadcastHelper;
    private DataInputStream reader;
    private Socket socketClient;
    public String targetAddress;
    public String targetPort;
    public String type;
    private DataOutputStream writer;
    
    public MSocket(String targetAddress, String targetPort) {
        AUTO_RELINK = true;
        AUTO_RELINK_DELAY = new int[] { 0, 500, 1000, 2000, 3000, 5000, 7000, 10000 };
        AUTO_RELINK_TIME = 0;
        AUTO_RELINK_MAX_TIME = Integer.MAX_VALUE;
        type = "LAN";
        isLink = false;
        setName("SocketThread");
        this.targetAddress = targetAddress;
        this.targetPort = targetPort;
        mBroadcastHelper = new BroadcastHelper();
    }
    
    public MSocket(String name, String targetAddress, String targetPort) {
        AUTO_RELINK = true;
        AUTO_RELINK_DELAY = new int[] { 0, 500, 1000, 2000, 3000, 5000, 7000, 10000 };
        AUTO_RELINK_TIME = 0;
        AUTO_RELINK_MAX_TIME = Integer.MAX_VALUE;
        type = "LAN";
        isLink = false;
        setName(name);
        this.targetAddress = targetAddress;
        this.targetPort = targetPort;
        mBroadcastHelper = new BroadcastHelper();
    }
    
    public void cancel() {
        log("cancel()");
        disconnect();
    }
    
    public void connect() {
        log("connect()");
        while (true) {
            try {
                if (socketClient != null) {
                    disconnect();
                }
                socketClient = new Socket();
                InetSocketAddress inetSocketAddress = new InetSocketAddress(targetAddress, Integer.parseInt(targetPort));
                isLink = true;
                socketClient.connect(inetSocketAddress, 5000);
                socketClient.setKeepAlive(true);
                socketClient.setSoLinger(true, 0);
                socketClient.setSoTimeout(30000);
                socketClient.setReceiveBufferSize(1024);
                reader = new DataInputStream(socketClient.getInputStream());
                writer = new DataOutputStream(socketClient.getOutputStream());
                AUTO_RELINK_TIME = 0;
                setLink();
            }
            catch (UnknownHostException ex2) {
                continue;
            }
            catch (NumberFormatException ex3) {
                continue;
            }
            catch (IOException ex) {
                continue;
            }
            break;
        }
    }
    
    public void disconnect() {
        log("disconnect()");
        try {
            if (socketClient != null) {
                if (!socketClient.isInputShutdown()) {
                    socketClient.shutdownInput();
                }
                if (!socketClient.isOutputShutdown()) {
                    socketClient.shutdownOutput();
                }
            }
            if (!(socketClient == null || socketClient.isClosed())) {
                socketClient.close();
            }
            if (writer != null) {
                writer.close();
            }
            if (reader != null) {
                reader.close();
            }
        }
        catch (Exception var1_1) {
            var1_1.printStackTrace();
        }
        finally {
            writer = null;
            reader = null;
            socketClient = null;
            //continue;
        }
        //do {
            try {
                MSocket.sleep(300);
            }
            catch (InterruptedException var1_3) {}
            setLink();
            //return;
            //break;
        //} while (true);
        
    }
    
    public int getAUTO_RELINK_DELAY() {
        if (AUTO_RELINK_TIME < 0) {
            AUTO_RELINK_TIME = 0;
        }
        if (AUTO_RELINK_TIME >= AUTO_RELINK_DELAY.length) {
            return AUTO_RELINK_DELAY[AUTO_RELINK_DELAY.length - 1];
        }
        return AUTO_RELINK_DELAY[AUTO_RELINK_TIME];
    }
    
    public boolean isLink() {
        return isLink;
    }
    
    public void log(String s) {
        Log.v(getName(), s);
    }
    
    @Override
    public void run() {
        log("run()");
        connect();
        while (!isInterrupted()) {
            try {
                Thread.sleep(getAUTO_RELINK_DELAY());
                if (isLink) {
                    byte[] array = new byte[1024];
                    try {
                        int read = reader.read(array);
                        if (read < 10) {
                            continue;
                        }
                        byte[] copyOfRange = ByteUtil.copyOfRange(array, 0, read);
                        log(ByteUtil.byte2hex(copyOfRange));
                        ReadData readData = new ReadData(copyOfRange);
                        if (readData.goodData != 0) {
                            //goto Label_0124;
                        	continue;
                        }
                        new BroadcastHelper().readData(readData);
                    }
                    catch (SocketException ex) {
                        setLink();
                    }
                    catch (Exception ex2) {
                        setLink();
                    }
                    continue;
                }
            }
            catch (InterruptedException ex3) {}
            if (!AUTO_RELINK) {
                log("\u867e\u7c73");
                break;
            }
            if (AUTO_RELINK_TIME >= AUTO_RELINK_MAX_TIME) {
                log("\u8d85\u8fc7\u91cd\u8fde\u6b21\u6570\u9650\u5236");
                break;
            }
            log("\u91cd\u8fde");
            connect();
            ++AUTO_RELINK_TIME;
        }
        super.run();
        disconnect();
    }
    
    public boolean setLink() {
    	boolean isLink;
        while (true) {
            if (socketClient == null) {
                isLink = false;
                break;
            }
            if (!socketClient.isConnected()) {
                isLink = false;
                break;
            }
            Label_0126: {
                if (writer == null) {
                    break Label_0126;
                }
                try {
                    if (type.equals("WAN")) {
                        socketClient.sendUrgentData(255);
                        writer.flush();
                    }
                    else {
                        writer.write(255);
                        writer.flush();
                    }
                    isLink = true;
                    this.isLink = isLink;
                    log(String.valueOf(getName()) + "'s link state -> " + isLink);
                    mBroadcastHelper.linkStateChange();
                    return isLink;
                }
                catch (IOException ex) {
                    isLink = false;
                    continue;
                }
            }
            isLink = false;
            //continue;
        }
        return isLink;
    }
    
    @Override
    public void start() {
        log("start()");
        super.start();
    }
    
    public boolean write(byte[] array) {
        log("write()");
        if (isLink()) {
            try {
                writer.write(array);
                writer.flush();
                return true;
            }
            catch (IOException ex) {
                log("\u53d1\u9001\u5931\u8d25");
                setLink();
                return false;
            }
        }
        setLink();
        return false;
    }
}
