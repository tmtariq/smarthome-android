package com.gk.wifictr.net;

import com.gk.wifictr.net.command.ReadData;
import com.gk.wifictr.net.command.WriteData;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;



public class BroadcastHelper
{
    Context context;
    
    public BroadcastHelper() {
        context = ConnectService.getInstance();
    }
    
    public void APModeChange(boolean b) {
        log("APModeChange");
        Intent intent = new Intent();
        intent.setAction("com.gk.wifictr.ui");
        intent.putExtra("cmd", 6);
        intent.putExtra("mode", b);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        log("\u53d1\u9001 AP\u6a21\u5f0f\u53d8\u5316\u5e7f\u64ad");
    }
    
    public void changeLanIP(String lan_SERVER_IP, String lan_SERVER_PORT) {
        ConnectService.LAN_SERVER_IP = lan_SERVER_IP;
        ConnectService.LAN_SERVER_PORT = lan_SERVER_PORT;
        ConnectService.defWifi.save(ConnectService.defWifinum);
        log("changeLanIP");
        Intent intent = new Intent();
        intent.setAction("com.gk.wifictr.ui");
        intent.putExtra("cmd", 1);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        log("\u53d1\u9001IP\u53d8\u5316\u5e7f\u64ad");
    }
    
    public void doneEasyLink() {
        log("startEasyLink");
        Intent intent = new Intent();
        intent.setAction("com.gk.wifictr.net");
        intent.putExtra("cmd", 8);
        intent.putExtra("status", "DONE");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        log("\u5b8c\u6210EasyLink\u5e7f\u64ad");
    }
    
    public void gotSN(String s, String s2) {
        log("gotSN");
        Intent intent = new Intent();
        intent.setAction("com.gk.wifictr.net");
        intent.putExtra("cmd", 9);
        intent.putExtra("SN", s);
        intent.putExtra("PASSWD", s2);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        log("\u83b7\u53d6\u5230SN&\u5bc6\u7801\u5e7f\u64ad");
    }
    
    public void linkStateChange() {
        log("linkStateChange");
        Intent intent = new Intent();
        intent.setAction("com.gk.wifictr.net");
        intent.putExtra("cmd", 0);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        log("\u53d1\u9001\u8fde\u63a5\u72b6\u6001\u53d8\u5316\u5e7f\u64ad");
    }
    
    public void log(String s) {
        Log.v("BroadcastHelper", s);
    }
    
    public void readData(ReadData readData) {
        log("readData");
        Intent intent = new Intent();
        intent.setAction("com.gk.wifictr.net");
        intent.putExtra("cmd", 4);
        intent.putExtra("data", (Parcelable)readData);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        log("\u6536\u5230\u6570\u636e");
    }
    
    public void sendLog(String s) {
        log("sendLog");
        Intent intent = new Intent();
        intent.setAction("com.gk.wifictr.net");
        intent.putExtra("cmd", 7);
        intent.putExtra("log", s);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        log("\u53d1\u9001Log\u5e7f\u64ad");
    }
    
    public void startEasyLink(String s, String s2) {
        log("startEasyLink");
        Intent intent = new Intent();
        intent.setAction("com.gk.wifictr.ui");
        intent.putExtra("cmd", 8);
        intent.putExtra("status", "START");
        intent.putExtra("SSID", s);
        intent.putExtra("PASS", s2);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        log("\u5f00\u59cbEasyLink\u5e7f\u64ad");
    }
    
    public void startUDPSearch() {
        log("startUDPSearch");
        Intent intent = new Intent();
        intent.setAction("com.gk.wifictr.ui");
        intent.putExtra("cmd", 3);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        log("\u5f00\u59cbUDP\u641c\u7d22");
    }
    
    public void startUDPSearch(int n) {
        log("startUDPSearch");
        Intent intent = new Intent();
        intent.setAction("com.gk.wifictr.ui");
        intent.putExtra("cmd", 3);
        intent.putExtra("UDP_TYPE", n);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        log("\u5f00\u59cb\u7c7b\u578b\u4e3a" + n + "\u7684UDP\u641c\u7d22");
    }
    
    public void stopEasyLink() {
        log("startEasyLink");
        Intent intent = new Intent();
        intent.setAction("com.gk.wifictr.ui");
        intent.putExtra("cmd", 8);
        intent.putExtra("status", "STOP");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        log("\u505c\u6b62EasyLink\u5e7f\u64ad");
    }
    
    public void writeData(WriteData writeData) {
        log("writeData");
        Intent intent = new Intent();
        intent.setAction("com.gk.wifictr.ui");
        intent.putExtra("cmd", 5);
        intent.putExtra("data", (Parcelable)writeData);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        log("\u53d1\u9001\u6570\u636e");
    }
}
